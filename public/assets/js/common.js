(function ($) {
    $(document).ready(function () {
        if ($(document).slick)
            $('.slick').slick({
                dots: true,
                autoplay: true,
                autoplaySpeed: 2000
            });
    });

    $('.st2').click(function () {
        $('body,html').animate({scrollTop: $('.b2').offset().top - 70}, 300);
    });

    $('.mobile-menu').click(function () {
        $('.main-menu').toggleClass('ac');
    });

    $("[scrollanchor]").click(function () {
        $('body').animate({
            scrollTop: $('#' + $(this).attr('scrollanchor')).offset().top - 60
        }, 'slow');
    });

    $('#newsletterForm').submit(function (e) {
        e.preventDefault();
        var email = $('#userEmail').val();
        var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
        if (email == '' || !pattern.test(email)) {
            $('.error').text('Please enter a valid email');
            return;
        }
        $('.error').text('Processing...');
        subscribe(email);
    });

    function changePassword(firstPassword, secondPassword, token, userId) {

        var body = {
            firstPassword: firstPassword,
            secondPassword: secondPassword,
            token: token,
            userId: userId
        }

        $.post("/api/auth/password/change", body).done(function (data) {

            $('.error').text(data.message);

            if (data.success) {
                setTimeout(function () {
                    window.location.href = '/';
                }, 3000);
            }
        }).fail(function (xhr, status, error) {

            var data = JSON.parse(xhr.responseText);
            $('.error').text(data.message);
            // error handling
        });
    }

    function subscribe(email) {
        var body = {
            email: email
        };
        $.post("/api/newsletter", body)
            .done(function (data) {
                $('.error').text('Done! Thank you for signing up');
                var email = $('#userEmail').val('');
            })
            .fail(function (xhr, status, error) {
                var data = JSON.parse(xhr.responseText);
                $('.error').text(data.message);
                // error handling
            });
    }
})(jQuery);


