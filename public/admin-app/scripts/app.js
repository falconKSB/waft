(function () {
    'use strict';

    // register main app module
    angular.module('adminApp',
        [
            'ngResource',
            'LocalStorageModule',

            'ui.router',
            'ui.bootstrap',
            'ui.grid',
            'ui.grid.pagination',
            'ui.select',

            'selectize',

            'adminApp.layout',
            'adminApp.pages',
            'adminApp.services'

        ]);

    // register all app custom modules
    angular.module('adminApp.services', []);
    angular.module('adminApp.pages', []);
    angular.module('adminApp.layout', []);
})();
(function () {
	
    'use strict';

	angular
		.module('adminApp')
		.config(configure);

	configure.$inject = 
	[
		'$httpProvider', 
		'localStorageServiceProvider'
	];

	function configure
	(
		$httpProvider, 
		localStorageServiceProvider
	) {	

		// LOCAL STORAGE CONFIGURATION
		localStorageServiceProvider.setPrefix('adminApp');

		$httpProvider.interceptors.push('tokenInjector');
        $httpProvider.interceptors.push('tokenRestorer');
		
	}
})();
(function () {
    'use strict';

    angular
        .module('adminApp')
        .config(RouteConfigure);

    RouteConfigure.$inject =
        [
            '$stateProvider',
            '$urlRouterProvider'
        ];

    function RouteConfigure
    ($stateProvider,
     $urlRouterProvider) {
        $urlRouterProvider.otherwise('/orders');

        // set up the states
        $stateProvider

        // Index pages section

            .state('app', {
                abstract: true,
                private: false,
                url: '/',
                views: {
                    'header@': {
                        templateUrl: appHelper.templatePath('header/header')
                    },
                    'footer@': {
                        templateUrl: appHelper.templatePath('footer/footer')
                    }
                }
            })
            .state('app.login', {
                url: 'login',
                private: false,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('login/login'),
                        controller: 'LoginCtrl as login'
                    },
                    'header@': {template: ''},
                    'footer@': {template: ''}
                }
            })
            .state('app.orders', {
                url: 'orders',
                private: true,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('orders/orders'),
                        controller: 'OrdersCtrl as orders'
                    }
                }
            })
            .state('app.users', {
                url: 'users',
                private: true,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('users/users'),
                        controller: 'UsersCtrl as users'
                    }
                }
            })
            .state('app.scents', {
                url: 'scents',
                private: true,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('scents/scents'),
                        controller: 'ScentsCtrl as scents'
                    }
                }
            })
            .state('app.scents.create', {
                url: '/create',
                private: true,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('createScent/createScent'),
                        controller: 'CreateScentCtrl as createScent'
                    }
                }
            })
            .state('app.scents.edit', {
                url: '/edit/:scentId',
                private: true,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('createScent/createScent'),
                        controller: 'EditScentCtrl as editScent'
                    }
                }
            })
            .state('app.journeys', {
                url: 'journeys',
                private: true,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('journeys/journeys'),
                        controller: 'JourneysCtrl as journeys'
                    }
                }
            })
            .state('app.dashboard', {
                url: 'dashboard',
                private: true,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('dashboard/dashboard'),
                        controller: 'DashboardCtrl as dashboard'
                    }
                }
            })
            .state('app.conversions', {
                url: 'conversions',
                private: true,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('conversions/conversions'),
                        controller: 'ConversionsCtrl as conversions'
                    }
                }
            })
            .state('app.prices', {
                url: 'prices',
                private: true,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('prices/prices'),
                        controller: 'PricesCtrl as prices'
                    }
                }
            })
            .state('app.fragrances', {
                url: 'fragrances',
                private: true,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('fragrances/fragrances'),
                        controller: 'FragrancesCtrl as fragrances'
                    }
                }
            })
            .state('app.adminsAccounts', {
                url: 'adminsAccounts',
                private: true,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('admins/admins'),
                        controller: 'AdminsCtrl as admins'
                    }
                }
            })
            .state('app.adminsAccounts.create', {
                url: '/create',
                private: true,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('createAdmin/createAdmin'),
                        controller: 'CreateAdminCtrl as createAdmin'
                    }
                }
            })
            .state('app.adminsAccounts.edit', {
                url: '/edit/:adminId',
                private: true,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('createAdmin/createAdmin'),
                        controller: 'EditAdminCtrl as editAdmin'
                    }
                }
            })
    }
})();

(function () {
    'use strict';

    angular
        .module('adminApp')

        .constant('CnstApiPath', 
        {
            token: '/api/admin/token',
            login: '/api/admin/login'
        })

})();
(function () {
    'use strict';

    angular
        .module('adminApp')
        .controller('ApplicationCtrl', ApplicationCtrl);

    ApplicationCtrl.$inject =
        [
            '$rootScope',
            '$injector',
            'authService',
            '$state'
        ];

    function ApplicationCtrl($root, $injector, authService, $state) {
        $root.logout = function () {
            authService.logout();
        };

        $root.isLogin = function () {
            return $state.includes('app.login');
        };

        $root.getAccessLevel = function () {
            return authService.getAccessLevel();
        };

        $root.canEdit = function () {
            return authService.getAccessLevel() != 'readonly';
        };

        $root.showMobileMenu = false;
    }

})();
(function () {
    'use strict';

    angular
        .module('adminApp')
        .run(AppRun);

    AppRun.$inject =
        [
            '$rootScope',
            '$state',
            'authService'
        ];

    function AppRun($root, $state, auth) {

        var isCredentialsChecked = false;

        auth.isSessionInStore() ? auth.refresh().then(appLoaded, appLoaded) : appLoaded();

        $root.$on('$stateChangeStart', function (event, toState, toParams) {

            if (toState.url=='/'){$state.go('app.scents'); return; } 
            if (!isCredentialsChecked) return;
            if (toState.private && !auth.isAuthenticated()) {
                $state.go('app.login');
            }
        
        });

        function appLoaded() { isCredentialsChecked = true; }

    };
})();
(function () {
    'use strict';

    angular
        .module('adminApp')
        .filter('badwords', function($sce) {
            return function(input) {
                let search = /(?:p.?\s*[o0]\.?|post\s+office)\s+b[o0]x|fuck|pussy|cunt|ass|shit|arse|bitch|bastard|bollocks|crap|damn|waft|nigger|nigga|whore|twat|piss/;
                return $sce.trustAsHtml(input.replace(new RegExp(search, 'gi'), '<span class="bad-text">$&</span>'));
            }
        });
})();
var public_vars = public_vars || {};
var appHelper = {
    // Vars (paths without trailing slash)
    templatesDir: '/admin-app/views',

    // Methods
    templatePath: function (view_name) {
        return this.templatesDir + '/' + view_name + '.template.html';
    },
};

function run() {
    setTimeout(function(){ angular.bootstrap(document, ["adminApp"]);}, 1000);
}


(function () {
    'use strict';

    angular
        .module('adminApp.services')
        .factory('admin', admin);

    admin.$inject = ['$http', '$q'];

    function admin($http, $q) {
        var service = {};

        var crudServiceBaseUrl = "/api/admin/accounts";

        service.getList = function (take, page) {
            return $http.get(crudServiceBaseUrl + '?&take=' + take + '&page=' + page).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        service.get = function (adminId) {
            return $http.get(crudServiceBaseUrl + '/' + adminId).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        service.post = function (admin) {
            return $http.post(crudServiceBaseUrl, admin).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        service.put = function (admin) {
            return $http.put(crudServiceBaseUrl, admin).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        return service;
    }
})();
(function () {
    'use strict';

    angular
        .module('adminApp.services')
        .factory('tokenInjector', injector);

    angular
        .module('adminApp.services')
        .factory('tokenRestorer', restorer);

    injector.$inject = ['$injector'];
    restorer.$inject = ['$injector', '$q'];

    function injector($injector) {
        return {
            request: function (config) {
                config.headers = config.headers || {};
                config.headers['Authorization'] = $injector.get('authService').getToken();
                return config;
            }
        };
    }

    function restorer($injector, $q) {
        return {
            responseError: function (response) {
                var auth = $injector.get('authService');

                if (((response.status === 403) || (response.status === 401)) && auth.isAuthenticated()) {

                    if (response.config.url=='/api/admin/token'||!auth.isSessionInStore()) {
                        $injector.get('$state').go('app.login');
                        return $q.reject(response);
                    }
                    // avoid circular dependency
                    var $http = $injector.get('$http');

                    var deferred = $q.defer();

                    // Create a new session (recover the session)
                    // refresh method that logs the user in using the current credentials and
                    // returns a promise
                    auth.refresh().then(deferred.resolve, deferred.reject);

                    // When the session recovered, make the same backend call again and chain the request
                    return deferred.promise.then(function () {
                        return $http(response.config);
                    }, function () {
                        $injector.get('$state').go('app.login');
                    });
                }
                if (((response.status === 403) || (response.status === 401)) && !auth.isAuthenticated()) {
                    $injector.get('$state').go('app.login');
                }

                if ((response.status === 403) || (response.status === 401)) {
                    $injector.get('$state').go('app.login');
                }

                return $q.reject(response);
            }
        };
    }
})();
(function () {
    'use strict';

    angular
        .module('adminApp.services')
        .factory('authService', authentication);

    authentication.$inject = ['$http', '$q', '$state', 'localStorageService', 'CnstApiPath'];

    function authentication($http, $q, $state, storage, apiPath) {

        var TOKENT_STORAGE = "REFRESH_TOKEN";

        var grant = {
            password: 'password',
            refresh: 'refresh_token'
        };

        var principal = {
            authenticated: false,
            remembered: true,

            account: {
                username: null,
                accessLevel: null
            },

            OAuth2: {
                access_token: null,
                refresh_token: undefined,
                token_type: null,
            }
        };

        return {
            login: login,
            logout: logout,
            refresh: refresh,
            getToken: getAccessString,
            getAccount: getAccount,
            getAccessLevel: getAccessLevel,
            isAuthenticated: isAuthenticated,
            isSessionInStore: isSessionInStore
        };
        ////////////////////////////////

        function login(model) {
            var deferred = $q.defer(),

            request = {
                method: 'POST',
                url: apiPath.login,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: getFormData(grant.password, model)
            };

            $http(request).success(
                function (response, status, headers, config) {
                    setCredentials(model, response);
                    deferred.resolve(response);
                }
            )
            .error(deferred.reject);

            return deferred.promise;
        }

        function logout() {
            principal.authenticated = false;
            principal.remembered = false;
            principal.account.username = null;
            principal.account.accessLevel = null;
            principal.OAuth2.access_token = null;
            principal.OAuth2.refresh_token = null;
            principal.OAuth2.token_type = null;
            principal.OAuth2.expires_in = null;

            storage.clearAll();

            $state.go('app.login');
        }

        function refresh() {
            var deferred = $q.defer();

            var request = {
                method: 'POST',
                url: apiPath.token,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: getFormData(grant.refresh),
                options: {'ignoreLoadingBar': true}
            };

            $http(request).success(
                function (response, status, headers, config) {
                    setCredentials(null, response);
                    deferred.resolve(response);
                }
            )
            .error(deferred.reject);

            return deferred.promise;
        }

        function setCredentials(account, oauth2) {
            if (oauth2 == undefined || oauth2 == null) return;

            if (account && account != null) {
                principal.account.email = account.email != undefined ? account.email : principal.account.email;
            }

            if (oauth2 && oauth2 != null) {
                principal.OAuth2.access_token = oauth2.access_token;
                principal.OAuth2.refresh_token = oauth2.refresh_token;
                principal.OAuth2.token_type = oauth2.token_type;

                storage.set(TOKENT_STORAGE, principal.OAuth2.refresh_token);

                principal.account.username = oauth2.currentUser.username;
                principal.account.accessLevel = oauth2.currentUser.accessLevel;
            }

            principal.authenticated = true;
        }

        function getAccessString() {
            return principal.OAuth2.token_type + ' ' + principal.OAuth2.access_token;
        }

        function getRefreshString() {
            return principal.OAuth2.token_type + ' ' + principal.OAuth2.refresh_token;
        }

        function getAccessToken() {
            return principal.OAuth2.access_token;
        }

        function getAccount() {
            return principal.account;
        }

        function getAccessLevel() {
            return principal.account.accessLevel;
        }

        function isAuthenticated() {
            return principal.authenticated;
        }

        function getFormData(grantType, model) {
            switch (grantType) {
                case grant.password: {
                    return "grant_type=" + grantType + "&username="
                        + model.username + "&password=" + model.password;
                }
                case grant.refresh: {
                    return "grant_type=" + grantType + "&refresh_token="
                        + storage.get(TOKENT_STORAGE) || principal.OAuth2.refresh_token;
                }
            }
        }

        function isSessionInStore() {
            return storage.get(TOKENT_STORAGE) ? true : false;
        }
    }

})();
(function () {
    'use strict';

    angular
        .module('adminApp.services')
        .factory('country', country);

    country.$inject = ['$http', '$q'];

    function country($http, $q) {
        var service = {};

        var COUNTRIES = [
            {
                countryCode: 'AD',
                country: 'Andorra'
            },
            {
                countryCode: 'AE',
                country: 'United Arab Emirates'
            },
            {
                countryCode: 'AF',
                country: 'Afghanistan'
            },
            {
                countryCode: 'AG',
                country: 'Antigua and Barbuda'
            },
            {
                countryCode: 'AI',
                country: 'Anguilla'
            },
            {
                countryCode: 'AL',
                country: 'Albania'
            },
            {
                countryCode: 'AM',
                country: 'Armenia'
            },
            {
                countryCode: 'AO',
                country: 'Angola'
            },
            {
                countryCode: 'AP',
                country: 'Asia/Pacific Region'
            },
            {
                countryCode: 'AQ',
                country: 'Antarctica'
            },
            {
                countryCode: 'AR',
                country: 'Argentina'
            },
            {
                countryCode: 'AS',
                country: 'American Samoa'
            },
            {
                countryCode: 'AT',
                country: 'Austria'
            },
            {
                countryCode: 'AU',
                country: 'Australia'
            },
            {
                countryCode: 'AW',
                country: 'Aruba'
            },
            {
                countryCode: 'AX',
                country: 'Aland Islands'
            },
            {
                countryCode: 'AZ',
                country: 'Azerbaijan'
            },
            {
                countryCode: 'BA',
                country: 'Bosnia and Herzegovina'
            },
            {
                countryCode: 'BB',
                country: 'Barbados'
            },
            {
                countryCode: 'BD',
                country: 'Bangladesh'
            },
            {
                countryCode: 'BE',
                country: 'Belgium'
            },
            {
                countryCode: 'BF',
                country: 'Burkina Faso'
            },
            {
                countryCode: 'BG',
                country: 'Bulgaria'
            },
            {
                countryCode: 'BH',
                country: 'Bahrain'
            },
            {
                countryCode: 'BI',
                country: 'Burundi'
            },
            {
                countryCode: 'BJ',
                country: 'Benin'
            },
            {
                countryCode: 'BL',
                country: 'Saint Bartelemey'
            },
            {
                countryCode: 'BM',
                country: 'Bermuda'
            },
            {
                countryCode: 'BN',
                country: 'Brunei Darussalam'
            },
            {
                countryCode: 'BO',
                country: 'Bolivia'
            },
            {
                countryCode: 'BQ',
                country: 'Bonaire, Saint Eustatius and Saba'
            },
            {
                countryCode: 'BR',
                country: 'Brazil'
            },
            {
                countryCode: 'BS',
                country: 'Bahamas'
            },
            {
                countryCode: 'BT',
                country: 'Bhutan'
            },
            {
                countryCode: 'BV',
                country: 'Bouvet Island'
            },
            {
                countryCode: 'BW',
                country: 'Botswana'
            },
            {
                countryCode: 'BY',
                country: 'Belarus'
            },
            {
                countryCode: 'BZ',
                country: 'Belize'
            },
            {
                countryCode: 'CA',
                country: 'Canada'
            },
            {
                countryCode: 'CC',
                country: 'Cocos (Keeling) Islands'
            },
            {
                countryCode: 'CD',
                country: 'Congo, The Democratic Republic of the'
            },
            {
                countryCode: 'CF',
                country: 'Central African Republic'
            },
            {
                countryCode: 'CG',
                country: 'Congo'
            },
            {
                countryCode: 'CH',
                country: 'Switzerland'
            },
            {
                countryCode: 'CI',
                country: 'Cote d\'Ivoire'
            },
            {
                countryCode: 'CK',
                country: 'Cook Islands'
            },
            {
                countryCode: 'CL',
                country: 'Chile'
            },
            {
                countryCode: 'CM',
                country: 'Cameroon'
            },
            {
                countryCode: 'CN',
                country: 'China'
            },
            {
                countryCode: 'CO',
                country: 'Colombia'
            },
            {
                countryCode: 'CR',
                country: 'Costa Rica'
            },
            {
                countryCode: 'CU',
                country: 'Cuba'
            },
            {
                countryCode: 'CV',
                country: 'Cape Verde'
            },
            {
                countryCode: 'CW',
                country: 'Curacao'
            },
            {
                countryCode: 'CX',
                country: 'Christmas Island'
            },
            {
                countryCode: 'CY',
                country: 'Cyprus'
            },
            {
                countryCode: 'CZ',
                country: 'Czech Republic'
            },
            {
                countryCode: 'DE',
                country: 'Germany'
            },
            {
                countryCode: 'DJ',
                country: 'Djibouti'
            },
            {
                countryCode: 'DK',
                country: 'Denmark'
            },
            {
                countryCode: 'DM',
                country: 'Dominica'
            },
            {
                countryCode: 'DO',
                country: 'Dominican Republic'
            },
            {
                countryCode: 'DZ',
                country: 'Algeria'
            },
            {
                countryCode: 'EC',
                country: 'Ecuador'
            },
            {
                countryCode: 'EE',
                country: 'Estonia'
            },
            {
                countryCode: 'EG',
                country: 'Egypt'
            },
            {
                countryCode: 'EH',
                country: 'Western Sahara'
            },
            {
                countryCode: 'ER',
                country: 'Eritrea'
            },
            {
                countryCode: 'ES',
                country: 'Spain'
            },
            {
                countryCode: 'ET',
                country: 'Ethiopia'
            },
            {
                countryCode: 'EU',
                country: 'Europe'
            },
            {
                countryCode: 'FI',
                country: 'Finland'
            },
            {
                countryCode: 'FJ',
                country: 'Fiji'
            },
            {
                countryCode: 'FK',
                country: 'Falkland Islands (Malvinas)'
            },
            {
                countryCode: 'FM',
                country: 'Micronesia, Federated States of'
            },
            {
                countryCode: 'FO',
                country: 'Faroe Islands'
            },
            {
                countryCode: 'FR',
                country: 'France'
            },
            {
                countryCode: 'GA',
                country: 'Gabon'
            },
            {
                countryCode: 'GB',
                country: 'United Kingdom'
            },
            {
                countryCode: 'GD',
                country: 'Grenada'
            },
            {
                countryCode: 'GE',
                country: 'Georgia'
            },
            {
                countryCode: 'GF',
                country: 'French Guiana'
            },
            {
                countryCode: 'GG',
                country: 'Guernsey'
            },
            {
                countryCode: 'GH',
                country: 'Ghana'
            },
            {
                countryCode: 'GI',
                country: 'Gibraltar'
            },
            {
                countryCode: 'GL',
                country: 'Greenland'
            },
            {
                countryCode: 'GM',
                country: 'Gambia'
            },
            {
                countryCode: 'GN',
                country: 'Guinea'
            },
            {
                countryCode: 'GP',
                country: 'Guadeloupe'
            },
            {
                countryCode: 'GQ',
                country: 'Equatorial Guinea'
            },
            {
                countryCode: 'GR',
                country: 'Greece'
            },
            {
                countryCode: 'GS',
                country: 'South Georgia and the South Sandwich Islands'
            },
            {
                countryCode: 'GT',
                country: 'Guatemala'
            },
            {
                countryCode: 'GU',
                country: 'Guam'
            },
            {
                countryCode: 'GW',
                country: 'Guinea-Bissau'
            },
            {
                countryCode: 'GY',
                country: 'Guyana'
            },
            {
                countryCode: 'HK',
                country: 'Hong Kong'
            },
            {
                countryCode: 'HM',
                country: 'Heard Island and McDonald Islands'
            },
            {
                countryCode: 'HN',
                country: 'Honduras'
            },
            {
                countryCode: 'HR',
                country: 'Croatia'
            },
            {
                countryCode: 'HT',
                country: 'Haiti'
            },
            {
                countryCode: 'HU',
                country: 'Hungary'
            },
            {
                countryCode: 'ID',
                country: 'Indonesia'
            },
            {
                countryCode: 'IE',
                country: 'Ireland'
            },
            {
                countryCode: 'IL',
                country: 'Israel'
            },
            {
                countryCode: 'IM',
                country: 'Isle of Man'
            },
            {
                countryCode: 'IN',
                country: 'India'
            },
            {
                countryCode: 'IO',
                country: 'British Indian Ocean Territory'
            },
            {
                countryCode: 'IQ',
                country: 'Iraq'
            },
            {
                countryCode: 'IR',
                country: 'Iran, Islamic Republic of'
            },
            {
                countryCode: 'IS',
                country: 'Iceland'
            },
            {
                countryCode: 'IT',
                country: 'Italy'
            },
            {
                countryCode: 'JE',
                country: 'Jersey'
            },
            {
                countryCode: 'JM',
                country: 'Jamaica'
            },
            {
                countryCode: 'JO',
                country: 'Jordan'
            },
            {
                countryCode: 'JP',
                country: 'Japan'
            },
            {
                countryCode: 'KE',
                country: 'Kenya'
            },
            {
                countryCode: 'KG',
                country: 'Kyrgyzstan'
            },
            {
                countryCode: 'KH',
                country: 'Cambodia'
            },
            {
                countryCode: 'KI',
                country: 'Kiribati'
            },
            {
                countryCode: 'KM',
                country: 'Comoros'
            },
            {
                countryCode: 'KN',
                country: 'Saint Kitts and Nevis'
            },
            {
                countryCode: 'KP',
                country: 'Korea, Democratic People\'s Republic of'
            },
            {
                countryCode: 'KR',
                country: 'Korea, Republic of'
            },
            {
                countryCode: 'KW',
                country: 'Kuwait'
            },
            {
                countryCode: 'KY',
                country: 'Cayman Islands'
            },
            {
                countryCode: 'KZ',
                country: 'Kazakhstan'
            },
            {
                countryCode: 'LA',
                country: 'Lao People\'s Democratic Republic'
            },
            {
                countryCode: 'LB',
                country: 'Lebanon'
            },
            {
                countryCode: 'LC',
                country: 'Saint Lucia'
            },
            {
                countryCode: 'LI',
                country: 'Liechtenstein'
            },
            {
                countryCode: 'LK',
                country: 'Sri Lanka'
            },
            {
                countryCode: 'LR',
                country: 'Liberia'
            },
            {
                countryCode: 'LS',
                country: 'Lesotho'
            },
            {
                countryCode: 'LT',
                country: 'Lithuania'
            },
            {
                countryCode: 'LU',
                country: 'Luxembourg'
            },
            {
                countryCode: 'LV',
                country: 'Latvia'
            },
            {
                countryCode: 'LY',
                country: 'Libyan Arab Jamahiriya'
            },
            {
                countryCode: 'MA',
                country: 'Morocco'
            },
            {
                countryCode: 'MC',
                country: 'Monaco'
            },
            {
                countryCode: 'MD',
                country: 'Moldova, Republic of'
            },
            {
                countryCode: 'ME',
                country: 'Montenegro'
            },
            {
                countryCode: 'MF',
                country: 'Saint Martin'
            },
            {
                countryCode: 'MG',
                country: 'Madagascar'
            },
            {
                countryCode: 'MH',
                country: 'Marshall Islands'
            },
            {
                countryCode: 'MK',
                country: 'Macedonia'
            },
            {
                countryCode: 'ML',
                country: 'Mali'
            },
            {
                countryCode: 'MM',
                country: 'Myanmar'
            },
            {
                countryCode: 'MN',
                country: 'Mongolia'
            },
            {
                countryCode: 'MO',
                country: 'Macao'
            },
            {
                countryCode: 'MP',
                country: 'Northern Mariana Islands'
            },
            {
                countryCode: 'MQ',
                country: 'Martinique'
            },
            {
                countryCode: 'MR',
                country: 'Mauritania'
            },
            {
                countryCode: 'MS',
                country: 'Montserrat'
            },
            {
                countryCode: 'MT',
                country: 'Malta'
            },
            {
                countryCode: 'MU',
                country: 'Mauritius'
            },
            {
                countryCode: 'MV',
                country: 'Maldives'
            },
            {
                countryCode: 'MW',
                country: 'Malawi'
            },
            {
                countryCode: 'MX',
                country: 'Mexico'
            },
            {
                countryCode: 'MY',
                country: 'Malaysia'
            },
            {
                countryCode: 'MZ',
                country: 'Mozambique'
            },
            {
                countryCode: 'NA',
                country: 'Namibia'
            },
            {
                countryCode: 'NC',
                country: 'New Caledonia'
            },
            {
                countryCode: 'NE',
                country: 'Niger'
            },
            {
                countryCode: 'NF',
                country: 'Norfolk Island'
            },
            {
                countryCode: 'NG',
                country: 'Nigeria'
            },
            {
                countryCode: 'NI',
                country: 'Nicaragua'
            },
            {
                countryCode: 'NL',
                country: 'Netherlands'
            },
            {
                countryCode: 'NO',
                country: 'Norway'
            },
            {
                countryCode: 'NP',
                country: 'Nepal'
            },
            {
                countryCode: 'NR',
                country: 'Nauru'
            },
            {
                countryCode: 'NU',
                country: 'Niue'
            },
            {
                countryCode: 'NZ',
                country: 'New Zealand'
            },
            {
                countryCode: 'OM',
                country: 'Oman'
            },
            {
                countryCode: 'PA',
                country: 'Panama'
            },
            {
                countryCode: 'PE',
                country: 'Peru'
            },
            {
                countryCode: 'PF',
                country: 'French Polynesia'
            },
            {
                countryCode: 'PG',
                country: 'Papua New Guinea'
            },
            {
                countryCode: 'PH',
                country: 'Philippines'
            },
            {
                countryCode: 'PK',
                country: 'Pakistan'
            },
            {
                countryCode: 'PL',
                country: 'Poland'
            },
            {
                countryCode: 'PM',
                country: 'Saint Pierre and Miquelon'
            },
            {
                countryCode: 'PN',
                country: 'Pitcairn'
            },
            {
                countryCode: 'PR',
                country: 'Puerto Rico'
            },
            {
                countryCode: 'PS',
                country: 'Palestinian Territory'
            },
            {
                countryCode: 'PT',
                country: 'Portugal'
            },
            {
                countryCode: 'PW',
                country: 'Palau'
            },
            {
                countryCode: 'PY',
                country: 'Paraguay'
            },
            {
                countryCode: 'QA',
                country: 'Qatar'
            },
            {
                countryCode: 'RE',
                country: 'Reunion'
            },
            {
                countryCode: 'RO',
                country: 'Romania'
            },
            {
                countryCode: 'RS',
                country: 'Serbia'
            },
            {
                countryCode: 'RU',
                country: 'Russian Federation'
            },
            {
                countryCode: 'RW',
                country: 'Rwanda'
            },
            {
                countryCode: 'SA',
                country: 'Saudi Arabia'
            },
            {
                countryCode: 'SB',
                country: 'Solomon Islands'
            },
            {
                countryCode: 'SC',
                country: 'Seychelles'
            },
            {
                countryCode: 'SD',
                country: 'Sudan'
            },
            {
                countryCode: 'SE',
                country: 'Sweden'
            },
            {
                countryCode: 'SG',
                country: 'Singapore'
            },
            {
                countryCode: 'SH',
                country: 'Saint Helena'
            },
            {
                countryCode: 'SI',
                country: 'Slovenia'
            },
            {
                countryCode: 'SJ',
                country: 'Svalbard and Jan Mayen'
            },
            {
                countryCode: 'SK',
                country: 'Slovakia'
            },
            {
                countryCode: 'SL',
                country: 'Sierra Leone'
            },
            {
                countryCode: 'SM',
                country: 'San Marino'
            },
            {
                countryCode: 'SN',
                country: 'Senegal'
            },
            {
                countryCode: 'SO',
                country: 'Somalia'
            },
            {
                countryCode: 'SR',
                country: 'Suriname'
            },
            {
                countryCode: 'SS',
                country: 'South Sudan'
            },
            {
                countryCode: 'ST',
                country: 'Sao Tome and Principe'
            },
            {
                countryCode: 'SV',
                country: 'El Salvador'
            },
            {
                countryCode: 'SX',
                country: 'Sint Maarten'
            },
            {
                countryCode: 'SY',
                country: 'Syrian Arab Republic'
            },
            {
                countryCode: 'SZ',
                country: 'Swaziland'
            },
            {
                countryCode: 'TC',
                country: 'Turks and Caicos Islands'
            },
            {
                countryCode: 'TD',
                country: 'Chad'
            },
            {
                countryCode: 'TF',
                country: 'French Southern Territories'
            },
            {
                countryCode: 'TG',
                country: 'Togo'
            },
            {
                countryCode: 'TH',
                country: 'Thailand'
            },
            {
                countryCode: 'TJ',
                country: 'Tajikistan'
            },
            {
                countryCode: 'TK',
                country: 'Tokelau'
            },
            {
                countryCode: 'TL',
                country: 'Timor-Leste'
            },
            {
                countryCode: 'TM',
                country: 'Turkmenistan'
            },
            {
                countryCode: 'TN',
                country: 'Tunisia'
            },
            {
                countryCode: 'TO',
                country: 'Tonga'
            },
            {
                countryCode: 'TR',
                country: 'Turkey'
            },
            {
                countryCode: 'TT',
                country: 'Trinidad and Tobago'
            },
            {
                countryCode: 'TV',
                country: 'Tuvalu'
            },
            {
                countryCode: 'TW',
                country: 'Taiwan'
            },
            {
                countryCode: 'TZ',
                country: 'Tanzania, United Republic of'
            },
            {
                countryCode: 'UA',
                country: 'Ukraine'
            },
            {
                countryCode: 'UG',
                country: 'Uganda'
            },
            {
                countryCode: 'UM',
                country: 'United States Minor Outlying Islands'
            },
            {
                countryCode: 'US',
                country: 'United States'
            },
            {
                countryCode: 'UY',
                country: 'Uruguay'
            },
            {
                countryCode: 'UZ',
                country: 'Uzbekistan'
            },
            {
                countryCode: 'VA',
                country: 'Holy See (Vatican City State)'
            },
            {
                countryCode: 'VC',
                country: 'Saint Vincent and the Grenadines'
            },
            {
                countryCode: 'VE',
                country: 'Venezuela'
            },
            {
                countryCode: 'VG',
                country: 'Virgin Islands, British'
            },
            {
                countryCode: 'VI',
                country: 'Virgin Islands, U.S.'
            },
            {
                countryCode: 'VN',
                country: 'Vietnam'
            },
            {
                countryCode: 'VU',
                country: 'Vanuatu'
            },
            {
                countryCode: 'WF',
                country: 'Wallis and Futuna'
            },
            {
                countryCode: 'WS',
                country: 'Samoa'
            },
            {
                countryCode: 'YE',
                country: 'Yemen'
            },
            {
                countryCode: 'YT',
                country: 'Mayotte'
            },
            {
                countryCode: 'ZA',
                country: 'South Africa'
            },
            {
                countryCode: 'ZM',
                country: 'Zambia'
            },
            {
                countryCode: 'ZW',
                country: 'Zimbabwe'
            }
        ]

        service.getList = function () {
            return COUNTRIES;
        };

        return service;

    }
})();

(function () {
    'use strict';

    angular
        .module('adminApp.services')
        .factory('dashboard', dashboard);

    dashboard.$inject = ['$http', '$q'];

    function dashboard($http, $q) {
        var service = {};

        var crudServiceBaseUrl = "/api/admin/dashboard";

        service.get = function(country) {
            return $http.get(
                crudServiceBaseUrl + (country ? "?country=" + country : '')
            ).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        service.getConversions = function(country) {
            return $http.get(
                crudServiceBaseUrl + '/conversions' + (country ? "?country=" + country : '')
            ).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        return service;
    }
})();

(function () {
    'use strict';

    angular
        .module('adminApp.services')
        .factory('fileUpload', fileUpload);

    fileUpload.$inject = ['$http', '$q'];

    function fileUpload($http, $q) {
        var service = {};

        service.uploadFileToUrl = function (file, uploadUrl, fieldName) {
            var deferred = $q.defer();

            var fd = new FormData();
            fd.append(fieldName, file);

            $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .success(function (result) {
                    if (result.success)
                        deferred.resolve(result);
                    else
                        deferred.reject(result);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        }

        return service;
    }
})();

(function () {
    'use strict';

    angular
        .module('adminApp.services')
        .factory('fragrances', fragrances);

    fragrances.$inject = ['$http', '$q'];

    function fragrances($http, $q) {
        var service = {};

        var crudServiceBaseUrl = "/api/admin/fragrances";

        service.getList = function (term, take, page) {
            return $http.get(crudServiceBaseUrl + '?term=' + term + '&take=' + take + '&page=' + page).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        service.get = function (id) {
            return $http.get(crudServiceBaseUrl + '/' + id).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        service.getJourneys = function (fragranceId) {
            return $http.get(crudServiceBaseUrl + '/journeys/' + fragranceId).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        return service;
    }
})();
(function () {
    'use strict';

    angular
        .module('adminApp.services')
        .factory('journeys', journeys);

    journeys.$inject = ['$http', '$q'];

    function journeys($http, $q) {
        var service = {};

        var crudServiceBaseUrl = "/api/admin/journeys";

        service.getList = function (take, page) {
            return $http.get(crudServiceBaseUrl + '?&take=' + take + '&page=' + page).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        service.getUserJourneys = function (userId) {
            return $http.get(crudServiceBaseUrl + '/' + userId).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        return service;
    }
})();
(function () {
    'use strict';

    angular
        .module('adminApp.services')
        .service('modalService', ['$uibModal', function ($uibModal) {

            var modalDefaults = {
                backdrop: true,
                keyboard: true,
                modalFade: true,
                templateUrl: '/app/partials/modal.html'
            };

            var modalOptions = {
                closeButtonText: 'Close',
                actionButtonText: 'OK',
                headerText: 'Proceed?',
                bodyText: 'Perform this action?'
            };

            this.showModal = function (customModalDefaults, customModalOptions) {
                if (!customModalDefaults) customModalDefaults = {};
                customModalDefaults.backdrop = 'static';
                return this.show(customModalDefaults, customModalOptions);
            };

            this.show = function (customModalDefaults, customModalOptions) {
                //Create temp objects to work with since we're in a singleton service
                var tempModalDefaults = {};
                var tempModalOptions = {};

                //Map angular-ui modal custom defaults to modal defaults defined in service
                angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

                //Map modal.html $scope custom properties to defaults defined in service
                angular.extend(tempModalOptions, modalOptions, customModalOptions);

                if (!tempModalDefaults.controller) {
                    tempModalDefaults.controller = function ($scope, $uibModalInstance) {
                        $scope.modalOptions = tempModalOptions;
                        $scope.modalOptions.ok = function (result) {
                            $uibModalInstance.close(result);
                        };
                        $scope.modalOptions.close = function (result) {
                            $uibModalInstance.dismiss('cancel');
                        };
                    };
                }

                return $uibModal.open(tempModalDefaults).result;
            };

        }]);
})();
(function () {
  'use strict';

  angular
    .module('adminApp.services')
    .factory('order', order);

  order.$inject = ['$http', '$q'];

  function order($http, $q) {
    var service = {};

    var crudServiceBaseUrl = "/api/admin/orders";

    service.getList = function (filter) {
      return $http.get(crudServiceBaseUrl, {
        params: filter
      })
        .then(
          function (response) {
            return response.data;
          },
          function (errResponse) {
            return $q.reject(errResponse);
          }
        );
    };

    service.get = function (orderId) {
      return $http.get(crudServiceBaseUrl + '/' + orderId).then(
        function (response) {
          return response.data;
        },
        function (errResponse) {
          return $q.reject(errResponse);
        }
      );
    };

    service.post = function (order) {
      return $http.post(crudServiceBaseUrl, {order: order}).then(
        function (response) {
          return response.data;
        },
        function (errResponse) {
          return $q.reject(errResponse);
        }
      );
    };

    return service;
  }
})();
(function () {
    'use strict';

    angular
        .module('adminApp.services')
        .factory('prices', prices);

    prices.$inject = ['$http', '$q'];

    function prices($http, $q) {
        var service = {};

        var crudServiceBaseUrl = "/api/admin/prices";

        service.getPrices = function() {
            return $http.get(
                crudServiceBaseUrl
            ).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        return service;
    }
})();

(function () {
    'use strict';

    angular
        .module('adminApp.services')
        .factory('scent', scent);

    scent.$inject = ['$http', '$q'];

    function scent($http, $q) {
        var service = {};

        var crudServiceBaseUrl = "/api/admin/scents";

        service.getList = function (take, page, warehouse) {
            return $http.get(crudServiceBaseUrl + '?&take=' + take + '&page=' + page
                             + (warehouse ? '&warehouse=' + warehouse : '')).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        service.get = function (scentId) {
            return $http.get(crudServiceBaseUrl + '/' + scentId).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        service.post = function (scent) {
            return $http.post(crudServiceBaseUrl, { scent: scent }).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        service.put = function (scent) {
            return $http.get(crudServiceBaseUrl, { scent: scent }).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        return service;
    }
})();

(function () {
    'use strict';

    angular
        .module('adminApp.services')
        .factory('user', user);

    user.$inject = ['$http', '$q'];

    function user($http, $q) {
        var service = {};
        var crudServiceBaseUrl = "/api/admin/users";

        service.get = function (userId) {
            return $http.get(crudServiceBaseUrl + '/' + userId).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        service.getList = function (term, take, page) {
            return $http.get(crudServiceBaseUrl + "?term=" + term + '&take=' + take + '&page=' + page).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        service.update = function (_user) {
            return $http.put(crudServiceBaseUrl, {email: _user.email, id: _user._id}).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        return service;
    }
})();
(function () {
    'use strict';

    angular
        .module('adminApp.services')
        .factory('warehouse', warehouse);

    warehouse.$inject = ['$http', '$q'];

    function warehouse($http, $q) {
        var service = {};

        var crudServiceBaseUrl = "/api/admin/inventory";

        service.getList = function () {
            return $http.get(crudServiceBaseUrl).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        return service;
    }
})();

(function () {
    'use strict';

    angular
        .module('adminApp')
        .controller('AdminsCtrl', AdminsCtrl);

    AdminsCtrl.$inject = ['$rootScope', '$scope', '$state', '$location', 'uiGridConstants', 'admin'];

    function AdminsCtrl($root, $scope, $state, $location, uiGridConstants, admin) {

        if ($scope.getAccessLevel() != 'admin')
            $location.path('/orders');

        var paginationOptions = {
            pageNumber: 1,
            pageSize: 50,
            sort: null
        };

        $scope.gridOptions = {
            paginationPageSizes: [50, 100],
            paginationPageSize: 50,
            useExternalPagination: true,
            enableColumnMenus: false,
            columnDefs: [
                {
                    field: 'email', displayName: 'EMAIL',
                    cellTemplate: '<div class="ui-grid-cell-contents"> <a href="" ng-click="grid.appScope.editAdmin(row.entity._id)">{{row.entity.email}}</a><div>'
                },
                {field: 'status', displayName: 'STATUS'}],

            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;

                gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
                    paginationOptions.pageNumber = newPage;
                    paginationOptions.pageSize = pageSize;
                    getPage(newPage, pageSize);
                });
            }
        };

        $scope.editAdmin = function (adminId) {
            $location.path('/adminsAccounts/edit/' + adminId);
        };

        var getPage = function (page, take) {

            admin.getList(take, page).then(
                function (data) {
                    $scope.gridOptions.data = data.docs;
                    $scope.gridOptions.totalItems = data.count;
                },
                function (errResponse) {
                }
            );
        };

        getPage(paginationOptions.pageNumber, paginationOptions.pageSize);

    }

})();
(function () {
    'use strict';

    angular
        .module('adminApp')
        .controller('ConversionsCtrl', ConversionsCtrl);

    ConversionsCtrl.$inject = ['$rootScope', '$scope', '$interval', '$state', '$location', 'uiGridConstants', 'dashboard', 'prices'];

    function ConversionsCtrl($root, $scope, $interval, $state, $location, uiGridConstants, dashboard, prices) {

        $scope.country = "";

        $scope.countries = [{countryCode: "", name: "Global"}];
        
        prices.getPrices().then(function(prices) {
            $scope.countries = $scope.countries.concat(prices.countries);
        });

        $scope.countriesConfig = {
            maxItems: 1,
            create: false,
            valueField: 'countryCode',
            labelField: 'name',
            delimiter: '|',
            searchField: ['name'],
            placeholder: 'Select',
            onChange: function (value) {
                getPage($scope.country)
            }
        };
        
        var getPage = function (country) {

            dashboard.getConversions(country).then(
                function (data) {
                    $scope.data = data;
                },
                function (errResponse) {
                }
            );
        };

        $scope.getPage = getPage;

        getPage($scope.country);

        var refresh = $interval(function() {
            getPage($scope.country);
        }, 60*1000);

        $scope.$on('$destroy', function() {
            $interval.cancel(refresh);
        });
    }

})();

(function () {
    'use strict';

    angular
        .module('adminApp')
        .controller('CreateAdminCtrl', CreateAdminCtrl);

    CreateAdminCtrl.$inject = ['$scope', '$state', '$location', 'admin'];

    function CreateAdminCtrl($scope, $state, $location, admin) {

        if ($scope.getAccessLevel() != 'admin')
            $location.path('/orders');

        $scope.header = 'CREATE NEW ADMIN ACCOUNT';
        $scope.message = '';
        $scope.isEditing = false;

        $scope.admin = {
            email: '',
            password1: '',
            password2: '',
            status: '',
            accessLevel: ''
        }

        $scope.statuses = ['Active', 'Inactive'];
        $scope.accessLevels = ['admin', 'operator', 'readonly'];

        $scope.selectStatus = function (status) {
            $scope.admin.status = status;
        }
        $scope.selectAccessLevel = function (accessLevel) {
            $scope.admin.accessLevel = accessLevel;
        }

        $scope.save = function () {
            admin.post($scope.admin).then(
                function (data) {
                    $scope.message = data.message;

                    if (data.success) {
                        $state.go('app.adminsAccounts');
                    }
                },
                function (errResponse) {
                }
            );
        }
    }

})();

(function () {
    'use strict';

    angular
        .module('adminApp')
        .controller('CreateScentCtrl', CreateScentCtrl);

    CreateScentCtrl.$inject = ['$rootScope', '$scope', '$state', '$location', 'uiGridConstants', 'scent'];

    function CreateScentCtrl($root, $scope, $state, $location, uiGridConstants, scent) {

        if ($scope.getAccessLevel() == 'readonly')
            $location.path('/orders');

        $scope.header = 'CREATE NEW SCENT';

        $scope.scent = {
            name: '',
            code: '',
            iffName: '',
            iffCode: '',
            family: '',
            subFamily: '',
            subGroup: '',
            subGroup2: '',
            subGroup3: '',
            subGroup4: '',
            marketReference: '',
            keyIngredients: '',
            otherIngredients: '',
            description: '',
            scentMood: '',
            scent: '',

            timeOfDay: [],
            mood: [],
            personalStyle: [],
            gender: [],
            scentGender: '',
            activity: [],
            waft: '',
            status: '',
            inStock: false
        };

        $scope.statuses = ['Active', 'Inactive'];
        $scope.activities = ['sport', 'social', 'work', 'dating'];
        $scope.wafts = ['subtle', 'balanced', 'daring'];
        $scope.genders = ['man', 'woman', 'unisex'];
        $scope.moods = ['fresh', 'relaxed', 'elegant', 'sexy'];
        $scope.personalStyles = ['classic', 'trendy'];
        $scope.timeOfDays = ['day', 'night'];

        $scope.save = function () {

            scent.post($scope.scent).then(
                function (data) {
                    if (data.success) {
                        $state.go('app.scents');
                    } else {
                        $scope.message = data.message;
                    }
                },
                function (err) {
                    if (err && err.data && err.data.message)
                        $scope.message = err.data.message;
                    else
                        $scope.message = "Unknown Server Error"
                }
            );
        }

        $scope.sync = function (bool, item, resultList) {

            if (bool) {
                // add item
                resultList.push(item);

            } else {
                // remove item
                for (var i = 0; i < resultList.length; i++) {
                    if (resultList[i] == item) {
                        resultList.splice(i, 1);
                    }
                }
            }
        };

        $scope.isChecked = function (item, resultList) {
            var match = false;
            for (var i = 0; i < resultList.length; i++) {
                if (resultList[i] == item) {
                    match = true;
                }
            }
            return match;
        };

    }

})();

(function () {
    'use strict';

    angular
        .module('adminApp')
        .controller('DashboardCtrl', DashboardCtrl);

    DashboardCtrl.$inject = ['$rootScope', '$scope', '$interval', '$state', '$location', 'uiGridConstants', 'dashboard', 'prices'];

    function DashboardCtrl($root, $scope, $interval, $state, $location, uiGridConstants, dashboard, prices) {

        $scope.country = "";

        $scope.countries = [{countryCode: "", name: "Global"}];
        
        prices.getPrices().then(function(prices) {
            $scope.countries = $scope.countries.concat(prices.countries);
        });

        $scope.countriesConfig = {
            maxItems: 1,
            create: false,
            valueField: 'countryCode',
            labelField: 'name',
            delimiter: '|',
            searchField: ['name'],
            placeholder: 'Select',
            onChange: function (value) {
                getPage($scope.country)
            }
        };
        
        var getPage = function (country) {

            dashboard.get(country).then(
                function (data) {
                    $scope.data = data;
                },
                function (errResponse) {
                }
            );
        };

        $scope.getPage = getPage;

        getPage($scope.country);

        var refresh = $interval(function() {
            getPage($scope.country);
        }, 60*1000);

        $scope.$on('$destroy', function() {
            $interval.cancel(refresh);
        });
        
    }

})();

(function () {
    'use strict';

    angular
        .module('adminApp')
        .controller('EditAdminCtrl', EditAdminCtrl);

    EditAdminCtrl.$inject = ['$scope', '$state', '$location', '$stateParams', 'admin'];

    function EditAdminCtrl($scope, $state, $location, $stateParams, admin) {

        if ($scope.getAccessLevel() != 'admin')
            $location.path('/orders');

        $scope.adminId = $stateParams.adminId;
        $scope.isEditing = true;

        $scope.header = 'EDIT';
        $scope.message = '';

        $scope.admin = {
            email: '',
            password1: '',
            password2: '',
            status: '',
            accessLevel: ''
        }

        $scope.statuses = ['Active', 'Inactive'];
        $scope.accessLevels = ['admin', 'operator', 'readonly'];

        admin.get($scope.adminId).then(
            function (data) {
                $scope.admin = data.admin;
                $scope.header = 'EDIT ' + $scope.admin.email;
            },
            function (errResponse) {
            }
        );

        $scope.selectStatus = function (status) {
            $scope.admin.status = status;
        }
        $scope.selectAccessLevel = function (accessLevel) {
            $scope.admin.accessLevel = accessLevel;
        }

        $scope.save = function () {
            admin.put($scope.admin).then(
                function (data) {
                    $scope.message = data.message;

                    if (data.success) {
                        $state.go('app.adminsAccounts');
                    }
                },
                function (errResponse) {
                }
            );
        }
    }

})();

(function () {
    'use strict';

    angular
        .module('adminApp')
        .controller('EditScentCtrl', EditScentCtrl);

    EditScentCtrl.$inject = ['$rootScope', '$scope', '$state', '$location', 'uiGridConstants', 'scent', '$stateParams'];

    function EditScentCtrl($root, $scope, $state, $location, uiGridConstants, scent, $stateParams) {

        if ($scope.getAccessLevel() == 'readonly')
            $location.path('/orders');

        $scope.scentId = $stateParams.scentId;

        $scope.header = 'EDIT SCENT';

        $scope.scent = {
            name: '',
            code: '',
            iffName: '',
            iffCode: '',
            family: '',
            subFamily: '',
            subGroup: '',
            subGroup2: '',
            subGroup3: '',
            subGroup4: '',
            marketReference: '',
            keyIngredients: '',
            otherIngredients: '',
            description: '',
            scentMood: '',
            scent: '',

            timeOfDay: [],
            mood: [],
            personalStyle: [],
            activity: [],
            gender: [],
            scentGender: '',
            waft: '',
            status: '',
            inStock: false
        };

        scent.get($scope.scentId).then(
            function (data) {
                $scope.scent = data.scent;
            },
            function (errResponse) {
            }
        );

        $scope.statuses = ['Active', 'Inactive'];
        $scope.activities = ['sport', 'social', 'work', 'dating'];
        $scope.wafts = ['subtle', 'balanced', 'daring'];
        $scope.genders = ['man', 'woman', 'unisex'];
        $scope.moods = ['fresh', 'relaxed', 'elegant', 'sexy'];
        $scope.personalStyles = ['classic', 'trendy'];
        $scope.timeOfDays = ['day', 'night'];

        $scope.save = function () {
            scent.post($scope.scent).then(
                function (data) {
                    if (data.success) {
                        $state.go('app.scents');
                    }
                },
                function (errResponse) {
                }
            );
        };

        $scope.sync = function (bool, item, resultList) {

            if (bool) {
                // add item
                resultList.push(item);

            } else {
                // remove item
                for (var i = 0; i < resultList.length; i++) {
                    if (resultList[i] == item) {
                        resultList.splice(i, 1);
                    }
                }
            }
        };

        $scope.isChecked = function (item, resultList) {
            var match = false;
            for (var i = 0; i < resultList.length; i++) {
                if (resultList[i] == item) {
                    match = true;
                }
            }
            return match;
        };

    }

})();

(function () {
    'use strict';

    angular
        .module('adminApp')
        .controller('FragrancesCtrl', FragrancesCtrl);

    FragrancesCtrl.$inject = ['$scope', 'fileUpload', 'fragrances'];

    function FragrancesCtrl($scope, fileUpload, fragrances) {

        $scope.newFile = {};
        $scope.message = '';
        $scope.details = '';
        $scope.term = '';
        $scope.fragrance = null;
        $scope.journeys = [];

        var paginationOptions = {
            pageNumber: 1,
            pageSize: 50,
            sort: null
        };

        $scope.gridOptions = {
            paginationPageSizes: [10, 50, 100],
            paginationPageSize: 50,
            useExternalPagination: true,
            enableColumnMenus: false,
            columnDefs: [
                {
                    field: 'brand',
                    displayName: 'Brand',
                    width: 150
                },
                {
                    field: 'short_name',
                    displayName: 'Name',
                    width: 150,
                    cellTemplate: '<div class="ui-grid-cell-contents"><a href="" ng-click="grid.appScope.showMore(row.entity._id)">{{row.entity.short_name}}</a></div>'
                },
                {
                    field: 'description',
                    displayName: 'Description',
                    minWidth: 600
                },
                {
                    field: 'scents.main',
                    displayName: 'Main',
                    width: 100,
                    cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.scents.main.join("-")}}</div>'
                },
                {
                    field: 'scents.unisexwomen',
                    displayName: 'UW',
                    width: 100,
                    cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.scents.unisexwomen.join("-")}}</div>'
                },
                {
                    field: 'scent.unisexmen',
                    displayName: 'UM',
                    width: 100,
                    cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.scents.unisexmen.join("-")}}</div>'
                }
            ],

            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;

                gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
                    paginationOptions.pageNumber = newPage;
                    paginationOptions.pageSize = pageSize;
                    getPage(newPage, pageSize);
                });
            }
        };

        $scope.showMore = function (fragranceId) {
            fragrances.get(fragranceId).then(
                function (data) {
                    $scope.fragrance = data.fragrance;
                    getJourneys(fragranceId);
                },
                function (errResponse) {
                }
            );
        };

        $scope.search = function() {
            getPage(1, paginationOptions.pageSize);
        };

        $scope.getJourneyDesc = function (journey) {
            if (!journey) return 'n/a';
            var gift = journey.gift;
            if (typeof gift === 'undefined')
                gift = ($scope.order.customPackaging.firstName === $scope.order.sender.firstName);
            if (gift === true) gift = 'gift';
            else gift = 'not a gift';
            var result = [gift, journey.recipientGender, journey.gender, journey.timeOfDay,
                journey.activity, journey.mood, journey.personalStyle, journey.waft];
            return result.join(' / ');
        };

        var element = document.getElementById('newFile');

        element.addEventListener('change', function (e) {
            $scope.newFile = e.target.files[0];
        });

        $scope.uploadFile = function () {
            var file = $scope.newFile;
            if (file == undefined) {
                return;
            }

            var uploadUrl = "/api/admin/fragrances";

            $scope.message = "Uploading...";
            $scope.details = '';

            fileUpload.uploadFileToUrl(file, uploadUrl, 'fragranceFile')
                .then(function ok(result) {
                        $scope.message = result.message;
                        $scope.details = JSON.stringify(result, null, '    ');
                        document.getElementById('newFile').value = "";
                    },
                    function err(err) {
                        $scope.message = err.message;
                        $scope.details = JSON.stringify(err, null, '    ');
                    });
        };

        getPage(paginationOptions.pageNumber, paginationOptions.pageSize);

        function getPage(page, take) {

            fragrances.getList($scope.term, take, page).then(
                function (data) {
                    $scope.gridOptions.data = data.docs;
                    $scope.gridOptions.totalItems = data.count;
                },
                function (errResponse) {
                }
            );
        }

        function getJourneys (fragranceId) {
            fragrances.getJourneys(fragranceId).then(
                function (data) {
                    $scope.journeys = data.journeys;
                },
                function (errResponse) {
                    $scope.journeys = [];
                });
        };
    }

})();

(function () {
  'use strict';

  angular
    .module('adminApp')
    .controller('HomeCtrl', HomeCtrl);

  HomeCtrl.$inject = ['$rootScope', '$scope', '$state', 'uiGridConstants', 'order'];

  function HomeCtrl($root, $scope, $state, uiGridConstants, order) {

    //  $scope.login=function(){
    //      $state.go('app.login');
    //  }

  }

})();
(function () {
    'use strict';

    angular
        .module('adminApp')
        .controller('JourneysCtrl', JourneysCtrl);

    JourneysCtrl.$inject = ['$rootScope', '$scope', '$state', '$location', 'uiGridConstants', 'journeys'];

    function JourneysCtrl($root, $scope, $state, $location, uiGridConstants, journeys) {

        var paginationOptions = {
            pageNumber: 1,
            pageSize: 50,
            sort: null
        };

        $scope.gridOptions = {
            paginationPageSizes: [50, 100],
            paginationPageSize: 50,
            useExternalPagination: true,
            enableColumnMenus: false,
            columnDefs: [
                {
                    field: 'createdAt',
                    displayName: 'DATE',
                    width: 130,
                    type: 'date',
                    cellFilter: 'date:\'yyyy-MM-dd HH:mm\''
                },
                {
                    field: '_id',
                    displayName: 'ID',
                    width: 200
                },
                {
                    field: 'email',
                    displayName: 'EMAIL',
                    cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.user?row.entity.user.email:(row.entity.email+" (email)")}}</div>',
                    width: 200
                },
                {
                    field: 'status',
                    displayName: 'STATUS',
                    width: 100
                },
                {
                    field: 'step',
                    displayName: 'LAST STEP',
                    width: 200
                },
                {
                    field: 'answers',
                    displayName: 'ANSWERS',
                    cellTemplate: "<div class='ui-grid-cell-contents'>"
                        +"<span>{{row.entity.gender? row.entity.recipientGender+' / ':''}}</span>"
                        +"<span>{{row.entity.gender? row.entity.gender+' / ':''}}</span>"
                        +"<span>{{row.entity.timeOfDay? row.entity.timeOfDay+' / ':''}}</span>"
                        +"<span>{{row.entity.activity? row.entity.activity+' / ':''}}</span>"
                        +"<span>{{row.entity.mood? row.entity.mood+' / ':''}}</span>"
                        +"<span>{{row.entity.personalStyle? row.entity.personalStyle+' / ':''}}</span>"
                        +"<span>{{row.entity.waft? row.entity.waft:''}}</span></div>"
                }
            ],

            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;

                gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
                    paginationOptions.pageNumber = newPage;
                    paginationOptions.pageSize = pageSize;
                    getPage(newPage, pageSize);
                });
            }
        };

        var getPage = function (page, take) {

            journeys.getList(take, page).then(
                function (data) {
                    $scope.gridOptions.data = data.docs;
                    $scope.gridOptions.totalItems = data.count;
                },
                function (errResponse) {
                }
            );
        };

        getPage(paginationOptions.pageNumber, paginationOptions.pageSize);

    }

})();
(function () {
    'use strict';

    angular
        .module('adminApp')
        .controller('LoginCtrl', LoginCtrl);

    LoginCtrl.$inject =
        [
            '$rootScope',
            '$state',
            'authService',
        ];

    function LoginCtrl($root, $state, auth) {

        var vm = this;

        vm.message = "";

        vm.loginData = {
            username: "",
            password: ""
        };

        vm.login = function () {
            fixAutofill();

            auth.login(vm.loginData).then(
                function (result) {
                    $state.go('app.orders');
                },
                function (err) {
                    vm.message = err.message;
                }
            );
        };

        function fixAutofill() {
            var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);

            if (iOS) {
                vm.loginData.username = $('input[name="username"]').val();
                vm.loginData.password = $('input[name="password"]').val();
            }
        }
    }

})();
(function () {
  'use strict';

  angular
    .module('adminApp')
    .controller('OrdersCtrl', OrdersCtrl);

  OrdersCtrl.$inject = ['$rootScope', '$scope', '$state', 'uiGridConstants', 'order', 'scent', 'country', 'modalService', 'warehouse'];

  function OrdersCtrl($root, $scope, $state, uiGridConstants, order, scent, country, modalService, warehouse) {

    $scope.term = '';
    $scope.warehouses = '';

    $scope.order = null;
    $scope.nextOrder = null;
    $scope.prevOrder = null;
    $scope.message = '';
    $scope.isEditing = false;
    $scope.isProcessing = false;
    $scope.searchWarehouse = null;
    $scope.searchStatus = null;
    $scope.searchDate = null;
    $scope.searchSamplesStatus = null;
    $scope.countries = [];

    $scope.orderDeliveryDate = '';

    $scope.otherOrders = [];

    function processDeliveryDate() {
      var d;
      if ($scope.order && $scope.order.deliveredOn)
        d = new Date($scope.order.deliveredOn);
      else
        d = new Date();
      d.setSeconds(0, 0);
      return d;
    }

    function refreshScents() {
      scent.getList(0, 1, $scope.order.warehouse).then(
        function (data) {
          $scope.scents = data.docs;
        },
        function (errResponse) {
        }
      );
    }

    $scope.genders = ['man', 'woman', 'unisex'];
    $scope.statuses = ['Gift Invitation', 'Created', 'Payment Failed', 'Received', 'On Hold', 'Completed Trial', 'Processing', 'Processed', 'Shipped', 'Delivered', 'Returned', 'Refunded'];
    $scope.orderSizes = ['big', 'medium', 'small', 'samples', '100ml', '50ml', '15ml', '5ml'];
    $scope.orderSizesMain = ['big', 'medium'];
    $scope.searchStatuses = ['All Statuses', 'Received-Refunded'].concat($scope.statuses);
    $scope.searchWarehouses = ['All Warehouses'];
    $scope.searchDates = ['All Dates', 'Today', 'Yesterday', 'This Month', 'Last Month', 'Last 7 Days', 'Last 30 Days', 'Last 60 Days'];
    $scope.samplesStatuses = ['Pending', 'Cancelled', 'Completed'];
    $scope.samplesStatuses = ['Pending', 'Cancelled', 'Completed'];
    $scope.searchSamplesStatuses = ['All Samples'].concat($scope.samplesStatuses);
    $scope.types = ['Intensify', 'Freshen', 'Dare'];
    $scope.styles = ['modern', 'classic', 'handwritten'];

    function formatQuantity(scent, size) {
      return (scent.availableQuantity && scent.availableQuantity[size]) ?
        scent.availableQuantity[size] : 0
    }

    function getInvQuantity(scent, isBase) {
      var size = $scope.order.orderSize;
      if (!size) return [0];
      if (!$scope.scents) return [0];
      switch (size) {
        case '100ml':
          size = 'big';
          break;
        case '50ml':
          size = 'medium';
          break;
        case '15ml':
          size = 'small';
          break;
        case '5ml':
          size = 'samples';
          break;
        case 'big':
          size = isBase ? 'big' : 'small';
          break;
        case 'medium':
          size = isBase ? 'medium' : 'small';
          break;
      }
      var s = getScentByCode(scent.code);
      if (!s) return [0];
      if ($scope.order.orderSize != 'samples')
        return [formatQuantity(s, size)];
      var mainOrderSize = $scope.order.orderSizeMain || 'big';
      if (!isBase) mainOrderSize = 'small';
      return [formatQuantity(s, size), formatQuantity(s, mainOrderSize)];
    }

    $scope.hasOtherImporantOrders = function () {
      for (var i = 0; i < $scope.otherOrders.length; i++) {
        var o = $scope.otherOrders[i];
        if ((o.status == 'Gift Invitation') ||
          (o.status == 'Created') ||
          (o.status == 'Payment Failed')) continue;
        return true;
      }
      return false;
    };

    $scope.warehouseName = function (warehouseId) {
      return warehouseId ? $scope.warehouses.find(e => e._id == warehouseId).name : '';
    };

    $scope.warehouseId = function (name) {
      return name ? $scope.warehouses.find(e => e.name == name)._id : null;
    };

    $scope.canCancelTrial = function () {
      return $root.canEdit() && ($scope.order.orderSize == 'samples') && ($scope.order.status != 'Payment Failed') &&
        ($scope.order.status != 'Created') && ($scope.order.samplesStatus == 'Pending');
    };

    $scope.cancelTrial = function () {
      if (!$scope.canCancelTrial()) return;
      $scope.selectSamplesStatus('Cancelled');
      $scope.save();
    };

    $scope.canExpediteTrial = function () {
      return $root.canEdit() && ($scope.order.orderSize == 'samples') && $scope.hasScents() &&
        ($scope.order.status != 'Created') && ($scope.order.status != 'Payment Failed');
    };

    $scope.expediteTrial = function () {
      if (!$scope.canExpediteTrial()) return;
      var modalOptions = {
        closeButtonText: 'Cancel',
        actionButtonText: 'Expedite',
        headerText: 'Expedite Order?',
        bodyText: 'Are you sure you want to expedite this order?'
      };
      if ($scope.order.samplesStatus == 'Completed')
        modalOptions.bodyText = 'This trial is already complted. Are you sure you want to expedite this order AGAIN?';

      if (confirm(modalOptions.bodyText)) {
        if ($scope.order.samplesStatus == 'Completed') {
          $scope.order.expediteAgain = true;
        }
        $scope.selectSamplesStatus('Completed');
        $scope.save();
      }
      // TODO: replace with below
      // modalService.showModal({}, modalOptions)
      //     .then(function (result) {
      //         $scope.selectSamplesStatus('Complated');
      //         $scope.save();
      //     });
    };

    $scope.canHold = function () {
      return ($scope.order.status == 'Received') || ($scope.order.status == 'Processing') || ($scope.order.status == 'Completed Trial')
    };

    $scope.putOnHold = function () {
      if (!$scope.canHold()) return;
      var reason = prompt("Reason?", "");
      if (reason == null) return;
      $scope.order.onHoldReason = reason;
      $scope.selectStatus('On Hold');
      $scope.save();
    };

    $scope.hasScents = function () {
      return ($scope.order.journey.scents && $scope.order.journey.scents.main && $scope.order.journey.scents.main.code) &&
        ($scope.order.journey.scents && $scope.order.journey.scents.add1.scent && $scope.order.journey.scents.add1.scent.code) &&
        ($scope.order.journey.scents && $scope.order.journey.scents.add2.scent && $scope.order.journey.scents.add2.scent.code);
    };

    $scope.canProcess = function () {
      return $root.canEdit() && ($scope.order.status == 'Received') && $scope.hasScents();
    };

    $scope.setToProcessing = function () {
      if (!$scope.canProcess()) return;
      $scope.selectStatus('Processing');
      $scope.save();
    };

    $scope.canConfirmRecommendation = function () {
      return $root.canEdit() && ($scope.order.status == 'Received') && $scope.order.recommendations &&
        $scope.order.recommendations.main && (!($scope.order.journey.scents && $scope.order.journey.scents.main && $scope.order.journey.scents.main.code)) &&
        $scope.order.recommendations.add1 && (!($scope.order.journey.scents && $scope.order.journey.scents.add1.scent && $scope.order.journey.scents.add1.scent.code)) &&
        $scope.order.recommendations.add2 && (!($scope.order.journey.scents && $scope.order.journey.scents.add2.scent && $scope.order.journey.scents.add2.scent.code))
    };

    $scope.confirmRecommendation = function () {
      if (!$scope.canConfirmRecommendation()) return;
      $scope.selectScent($scope.order.recommendations.main.code, 'main');
      $scope.selectScent($scope.order.recommendations.add1.code, 'add1');
      $scope.selectScent($scope.order.recommendations.add2.code, 'add2');
      if ($scope.order.status == 'Received') $scope.selectStatus('Processing');
      $scope.save();
    };

    $scope.getScentFullName = function (scent, isBase, isQuantity) {
      if (!(scent && scent.code)) return '';
      var res = scent.code + ' - ' + scent.name;
      if (isQuantity)
        res += ' [' + getInvQuantity(scent, isBase).join(',') + ']';
      return res;
    };

    $scope.getJourneyDesc = function (journey) {
      if (!journey) return 'n/a';
      var gift = journey.gift;
      if (typeof gift === 'undefined')
        gift = ($scope.order.customPackaging.firstName === $scope.order.sender.firstName);
      if (gift === true) gift = 'gift';
      else gift = 'not a gift';
      var result = [gift, journey.recipientGender, journey.gender, journey.timeOfDay,
        journey.activity, journey.mood, journey.personalStyle, journey.waft];
      return result.join(' / ');
    };

    $scope.next = function () {
      $scope.showMore($scope.nextOrder);
    };

    $scope.prev = function () {
      $scope.showMore($scope.prevOrder);
    };

    $scope.editOrder = function () {
      $scope.isEditing = true;
      $scope.message = '';
      $scope.orderDeliveryDate = processDeliveryDate();
      if (!$scope.order.journey) return;
      if (!$scope.order.journey.scents)
        initScents();
    };

    $scope.selectStyle = function (style) {
      $scope.order.customBottle.style = style;
    };

    $scope.selectStatus = function (status) {
      $scope.order.status = status;
      if (status == 'Delivered') {
        if ((!$scope.order.deliveredOn) || ($scope.order.deliveredOn == ''))
          $scope.order.deliveredOn = (new Date()).toISOString();
      }
    };

    $scope.selectSamplesStatus = function (status) {
      $scope.order.samplesStatus = status;
    };

    $scope.selectGender = function (gender) {
      $scope.order.gender = gender;
    };

    $scope.selectWarehouse = function (warehouseId) {
      $scope.order.warehouse = warehouseId;
      refreshScents();
    };

    $scope.selectOrderSize = function (orderSize) {
      $scope.order.orderSize = orderSize;
    };

    $scope.selectMainOrderSize = function (orderSize) {
      $scope.order.orderSizeMain = orderSize;
    };

    $scope.refreshTable = function () {
      getPage(paginationOptions.pageNumber, paginationOptions.pageSize)
    };

    $scope.getTrackingUrl = function () {
      return "http://track.waft.com/" + $scope.order.shippingInfo.trackingNumber.replace(/\s/g, '');
    };

    $scope.save = function () {

      if ($scope.isProcessing) return;
      startProcessing();

      order.post($scope.order).then(
        function (data) {
          if (data.success) {
            $scope.message = data.message;
            $scope.isEditing = false;
            $scope.showMore($scope.order._id, true);
            $scope.refreshTable();
          } else {
            $scope.message = data.message;
          }
          endProcessing();
        },
        function (err) {
          if (err && err.data && err.data.message)
            $scope.message = err.data.message;
          else
            $scope.message = "Unknown Server Error";
          endProcessing();
        }
      );
    }

    $scope.cancel = function () {
      $scope.isEditing = false;
      $scope.message = '';

      $scope.showMore($scope.order._id, true);
    };

    function getScentByCode(code) {
      for (var i = 0; i < $scope.scents.length; i++) {
        var scent = $scope.scents[i];
        if (scent.code == code) return scent;
      }
      return null;
    }

    $scope.selectScent = function (code, type) {
      if (!$scope.order.journey.scents) initScents();
      var scent = getScentByCode(code);
      switch (type) {
        case 'main':
          $scope.baseScent = scent.code;
          $scope.order.journey.scents.main = scent;
          break;
        case 'add1':
          $scope.add1Scent = scent.code;
          $scope.order.journey.scents.add1.scent = scent;
          break;
        case 'add2':
          $scope.add2Scent = scent.code;
          $scope.order.journey.scents.add2.scent = scent;
          break;
      }
    };

    $scope.selectAddType = function (addType, type) {
      if (!$scope.order.journey.scents) initScents();
      switch (type) {
        case 'add1':
          $scope.order.journey.scents.add1.addType = addType;
          break;
        case 'add2':
          $scope.order.journey.scents.add2.addType = addType;
          break;
      }
    };

    function startProcessing() {
      $scope.message = 'Processing...';
      $scope.isProcessing = true;
    }

    function endProcessing() {
      $scope.isProcessing = false;
    }

    function initScents() {
      $scope.order.journey.scents = {
        main: '',
        add1: {
          addType: '',
          scent: ''
        },
        add2: {
          addType: '',
          scent: ''
        }
      };
    }

    $scope.baseScent = '';
    $scope.add1Scent = '';
    $scope.add2Scent = '';

    $scope.baseScentConfig = {
      maxItems: 1,
      create: false,
      valueField: 'code',
      labelField: 'name',
      delimiter: '|',
      searchField: ['code', 'name'],
      placeholder: 'Select',
      onChange: function (value) {
        $scope.selectScent(value, 'main')
      },
      render: {
        item: function (item, escape) {
          return "<div>" + escape($scope.getScentFullName(item, true, true)) + "</div>";
        },
        option: function (item, escape) {
          return "<div>" + escape($scope.getScentFullName(item, true, true)) + "</div>";
        }
      }
    };

    $scope.add1ScentConfig = {
      maxItems: 1,
      create: false,
      valueField: 'code',
      labelField: 'name',
      delimiter: '|',
      searchField: ['code', 'name'],
      placeholder: 'Select',
      onChange: function (value) {
        $scope.selectScent(value, 'add1')
      },
      render: {
        item: function (item, escape) {
          return "<div>" + escape($scope.getScentFullName(item, false, true)) + "</div>";
        },
        option: function (item, escape) {
          return "<div>" + escape($scope.getScentFullName(item, false, true)) + "</div>";
        }
      }
    };

    $scope.add2ScentConfig = {
      maxItems: 1,
      create: false,
      valueField: 'code',
      labelField: 'name',
      delimiter: '|',
      searchField: ['code', 'name'],
      placeholder: 'Select',
      onChange: function (value) {
        $scope.selectScent(value, 'add2')
      },
      render: {
        item: function (item, escape) {
          return "<div>" + escape($scope.getScentFullName(item, false, true)) + "</div>";
        },
        option: function (item, escape) {
          return "<div>" + escape($scope.getScentFullName(item, false, true)) + "</div>";
        }
      }
    };

    function defineScents(_order) {
      $scope.baseScent = '';
      $scope.add1Scent = '';
      $scope.add2Scent = '';

      if (_order.journey && _order.journey.scents) {
        if (_order.journey.scents.main)
          $scope.baseScent = _order.journey.scents.main.code;
        if (_order.journey.scents.add1.scent)
          $scope.add1Scent = _order.journey.scents.add1.scent.code;
        if (_order.journey.scents.add2.scent)
          $scope.add2Scent = _order.journey.scents.add2.scent.code;
      }
    }

    var paginationOptions = {
      pageNumber: 1,
      pageSize: 50,
      sort: null
    };

    $scope.searchByStatus = function (st) {
      $scope.searchStatus = (st === $scope.searchStatuses[0]) ? null : st;
      $scope.search();
    };

    $scope.searchByWarehouse = function (st) {
      $scope.searchWarehouse = (st === $scope.searchWarehouses[0]) ? null : st;
      $scope.search();
    };

    $scope.searchByDate = function (d) {
      $scope.searchDate = (d === $scope.searchStatuses[0]) ? null : d;
      $scope.search();
    };

    $scope.getExportLink = function (fields) {
      var url = '/api/admin/orders/export.csv';
      var params = [];
      if ($scope.searchStatus)
        params.push({ name: 'status', value: $scope.searchStatus });
      if ($scope.searchDate)
        params.push({ name: 'dates', value: $scope.searchDate });
      if ($scope.searchWarehouse)
        params.push({ name: 'warehouse', value: $scope.warehouseId($scope.searchWarehouse) });
      if (fields)
        params.push({ name: 'fields', value: fields});
      let query = params.map( v => v.name + '=' + encodeURIComponent(v.value)).join('&');
      if (query)
        url += '?' + query;
      return url;
    };

    $scope.searchBySamplesStatus = function (st) {
      $scope.searchSamplesStatus = st;
      $scope.search();
    };

    $scope.search = function () {
      getPage(1, paginationOptions.pageSize);
    };

    var dateFormat = "yyyy-MM-dd HH:mm:ss Z";

    $scope.gridOptions = {
      paginationPageSizes: [10, 50, 100],
      paginationPageSize: 50,
      useExternalPagination: true,
      enableColumnMenus: false,
      columnDefs: [
        {
          field: '_id',
          displayName: 'ID',
          width: 200,
          cellTemplate: '<div class="ui-grid-cell-contents"><a href="" ng-click="grid.appScope.showMore(row.entity._id)">{{row.entity._id}}</a></div>'
        },
        {
          field: 'countryCode',
          displayName: 'NAME',
          width: 200,
          cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.sender.firstName + " " + row.entity.sender.lastName}}</div>'
        },
        {
          field: 'sender.email',
          displayName: 'EMAIL',
          width: 240
        },
        {
          field: 'countryCode',
          displayName: 'CC',
          cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.countryCode | uppercase}}</div>'
        },
        {
          field: 'createdAt',
          displayName: 'DATE',
          width: 130,
          type: 'date',
          cellFilter: 'date:\'yyyy-MM-dd HH:mm\''
        },
        {
          field: 'type',
          displayName: 'TYPE',
          width: 150,
          cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.type}} {{row.entity.orderSize}}</div>'
        },
        {
          field: 'amount',
          displayName: 'AMOUNT',
          width: 90,
          cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.currency|uppercase}}{{row.entity.amount}}</div>'
        },
        {field: 'status', displayName: 'STATUS', width: 100}
      ],

      onRegisterApi: function (gridApi) {
        $scope.gridApi = gridApi;

        gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
          paginationOptions.pageNumber = newPage;
          paginationOptions.pageSize = pageSize;
          getPage(newPage, pageSize);
        });
      }
    };

    $scope.showMore = function (orderId, refresh) {
      if (!orderId) return;
      order.get(orderId).then(
        function (data) {
          $scope.order = data.order;
          refreshScents();
          $scope.countries = country.getList();
          if (!refresh) $scope.message = '';
          defineScents($scope.order);
          $scope.isEditing = false;
          $scope.orderDeliveryDate = processDeliveryDate();
          $scope.getOtherOrders(data.order.sender._id, orderId);
          var i = getOrderIndex(orderId);
          if (i != -1) {
            var all_orders = $scope.gridOptions.data;
            $scope.nextOrder = (i != 0) ? all_orders[i - 1]._id : null;
            $scope.prevOrder = (i != (all_orders.length - 1)) ? all_orders[i + 1]._id : null;
          }
        },
        function (errResponse) {
        }
      );
    };

    function getOrderIndex(orderId) {
      var all_orders = $scope.gridOptions.data;
      for (var i = 0; i < all_orders.length; i++)
        if (all_orders[i]._id == orderId)
          return i;
      return -1;
    }

    $scope.getOtherOrders = function (userid, excludedOrder) {
      order.getList({ userid, excludedOrder}).then(
        function (data) {
          $scope.otherOrders = data.docs;
        },
        function (errResponse) {
          $scope.otherOrders = [];
        }
      );
    };

    $scope.selectCountry = function (country) {
      $scope.order.delivery.country = country.country;
    };

    $scope.getFragranceDescription = function (f, main_order) {
      if (!f) return '';
      var sex = (f.male && f.female) ? 'unisex' : (f.male ? "men's" : "women's");
      var accords = [];
      f.accords = f.accords.sort(function (a, b) {
        return b.weight - a.weight;
      });
      for (var i = 0; i < f.accords.length; i++) {
        accords.push(f.accords[i].name)
      }
      var scents = '';
      if (f.scents.main && f.scents.main.length > 0) {
        scents = ' ' + f.scents.main.join('-');
        if (f.scents.unisexwomen && f.scents.unisexwomen.length > 0)
          scents += ' UW:' + f.scents.unisexwomen.join('-');
        if (f.scents.unisexmen && f.scents.unisexmen.length > 0)
          scents += ' UM:' + f.scents.unisexmen.join('-');
      } else if (main_order && $scope.order.recommendations && $scope.order.recommendations.history) {
        scents = ' History: ' + $scope.order.recommendations.history.join('-');
      }
      return f.name + ' (' + sex + ' / ' + (f.family || 'Unknown family ') + ' / ' + f.main_accord +
        ' / ' + accords.join(', ') + ')' + scents;
    };

    function getSearchHash(page, take) {
      return $scope.term + $scope.searchStatus + $scope.searchSamplesStatus + $scope.searchDate
    }

    var getPage = function (page, take) {
      var searchHash = getSearchHash(page, take);
      order.getList({
        take,
        page,
        term : $scope.term,
        status: $scope.searchStatus,
        samplesStatus: $scope.searchSamplesStatus,
        warehouse: $scope.warehouseId($scope.searchWarehouse),
        dates: $scope.searchDate
      })
        .then(
          function (data) {
            if (searchHash != getSearchHash(page, take)) return;
            $scope.gridOptions.data = data.docs;
            $scope.gridOptions.totalItems = data.count;
          },
          function (errResponse) {
          }
      );
    };

    function readWarehouses() {
      warehouse.getList().then(
        function (data) {
          $scope.warehouses = data.warehouses;
          $scope.searchWarehouses = ['All Warehouses'].concat(data.warehouses.map(w => w.name));
        },
        function (errResponse) {
        }
      );
    }

    readWarehouses();
    getPage(paginationOptions.pageNumber, paginationOptions.pageSize);
  }
})();

(function () {
    'use strict';

    angular
        .module('adminApp')
        .controller('PricesCtrl', PricesCtrl);

    PricesCtrl.$inject = ['$rootScope', '$scope', '$state', '$location', 'uiGridConstants', 'prices'];

    function PricesCtrl($root, $scope, $state, $location, uiGridConstants, prices) {

        var getPage = function () {

            prices.getPrices().then(
                function (data) {
                    $scope.data = data;
                },
                function (errResponse) {
                }
            );
        };

        $scope.getPage = getPage;

        getPage();

    }

})();

(function () {
    'use strict';

    angular
        .module('adminApp')
        .controller('ScentsCtrl', ScentsCtrl);

    ScentsCtrl.$inject = ['$rootScope', '$scope', '$state', '$location', 'uiGridConstants', 'scent', 'authService', 'fileUpload', 'warehouse'];

    function ScentsCtrl($root, $scope, $state, $location, uiGridConstants, scent, authService, fileUpload, warehouse) {

        $scope.warehouses = '';
        $scope.warehouse = '';
        $scope.newFile = {};
        $scope.message = '';
        $scope.details = '';
        $scope.warehouseToUpload = 'US';

        var paginationOptions = {
            pageNumber: 1,
            pageSize: 50,
            sort: null
        };

        var baseNameTemplate = "<div class='ui-grid-cell-contents'><a href='' ng-click='grid.appScope.editScent(row.entity._id)'>{{row.entity.name}}</a><div>";
        var readonlyNameTemplate = "<div class='ui-grid-cell-contents'>{{row.entity.name}}<div>";

        $scope.setWarehouse = function (warehouse) {
            if ($scope.warehouse != warehouse) {
                $scope.warehouse = warehouse;
                getPage(paginationOptions.pageNumber, paginationOptions.pageSize, $scope.warehouse);
            }
        };

        $scope.gridOptions = {
            paginationPageSizes: [50, 100],
            paginationPageSize: 50,
            useExternalPagination: true,
            enableColumnMenus: false,
            columnDefs: [
                {
                    field: 'code', displayName: 'CODE'
                },
                {
                    field: 'name', displayName: 'NAME',
                    cellTemplate: isReadOnly() ? readonlyNameTemplate : baseNameTemplate
                },
                {
                    field: 'family', displayName: 'FAMILY'
                },
                {
                    field: 'scentGender', displayName: 'GENDER'
                },
                {
                    field: 'availableQuantity.big', displayName: '100ml',
                    cellTemplate: "<div class='ui-grid-cell-contents'><span >{{row.entity.availableQuantity.big || 0}}</span></div>"
                },
                {
                    field: 'availableQuantity.medium', displayName: '50ml',
                    cellTemplate: "<div class='ui-grid-cell-contents'><span >{{row.entity.availableQuantity.medium || 0}}</span></div>"
                },
                {
                    field: 'availableQuantity.small', displayName: '15ml',
                    cellTemplate: "<div class='ui-grid-cell-contents'><span >{{row.entity.availableQuantity.small || 0}}</span></div>"
                },
                {
                    field: 'availableQuantity.samples', displayName: '5ml',
                    cellTemplate: "<div class='ui-grid-cell-contents'><span >{{row.entity.availableQuantity.samples || 0}}</span></div>"
                },
                {field: 'status', displayName: 'STATUS'}],

            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;

                gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
                    paginationOptions.pageNumber = newPage;
                    paginationOptions.pageSize = pageSize;
                    getPage(newPage, pageSize);
                });
            }
        };

        $scope.editScent = function (scentId) {
            $location.path('/scents/edit/' + scentId);
        };

        var element = document.getElementById('newFile');

        element.addEventListener('change', function (e) {
            $scope.newFile = e.target.files[0];
        });

        $scope.uploadFile = function () {
            var file = $scope.newFile;
            if (file == undefined) {
                return;
            }

            var uploadUrl = "/api/admin/inventory?warehouse=" + $scope.warehouseToUpload;

            $scope.message = "Uploading...";
            $scope.details = '';

            fileUpload.uploadFileToUrl(file, uploadUrl, 'inventoryFile')
                .then(function ok(result) {
                        $scope.message = result.message;
                        $scope.details = JSON.stringify(result, null, '    ');
                        document.getElementById('newFile').value = "";
                    },
                    function err(err) {
                        $scope.message = err.message;
                        $scope.details = JSON.stringify(err, null, '    ');
                    });
        };

        var getPage = function (page, take, warehouse) {

            scent.getList(take, page, warehouse).then(
                function (data) {
                    $scope.gridOptions.data = data.docs;
                    $scope.gridOptions.totalItems = data.count;
                },
                function (errResponse) {
                }
            );
        };

        function isReadOnly() {
            return authService.getAccessLevel() == 'readonly';
        }

        function readWarehouses() {
            warehouse.getList().then(
                function (data) {
                    $scope.warehouses = data.warehouses;
                },
                function (errResponse) {
                }
            );
        }

        readWarehouses();
        getPage(paginationOptions.pageNumber, paginationOptions.pageSize, $scope.warehouse);

    }

})();

(function () {
    'use strict';

    angular
        .module('adminApp')
        .controller('UsersCtrl', UsersCtrl);

    UsersCtrl.$inject = ['$rootScope', '$scope', 'user', 'order', 'journeys'];

    function UsersCtrl($root, $scope, user, order, journeys) {

        $scope.term = '';

        $scope.user = null;
        $scope.logs = null;
        $scope.message = '';
        $scope.isEditing = false;
        $scope.isProcessing = false;
        $scope.orders = [];
        $scope.journeys = [];
        $scope.showjson = false;

        var oldEmail = '';

        $scope.refreshTable = function () {
            getPage(paginationOptions.pageNumber, paginationOptions.pageSize)
        };

        $scope.cancel = function () {
            $scope.isEditing = false;
            $scope.message = '';
            $scope.user.email = oldEmail;
        };

        function startProcessing() {
            $scope.message = 'Processing...';
            $scope.isProcessing = true;
        }

        function endProcessing() {
            $scope.isProcessing = false;
        }

        var paginationOptions = {
            pageNumber: 1,
            pageSize: 50,
            sort: null
        };

        $scope.search = function () {
            getPage(1, paginationOptions.pageSize);
        };

        $scope.gridOptions = {
            paginationPageSizes: [10, 50, 100],
            paginationPageSize: 50,
            useExternalPagination: true,
            enableColumnMenus: false,
            columnDefs: [
                {
                    field: '_id',
                    displayName: 'ID',
                    width: 300,
                    cellTemplate: '<div class="ui-grid-cell-contents"><a href="" ng-click="grid.appScope.showMore(row.entity._id)">{{row.entity._id}}</a></div>'
                },
                {
                    field: 'email',
                    displayName: 'EMAIL',
                    width: 300
                },
                {
                    field: 'firstName',
                    displayName: 'NAME',
                    cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.firstName + " " + row.entity.lastName}}</div>'
                }
            ],
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;

                gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
                    paginationOptions.pageNumber = newPage;
                    paginationOptions.pageSize = pageSize;
                    getPage(newPage, pageSize);
                });
            }
        };

        $scope.showMore = function (userId, refresh) {
            user.get(userId).then(
                function (data) {
                    oldEmail = data.user.email;

                    $scope.user = data.user;
                    $scope.logs = data.logs;
                    if (!refresh) $scope.message = '';
                    $scope.isEditing = false;
                    $scope.getOrders($scope.user._id);
                    $scope.getJourneys($scope.user._id);
                },
                function (errResponse) {
                }
            );
        };

        $scope.save = function () {
            if ($scope.isProcessing) return;
            startProcessing();

            user.update($scope.user).then(
                function (data) {
                    if (data.success) {
                        oldEmail = '';

                        $scope.message = data.message;
                        $scope.isEditing = false;
                        $scope.showMore($scope.user._id, true);
                        $scope.refreshTable();
                    } else {
                        $scope.message = data.message;
                    }
                    endProcessing();
                },
                function (err) {
                    if (err && err.data && err.data.message)
                        $scope.message = err.data.message;
                    else
                        $scope.message = "Unknown Server Error";
                    endProcessing();
                }
            );
        };

        $scope.getOrders = function (userid) {
            order.getList({userid}).then(
                function (data) {
                    $scope.orders = data.orders;
                },
                function (errResponse) {
                    $scope.orders = [];
                });
        };

        $scope.getJourneys = function (userId) {
            journeys.getUserJourneys(userId).then(
                function (data) {
                    $scope.journeys = data.journeys;
                },
                function (errResponse) {
                    $scope.journeys = [];
                });
        };

        $scope.editEmail = function () {
            $scope.isEditing = true;
            $scope.message = '';
        };

        var getPage = function (page, take) {
            user.getList($scope.term, take, page).then(
                function (data) {
                    $scope.gridOptions.data = data.docs;
                    $scope.gridOptions.totalItems = data.count;
                },
                function (errResponse) {
                }
            );
        };

        getPage(paginationOptions.pageNumber, paginationOptions.pageSize);
    }
})();
