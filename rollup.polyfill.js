import rollup      from 'rollup'
import nodeResolve from 'rollup-plugin-node-resolve'
import commonjs    from 'rollup-plugin-commonjs';
import uglify      from 'rollup-plugin-uglify';
import typescript from 'rollup-plugin-typescript';

var ENV = process.env.ENV || 'dev';
var config = require("./build/config." + ENV + ".js");

export default {
  entry: 'src/waft/polyfills.browser.ts',
  dest: 'dist/polyfill.js', // output a single application bundle
  sourceMap: false,
  treeshake: true,
  format: 'iife',
  moduleName: 'polyfill',
  context: 'window',
  intro: "\
    var ENV = '" + config.ENV + "';",
  plugins: [
    nodeResolve({jsnext: true, module: true}),
    typescript({
      typescript: require("typescript"),
      target: "es5",
      module: "es2015",
      outDir: "dist/polyfill"
    }),
    commonjs({
      include: ['node_modules/core-js/**']
    }),
    uglify()
  ]
}
