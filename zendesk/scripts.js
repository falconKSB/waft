(function ($) {
    $('.mobile-menu').click(function () {
        $('.main-menu').toggleClass('ac');
    });
})(jQuery);

if (!(('ontouchstart' in window) || (navigator.maxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0))) document.documentElement.className += " nontouch";
