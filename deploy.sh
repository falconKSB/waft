#!/usr/bin/env bash

set -e

branch_name="$(git symbolic-ref HEAD 2>/dev/null)" ||
branch_name="(unnamed branch)"     # detached HEAD
branch_name=${branch_name##refs/heads/}
echo On branch $branch_name

build_env="stage"
if [ "$branch_name" = "master" ]; then
       build_env="prod"
fi

eb deploy --staged

if [ "$branch_name" = "master" ]; then
       . invalidateCloudFront.sh
fi
