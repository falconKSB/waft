Waft (MongoDB, Express, Angular 2, Node JS)

Personalised perfume at your fingertips. Design your personal fragrance, customise your bottle 
and name your signature scent in just a couple of clicks.