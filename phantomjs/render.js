var system = require('system');

function printUsageAndExit(code) {
    console.log('Usage: phantomjs render.js <template_name> <image_type> <output_filename> <style> <name> <message> <initials>');
    phantom.exit(code || 0);
}

if (system.args.length < 8) {
    printUsageAndExit(1);
}

var templateName = system.args[1];
var imageType = system.args[2];
if (['png', 'jpeg'].indexOf(imageType) == -1) {
    console.log('Error: Unknown image format');
    printUsageAndExit(1);
}

var outputFilename = system.args[3];
var style = system.args[4];
var name = system.args[5];
var message = system.args[6];
var initials = system.args[7];

var page = require('webpage').create();

var customize = function(style, name, message, initials) {
    document.getElementById('bottle').setAttribute('class', 'bottle ' + style);
    document.getElementById('name').innerHTML = name;
    document.getElementById('message').innerHTML = message;
    document.getElementById('initials').innerHTML = initials;
};

var setBackground = function() {
    document.body.setAttribute('style', 'background-color: white;');
}

page.viewportSize = { width: 1, height: 1 };
page.open('phantomjs/templates/' + templateName + '.html', function() {
    if (imageType == 'jpeg') page.evaluate(setBackground);
    page.evaluate(customize, style, name, message, initials);    
    page.render(outputFilename, {format: imageType});
    phantom.exit();
});
