/*
 * Angular bootstraping
 */
import { platformBrowser } from '@angular/platform-browser';
import { decorateModuleRef } from './app/environment';
import { ApplicationRef } from '@angular/core';
/*
 * App Module
 * our top level module that holds all of our components
 */
import { AppModuleNgFactory } from '../../aot/src/waft/app/app.module.ngfactory';

/*
 * Bootstrap our Angular app with a top level NgModule
 */
  platformBrowser()
    .bootstrapModuleFactory(AppModuleNgFactory)
    .then(decorateModuleRef)
    .catch(err => console.error(err));
