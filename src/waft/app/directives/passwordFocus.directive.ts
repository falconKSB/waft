import {HostListener, Directive} from "@angular/core";

@Directive({selector: '[passwordFocus]'})
export class PasswordFocusDirective {
    @HostListener('focus', ['$event.target'])
    onFocus(target) {
        target.type = 'text';
    }
    @HostListener('focusout', ['$event.target'])
    onFocusout(target) {
        target.type = 'password';
    }
}