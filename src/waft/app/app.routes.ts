import { Routes } from '@angular/router';

import { AuthGuard } from './guards/auth.guard';

import { ProfileComponent } from './components/profile/profile.component';
import {MyOrdersComponent} from "./components/profile/myOrders/myOrders.component";
import {MyJourneysComponent} from "./components/profile/myJourneys/myJourneys.component";
import {ReferralComponent} from "./components/profile/referral/referral.component";
import {AppRedirectComponent} from "./components/app-redirect.component";
import {KioskComponent} from "./components/kiosk/kiosk.component";

export const ROUTES: Routes = [
  { path: 'app/kiosk', component: KioskComponent},
  { path: 'app/profile/journeys', component: MyJourneysComponent, canActivate: [AuthGuard]},
  { path: 'app/profile/orders', component: MyOrdersComponent, canActivate: [AuthGuard]},
  { path: 'app/profile/referral', component: ReferralComponent, canActivate: [AuthGuard]},
  { path: 'app/profile', component: ProfileComponent, canActivate: [AuthGuard]},
  { path: 'app', pathMatch: 'full', component: AppRedirectComponent },
  { path: '**', redirectTo: '/app/wizard/type' },
];