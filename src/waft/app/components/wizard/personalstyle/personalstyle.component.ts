import { Component } from "@angular/core";

import { WizardService } from '../../../services/wizard.service';
import {HostListener} from "@angular/core/src/metadata/directives";
import {WizardChoiceComponent} from "../wizard-choice/wizard-choice.component";
import {GTMService} from "../../../services/gtm.service";
import {GiftService} from "../../../services/gift.service";
import {AuthService} from "../../../services/auth.service";
import {ActivatedRoute} from "@angular/router";
import {KioskService} from "../../../services/kiosk.service";

@Component({
    selector: "personalstyle",
    templateUrl: 'personalstyle.template.html',
    styleUrls: ['personalstyle.style.css']
})

export class PersonalStyleComponent extends WizardChoiceComponent {
    constructor(wizard: WizardService, gtm: GTMService, kiosk: KioskService, gift: GiftService,
                route: ActivatedRoute,  auth: AuthService) {
        super(wizard, gtm, 'lab_personal_style', kiosk, gift, route, auth);
    }

    public ngAfterContentInit(){
        this.resize();
    }

    saveChoice(choice: string) : void {
        this.wizard.order.journey.personalStyle = choice;
    }

    @HostListener('window:resize', ['$event.target'])
    public windowResize(target: any): void {
        this.resize();
    }
}
