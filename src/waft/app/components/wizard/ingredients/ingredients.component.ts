import { Component } from "@angular/core";

import { WizardService } from '../../../services/wizard.service';

import { IngredientComponent } from "../../wizard/ingredients/ingredient/ingredient.component";
import { IngredientModel } from "../../../models/ingredient.model"
import {GTMService} from "../../../services/gtm.service";

@Component({
    selector: "ingredients",
    templateUrl: 'ingredients.template.html',
    styleUrls: ['ingredients.style.css']
})

export class IngredientsComponent {

    constructor(public wizard: WizardService, private gtm: GTMService) {
        gtm.event('lab_ingredients');
        this.ingredients = IngredientModel.GetAll();
        this.initSelectedIngredients();
    }

    public ingredients: Array<IngredientModel>;

    public selectIngredient(selectedItem) {
        if (selectedItem.isSelect) {
            selectedItem.isSelect = !selectedItem.isSelect;
            this.removeIngredient(selectedItem.name);
        }
        else {
            selectedItem.isSelect = true;
            this.wizard.order.journey.ingredients.push(selectedItem.name);
        }
    }

    private initSelectedIngredients() {
        for (let i = 0; i < this.wizard.order.journey.ingredients.length; i++) {
            let name = this.wizard.order.journey.ingredients[i];
            var index = this.ingredients.map(function (e) { return e.name; }).indexOf(name);
            this.ingredients[index].isSelect = true;
        }
    }

    private removeIngredient(item: string) {
        var index = this.wizard.order.journey.ingredients.indexOf(item);
        this.wizard.order.journey.ingredients.splice(index, 1);
    }

    public numberSelected() {
        return this.wizard.order.journey.ingredients.length;
    }

    public anySelected() {
        return this.numberSelected() != 0;
    }

    public toNextStep() {
        this.wizard.putToStorage();
        this.wizard.next(true);
        if (this.anySelected()) {
            this.gtm.event('lab_ingredients_selected');
        }
        else {
            this.gtm.event('lab_ingredients_skip');
        }
    }

}
