import { Component, Input, Output, EventEmitter } from "@angular/core";
import { IngredientModel } from "../../../../models/ingredient.model"
import { WizardService } from "../../../../services/wizard.service";

@Component({
    selector: "ingredient",
    templateUrl: 'ingredient.template.html',
    styleUrls: ['ingredient.style.css'],
})

export class IngredientComponent {

    constructor(public wizard: WizardService) {
        this.isClicked = false;
    }

    @Input() ingredient: IngredientModel;

    @Output() onSelect = new EventEmitter();

    private isClicked: boolean;

    public selectIngredient(ingredient: IngredientModel) {
        if (!this.isClicked) {
            this.onSelect.emit(ingredient); this.isClicked = true;
        }
        else {
            this.isClicked = false;
            return;
        }
    }
}
