import {Component, ViewChild, Renderer, ElementRef} from "@angular/core";
import {Router} from '@angular/router';
import {Modal} from "ng2-modal/Modal";

import {WizardService} from '../../../services/wizard.service';
import {WaftAPIService} from "../../../services/waftapi.service";
import {GTMService} from "../../../services/gtm.service";

@Component({
    selector: "fragrances-choise",
    templateUrl: 'fragrancesChoice.template.html',
    styleUrls: ['fragrancesChoice.style.css']
})

export class FragrancesChoiceComponent {

    public yourFragrances: any[];

    public show: boolean = false;
    public searchInProgress: boolean = false;

    public term: string = "";

    searchClosed = false;

    public listOfFragrances: any[];

    constructor(public wizard: WizardService, public router: Router, private api: WaftAPIService, gtm: GTMService, private renderer: Renderer) {
        gtm.event('lab_inspiration');
    }

    @ViewChild('legalNotice') legalNotice: Modal;
    @ViewChild('search') search: ElementRef;

    public madalScreen: string = 'register';

    resetSearch(inputSearch): void {
        this.term = '';
        this.searchClosed = true;
        this.resetSearchResults();
        inputSearch.focus();
    }

    resetSearchResults(): void {
        this.searchInProgress = false;
        this.listOfFragrances = [];
    }

    removeFragranceCard(fragrance: any): void {
        this.wizard.order.removeFragrance(fragrance);
        this.wizard.putToStorage();
    }

    public openLegalNotice() {
        this.legalNotice.open();
    }

    onFocus(event: any): void {
        if (this.searchClosed) return;
        this.onKeyPress(event);
    }

    unFocus(): void {
        this.renderer.invokeElementMethod(this.search.nativeElement, 'blur', []);
        this.searchInProgress = false;
    }

    onKeyPress(event: any): void {

        if (event.type == "keyup") {
            let keyCode = event.keyCode;
            if (keyCode == 27 || keyCode == 38 || keyCode == 40) return;

        }

        let term: string = event.target.value;

        this.api.getFragrances(term).subscribe(
            data => {
                if (term != event.target.value) {
                    return;
                }
                this.listOfFragrances = data;
                this.listOfFragrances.length > 0 ? this.searchInProgress = true : this.searchInProgress = false;
                this.searchClosed = false;
            });
    }

    public onFragranceSelected(selectedItem: any) {
        this.wizard.order.addFragrance(selectedItem);
        this.searchInProgress = false;
        this.term = "";
        this.listOfFragrances = [];
    }

    public continue(fragrance?: any) {
        this.wizard.order.setPrimaryFragrance(fragrance);
        this.wizard.putToStorage();
        this.toNextStep();
    }

    public toNextStep() {
        this.wizard.next(true);
    }
}
