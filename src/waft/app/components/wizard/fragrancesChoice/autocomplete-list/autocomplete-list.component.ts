"use strict";
import {
    Component, Output, Input, EventEmitter, HostListener, ViewChild, ElementRef, SimpleChange, OnChanges
} from "@angular/core";

@Component({
    selector: "autocomplete-list",
    templateUrl: './autocomplete-list.template.html',
    styleUrls: ['./autocomplete-list.style.css']
})
export class AutocompleteListComponent implements OnChanges {

    @Output() public selectItem = new EventEmitter();
    @Output() public closeList = new EventEmitter();

    @Input() public listOfFragrances: any;

    @ViewChild('list') list: ElementRef;

    public focusIndex: number = 0;
    public scrollIndex: number = 0;

    public onClick(item: {text: string, data: any}) {
        this.selectItem.emit(item);
    }

    public onHoverItem(index: number) {
        this.focusIndex = index;
    }

    @HostListener('window:keydown.escape', ['$event']) onKeyEscape(event) {
        this.closeList.emit();
    }

    @HostListener('window:keyup.enter', ['$event']) onKeyEnter(event) {
        this.selectItem.emit(this.listOfFragrances[this.focusIndex]);
    }

    @HostListener('window:keydown.arrowdown', ['$event']) onKeyArrowDown(event) {
        this.changeCurrentItem(this.listOfFragrances.length - 1, 'down');
    }

    @HostListener('window:keydown.arrowup', ['$event']) onKeyArrowUp(event) {
        this.changeCurrentItem(0, 'up');
    }

    changeCurrentItem(extremeIndex: number, direction: string) {
        if (this.focusIndex == extremeIndex) return;

        switch (direction) {
            case 'up':
                this.focusIndex -= 1;
                if (this.focusIndex - this.scrollIndex == -1) {
                    this.scrollIndex -= 1;
                }
                break;
            case 'down':
                this.focusIndex += 1;
                if (this.focusIndex - this.scrollIndex == 4) {
                    this.scrollIndex += 1;
                }
                break;
        }
    }

    ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
        this.focusIndex = 0;
        this.scrollIndex = 0;
    }
}