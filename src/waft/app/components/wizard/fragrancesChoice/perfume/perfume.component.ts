import {Component, Input, Output, EventEmitter} from "@angular/core";

@Component({
    selector: "perfume",
    templateUrl: 'perfume.template.html',
    styleUrls: ['perfume.style.css']
})
export class PerfumeComponent {
    constructor() {}

    @Output() create = new EventEmitter();
    @Output() remove = new EventEmitter();
    @Input() fragrance: any;

    onClickClose(): void {
        this.remove.emit(this.fragrance);
    }

    createYourScent(fragrance: any): void {
        this.create.emit(fragrance);
    }

    public getDisplayName(): string {
        let name = this.fragrance.name.toUpperCase();
        if (this.fragrance.gender == 'unisex') {
            if (name.indexOf("UNISEX")==-1)
                name += " (UNISEX)";
        }
        else if (this.fragrance.gender == 'woman') {
            if (name.indexOf("FOR WOMEN")==-1)
                name += " (FOR WOMEN)";
        }
        else if (this.fragrance.gender == 'man')  {
            if (name.indexOf("FOR MEN")==-1)
                name += " (FOR MEN)";
        }
        return name;
    }
}
