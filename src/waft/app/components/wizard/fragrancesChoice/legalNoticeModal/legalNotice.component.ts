import {Component, Output, EventEmitter} from '@angular/core';
import {WizardService} from '../../../../services/wizard.service';

@Component({
    selector: 'legal-notice',
    templateUrl: 'legalNotice.template.html',
    styleUrls: ['legalNotice.style.css']
})
export class LegalNoticeModalComponent {
    constructor(public wizard: WizardService) {
    }

    @Output() closeModal = new EventEmitter();

    public close() {
        this.closeModal.emit();
    }
}
