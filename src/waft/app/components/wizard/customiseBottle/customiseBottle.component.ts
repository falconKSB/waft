import {Component} from "@angular/core";

import {WizardService} from '../../../services/wizard.service';
import {AuthService} from "../../../services/auth.service";

import {Router} from '@angular/router';

import {window} from '@angular/platform-browser/src/facade/browser';
import {StyleModel} from "../../../models/style.model";
import {GTMService} from "../../../services/gtm.service";


@Component({
    selector: "customise-bottle",
    templateUrl: 'customiseBottle.template.html',
    styleUrls: ['../../shared/forms.style.css', '../../shared/bottle.style.css', '../../shared/bottleFonts.style.css', 'customiseBottle.style.css']
})

export class CustomiseBottleComponent {

    constructor(public wizard: WizardService, private gtm: GTMService) {
        wizard.saveJourney();
        gtm.event('lab_customise_bottle');
        this.styles = StyleModel.getAllStyles();
        this.restoreStyle();
    }

    public isStyleSelection: boolean = false;
    public showPreview: boolean = false;
    public showLoader: boolean = false;
    public styles: any;
    public selectedStyle: any;
    public initialsStyle: any = {
        'text-transform': 'uppercase'
    };

    public clickContinue() {
        if (this.wizard.order.journey.customBottle.name !== '') {
            this.showPreview = false;
            this.gtm.event('lab_customise_bottle_continue');
            this.wizard.putToStorage();
            this.toNextStep();
        }
    }

    public skip() {
        this.gtm.event('lab_customise_bottle_skip');
        this.toNextStep();
    }

    public toNextStep() {
        if (!this.showLoader) {
            this.showLoader = true;
            return;
        }
        if (this.wizard.order.isGiftRedemption)
            this.wizard.toStepFromRoute('/app/wizard/login', true, true);
        else
            this.wizard.toStepFromRoute('/app/wizard/the-new-you', true, true);
    }

    public restoreStyle() {
        for (let s of this.styles) {
            if (s.style == this.wizard.order.journey.customBottle.style) {
                this.selectStyle(s);
                return;
            }
        }
        this.selectStyle(this.styles[0])
    }

    public showStyles() {
        this.isStyleSelection = !this.isStyleSelection;
    }

    public togglePreview() {
        if (this.wizard.order.journey.customBottle.name !== '') {
            this.showPreview = !this.showPreview;
            if (this.showPreview) {
                window.scrollTo(0, 0);
            }
        }
    }

    public selectStyle(style: any) {
        this.selectedStyle = style;
        this.wizard.order.journey.customBottle.style = style.style;
        this.isStyleSelection = false;
    }
}
