import {FormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {ModalModule} from "ng2-modal";

import {ROUTES} from './wizard.routes';

import {LandingComponent} from './landing/landing.component';
import {FragrancesChoiceComponent} from './fragrancesChoice/fragrancesChoice.component';
import {AutocompleteListComponent} from './fragrancesChoice/autocomplete-list/autocomplete-list.component';
import {AskGenderComponent} from './fragranceType/askGender.component';
import {ActivityComponent} from './activity/activity.component';
import {MoodComponent} from './mood/mood.component';
import {PersonalStyleComponent} from './personalstyle/personalstyle.component';
import {TimeOfDayComponent} from './timeOfDay/timeOfDay.component';
import {WaftComponent} from './waft/waft.component';
import {PerfumeComponent} from './fragrancesChoice/perfume/perfume.component';
import {IngredientsComponent} from './ingredients/ingredients.component';
import {IngredientComponent} from './ingredients/ingredient/ingredient.component';
import {CustomiseBottleComponent} from './customiseBottle/customiseBottle.component';
import {LayeringPopupComponent} from './theNewYou/layering/layeringPopup.conponent'
import {SamplesComponent} from './theNewYou/samples/samples.component';
import {TheNewYouComponent} from './theNewYou/theNewYou.component'
import {EmailFormComponent} from './theNewYou/emailForm/emailForm.component'
import {GiftDeliveryComponent} from './giftDelivery/giftDelivery.component';
import {WaftThankYouComponent} from './thankYou/thankYou.component';
import {WizardLoginComponent} from './login/login.component';
import {CheckoutComponent} from './checkout/checkout.component';
import {PaymentComponent} from './payment/payment.component';
import {SharedModule} from '../shared/index';
import {LegalNoticeModalComponent} from './fragrancesChoice/legalNoticeModal/legalNotice.component';
import {JourneyComponent} from './journey/journey.component';
import {OrderDesignComponent} from "./orderDesign/orderDesign.component";
import {OrderTypeComponent} from "./orderType/orderType.component";
import {RecipientGenderComponent} from "./recipientGender/recipientGender.component";

import {PaymentValidationService} from '../../services/paymentValidation.service';
import {SpideRenderingService} from "../../services/spide.service"
import {LayoutModule} from "../layout/index";


@NgModule({
    declarations: [
        // Components / Directives/ Pipes
        LandingComponent,
        FragrancesChoiceComponent,
        LegalNoticeModalComponent,
        AutocompleteListComponent,
        AskGenderComponent,
        ActivityComponent,
        MoodComponent,
        PersonalStyleComponent,
        TimeOfDayComponent,
        WaftComponent,
        PerfumeComponent,
        IngredientComponent,
        IngredientsComponent,
        CustomiseBottleComponent,
        LayeringPopupComponent,
        TheNewYouComponent,
        EmailFormComponent,
        SamplesComponent,
        GiftDeliveryComponent,
        WizardLoginComponent,
        CheckoutComponent,
        PaymentComponent,
        WaftThankYouComponent,
        JourneyComponent,
        OrderTypeComponent,
        OrderDesignComponent,
        RecipientGenderComponent
    ],
    imports: [
        ModalModule,
        FormsModule,
        SharedModule,
        LayoutModule,
        RouterModule.forChild(ROUTES)
    ],
    providers: [PaymentValidationService, SpideRenderingService]
})
export class WizardModule {
}
