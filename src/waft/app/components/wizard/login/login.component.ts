import { Component, ViewChild } from '@angular/core';
import {WizardService} from "../../../services/wizard.service";
import {AuthService} from "../../../services/auth.service";
import {Modal} from "ng2-modal/Modal";
import {GTMService} from "../../../services/gtm.service";
import {KioskService} from "../../../services/kiosk.service";

@Component({
    selector: 'wizard-login',
    templateUrl: 'login.template.html',
    styleUrls: ['../../shared/forms.style.css', '../../shared/login.style.css']
})
export class WizardLoginComponent {
    constructor(public wizard: WizardService, private auth: AuthService, gtm: GTMService, public kiosk: KioskService) {
        gtm.event('lab_login');
        if (!wizard.showLoginOnCheckout()) {
            wizard.toStepFromRoute('/app/wizard/checkout', true, true);
        }
    }

    @ViewChild('forgotPassword') forgotPassword: Modal;

    facebookConnect(){
        this.wizard.enableStep('/app/wizard/checkout');
        this.wizard.putToStorage();
        this.auth.facebookConnect();
    }

    email: string = '';
    password: string = '';
    error: string;

    login() {
        if ((this.email == '') || (this.password == '')) {
            this.error = "Please fill in email and password";
            return;
        }
        this.error = 'Processing...';
        this.auth.login(this.email, this.password).subscribe(
            data => {
                if (data.success) {
                    this.clickContinue();
                } else {
                    this.error = data.message;
                }
            }
        )
    }

    clickContinue() : void {
        this.wizard.next(true);
    }
}
