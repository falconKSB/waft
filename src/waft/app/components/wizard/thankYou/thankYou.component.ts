import { Component } from "@angular/core";
import { Router } from '@angular/router';
import { WizardService } from '../../../services/wizard.service';
import {GTMService} from "../../../services/gtm.service";
import {AuthService} from "../../../services/auth.service";
import {KioskService} from "../../../services/kiosk.service";

@Component({
    selector: "waft-thank-you",
    templateUrl: 'thankYou.template.html',
    styleUrls: ['thankYou.style.css'],
})

export class WaftThankYouComponent {

    constructor(private router: Router, public wizard: WizardService, public auth: AuthService, gtm: GTMService,
        public kiosk: KioskService) {
        if(wizard.order.isGiftRedemption) {
            gtm.event('gift_thank_you_receiver')
        }
        else {
            gtm.purchase('lab_thankyou', wizard.country, wizard.order);
        }
        wizard.resetWizard();
    }

    public newScent() {
        this.wizard.navToStart();
    }

    public newGift() {
        this.router.navigate(['/gift']);
    }

    public onDone() {
        this.auth.logout();
        this.router.navigate(['/app/wizard/type']);
    }
}
