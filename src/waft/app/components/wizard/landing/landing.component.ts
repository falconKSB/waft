import {Component, AfterViewInit, ViewChild, AfterContentInit} from "@angular/core";

import {WizardService} from '../../../services/wizard.service';
import {WizardChoiceComponent} from "../wizard-choice/wizard-choice.component";
import {HostListener} from "@angular/core/src/metadata/directives";
import {GTMService} from "../../../services/gtm.service";
import {Modal} from "ng2-modal/Modal";
import {AuthService} from "../../../services/auth.service";
import {ActivatedRoute} from "@angular/router";
import {GiftService} from "../../../services/gift.service";
import {KioskService} from "../../../services/kiosk.service";

@Component({
    selector: "type",
    templateUrl: 'landing.template.html',
    styleUrls: ['landing.style.css'],
})

export class LandingComponent extends WizardChoiceComponent implements AfterContentInit  {
    constructor(wizard: WizardService, gtm: GTMService, kiosk: KioskService, gift: GiftService,
                route: ActivatedRoute,  auth: AuthService) {
        super(wizard, gtm, 'lab_order_type', kiosk, gift, route, auth);
    }

    @ViewChild('continueModal') continueModal: Modal;

    public ngAfterContentInit(){
        this.firstPage();
        if (this.wizard.canSkipGift()) {
            this.wizard.toStepFromRoute('/app/wizard/order-design', true, true);
            return;
        }
        this.resize();
        if (this.wizard.isEmpty())
            this.wizard.resetWizard();
        else
            this.continueModal.open();
    }

    public saveChoice(choice: string): void {
        this.wizard.order.journey.gift = choice === 'gift';
        this.wizard.toStepFromRoute('/app/wizard/type');
    }

    @HostListener('window:resize', ['$event.target'])
    public windowResize(target: any): void {
        this.resize();
    }
}
