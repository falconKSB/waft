import {Component, HostListener} from '@angular/core';
import {WizardChoiceComponent} from "../wizard-choice/wizard-choice.component";
import {WizardService} from "../../../services/wizard.service";
import {GTMService} from "../../../services/gtm.service";
import {GiftService} from "../../../services/gift.service";
import {AuthService} from "../../../services/auth.service";
import {ActivatedRoute} from "@angular/router";
import {KioskService} from "../../../services/kiosk.service";

@Component({
    selector: 'recipient-gender',
    templateUrl: 'recipientGender.template.html',
    styleUrls: ['recipientGender.style.css']
})

export class RecipientGenderComponent extends WizardChoiceComponent {
    constructor(wizard: WizardService, gtm: GTMService, kiosk: KioskService, gift: GiftService,
                route: ActivatedRoute,  auth: AuthService) {
        super(wizard, gtm, 'lab_recipient_gender', kiosk, gift, route, auth);
    }

    public ngAfterContentInit(){
        this.resize();
    }

    public showBack() {
        return !this.wizard.order.isGiftRedemption;
    }

    saveChoice(choice: string) : void {
        this.wizard.order.journey.recipientGender = choice;
        this.wizard.order.setGender();
        this.gift.order.gender = choice;
        this.gift.putToStorage();
    }

    @HostListener('window:resize', ['$event.target'])
    public windowResize(target: any): void {
        this.resize();
    }
}