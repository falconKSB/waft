import {Component, ViewChild} from '@angular/core';
import {WizardService} from '../../../services/wizard.service';
import {GTMService} from "../../../services/gtm.service";

@Component({
    selector: 'gift-delivery',
    templateUrl: 'giftDelivery.template.html',
    styleUrls: ['../../shared/forms.style.css', 'giftDelivery.style.css']
})
export class GiftDeliveryComponent {
    constructor(public wizard: WizardService, gtm: GTMService) {
        gtm.event('gift_delivery_receiver');
    }

    public onLogin(): void {
        this.wizard.back();
    }

    public save(): void {
        this.wizard.putToStorage();
    }

    public onDone(): void {
        this.wizard.toStepFromRoute('/app/wizard/thank-you', true, true);
    }
}
