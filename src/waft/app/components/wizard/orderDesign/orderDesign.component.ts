import {Component, HostListener} from '@angular/core';
import {WizardChoiceComponent} from "../wizard-choice/wizard-choice.component";
import {GTMService} from "../../../services/gtm.service";
import {WizardService} from "../../../services/wizard.service";
import {GiftService} from "../../../services/gift.service";
import {AuthService} from "../../../services/auth.service";
import {ActivatedRoute} from "@angular/router";
import {KioskService} from "../../../services/kiosk.service";

@Component({
    selector: 'order-design',
    templateUrl: 'orderDesign.template.html',
    styleUrls: ['orderDesign.style.css']
})

export class OrderDesignComponent extends WizardChoiceComponent {
    constructor(wizard: WizardService, gtm: GTMService, kiosk: KioskService, gift: GiftService,
                route: ActivatedRoute,  auth: AuthService) {
        super(wizard, gtm, 'lab_order_design', kiosk, gift, route, auth);
    }

    public ngAfterContentInit(){
        this.resize();
    }

    saveChoice(choice: string) : void {
        this.wizard.isLetThemDesign = choice === 'them';
    }

    @HostListener('window:resize', ['$event.target'])
    public windowResize(target: any): void {
        this.resize();
    }
}