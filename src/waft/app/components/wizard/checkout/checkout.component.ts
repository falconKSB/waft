import {Component, ViewChild} from '@angular/core';
import { NgForm, NgModel} from '@angular/forms';
import {WizardService} from '../../../services/wizard.service';
import {CountryModel} from '../../../models/country.model';
import {AuthService} from "../../../services/auth.service";
import {GTMService} from "../../../services/gtm.service";
import {RegistrationModel} from "../../../models/registration.model";


@Component({
    selector: 'checkout',
    templateUrl: 'checkout.template.html',
    styleUrls: ['checkout.style.css']
})
export class CheckoutComponent {
    constructor(public wizard: WizardService, public auth: AuthService, gtm: GTMService) {
        if (wizard.selectAlternativeCheckout()) return;
        gtm.checkout('lab_delivery', wizard.country, 'lab', wizard.order.orderSize);
        wizard.saveJourney();
        if (auth.isAuth()) {
            let user = auth.getCurrentUser();
            this.wizard.order.customPackaging.firstName = user.firstName;
            if (wizard.order.delivery.fullName === '')
                wizard.order.delivery.fullName = user.firstName + ' ' + user.lastName;
        }
    }

    public onCountryChange(countryCode) : void {
        this.wizard.initCountry(countryCode, true);
    }

    public onLogin(): void {
        this.wizard.back();
    }

    public save(): void {
        this.wizard.putToStorage();
    }

    public onDone(): void {
        this.wizard.putToStorage();
        this.wizard.toStepFromRoute('/app/wizard/payment', true, true);
    }
}
