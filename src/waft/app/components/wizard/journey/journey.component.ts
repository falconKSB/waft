import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';

import {WizardService} from '../../../services/wizard.service';
import {WaftAPIService} from "../../../services/waftapi.service";

@Component({
    selector: 'journey.component',
    template: ''
})

export class JourneyComponent implements OnInit {
    constructor(private wizard: WizardService,
                private route: ActivatedRoute,
                private api: WaftAPIService) {
    }

    reset() {
        this.wizard.resetWizard();
        this.wizard.navToStart();
    }

    ngOnInit() {
        if (!this.route.snapshot.params['id']) {
            return this.reset();
        }
        this.api.getJourney(this.route.snapshot.params['id']).subscribe(
            journey => {
                if (journey.status === 'Completed')
                    this.reset();
                else
                    this.wizard.restoreWizard(journey);
            },
            error => this.reset());
    }
}