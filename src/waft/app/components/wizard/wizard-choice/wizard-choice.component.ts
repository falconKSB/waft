import {WizardService} from "../../../services/wizard.service";
// import * as $ from 'jquery';
import {AfterContentInit} from "@angular/core";
import {GTMService} from "../../../services/gtm.service";
import {OrderModel} from "../../../models/order.model";
import {KioskService} from "../../../services/kiosk.service";
import {ActivatedRoute} from "@angular/router";
import {AuthService} from "../../../services/auth.service";
import {GiftService} from "../../../services/gift.service";

declare var $: any;

export abstract class WizardChoiceComponent implements AfterContentInit {
    constructor(protected wizard: WizardService, protected gtm: GTMService, screenName: string,
                protected kiosk: KioskService, protected gift: GiftService,
                protected route: ActivatedRoute, protected auth: AuthService) {
        gtm.event(screenName);
    }

    public ngAfterContentInit() {
        this.resize();
    }

    abstract saveChoice(choice: string): void;

    public toNextStep(currentChoice: string): void {
        this.saveChoice(currentChoice);
        this.wizard.putToStorage();
        this.wizard.next(true);
    }

    public isGift() {
        return this.wizard.order.journey.gift
    }

    protected resize(): void {
        // TODO: rewrite to native angular 2
        let headerHeight = 122; // before - 115

        $('.modal-tb').width($(window).width()).height($(window).height());

        $('.b1').height($(window).height() - 73);

        $('.dc1,.dc2').height(($(window).height() - 73 * 2) / 2).width($(window).width());

        $('.b2 .ll2 .df').each(function () {
            $(this).attr('style', '').css({'min-height': $(this).parent().parent().height() - 69});
        });

        $('.line1').each(function () {
            $(this).height($(this).parent().find('li:last-child').position().top + 2);
        });
        $('.page-ship').width($(window).width()).height($(window).height() - headerHeight - 89).css({'min-height': $('.page-ship .ct').innerHeight()});

        $('.dc-poss1').width($(window).width()).height($(window).height() - headerHeight).css({'min-height': $('.dc-poss1 .jp').innerHeight()});

        $('.bl-sld').height($(window).height() - headerHeight);

        // if ($(window).width() > 1024) {
        $('.bl-sld .dc').height($(window).height() - headerHeight).width($('.bl-sld .item1').width());
        // }

        if ($(window).width() <= 1024) {
            $('.pd1 .bl-sld .item1.j4 .ov1').css('top', (($(window).height() - headerHeight) / 4 - 100) / 2);
            $('.pd1 .bl-sld .item1.j3 .ov1').css('top', (($(window).height() - headerHeight) / 3 - 100) / 2);
            $('.pd1 .bl-sld .item1.j2 .ov1').css('top', (($(window).height() - headerHeight) / 2 - 100) / 2);
        }
    }

    protected firstPage() : void {
        if (this.kiosk.isEnabled()) {
            this.wizard.resetWizard();
            this.auth.logout();
        }
        this.wizard.initParams(this.route.snapshot.queryParams);
    }
}
