import { Component } from "@angular/core";
import { WizardService } from '../../../services/wizard.service';
import {HostListener} from "@angular/core/src/metadata/directives";
import {WizardChoiceComponent} from "../wizard-choice/wizard-choice.component";
import {GTMService} from "../../../services/gtm.service";
import {GiftService} from "../../../services/gift.service";
import {AuthService} from "../../../services/auth.service";
import {ActivatedRoute} from "@angular/router";
import {KioskService} from "../../../services/kiosk.service";


@Component({
    selector: "activity",
    templateUrl: 'activity.template.html',
    styleUrls: ['activity.style.css'],
})
export class ActivityComponent extends WizardChoiceComponent {
    constructor(wizard: WizardService, gtm: GTMService, kiosk: KioskService, gift: GiftService,
                route: ActivatedRoute,  auth: AuthService) {
        super(wizard, gtm, 'lab_activity', kiosk, gift, route, auth);
    }

    public ngAfterContentInit(){
        this.resize();
    }

    saveChoice(choice: string) : void {
        this.wizard.order.journey.activity = choice;
    }

    @HostListener('window:resize', ['$event.target'])
    public windowResize(target: any): void {
        this.resize();
    }
}
