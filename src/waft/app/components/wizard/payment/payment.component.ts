import {Component} from '@angular/core';

import {WizardService} from '../../../services/wizard.service';
import {GTMService} from "../../../services/gtm.service";

@Component({
    selector: 'payment',
    templateUrl: 'payment.template.html',
    styleUrls: ['payment.style.css']
})
export class PaymentComponent {
    constructor(public wizard: WizardService, gtm: GTMService) {
        gtm.checkout('lab_payment', wizard.country, 'lab', wizard.order.orderSize, 2);
        gtm.trackReferralRock(wizard.order.id);
    }

    public onDone() {
        this.wizard.toStepFromRoute('/app/wizard/thank-you', true, true);
    }

    public save(): void {
        this.wizard.putToStorage();
    }
}
