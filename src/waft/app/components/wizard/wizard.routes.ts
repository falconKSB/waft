import {Routes} from '@angular/router';

import {AuthGuard} from '../../guards/auth.guard';

import {LandingComponent} from './landing/landing.component';
import {FragrancesChoiceComponent} from './fragrancesChoice/fragrancesChoice.component';
import {AskGenderComponent} from './fragranceType/askGender.component';
import {TimeOfDayComponent} from './timeOfDay/timeOfDay.component';
import {ActivityComponent} from './activity/activity.component';
import {MoodComponent} from './mood/mood.component';
import {PersonalStyleComponent} from './personalstyle/personalstyle.component';
import {WaftComponent} from './waft/waft.component';
import {IngredientsComponent} from './ingredients/ingredients.component';
import {CustomiseBottleComponent} from './customiseBottle/customiseBottle.component';
import {GiftDeliveryComponent} from './giftDelivery/giftDelivery.component';
import {WizardLoginComponent} from './login/login.component';
import {PaymentComponent} from './payment/payment.component';
import {CheckoutComponent} from './checkout/checkout.component';
import {WaftThankYouComponent} from './thankYou/thankYou.component';
import {JourneyComponent} from './journey/journey.component';
import {TheNewYouComponent} from "./theNewYou/theNewYou.component";
import {OrderTypeComponent} from "./orderType/orderType.component";
import {OrderDesignComponent} from "./orderDesign/orderDesign.component";
import {RecipientGenderComponent} from "./recipientGender/recipientGender.component";
import {WizardGuard} from "../../guards/wizard.guard";

export const ROUTES: Routes = [
    { path: 'app/wizard', children: [
        {path: '', pathMatch: 'full', redirectTo: '/type'},
        {path: 'landing', component: LandingComponent},
        {path: 'type', component: OrderTypeComponent},
        {path: 'order-design', component: OrderDesignComponent, canActivate: [WizardGuard]},
        {path: 'gender', component: RecipientGenderComponent, canActivate: [WizardGuard]},
        {path: 'fragrance-type', component: AskGenderComponent, canActivate: [WizardGuard]},
        {path: 'time-of-day', component: TimeOfDayComponent, canActivate: [WizardGuard]},
        {path: 'activity', component: ActivityComponent, canActivate: [WizardGuard]},
        {path: 'mood', component: MoodComponent, canActivate: [WizardGuard]},
        {path: 'personalstyle', component: PersonalStyleComponent, canActivate: [WizardGuard]},
        {path: 'waft', component: WaftComponent, canActivate: [WizardGuard]},
        {path: 'ingredients', component: IngredientsComponent, canActivate: [WizardGuard]},
        {path: 'fragrances-choice', component: FragrancesChoiceComponent, canActivate: [WizardGuard]},
        {path: 'customise-bottle', component: CustomiseBottleComponent, canActivate: [WizardGuard]},
        {path: 'the-new-you', component: TheNewYouComponent, canActivate: [WizardGuard]},
        {path: 'the-new-you3', component: TheNewYouComponent, canActivate: [WizardGuard]},
        {path: 'login', component: WizardLoginComponent},
        {path: 'checkout', component: CheckoutComponent, canActivate: [WizardGuard]},
        {path: 'gift-delivery', component: GiftDeliveryComponent, canActivate: [WizardGuard]},
        {path: 'payment', component: PaymentComponent, canActivate: [WizardGuard, AuthGuard]},
        {path: 'thank-you', component: WaftThankYouComponent, canActivate: [WizardGuard, AuthGuard]},
        {path: 'journey/:id', component: JourneyComponent},
        {path: '**', redirectTo: 'type'},
    ]}
];
