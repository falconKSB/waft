import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import {NgForm} from "@angular/forms";
import {AuthService} from '../../../../services/auth.service';
import {WizardService} from '../../../../services/wizard.service';

@Component({
    selector: 'email-form',
    templateUrl: 'emailForm.template.html',
    styleUrls:['../../../shared/forms.style.css','emailForm.style.css']
})

export class EmailFormComponent {
    constructor(public auth: AuthService, public wizard: WizardService) {
    }

    public showErrors: boolean = false;

    @Output() closeModal = new EventEmitter();
    @ViewChild('emailForm') emailForm: NgForm;

    saveEmail() {
        this.showErrors = true;
        if (this.emailForm.valid) {
            this.wizard.saveJourney();
            this.closeModal.emit();
        }
    }

}