import {Component, ViewChild, AfterViewInit} from '@angular/core';

import {Modal} from "ng2-modal/Modal";

import {WizardService} from '../../../services/wizard.service';
import {GTMService} from "../../../services/gtm.service";
import {WaftAPIService} from "../../../services/waftapi.service";
import {LocalStorageService} from '../../../services/localStorage.service';
import {AuthService} from '../../../services/auth.service';
import {ABTestService} from "../../../services/abtest.service";
import {KioskService} from "../../../services/kiosk.service";

declare var $: any;

@Component({
    selector: 'the-new-you',
    templateUrl: 'theNewYou.template.html',
    styleUrls: ['theNewYou.style.css']
})
export class TheNewYouComponent implements AfterViewInit {
    @ViewChild('emailFormModal') emailFormModal: Modal;
    @ViewChild('samplesModal') samplesModal: Modal;
    public accords: any = null;
    public scentCode: number = null;
    public description: string = null;

    constructor(public wizard: WizardService, public storage: LocalStorageService,
                private gtm: GTMService, api: WaftAPIService, private auth: AuthService,
                public abtest: ABTestService, public kiosk: KioskService) {
        wizard.saveJourney();
        let redirectUrl = this.storage.get('redirectUrl','/app/wizard/type');
        if (this.auth.isAuth() && redirectUrl=='/app/wizard/the-new-you') {
            this.storage.set('emailForm','hide')
        }

        gtm.productDetail('lab_say_hello', wizard.country, 'lab', [wizard.order.getMainOrderSize()]);
        api.matchScent(wizard.order.journey).subscribe(
            data => {
                this.accords = data.main.accords;
                this.scentCode = data.main.code;
                this.description = data.main.description;

                if (this.kiosk.isEnabled())
                    this.wizard.order.preferredSample = this.scentCode.toString();
            },
            error => {});
    }

    isShowEmailForm() : boolean {
        return this.abtest.isABVariation("NewYouEmailForm") && (this.storage.get('emailForm', 'show') == 'show')
            && (!this.auth.isAuth()) && ((!this.wizard.order.journey.email) || (this.wizard.order.journey.email === ''));
    }

    showPerfumeDetails(): boolean {
        return this.kiosk.isEnabled()
    }

    closeEmailForm() {
        this.emailFormModal.close();
        this.storage.set('emailForm', 'hide');
    }

    public allowSamples(): boolean {
        return (!this.abtest.isABVariation('NoSamples')) && (!this.kiosk.isEnabled()) &&
            this.wizard.country.price && (this.wizard.country.price.samples) &&
            (this.wizard.country.price.samples !== null);
    }

    ngAfterViewInit() {
        if(this.isShowEmailForm())
            this.emailFormModal.open();
        let $wrapper = $("#wrapper");
        let $window = $(window);
        window.scrollTo(0, 0);
        let $bottleInnerContent = $(".bottle-animation .inner-content");

        calculateSteps();
        $window.on('scroll', calculateSteps);

        function calculateSteps() {
            let scrollPosition = $(this).scrollTop();
            if ($window.width() > 992) {
                // setClasses(['step1', 'step2', 'step3', 'step4']);
                let offsetTop = $(".first-screen").height();

                if (scrollPosition >= offsetTop) {
                    $bottleInnerContent.addClass("fixed");
                    $wrapper.attr("data-fixed", "true");
                }
                else {
                    $wrapper.attr("data-fixed", "false");
                    $bottleInnerContent.removeClass("fixed");
                    $bottleInnerContent.removeClass("show");
                }

                if (scrollPosition > offsetTop && scrollPosition <= 120 + offsetTop) {
                    setClasses(['step1']);
                }
                else if (scrollPosition > 120 + offsetTop && scrollPosition <= 189 + offsetTop) {
                    setClasses(['step1', 'step2']);
                    $bottleInnerContent.removeClass("show");
                }
                else if (scrollPosition > 189 + offsetTop && scrollPosition <= 510 + offsetTop) {
                    setClasses(['step1', 'step2', 'step3']);
                    $bottleInnerContent.removeClass("fixed-bottom");
                }
                else if (scrollPosition > 510 + offsetTop && scrollPosition <= 542 + offsetTop) {
                    setClasses(['step1', 'step2', 'step3', 'step4']);
                    $bottleInnerContent.addClass("show");
                }
                else if (scrollPosition > 542 + offsetTop) {
                    setClasses(['step1', 'step2', 'step3', 'step4', 'fixed-bottom']);
                    $bottleInnerContent.addClass("fixed-bottom show");
                }
                else {
                    $wrapper.removeClass();
                    $bottleInnerContent.removeClass("fixed-bottom");
                }

            } else {

                let offsetTop = $(".first-screen").height() - 173;
                //mobile----------------------------------------------------------------------------------
                if (scrollPosition >= offsetTop) {
                    $wrapper.addClass("fixed");
                    $bottleInnerContent.addClass("fixed");
                }
                else {
                    $bottleInnerContent.removeClass("fixed");
                    $wrapper.removeClass("fixed");
                    $wrapper.attr("data-fixed", "false");
                }

                if (scrollPosition > offsetTop && scrollPosition <= 97 + offsetTop) {
                    setClasses(['fixed', 'step1']);
                }
                else if (scrollPosition > 97 + offsetTop && scrollPosition <= 191 + offsetTop) {
                    setClasses(['fixed', 'step1', 'step2']);
                }
                else if (scrollPosition > 191 + offsetTop && scrollPosition <= 427 + offsetTop) {
                    setClasses(['fixed', 'step1', 'step2', 'step3']);
                }
                else if (scrollPosition > 427 + offsetTop && scrollPosition <= 525 + offsetTop) {
                    setClasses(['fixed', 'step1', 'step2', 'step3', 'step4']);
                    $(".mobile-inner#mi1").removeClass("show");
                }
                else if (scrollPosition > 525 + offsetTop && scrollPosition <= 830 + offsetTop) {
                    $(".mobile-inner#mi1").addClass("show");
                    $(".mobile-inner#mi2").removeClass("show");
                }
                else if (scrollPosition > 830 + offsetTop && scrollPosition <= 880 + offsetTop) {
                    $(".mobile-inner#mi2").addClass("show");
                }
                else if (scrollPosition > 880 + offsetTop && scrollPosition <= 925 + offsetTop) {
                    setClasses(['fixed', 'step1', 'step2', 'step3', 'step4', 'step5']);
                }
                else if (scrollPosition > 925 + offsetTop) {
                    setClasses(['fixed', 'step1', 'step2', 'step3', 'step4', 'step5', 'step6']);
                    $(".mobile-inner#mi1").addClass("show");
                    $(".mobile-inner#mi2").addClass("show");
                }
                else {
                    $wrapper.removeClass();
                    $bottleInnerContent.removeClass("fixed-bottom");
                }
            }
        }

        function setClasses(classes: Array<string>) {
            let existingClasses : Array<string> = $wrapper.attr('class').split(' ');

            existingClasses.forEach(function(item) {
                if (classes.indexOf(item) < 0){
                    $wrapper.removeClass(item);
                }
            });

            classes.forEach(function(item){
                if (existingClasses.indexOf(item) < 0) {
                    $wrapper.addClass(item);
                }
            })
        }
    }
}
