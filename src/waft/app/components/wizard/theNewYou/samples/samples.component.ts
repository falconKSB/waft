import {Component, EventEmitter, Output} from '@angular/core';
import {WizardService} from '../../../../services/wizard.service';
import {GTMService} from "../../../../services/gtm.service";

@Component({
    selector: 'samples',
    templateUrl: 'samples.template.html',
    styleUrls: ['samples.style.css']
})
export class SamplesComponent {
    constructor(public wizard: WizardService, gtm: GTMService) {
        gtm.productDetail('lab_say_hello_select', this.wizard.country, 'lab', ['samples']);
    }

    @Output() onSelectSamples = new EventEmitter();
    @Output() onSelectMain = new EventEmitter();

    @Output() closeModal = new EventEmitter();

    public close() {
        this.closeModal.emit();
    }
}
