/**
 * Created by alexl on 08.12.2016.
 */
import {Component, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'layering-popup',
    templateUrl: 'layeringPopup.template.html',
    styleUrls: ['layeringPopup.style.css']
})
export class LayeringPopupComponent implements OnInit {
    constructor() {
    }

    ngOnInit() {
    }

    @Output() closeModal = new EventEmitter();

    public close() {
        this.closeModal.emit();
    }

}