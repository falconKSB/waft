import {AfterContentInit, Component, HostListener, ViewChild} from '@angular/core';
import {Modal} from "ng2-modal/Modal";

import {WizardChoiceComponent} from "../wizard-choice/wizard-choice.component";
import {WizardService} from "../../../services/wizard.service";
import {GTMService} from "../../../services/gtm.service";
import {GiftService} from "../../../services/gift.service";
import {AuthService} from "../../../services/auth.service";
import {ActivatedRoute} from "@angular/router";
import {KioskService} from "../../../services/kiosk.service";

@Component({
    selector: 'order-type',
    templateUrl: 'orderType.template.html',
    styleUrls: ['orderType.style.css']
})

export class OrderTypeComponent extends WizardChoiceComponent implements AfterContentInit {
    @ViewChild('continueModal') continueModal: Modal;

    constructor(wizard: WizardService, gtm: GTMService, kiosk: KioskService, gift: GiftService,
                route: ActivatedRoute,  auth: AuthService) {
        super(wizard, gtm, 'lab_order_type', kiosk, gift, route, auth);
    }

    public ngAfterContentInit(){
        this.firstPage();
        if (this.wizard.canSkipGift()) {
            this.wizard.toStepFromRoute('/app/wizard/order-design', true, true);
            return;
        }
        this.resize();
        if (this.wizard.isEmpty())
            this.wizard.resetWizard();
        else
            this.continueModal.open();
    }

    saveChoice(choice: string) : void {
        this.wizard.order.journey.gift = choice === 'gift';
    }

    @HostListener('window:resize', ['$event.target'])
    public windowResize(target: any): void {
        this.resize();
    }
}