import {Component, AfterViewInit} from "@angular/core";

import {WizardService} from '../../../services/wizard.service';
import {WizardChoiceComponent} from "../wizard-choice/wizard-choice.component";
import {HostListener} from "@angular/core/src/metadata/directives";
import {GTMService} from "../../../services/gtm.service";
import {GiftService} from "../../../services/gift.service";
import {AuthService} from "../../../services/auth.service";
import {ActivatedRoute} from "@angular/router";
import {KioskService} from "../../../services/kiosk.service";

@Component({
    selector: "type",
    templateUrl: 'askGender.template.html',
    styleUrls: ['askGender.style.css'],
})

export class AskGenderComponent extends WizardChoiceComponent implements AfterViewInit {
    constructor(wizard: WizardService, gtm: GTMService, kiosk: KioskService, gift: GiftService,
                route: ActivatedRoute,  auth: AuthService) {
        super(wizard, gtm, 'lab_gender', kiosk, gift, route, auth);
    }

    public ngAfterViewInit() {
        this.resize();
    }

    public saveChoice(choice: string): void {
        this.wizard.order.journey.gender = choice;
        this.wizard.order.setGender();
    }

    @HostListener('window:resize', ['$event.target'])
    public windowResize(target: any): void {
        this.resize();
    }
}
