import { Component } from "@angular/core";

import { WizardService } from '../../../services/wizard.service';
import {WizardChoiceComponent} from "../wizard-choice/wizard-choice.component";
import {HostListener} from "@angular/core/src/metadata/directives";
import {GTMService} from "../../../services/gtm.service";
import {GiftService} from "../../../services/gift.service";
import {AuthService} from "../../../services/auth.service";
import {ActivatedRoute} from "@angular/router";
import {KioskService} from "../../../services/kiosk.service";

@Component({
    selector: "mood",
    templateUrl: 'mood.template.html',
    styleUrls: ['mood.style.css'],
})

export class MoodComponent extends WizardChoiceComponent {
    constructor(wizard: WizardService, gtm: GTMService, kiosk: KioskService, gift: GiftService,
                route: ActivatedRoute,  auth: AuthService) {
        super(wizard, gtm, 'lab_mood', kiosk, gift, route, auth);
    }

    public ngAfterContentInit(){
        this.resize();
    }

    saveChoice(choice: string) : void {
        this.wizard.order.journey.mood = choice;
    }

    @HostListener('window:resize', ['$event.target'])
    public windowResize(target: any): void {
        this.resize();
    }
}
