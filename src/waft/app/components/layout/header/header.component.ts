import {Component, Input,} from "@angular/core";

import { WizardService } from '../../../services/wizard.service';
import { AuthService } from "../../../services/auth.service";

import { Router } from '@angular/router';
import {GiftService} from "../../../services/gift.service";

@Component({
    selector: "waft-header",
    styleUrls: ['header.style.css'],
    templateUrl: 'header.template.html'
})
export class WaftHeaderComponent {
    @Input() type: string = "wizard";
    @Input() page: string = "";
    @Input() stepName: string;
    @Input() stepDescription: string;
    @Input() showBack: boolean = true;
    @Input() scentCode: number = null;
    public mobileMenuOpen: boolean = false;

    constructor(public gift: GiftService, public wizard: WizardService, private router: Router,
                public auth: AuthService) {
        wizard.toStepFromRoute();
    }


    private logout() {
        this.gift.reset();
        this.wizard.resetWizard();
        this.auth.logout();
        (<any>window).location.href = '/';
    }
}
