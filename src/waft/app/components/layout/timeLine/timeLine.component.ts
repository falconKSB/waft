import {Component, Input} from "@angular/core";

@Component({
    selector: "waft-timeline",
    templateUrl: 'timeLine.template.html',
    styleUrls: [ 'timeLine.style.css' ]
})

export class TimeLineComponent {
    @Input() step: number;
}
