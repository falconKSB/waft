import {NgModule} from '@angular/core';
import {BrowserModule} from "@angular/platform-browser";

import {SharedModule} from "../shared/index";


import {TimeLineComponent} from './timeLine/timeLine.component';
import {WaftHeaderComponent} from "./header/header.component";
import {FooterComponent} from "./footer/footer.component";
import {RouterModule} from "@angular/router";


@NgModule({
    imports: [
        BrowserModule,
        SharedModule,
        RouterModule
    ],
    declarations: [
        TimeLineComponent, WaftHeaderComponent, FooterComponent
    ],
    exports: [
        TimeLineComponent, WaftHeaderComponent, FooterComponent
    ]
})
export class LayoutModule {
}
