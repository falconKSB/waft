import { Component } from "@angular/core";
import {KioskService} from "../../../services/kiosk.service";

@Component({
    selector: "waft-footer",
    styleUrls: [ './footer.style.css' ],
    templateUrl: './footer.template.html'
})
export class FooterComponent {
    constructor(public kiosk: KioskService) {
    }
}
