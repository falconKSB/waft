import { Component } from '@angular/core';
import {AuthService} from "../../../services/auth.service";
import {ProfileService} from "../../../services/profile.service";

declare var $: any;

@Component({
    selector: 'referral',
    templateUrl: 'referral.template.html',
    styleUrls: ['referral.style.css']
})
export class ReferralComponent {
    currentLoadedIframe : number = 0;

    constructor(private auth: AuthService, public profile: ProfileService) {
        this.init();
    }

    private init() {
        this.profile.setCurrentUser();
    }

    public getRR1URL(n: number) : string {
        return this.getRRURL('https://waft.referralrock.com/access/?programidentifier=81123e63-a327-4541-acf4-3dc2169f0702&view=iframe', n)
    }

    public getRR2URL(n: number) : string {
        return this.getRRURL('https://waft.referralrock.com/access/?programidentifier=d8b2b31a-db44-4109-ac40-984033afe623&view=iframe', n)
    }

    private getRRURL(base: string, n: number) : string {
        if (n > this.currentLoadedIframe + 1) return 'about:blank';
        let user = this.auth.getCurrentUser();
        if (user == null) return 'about:blank';
        return base + "&fullname=" + encodeURIComponent(user.firstName + ' ' + user.lastName)
            + '&email=' + encodeURIComponent(user.email)
    }

    public rflSectionOpen(section: string): void {
        $(section).toggleClass("rfl-acc-open");
        $(section).find("iframe").slideToggle();
    }

    public rflSectionClose(section: string): void {
        $(section).removeClass("rfl-acc-open");
        $(section).find("iframe").slideUp();
    }

    public onLoad(n: number) {
        this.currentLoadedIframe = n;
    }
}