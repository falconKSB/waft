import {Component, OnInit, Input} from '@angular/core';
//import {JourneyModel} from '../../../models/journey.model';
import {ProfileService} from '../../../services/profile.service';

@Component({
    selector: 'journey-card',
    templateUrl: 'journeyCard.template.html',
    styleUrls: ['journeyCard.style.css']
})
export class JourneyCardComponent implements OnInit {
    constructor(public profile: ProfileService) {
    }

    @Input('journey') journey: any;

    ngOnInit() {}
}