import {FragranceCardModel} from '../../../../models/fragranceCard.model';
import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {ProfileService} from '../../../../services/profile.service';

@Component({
    selector: 'change-primary-fragrance',
    templateUrl: 'changePrimaryFragrance.template.html',
    styleUrls: ['changePrimaryFragrance.style.css']
})
export class ChangePrimaryFragranceComponent implements OnInit {
    constructor(public profile: ProfileService) {
    }

    @Output() onClose = new EventEmitter();

    public close() {
        this.onClose.emit();
    }

    public save() {
        this.profile.savePrimary().subscribe(
            data => {
                this.close();
            })
    }

    ngOnInit() {
    }

}