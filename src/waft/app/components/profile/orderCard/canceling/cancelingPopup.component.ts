import {Component, OnInit, Output, EventEmitter} from '@angular/core';

const cancelQuotes = [
    {
        title: '1. I don’t like any of the fragrances sent to me, because',
        reasons: [
            {
                value: 'I do not smell the ingredients I chose',
                checked: false
            },
            {
                value: 'The fragrances are too feminine',
                checked: false
            },
            {
                value: 'The fragrances are too masculine',
                checked: false
            },
            {
                value: 'The fragrances are too strong',
                checked: false
            },
            {
                value: 'The fragrances are too weak',
                checked: false
            }]
    },
    {
        title: '2. I never had the intention to order the full sized as it is',
        reasons: [
            {
                value: 'Too big',
                checked: false
            },
            {
                value: 'Too expensive',
                checked: false
            }]
    },
    {
        title: '3. Packaging',
        reasons: [
            {
                value: 'I don’t like the box',
                checked: false
            },
            {
                value: 'I don’t like the bottle',
                checked: false
            },
            {
                value: 'I don’t like the fragrance card',
                checked: false
            }]
    },
    {
        title: '4. Layering',
        reasons: [
            {
                value: 'I don’t understand the concept of layering and combining the 3 fragrances',
                checked: false
            },
            {
                value: 'The suggested layering didn’t work for me',
                checked: false
            }]
    },
    {
        title: '5. Shipping',
        reasons: [
            {
                value: 'Shipping was too slow',
                checked: false
            },
            {
                value: 'My goods were damaged',
                checked: false
            }]
    },
    {
        title: '6. Customer Support',
        reasons: [
            {
                value: 'My questions were not answered to my satisfaction',
                checked: false
            }]
    }
]

@Component({
    selector: 'canceling-popup',
    templateUrl: 'cancelingPopup.template.html',
    styleUrls: ['cancelingPopup.style.css']
})

export class CancelingPopupComponent implements OnInit {
    constructor() {
    }

    @Output() cancelOrder = new EventEmitter();
    reasonsResult: Array<string> = [];
    quotes: any = cancelQuotes;
    otherReason: string = "";

    public message = '';

    cancel() {
        if (this.otherReason != "")
            this.reasonsResult.push(this.otherReason);
        if (this.reasonsResult.length == 0) {
            this.message = 'Please provide at least one answer, this helps us to improve the service';
            return;
        }
        this.message = '';
        this.cancelOrder.emit(this.reasonsResult);
    }

    chooseReason(reason: string) {
        let index = this.reasonsResult.indexOf(reason);
        if (index == -1)
            this.reasonsResult.push(reason);
        else
            this.reasonsResult.splice(index, 1);
    }

    samplesAgreed: boolean = false;

    ngOnInit() {
    }

    @Output() closeModal = new EventEmitter();

    public close() {
        this.reasonsResult = [];
        for (let quote of this.quotes)
            for (let reason of quote.reasons)
                reason.checked = false;

        this.closeModal.emit();
    }

}