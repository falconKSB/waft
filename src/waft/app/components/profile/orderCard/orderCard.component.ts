import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ProfileService} from '../../../services/profile.service';
import {Modal} from "ng2-modal/Modal";
import {AuthHttp} from "angular2-jwt";
import {Response} from "@angular/http";

@Component({
    selector: 'order-card',
    templateUrl: 'orderCard.template.html',
    styleUrls: ['orderCard.style.css']
})
export class OrderCardComponent implements OnInit {
    @Input('order') order: any;
    isProcessing: boolean = false;
    linkForGift: string = '';
    isCopied: boolean = false;

    @ViewChild('changePrimaryFragranceModal') changePrimaryModal: Modal;
    @ViewChild('cancelModal') cancelModal: Modal;

    constructor(public profile: ProfileService, private http: AuthHttp) {
    }

    ngOnInit() {
        if (this.order.status=='Gift Invitation') {
            this.linkForGift = this.getLinkForGif();
        }
    }

    public canChangeSample() : boolean {
        return (this.order.samplesStatus =='Pending');
    }

    changePrimary(sample: any) {
        if (!this.canChangeSample()) return;
        this.profile.activeSample = sample;
        this.openModal();
    }

    closeModal() {
        this.profile.getSamples();
        this.changePrimaryModal.close();
    }

    openModal() {
        this.profile.initFragrancesCards();
        this.changePrimaryModal.open();
    }

    private extractData(res: Response) {
        let body = res.json();
        return body.data || {};
    }


    // TODO move all these methods to API service
    expediteOrder(orderId: string) {
        if (!this.canChangeSample()) return;
        if (!(<any> window).confirm("You are about to expedite your main order. Your card will be charged full order price. Press OK to proceed.")) return;
        this.isProcessing = true;
        return this.http.post("/api/profile/orders/" + orderId + "/expediteTrial/", {})
            .map(this.extractData)
            .subscribe(
                data => {
                    this.isProcessing = false;
                    this.profile.getSamples();
                    this.profile.getOrders();
                },
                error => {
                    this.isProcessing = false;
                });
    }

    cancelOrder(orderId: string, reasonsCancel: any) {
        if (!this.canChangeSample()) return;
        if (!(<any> window).confirm("You are about to cancel your main order. Press OK to proceed.")) return;
        return this.http.post("/api/profile/orders/" + orderId + "/cancelTrial/", {reasonsCancel: reasonsCancel})
            .map(this.extractData)
            .subscribe(
                data => {
                    this.isProcessing = false;
                    this.profile.getSamples();
                },
                error => {
                    this.isProcessing = false;
                });
    }

    public isGreen(status: string): boolean {
        switch (status) {
            case 'Received':
                return true;
            case 'Delivered':
                return true;
            case 'Refunded':
                return true;
            default:
                return false;
        }

    }

    getTrackLink() {
        if (this.order.shippingInfo && this.order.shippingInfo.trackingNumber)
            return 'http://track.waft.com/' + this.order.shippingInfo.trackingNumber;
        else return '';
    }

    getBottleSize(bottle) {

        switch (bottle) {
            case 'main':
                if (this.order.orderSize == 'samples') return '1x5ml';
                if (this.order.orderSize == 'medium') return '1x50ml';
                else return '1x100ml';

            case 'secondary':
                return this.order.orderSize == 'samples' ? '1x5ml' : '1x15ml';
        }
    }

    getScentType(scent) : string {
        if (!(this.order.journey && this.order.journey.scents)) return '';
        let scents = this.order.journey.scents;
        switch (scent) {
            case 'add1':
                if (scents.add1)
                    return scents.add1.addType;
                else return '';
            case 'add2':
                if (scents.add2)
                    return scents.add2.addType;
                else return '';
        }
        return '';
    }

    getScentCode(scent) : string {
        if (!(this.order.journey && this.order.journey.scents)) return '';
        let scents = this.order.journey.scents;
        switch (scent) {
            case 'main':
                if (scents.main)
                    return this.order.journey.customBottle.initials + scents.main.code;
                else return '';
            case 'add1':
                if (scents.add1 && scents.add1.scent)
                    return this.order.journey.customBottle.initials + scents.add1.scent.code;
                else return '';
            case 'add2':
                if (scents.add2 && scents.add2.scent)
                    return this.order.journey.customBottle.initials + scents.add2.scent.code;
                else return '';
        }
    }

    public onCopy() {
        this.isCopied = true;

        setTimeout(() => {
            this.isCopied = false;
        }, 2000);
    }

    private getLinkForGif() : string {
        let url = (<any>window).location.origin;
        return url + "/app/gift/receive/" + this.order._id
    }
}