import {Component} from '@angular/core';
import {ProfileService} from '../../../services/profile.service';

@Component({
    selector: 'my-orders',
    templateUrl: './myOrders.template.html',
    styleUrls: ['./myOrders.style.css']
})

export class MyOrdersComponent {
    constructor(public profile: ProfileService) {
        this.profile.setCurrentUser();
        this.profile.getOrders();
        this.profile.getSamples();
    }
}