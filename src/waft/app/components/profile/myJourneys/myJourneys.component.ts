import { Component } from '@angular/core';
import {ProfileService} from "../../../services/profile.service";

@Component({
    selector: 'my-journeys',
    templateUrl: './myJourneys.template.html',
    styleUrls: ['./myJourneys.style.css']
})
export class MyJourneysComponent {
    constructor(public profile: ProfileService) {
        this.profile.setCurrentUser();
        this.profile.getJourney();
    }
}