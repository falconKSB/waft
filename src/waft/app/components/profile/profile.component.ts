import {Component} from "@angular/core";
import {ProfileService} from '../../services/profile.service';

@Component({
    selector: "profile",
    templateUrl: 'profile.template.html',
    styleUrls: ['profile.style.css'],
})
export class ProfileComponent {

    constructor(public profile: ProfileService) {
        this.init();
    }

    private init() {
        this.profile.setCurrentUser();
        this.profile.getJourney();
        this.profile.getOrders();
        this.profile.getSamples();
    }
}
