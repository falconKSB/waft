import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {GiftService} from '../../../services/gift.service';
import {GTMService} from "../../../services/gtm.service";

@Component({
    selector: 'gift-checkout',
    templateUrl: 'giftCheckout.template.html',
    styleUrls: ['../../shared/forms.style.css', 'giftCheckout.style.css']
})
export class GiftCheckoutComponent {
    constructor(public router: Router,
                public gift: GiftService,
                gtm: GTMService) {
        gtm.checkout('gift_delivery', gift.order.delivery.countryObj, 'gift', 'big');
    }

    public onCountryChange(countryCode) : void {
        this.gift.initCountry(countryCode);
    }

    public onLogin(): void {
        this.router.navigate(['/app/gift/login']);
    }

    public save(): void {
        // this.gift.order.orderSize = this.gift.country.price.getMainOrderSize(this.gift.order.gender);
        this.gift.putToStorage();
    }

    public onDone(): void {
        this.gift.putToStorage();
        this.router.navigate(['/app/gift/payment']);
    }
}
