import { Component, OnInit } from '@angular/core';
import {GTMService} from "../../../services/gtm.service";
import {WizardService} from "../../../services/wizard.service";
import {GiftService} from "../../../services/gift.service";
import {Router} from "@angular/router";
import {AuthService} from "../../../services/auth.service";

@Component({
    selector: 'notify-recipient',
    templateUrl: 'notifyRecipient.template.html',
    styleUrls: ['../../shared/forms.style.css', '../gift-common.style.css', '../../shared/bottle.style.css', 'notifyRecipient.style.css']
})
export class GiftNotifyRecipientComponent {
    isCopied: boolean = false;
    url: string = '';

    constructor(private router: Router, private auth: AuthService,
                public gift: GiftService, wizard: WizardService, gtm: GTMService) {
        gtm.purchase('gift_thank_you', gift.order.delivery.countryObj, gift.order);
        this.url = this.gift.order.getGiftURL();
    }

    public onContinue() {
        this.router.navigate(['/app/gift/personalisation']);
    }

    public onCopy() {
        this.isCopied = true;

        setTimeout(() => {
            this.router.navigate(['/app/gift/thank-you']);
        }, 1000);
    }
}