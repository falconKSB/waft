import {Component} from '@angular/core';

import {Router, ActivatedRoute, Params} from '@angular/router';

import {GiftService} from '../../../services/gift.service';
import {WizardService} from '../../../services/wizard.service';

import {GTMService} from "../../../services/gtm.service";
import {WaftAPIService} from "../../../services/waftapi.service";
import {AddressModel} from "../../../models/address.model";
import {JourneyModel} from "../../../models/journey.model";
import {CustomBottleModel} from "../../../models/customBottleModel";


@Component({
    selector: 'gift-receive',
    styleUrls: ['../../shared/bottle.style.css', 'giftReceive.style.css'],
    templateUrl: 'giftReceive.template.html'
})
export class GiftReceiveComponent {
    constructor(private wizard: WizardService, private route: ActivatedRoute,
                private router: Router, public gift: GiftService,
                private gtm: GTMService, private api: WaftAPIService) {
        gtm.event('gift_receive_loaded');
        this.getData();
    }

    private receiverFirstName: string;
    public giftSenderName: string;
    public customBottle: CustomBottleModel = new CustomBottleModel();

    public clickContinue() {
        this.gtm.event('gift_receive_continue');
        this.router.navigate(['/app/wizard/gender']);
    }

    public getData() {
        this.route.params.forEach((params: Params) => {
            this.api.getGiftDetails(params['id']).subscribe(
                data => {
                    if (data) {
                        this.gift.order.id = params['id'];
                        this.gift.order.customBottle = data.customBottle;
                        this.gift.order.gift = data.gift;
                        this.gift.order.delivery = new AddressModel(data.delivery);
                        this.gift.order.countryCode = data.countryCode;

                        this.receiverFirstName = this.gift.order.gift.recipient.firstName;
                        this.giftSenderName = this.gift.order.gift.senderName;
                        this.customBottle = this.gift.order.customBottle;

                        this.wizard.resetWizard();
                        this.wizard.order = this.gift.order;
                        this.wizard.order.isGiftRedemption = true;
                        this.wizard.order.journey = new JourneyModel();
                        this.wizard.order.journey.gift = false;
                        this.wizard.order.journey.customBottle = data.customBottle;
                        this.wizard.initCountry(data.countryCode);
                        this.wizard.putToStorage();
                    }
                    else {
                        (<any>window).location.href = '/';
                    }
                },
                error => {
                    (<any>window).location.href = '/';
                });
        });
    }
}
