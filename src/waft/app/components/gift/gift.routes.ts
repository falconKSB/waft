import {Routes} from '@angular/router';

import {AuthGuard} from '../../guards/auth.guard';

import {GiftLoginComponent} from './login/login.component';
import {GiftReceiveComponent} from './receive/giftReceive.component';
import {GiftCheckoutComponent} from './checkout/giftCheckout.component';
import {GiftCustomiseBottleComponent} from './customiseBottle/customiseBottle.component';
import {EmailPersonalisationComponent} from './emailPersonalisation/emailPersonalisation.component';
import {ThankYouPageComponent} from './thankYouPage/thankYouPage.component';
import {GiftPaymentComponent} from "./payment/payment.component";
import {GiftNotifyRecipientComponent} from "./notifyRecipient/notifyRecipient.component";
import {GiftGuard} from "../../guards/gift.guard";

export const ROUTES: Routes = [
    { path: 'app/gift', children: [
        {path: 'login', component: GiftLoginComponent},
        {path: 'receive/:id', component: GiftReceiveComponent},
        {path: 'customise-bottle', component: GiftCustomiseBottleComponent},
        {path: 'personalisation', component: EmailPersonalisationComponent, canActivate: [GiftGuard, AuthGuard]},
        {path: 'checkout', component: GiftCheckoutComponent},
        {path: 'payment', component: GiftPaymentComponent, canActivate: [GiftGuard, AuthGuard]},
        {path: 'notify-recipient', component: GiftNotifyRecipientComponent, canActivate: [GiftGuard, AuthGuard]},
        {path: 'thank-you', component: ThankYouPageComponent, canActivate: [GiftGuard, AuthGuard]},
        {path: '**', redirectTo: 'customise-bottle'},
    ]}
];
