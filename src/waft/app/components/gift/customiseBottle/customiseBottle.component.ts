import {Component} from "@angular/core";

import {AuthService} from "../../../services/auth.service";
import {GiftService} from "../../../services/gift.service"

import {Router} from '@angular/router';

import {StyleModel} from "../../../models/style.model";
import {GTMService} from "../../../services/gtm.service";


@Component({
    selector: "gift-customise-bottle",
    templateUrl: 'customiseBottle.template.html',
    styleUrls: ['../../shared/forms.style.css', '../gift-common.style.css', '../../shared/bottle.style.css',
        '../../shared/bottleFonts.style.css', 'customiseBottle.style.css']
})

export class GiftCustomiseBottleComponent {

    constructor(public router: Router, public auth: AuthService, public gift: GiftService, private gtm: GTMService) {
        gift.reset();
        gtm.event('gift_customise_bottle');
        this.styles = StyleModel.getAllStyles();
        this.restoreStyle();
    }

    public isStyleSelection: boolean = false;
    public showPreview: boolean = false;
    public styles: any;
    public selectedStyle: any;
    public initialsStyle: any = {
        'text-transform': 'uppercase'
    };

    public continue() {
        if (this.gift.order.customBottle.name !== '') {
            this.gtm.event('gift_customise_bottle_continue');
            this.gift.putToStorage();
            if (this.auth.isAuth())
                this.router.navigate(['/app/gift/checkout']);
            else
                this.router.navigate(['/app/gift/login']);
        }
    }

    public skip() {
        this.gift.order.customBottle.name = "";
        this.gift.order.customBottle.initials = "";
        this.gift.order.customBottle.message = "";
        this.gift.putToStorage();
        this.gtm.event('gift_customise_bottle_skip');
        if (this.auth.isAuth())
            this.router.navigate(['/app/gift/checkout']);
        else
            this.router.navigate(['/app/gift/login']);
    }

    public restoreStyle() {
        for (let s of this.styles) {
            if (s.style == this.gift.order.customBottle.style) {
               this.selectStyle(s);
               return;
            }
        }
        this.selectStyle(this.styles[0]);
    }

    public showStyles() {
        this.isStyleSelection = !this.isStyleSelection;
    }

    public togglePreview() {
        if (this.gift.order.customBottle.name !== '') {
            this.showPreview = !this.showPreview;
            window.scrollTo(0, 0);
        }
    }

    public selectStyle(style: any) {
        this.selectedStyle = style;
        this.gift.order.customBottle.style = style.style;
        this.isStyleSelection = false;
    }
}
