import {Component, ViewChild} from '@angular/core';
import {FormBuilder, NgForm} from '@angular/forms';
import {Router} from '@angular/router';

import {GiftService} from '../../../services/gift.service';
import {AuthService} from '../../../services/auth.service';
import {GTMService} from "../../../services/gtm.service";
import {WaftAPIService} from "../../../services/waftapi.service";

@Component({
    selector: 'email-personalisation',
    templateUrl: 'emailPersonalisation.template.html',
    styleUrls: ['../../shared/forms.style.css', '../gift-common.style.css', 'emailPersonalisation.style.css']
})

export class EmailPersonalisationComponent {
    isValidated = false;
    @ViewChild('personalisationForm') personalisationForm: NgForm;
    public isPreview: boolean = false;
    public mainOrderSize: string = 'big';
    public mainVolume: string = '100ml';
    public message: string = "";
    public isProcessing: boolean = false;
    public isSendByWaft = true;

    constructor(private router: Router, public gift: GiftService, private auth: AuthService, private gtm: GTMService, private api: WaftAPIService) {
        //this.gift.checkOnLock();
        let user = this.auth.getCurrentUser();
        if (user && (gift.order.gift.senderName == '')) {
            gift.order.gift.senderName = user.firstName;
        }
        this.mainOrderSize = gift.order.orderSize;
        this.mainVolume = gift.order.delivery.countryObj.price.getMainOrderVolume(this.mainOrderSize);
    }


    public togglePreview(): void {
        if (this.validate()) {
            this.isPreview = !this.isPreview;
            window.scrollTo(0, 0);
        }
    }

    public clickContinue(): void {
        if (this.validate()) {
            this.saveGiftEmail();
            this.gtm.productDetail('gift_personalise_email', this.gift.order.delivery.countryObj, 'gift', [this.gift.order.orderSize]);
            this.gtm.addToCart('gift_personalise_email', this.gift.order.delivery.countryObj, 'gift', this.gift.order.orderSize);
            this.gift.putToStorage();
        }
    }

    private validate(): boolean {
        this.isValidated = true;
        return this.isValidEmailDate(this.gift.order.gift.inviteEmailDate) && this.personalisationForm.valid;
    }

    public isValidEmailDate(selectedDate): boolean {
        let todayDateVal = this.getDateVal();
        let emailDateVal = this.getDateVal(new Date(selectedDate));

        return emailDateVal >= todayDateVal;
    }

    private getDateVal(date?: Date) : number {
        let res = new Date();
        if (date) res.setFullYear(date.getFullYear(), date.getMonth(), date.getDate());
        res.setHours(0, 0, 0, 0);
        return res.valueOf();
    }

    private saveGiftEmail(): void {
        this.message = 'Processing...';
        this.isProcessing = true;
        this.api.saveGiftEmail(this.gift.order.id, this.gift.order.gift).subscribe(
            data => {
                if (Object.keys(data).length != 0) {
                    this.router.navigate(['/app/gift/thank-you']);
                }
                else {
                    this.message = data.message || 'Unknown Error';
                    this.isProcessing = false;
                }
            },
            error => {
                this.message = 'Unknown Error';
                this.isProcessing = false;
            });
    }
}
