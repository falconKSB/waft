import {Component} from '@angular/core';

import {GTMService} from "../../../services/gtm.service";
import {GiftService} from "../../../services/gift.service";
import {Router} from "@angular/router";
import {WizardService} from "../../../services/wizard.service";

@Component({
    selector: 'payment',
    templateUrl: 'payment.template.html',
    styleUrls: ['payment.style.css']
})
export class GiftPaymentComponent {
    constructor(public router: Router, public gift: GiftService, gtm: GTMService) {
        gtm.checkout('gift_payment', gift.order.delivery.countryObj, 'gift', gift.order.orderSize, 2);
        gtm.trackReferralRock(gift.order.id);
    }

    public onDone() {
        this.router.navigate(['/app/gift/notify-recipient']);
    }

    public save(): void {
        this.gift.putToStorage();
    }
}
