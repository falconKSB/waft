import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {ModalModule} from "ng2-modal";

import {ROUTES} from './gift.routes';

import {SharedModule} from '../shared/index';

import {GiftLoginComponent} from './login/login.component';
import {GiftReceiveComponent} from './receive/giftReceive.component';
import {GiftCheckoutComponent} from './checkout/giftCheckout.component';
import {GiftCustomiseBottleComponent} from './customiseBottle/customiseBottle.component';
import {EmailPersonalisationComponent} from './emailPersonalisation/emailPersonalisation.component';
import {ThankYouPageComponent} from './thankYouPage/thankYouPage.component';
import {DatePicker} from '../../../../lib/ng2-datepicker/ng2-datepicker';

import {PaymentValidationService} from '../../services/paymentValidation.service';
import {LayoutModule} from "../layout/index";
import {GiftPaymentComponent} from "./payment/payment.component";
import {GiftNotifyRecipientComponent} from "./notifyRecipient/notifyRecipient.component";

@NgModule({
    exports: [],
    imports: [
        SharedModule,
        ModalModule,
        LayoutModule,
        RouterModule.forChild(ROUTES)
    ],
    declarations: [
        GiftLoginComponent,
        GiftReceiveComponent,
        GiftCheckoutComponent,
        GiftPaymentComponent,
        GiftCustomiseBottleComponent,
        EmailPersonalisationComponent,
        ThankYouPageComponent,
        GiftNotifyRecipientComponent,
        DatePicker
    ],
    providers: [PaymentValidationService],
})
export class GiftModule {
}
