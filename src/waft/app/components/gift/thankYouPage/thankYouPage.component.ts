import { Component, OnInit } from '@angular/core';
import { WizardService} from '../../../services/wizard.service';
import { GiftService } from '../../../services/gift.service';
import {GTMService} from "../../../services/gtm.service";
import {KioskService} from "../../../services/kiosk.service";
import {Router} from "@angular/router";
import {AuthService} from "../../../services/auth.service";

@Component({
    selector: 'thank-you-page',
    templateUrl: 'thankYouPage.template.html',
    styleUrls: ['../gift-common.style.css', 'thankYouPage.style.css']
})
export class ThankYouPageComponent {
    constructor(private router: Router, private auth: AuthService,
                gift: GiftService, wizard: WizardService, gtm: GTMService, public kiosk: KioskService) {
        gtm.event('gift_thank_you_final');
        gift.reset();
        wizard.resetWizard();
    }

    public onDone() {
        this.auth.logout();
        this.router.navigate(['/app/wizard/type']);
    }
}