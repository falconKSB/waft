import { Component, ViewChild } from '@angular/core';
import {GiftService} from '../../../services/gift.service';
import {AuthService} from "../../../services/auth.service";
import {Router} from "@angular/router";
import {Modal} from "ng2-modal/Modal";
import {GTMService} from "../../../services/gtm.service";

@Component({
    selector: 'gift-login',
    templateUrl: 'login.template.html',
    styleUrls: ['../../shared/forms.style.css','../../shared/login.style.css']
})
export class GiftLoginComponent { //TO DO: check all logic
    constructor(public auth: AuthService, private router: Router, public gift: GiftService, gtm: GTMService) {
        gtm.event('gift_login');
        if (this.auth.isAuth()) {
            this.router.navigate(['/app/gift/customise-bottle']); // allow user to return to the previous page
        }
    }

    @ViewChild('forgotPassword') forgotPassword: Modal;

    email: string = '';
    password: string = '';
    error: string = '';

    login() {
        if ((this.email == '') || (this.password == '')) {
            this.error = "Please fill in email and password";
            return;
        }
        this.error = 'Processing...';
        this.auth.login(this.email, this.password).subscribe(
            data => {
                if (data.success) {
                    this.clickContinue();
                } else {
                    this.error = data.message;
                }
            },
            error => {
                if (console) console.log(error)
            }
        )
    }

    clickContinue() : void {
        this.router.navigate(['/app/gift/checkout']);
    }
}
