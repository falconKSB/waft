import {Component, ViewChild}      from "@angular/core";
import {NgForm} from "@angular/forms";
import {WizardService}  from "../../services/wizard.service";
import {AuthService}    from "../../services/auth.service";
import {GiftService}    from "../../services/gift.service";
import {Router} from "@angular/router";
import {KioskService} from "../../services/kiosk.service";


@Component({
    selector: 'kiosk',
    templateUrl: 'kiosk.template.html',
    styleUrls: ['../shared/forms.style.css', 'kiosk.style.css']
})
export class KioskComponent {
    salesRep: string = '';
    @ViewChild('kioskForm') kioskForm: NgForm;

    constructor(private gift: GiftService, public kiosk: KioskService, private wizard:WizardService,
                private auth: AuthService, private router: Router) {
        if (this.auth.getCurrentUser) {
            this.gift.reset();
            this.wizard.resetWizard();
            this.auth.logout();
        }
    }

    signIn() {
        if(this.kioskForm.invalid) return;
        this.kiosk.switchKioskMode(this.salesRep);
    }
}