import { Component, } from '@angular/core';
import { Router } from "@angular/router";

@Component({
    selector: 'forgot-password-page',
    templateUrl: 'forgotPasswordPage.template.html',
    styleUrls: ['forgotPasswordPage.style.css']
})
export class ForgotPasswordPageComponent {
    constructor(private router: Router) { }

    public changeScreen(modalName) {

        switch (modalName) {
            case 'close':
                this.router.navigate(['/app/account/login']);
                break;
            case 'login':
                this.router.navigate(['/app/account/login']);
                break;
            case 'register':
                this.router.navigate(['/app/account/register']);
                break;
        }
    }
} 