
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from './login/loginPage.component';
import { RegisterPageComponent } from './register/registerPage.component';
import { ForgotPasswordPageComponent } from './forgotPassword/forgotPasswordPage.component';

import { DataResolver } from '../../app.resolver';


export const ROUTES: Routes = [
    { path: 'app/account', children: [
        { path: '', pathMatch: 'full', redirectTo: 'login' },
        { path: 'login', component: LoginPageComponent },
        { path: 'register', component: RegisterPageComponent },
        { path: 'forgot-password', component: ForgotPasswordPageComponent }
    ]}
];
