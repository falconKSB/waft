import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/index';

import { LoginPageComponent } from './login/loginPage.component';
import { RegisterPageComponent } from './register/registerPage.component';
import { ForgotPasswordPageComponent } from './forgotPassword/forgotPasswordPage.component';

import { ROUTES } from './account.routes';


@NgModule({
  declarations: [ LoginPageComponent, RegisterPageComponent, ForgotPasswordPageComponent ],
  imports: [
    FormsModule,
    SharedModule,
    RouterModule.forChild(ROUTES)
  ]
})
export class AccountModule { }
