import { Component } from "@angular/core";
import { Router } from "@angular/router";

import { LocalStorageService } from '../../../services/localStorage.service';

@Component({
    selector: "login-page",
    templateUrl: './loginPage.template.html',
    styleUrls: ['./loginPage.style.css']
})

export class LoginPageComponent {

    constructor(private router: Router, private storage: LocalStorageService) { }

    public changeScreen(modalName) {

        switch (modalName) {
            case 'close':
                let redirectUrl = this.storage.get('redirectUrl', '/app/profile');
                this.router.navigate([redirectUrl]);
                break;
            case 'register':
                this.router.navigate(['/app/account/register']);
                break;
            case 'forgotPassword':
                this.router.navigate(['/app/account/forgot-password']);
                break;
        }
    }

};
