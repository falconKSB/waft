import { Component } from '@angular/core';
import { Router } from "@angular/router";

import { LocalStorageService } from '../../../services/localStorage.service';

@Component({
    selector: 'register-page',
    templateUrl: './registerPage.template.html',
    styleUrls: ['./registerPage.style.css']
})
export class RegisterPageComponent {
    constructor(private router: Router, private storage: LocalStorageService) {

    }

    public changeScreen(modalName) {
        switch (modalName) {
            case 'close':
                let redirectUrl = this.storage.get('redirectUrl', '/app/profile');
                this.router.navigate([redirectUrl]);
                break;
            case 'login':
                this.router.navigate(['/app/account/login']);
                break;
        }
    }

}