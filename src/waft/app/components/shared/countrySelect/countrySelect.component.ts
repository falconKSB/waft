import {Component, Input, Output, EventEmitter} from '@angular/core';

import {AddressModel} from "../../../models/address.model";
import {CountryModel} from "../../../models/country.model";

@Component({
    selector: 'country-select',
    templateUrl: 'countrySelect.template.html',
    styleUrls: ['../forms.style.css']
})
export class CountrySelectComponent {
    @Input() address: AddressModel;
    @Input() showErrors: boolean;
    @Input() allowCountryChange: boolean = true;
    @Output() onCountryChange = new EventEmitter();

    public countries: Array<CountryModel>;
    public isCountrySelection: boolean = false;

    constructor() {
        this.countries = CountryModel.GetAll();
    }

    public selectCountry(country: string) {
        let countryCode = this.countries.filter(item => item.country == country)[0].countryCode;
        this.onCountryChange.emit(countryCode);
    }
}