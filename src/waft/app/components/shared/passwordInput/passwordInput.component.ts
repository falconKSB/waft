import {Component, Input, Output, EventEmitter, SimpleChanges} from '@angular/core';

@Component({
    selector: 'password-input',
    templateUrl: 'passwordInput.template.html',
    styleUrls: ['../forms.style.css']
})
export class PasswordInputComponent {
    @Input() password: string = '';
    @Input() showErrors: boolean;
    @Output() onPasswordChange = new EventEmitter();

    public type: string = 'password';

    public changeType() {
        this.type == 'password' ? this.type = 'text' : this.type = 'password';
    }
}

