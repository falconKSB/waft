import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../../services/auth.service';
import {RegistrationModel} from "../../../models/registration.model";

@Component({
    selector: 'register',
    templateUrl: 'register.template.html',
    styleUrls: ['../login-register.style.css']
})
export class RegisterComponent implements OnInit {
    constructor(public auth: AuthService, private router: Router) {
        if (router.url == '/app/account/register') {
            this.notModal = true;
        }
    }

    ngOnInit() { }

    public regData: RegistrationModel = new RegistrationModel();
    public error: string;


    public notModal: boolean;

    @Output() changeScreen = new EventEmitter();

    public register() {
        if ((this.regData.firstName == '') || (this.regData.lastName == '') ||
            (this.regData.password == '') || (this.regData.email == '')) {
            this.error = "Please fill out all the fields";
            return;
        }
        this.error = 'Processing...';
        this.auth.register(this.regData).subscribe(
            data => {
                if (data.success) {
                    this.changeScreen.emit('close');
                }
                else {
                    this.error = data.message;
                }
            },
            error => {
                if (console) console.log(error)
            }
        )
    }

    public toChangeScreen(screen: string) {
        this.changeScreen.emit(screen);
    }
}
