import {NgModule} from '@angular/core';
import {CommonModule}      from '@angular/common';
import {RegisterComponent} from './register/register.component';
import {LoginComponent} from './login/login.component';
import {ForgotPasswordComponent} from './forgotPassword/forgotPassword.component';
import {GuaranteePopupComponent} from './guaranteePopup/guaranteePopup.component';
import {SpiderDiagramComponent} from './spiderDiagram/spiderDiagram.component';
import {ValidationModule} from './validation/index';
import {FormsModule} from '@angular/forms';
import {ModalModule} from "ng2-modal";
import {CDNImageDirective} from './directives/cdnImage.directive'
import {CDNBackgroundDirective} from './directives/cdnBackground.directive'
import {LabLoaderComponent} from './labLoader/labLoader.component';
import {CustomInputComponent} from './customElements/input.component';
import {CustomTextAreaComponent} from './customElements/textArea.component';
import {PaymentFormComponent} from "./paymentForm/paymentForm.component";
import {CountrySelectComponent} from "./countrySelect/countrySelect.component";
import {StateSelectComponent} from "./stateSelect/stateSelect.component";
import {PasswordInputComponent} from "./passwordInput/passwordInput.component";
import {CheckoutFormComponent} from "./checkoutForm/checkoutForm.component";
import {SafePipe} from './pipes/safe.pipe';
import {ContinuePopupComponent} from "./continuePopup/continuePopup.component";
import {ClipboardDirective} from "../../directives/clipboard.directive";
import {BottlePreviewComponent} from "./bottlePreview/bottlePreview.component";

@NgModule({
    imports: [CommonModule, FormsModule, ModalModule, ValidationModule],
    declarations: [
        CDNImageDirective, CDNBackgroundDirective, PasswordInputComponent,
        LabLoaderComponent, RegisterComponent, LoginComponent, ForgotPasswordComponent,
        GuaranteePopupComponent, SpiderDiagramComponent, CustomInputComponent, CustomTextAreaComponent,
        PaymentFormComponent, CountrySelectComponent, StateSelectComponent, CheckoutFormComponent, SafePipe,
        ContinuePopupComponent, ClipboardDirective, BottlePreviewComponent
    ],
    exports: [
        CDNImageDirective, CDNBackgroundDirective,
        RegisterComponent, LoginComponent, ForgotPasswordComponent, PasswordInputComponent,
        LabLoaderComponent, SpiderDiagramComponent, GuaranteePopupComponent, CommonModule,
        FormsModule, ValidationModule, CustomInputComponent, CustomTextAreaComponent,
        PaymentFormComponent, CountrySelectComponent, StateSelectComponent, CheckoutFormComponent, SafePipe,
        ContinuePopupComponent, ClipboardDirective, BottlePreviewComponent
    ]
})
export class SharedModule {
}
