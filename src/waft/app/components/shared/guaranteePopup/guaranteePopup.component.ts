import {Component, Output, EventEmitter} from '@angular/core';

import {WizardService} from '../../../services/wizard.service';

@Component({
    selector: 'guarantee-popup',
    templateUrl: 'guaranteePopup.template.html',
    styleUrls: ['guaranteePopup.style.css']
})
export class GuaranteePopupComponent {
    constructor(public wizard: WizardService) {
    }

    @Output() closeModal = new EventEmitter();

    public close() {
        this.closeModal.emit();
    }
}