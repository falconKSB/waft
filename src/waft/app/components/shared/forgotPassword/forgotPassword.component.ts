import { Component, Output, EventEmitter } from '@angular/core';

import { AuthService } from '../../../services/auth.service';

@Component({
    selector: 'forgot-password',
    templateUrl: 'forgotPassword.template.html',
    styleUrls: ['forgotPassword.style.css']
})
export class ForgotPasswordComponent {
    @Output() closeModal = new EventEmitter();
    public email: string = '';
    public error: string;

    constructor(private auth: AuthService) {
    }

    public close() {
        this.closeModal.emit();
    }

    public restorePassword() {
        if (this.email == '') {
            this.error = "Please fill in the e-mail address";
            return false;
        }
        this.error = 'Processing...';
        this.auth.restorePassword(this.email).subscribe(
            data => {
                if (data.success) {
                    this.error = data.message;
                } else {
                    this.error = data.message;
                }
            });
        return false;
    }
}