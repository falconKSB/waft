import {Directive, ElementRef, Input, Renderer, OnInit} from '@angular/core';

declare var CDN: string;

@Directive({
    selector: '[cdn-src]'
})
export class CDNImageDirective implements  OnInit {
    constructor(private el: ElementRef, private renderer: Renderer) {
    }

    @Input('cdn-src') src: string;

    ngOnInit() {
        this.renderer.setElementAttribute(this.el.nativeElement, 'src', CDN + this.src)
    }
}