import {Directive, ElementRef, Input, Renderer, AfterContentInit} from '@angular/core';

declare var CDN: string;

@Directive({
    selector: '[cdn-bg-src]'
})
export class CDNBackgroundDirective implements AfterContentInit {
    constructor(private el: ElementRef, private renderer: Renderer) {
    }

    @Input('cdn-bg-src') src: string;

    ngAfterContentInit() {
        this.renderer.setElementStyle(this.el.nativeElement, 'background-image', 'url('+CDN + this.src+')')
    }
}