import {Component, Input, ViewChild, forwardRef, ElementRef} from '@angular/core';
import {NG_VALUE_ACCESSOR, ControlValueAccessor} from '@angular/forms';

const noop = () => {
};

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => CustomInputComponent),
    multi: true
};

@Component({
    selector: 'custom-input',
    template: ` <input #input [(ngModel)]="value" type="text" [ngStyle]="styles" placeholder="{{placeHolder}}" maxlength="{{maxLength}}" (blur)="onBlur()" >`,
    styleUrls: ['../forms.style.css'],
    providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
})
export class CustomInputComponent implements ControlValueAccessor {

    @Input('max-length') maxLength: number;
    @Input('place-holder') placeHolder: string;
    @Input('ngStyle') styles: any;
    @ViewChild('input') input: ElementRef;

    //The internal data model
    private innerValue: any = '';

    //Placeholders for the callbacks which are later provided
    //by the Control Value Accessor
    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_: any) => void = noop;
    private regExp = /[^\u0370-\u03ff\u0400-\u04ff\u4e00-\u9eff\u3400-\u4dbf\x00-\x7FÀ-ÿ]+/g;

    //get accessor
    get value(): any {
        return this.innerValue;
    };

    //set accessor including call the onchange callback
    set value(v: any) {
        if (v !== this.innerValue) {
            this.innerValue = this.input.nativeElement.value = v.replace(this.regExp, '');
            this.onChangeCallback(this.innerValue);
        }
    }

    //Set touched on blur
    onBlur() {
        this.onTouchedCallback();
    }

    //From ControlValueAccessor interface
    writeValue(value: any) {
        if (value !== this.innerValue) {
            this.innerValue = value;
        }
    }

    //From ControlValueAccessor interface
    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    //From ControlValueAccessor interface
    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }

}