import {Component, Output, EventEmitter, OnInit} from "@angular/core";

@Component({
    selector: "waft-lab-loader",
    templateUrl: 'labLoader.template.html',
    styleUrls: ['labLoader.style.css']
})

export class LabLoaderComponent implements OnInit {
    public loadPercent: number = 1;
    public notificationPercent: string = 'Calculating potentials...';

    @Output() complete = new EventEmitter();

    onCompleted() {
        this.complete.emit('complete');
    }

    ngOnInit() {
        let interval = setInterval(() => {

            if (this.loadPercent < 99) this.loadPercent++;

            switch (this.loadPercent) {
                case 25: {
                    this.notificationPercent = 'Activating flux capacitors...';
                    break;
                }
                case 55: {
                    this.notificationPercent = 'Waft hyperdrive engaged...';
                    break;
                }
                case 85: {
                    this.notificationPercent = 'Almost there...';
                    break;
                }
                case 99: {
                    this.onCompleted();
                    break;
                }
            }
        }, 40);
    }
}
