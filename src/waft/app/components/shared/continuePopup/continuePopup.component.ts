import {Component, Output, EventEmitter} from '@angular/core';
import {Router} from '@angular/router';


import {WizardService} from '../../../services/wizard.service';
@Component({
    selector: 'continue-popup',
    templateUrl: 'continuePopup.template.html',
    styleUrls: ['continuePopup.style.css']
})

export class ContinuePopupComponent {
    constructor(public wizard: WizardService, public router: Router) {
        let lastStep = this.wizard.lastStep;
    }

    @Output() closeModal = new EventEmitter();

    public continueWizard() {
        let lastStepIndex = this.wizard.lastStep.index;
        if (lastStepIndex < 10) this.wizard.toStep(lastStepIndex, true, true);
        else this.wizard.toStepFromRoute('/app/wizard/the-new-you', true, true);
        this.closeModal.emit();
    }

    public resetWizard() {
        this.wizard.resetWizard();
        this.closeModal.emit();
    }

}