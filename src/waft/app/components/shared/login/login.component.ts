import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../../services/auth.service';
import {Modal} from "ng2-modal/Modal";

@Component({
    selector: 'login',
    templateUrl: 'login.template.html',
    styleUrls: ['../forms.style.css','../login.style.css']
})
export class LoginComponent {
    constructor(public auth: AuthService, private router: Router) {
        if (router.url == '/app/account/login') {
            this.notModal = true;
        }
    }

    @Output() changeScreen = new EventEmitter();

    toChangeScreen(screen: string) {
        this.changeScreen.emit(screen);
    }

    notModal: boolean;


    @ViewChild('forgotPassword') forgotPassword: Modal;

    email: string = '';
    password: string = '';
    error: string = '';

    login() {
        if ((this.email == '') || (this.password == '')) {
            this.error = "Please fill in e-mail and password";
            return false;
        }
        this.error = 'Processing...';
        this.auth.login(this.email, this.password).subscribe(
            data => {
                if (data.success) {
                    this.changeScreen.emit('close');
                } else {
                    this.error = data.message;
                }
            },
            error => {
                if (console) console.log(error)
            }
        )
        return false;
    }

    clickContinue() : void {
        this.router.navigate(['/app/wizard/type']);
    }
}
