import {NgModule} from '@angular/core';
import {CommonModule}      from '@angular/common';
import {FormsModule} from '@angular/forms';
import {PhoneNumberValidatorDirective} from './phoneNumberValidator.directive';
import {EmailValidatorDirective} from './emailValidator.directive';
import {PostalCodeValidatorDirective} from './postalCodeValidator.directive';

@NgModule({
    imports: [],
    declarations: [EmailValidatorDirective, PostalCodeValidatorDirective, PhoneNumberValidatorDirective],
    exports: [EmailValidatorDirective, PostalCodeValidatorDirective, PhoneNumberValidatorDirective]
})
export class ValidationModule {
}
