import {Directive, Input, OnChanges, SimpleChanges, Output, EventEmitter} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators} from '@angular/forms';

@Directive({
    selector: '[postalCode]',
    providers: [{provide: NG_VALIDATORS, useExisting: PostalCodeValidatorDirective, multi: true}]
})
export class PostalCodeValidatorDirective implements Validator {

    private coutryCode: string;

    @Input()
    set postalCode(code: string) {
        this.coutryCode = code || '';
    }

    validate(control: AbstractControl): {[key: string]: any} {
        let postalCodeRe, isValid;
        let postalCode = control.value || '';
        if (postalCode == '') return null;

        switch (this.coutryCode) {
            case 'SG':
                postalCodeRe = /^[\d\\ +\-()\s]+$/;
                let digits = postalCode.replace(/\D/g, '');
                isValid = postalCodeRe.test(postalCode) && digits.length == 6;
                return !isValid ? {'postalCode': {postalCode}} : null;
            case 'US':
                postalCodeRe = /^\d{5}(?:[-\s]\d{4})?$/;
                break;
            case 'GB':
                postalCode = postalCode.toUpperCase();
                //(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKPSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2})
                // A	B	C	D	E	F	G	H	I	J	K	L	M	N	O	P	Q	R	S	T	U	V	W	X	Y	Z
                postalCodeRe = /^(GIR 0AA)|((([A-PR-UWYZ][0-9][0-9]?)|(([A-PR-UWYZ][A-HK-Y][0-9][0-9]?)|(([A-PR-UWYZ][0-9][A-HJKPSTUW])|([A-PR-UWYZ][A-HK-Y][0-9][ABEHMNPRVWXY]))))\s?[0-9][ABD-HJLNP-UW-Z]{2})$/
                break;
            default:
                return null;
        }

        isValid = postalCodeRe.test(postalCode);
        return !isValid ? {'postalCode': {postalCode}} : null;
    }
}