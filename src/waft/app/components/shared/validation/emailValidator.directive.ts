import {Directive, Input, OnChanges, SimpleChanges} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators} from '@angular/forms';

@Directive({
    selector: '[emailFormat]',
    providers: [{provide: NG_VALIDATORS, useExisting: EmailValidatorDirective, multi: true}]
})

export class EmailValidatorDirective implements Validator {

    private valFn = this.emailFormatValidator();

    validate(control: AbstractControl): {[key: string]: any} {
        return this.valFn(control);
    }

    emailFormatValidator(): ValidatorFn {
        return (control: AbstractControl): {[key: string]: any} => {
            let pattern: RegExp = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
            let email = control.value;
            let isValid;
            if (email == '') return null;
            isValid = pattern.test(email);
            return !isValid ? {'emailFormat': {email}} : null;
        };
    }
}

