import {Directive, Input, OnChanges, Output, SimpleChanges, EventEmitter, HostListener} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators} from '@angular/forms';
import {PhoneNumberModel} from '../../../models/phonenumber.model';

@Directive({
    selector: '[phoneNumberFormat]',
    providers: [{provide: NG_VALIDATORS, useExisting: PhoneNumberValidatorDirective, multi: true}]
})

export class PhoneNumberValidatorDirective implements Validator {

    @Output() onModelChange = new EventEmitter();
    private countryCode: string;

    validate(control: AbstractControl): {[key: string]: any} {
        let number = control.value || '';
        return ((number == '') || PhoneNumberModel.Validate(number, this.countryCode)) ? null : {'phoneNumberFormat': {number}};
    }

    @Input()
    set phoneNumberFormat(code: string) {
        this.countryCode = code || '';
    }
}