import {Component, Input, ViewChild, OnInit, EventEmitter, Output, NgZone} from '@angular/core';

import {WaftAPIService} from "../../../services/waftapi.service";
import {OrderModel} from "../../../models/order.model";
import {PriceModel} from "../../../models/country.model";
import {NgForm} from "@angular/forms";
import {KioskService} from "../../../services/kiosk.service";

declare var $: any;
declare var STRIPE_KEY_US: string;
declare var STRIPE_KEY_SG: string;

@Component({
    selector: 'payment-form',
    templateUrl: 'paymentForm.template.html',
    styleUrls: ['../forms.style.css', 'paymentForm.style.css']
})
export class PaymentFormComponent implements OnInit {
    @Input() order: OrderModel;
    @Output() onDone = new EventEmitter();
    @Output() save = new EventEmitter();

    @ViewChild('cardNumberField') cardNumberField;
    @ViewChild('expireDateField') expireDateField;
    @ViewChild('cvCodeField') cvCodeField;
    @ViewChild('checkoutForm') checkoutForm: NgForm;

    public isKioskModeEnabled: boolean;
    public cardNumberValidationMsg: string;
    public expireDateValidationMsg: string;
    public cvCodeValidationMsg: string;
    public showPromo: boolean = false;
    public promoCodeMessage: string = '';
    public previousPromoCode: string = '';

    public expiryMonth: string;
    public expiryYear: string;
    public showErrors: boolean = false;
    public samplesAgreed: boolean = false;
    public samplesValidated: boolean = false;
    public message: string = "";
    public isProcessing: boolean = false;
    public isValidatingPromoCode: boolean = false;
    public isFree: boolean = false;
    public isValidCardNumber: boolean = true;
    public isValidExpireDate: boolean = true;
    public isValidCvCode: boolean = true;

    constructor(private _zone: NgZone, private api: WaftAPIService, public kiosk: KioskService) {
        this.isKioskModeEnabled = kiosk.isEnabled();
    }

    public getMainOrderSize(): string {
        if (this.order.orderSize != 'samples') return '';
        return this.order.delivery.countryObj.price.getMainOrderSize(this.order.getGender());
    }

    public getMainOrderVolume(): string {
        if (this.order.orderSize != 'samples') return '';
        return this.order.delivery.countryObj.price.getMainOrderVolume(this.order.journey.gender).toUpperCase();
    }

    public placeOrder(fromPromo = false) {
        if (this.isProcessing && (!fromPromo)) return;
        if (this.isValidatingPromoCode) {
            this.message = 'Processing...';
            this.isProcessing = true;
            setTimeout(() => { this.placeOrder(true) }, 100);
            return;
        }
        this.message = "";
        if (!this.validate()) {
            this.message = "Please correct the form errors";
            this.isProcessing = false;
            if (this.order.orderSize == 'samples') {
                this.message += " and agree to the terms";
            }
            return;
        }
        this.save.emit();
        this.message = 'Processing...';
        this.isProcessing = true;
        this.getStripeToken();
    }

    samplesValidate(): boolean {
        if (this.order.orderSize != 'samples')
            return true;
        if (!this.samplesAgreed) {
            this.samplesValidated = true;
            return false;
        }
        return true;
    }

    private validate(): boolean {
        this.showErrors = true;
        let result = !this.checkoutForm.invalid;
        result = this.paymentValidate() && result;
        result = this.samplesValidate() && result;
        result = this.order.payment.isValidPromoCode() && result;
        return result;
    }

    static setStripePublishableKey(paymentAccount: string) {
        let key = paymentAccount == "Stripe SG"? STRIPE_KEY_SG : STRIPE_KEY_US;
        (<any>window).Stripe.setPublishableKey(key);
    }

    /** Payment */
    private getStripeToken(): void {
        if (this.isFree) {
            this.processOrder('', '');
            return;
        }
        let self = this;

        let cardDetails = {
            number: this.order.payment.cardNumber,
            exp_month: this.expiryMonth,
            exp_year: this.expiryYear,
            cvc: this.order.payment.cvCode
        };
        if (this.order.delivery.hasBillingPostal()) {
            cardDetails['address_zip'] = this.order.payment.zipCode;
        }
        let paymentAccount = this.order.delivery.getPaymentAccount();
        PaymentFormComponent.setStripePublishableKey(paymentAccount);
        (<any>window).Stripe.card.createToken(cardDetails, (status: number, response: any) => {
            // Wrapping inside the Angular zone
            this._zone.run(() => {
                if (status === 200) {
                    self.processOrder(paymentAccount, response.id);
                } else {
                    self.message = response.error.message;
                    self.isProcessing = false;
                }
            });
        });
    }

    private processOrder(paymentAccount: string, token: string): void {
        this.api.placeOrder(this.order.id, paymentAccount, token, this.order.payment.promoCode, this.order.preferredSample).subscribe(
            data => {
                if (Object.keys(data).length != 0) {
                    this.order.id = data.orderId;
                    this.message = 'Success!';
                    this.onDone.emit();
                }
                else {
                    this.isProcessing = false;
                    this.message = data.message || "Unknown Error. Please reload the page and try again.";
                }
            },
            error => {
                this.isProcessing = false;
                this.message = error && error.message;
                if ((!this.message) || (this.message == "")) {
                    this.message = "Unknown Error. Please reload the page and try again.";
                }
            });
    }

    public paymentValidate(): boolean {
        if (this.isFree) return true;
        if(!$.payment) {
            this.cardNumberValidationMsg = 'Unable to load Stripe. Please reload the page.';
            this.isValidCardNumber = false;
            return false;
        }
        let expireDate = this.order.payment.expireDate.replace(/\s/g, '');
        let number = this.order.payment.cardNumber.replace(/\s/g, '');
        this.expiryMonth = expireDate.substr(0, expireDate.indexOf('/'));
        this.expiryYear = expireDate.substr(expireDate.indexOf('/') + 1, expireDate.length - 1);
        let type = $.payment.cardType(number);
        let isValid = true;

        this.cardNumberValidationMsg = "";
        this.expireDateValidationMsg = "";
        this.cvCodeValidationMsg = "";

        if (!$.payment.validateCardNumber(number)) {
            number == '' ?
                this.cardNumberValidationMsg = 'Card number is required' :
                this.cardNumberValidationMsg = 'Invalid card number';
            this.isValidCardNumber = isValid = false;
        }
        if (!$.payment.validateCardExpiry(this.expiryMonth, this.expiryYear)) {
            this.order.payment.expireDate == '' ?
                this.expireDateValidationMsg = 'Card number is required' :
                this.expireDateValidationMsg = 'Invalid expire date';
            this.isValidExpireDate = isValid = false;
        }
        if (!$.payment.validateCardCVC(this.order.payment.cvCode, type)) {
            this.order.payment.cvCode == '' ?
                this.cvCodeValidationMsg = 'CVC is required' :
                this.cvCodeValidationMsg = 'Invalid CVC';
            this.isValidCvCode = isValid = false;
        }
        return isValid;
    }

    initStripe() {
        if ($.payment) {
            $(this.cardNumberField.nativeElement).payment('formatCardNumber');
            $(this.expireDateField.nativeElement).payment('formatCardExpiry');
            $(this.cvCodeField.nativeElement).payment('formatCardCVC');
        }
        else setTimeout(() => { this.initStripe() }, 500);
    }

    ngOnInit() {
        if (this.isKioskModeEnabled) {
            this.order.salesRep = this.kiosk.getSalesRep();
        }
        if (this.order.payment.promoCode && (this.order.payment.promoCode != '')) {
            this.showPromo = true;
            this.onPromoCodeChange(this.order.payment.promoCode)
        }
        this.initStripe();
    }

    public onPromoCodeChange(promoCode: string) {
        if (this.previousPromoCode == promoCode) return;
        this.previousPromoCode = promoCode;
        if ((!promoCode) || (promoCode == '')) {
            this.order.payment.promoData = null;
            this.promoCodeMessage = '';
            this.isFree = false;
            return;
        }
        this.isValidatingPromoCode = true;
        this.promoCodeMessage = 'Validating...';
        this.api.getPromoCodeStatus(promoCode, this.order.delivery.countryObj.price.currency).subscribe(
            data => {
                this.promoCodeMessage = '';
                this.order.payment.promoData = data;
                this.isFree = (this.order.orderSize != 'samples') &&
                    (this.order.delivery.countryObj.price.getGrandTotal(this.order.orderSize, data) == 0);
                this.isValidatingPromoCode = false;
            },
            error => {
                this.promoCodeMessage = 'Promo code is invalid';
                this.order.payment.promoData = null;
                this.isFree = false;
                this.isValidatingPromoCode = false;
            });
    }

    public onPromoCodeKeyPress(event: any) {
        if(event.key == 'Enter') {
            event.preventDefault();
            this.onPromoCodeChange(this.order.payment.promoCode);
            return false;
        }
    }

    public onBillingZipCodeFocus(event: any) {
        if(this.order.payment.zipCode == '') {
            this.order.payment.zipCode = this.order.delivery.zipCode;
            setTimeout((function(el, newValue) {
                let strLength = newValue.length;
                return function() {
                    if(el.setSelectionRange !== undefined) {
                        el.setSelectionRange(strLength, strLength);
                    } else {
                        el.value = '';
                        el.value = newValue;
                    }
                }}(event.target, this.order.delivery.zipCode)), 10);
        }
    }
}