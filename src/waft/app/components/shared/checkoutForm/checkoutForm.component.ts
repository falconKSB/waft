import {Component, Input, Output, EventEmitter, ViewChild} from '@angular/core';

import {RegistrationModel} from "../../../models/registration.model";
import {OrderModel} from "../../../models/order.model";
import {WaftAPIService} from "../../../services/waftapi.service";
import {AuthService} from "../../../services/auth.service";
import {NgForm} from "@angular/forms";
import {PhoneNumberModel} from '../../../models/phonenumber.model';

@Component({
    selector: 'checkout-form',
    templateUrl: 'checkoutForm.template.html',
    styleUrls: ['../forms.style.css', 'checkoutForm.style.css']
})
export class CheckoutFormComponent {
    @Input() order: OrderModel;
    @Input() submitCTA: string;
    @Output() onCountryChange = new EventEmitter();
    @Output() onDone = new EventEmitter();
    @Output() onLogin = new EventEmitter();
    @Output() save = new EventEmitter();

    @ViewChild('checkoutForm') checkoutForm: NgForm;

    public regData: RegistrationModel = new RegistrationModel();
    public showErrors: boolean = false;
    public message: string = "";
    public isProcessing: boolean = false;
    public isNotifyRecipient = true;
    public isProvideAddressYourself = false;
    public phone;

    constructor(public auth: AuthService,
                private api: WaftAPIService) {

    }

    public ngAfterViewInit() {
        this.phone = this.order.delivery.phoneNumber;
        if (this.auth.isAuth() && this.order.isLab()) {
            let user = this.auth.getCurrentUser();
            this.order.delivery.fullName = user.firstName + ' ' + user.lastName;
        }
        if(this.order.journey && this.order.journey.email)
            this.regData.email = this.order.journey.email;
    }

    public changeAddressOption(optionNumber: number): void {
        if (optionNumber == 1) {
            this.isProvideAddressYourself = false;
            this.isNotifyRecipient = true;
            this.order.delivery.fullName = "";
        }
        if (optionNumber == 2) {
            this.isProvideAddressYourself = true;
            this.isNotifyRecipient = false;
            if (this.auth.isAuth()) {
                let user = this.auth.getCurrentUser();
                this.order.delivery.fullName = user.firstName + ' ' + user.lastName;
            }
            else
                this.order.delivery.fullName = this.regData.firstName + ' ' + this.regData.lastName;
        }
    }

    public changePassword(password) {
        this.regData.password = password;
    }

    public changeState(stateCode) {
        this.order.delivery.county = stateCode;
    }

    public onSubmit() {
        this.showErrors = true;
        if (!this.isProcessing) {
            this.message = "";
            let fullAddress = this.order.isLab() || this.order.isGiftRedemption || this.isProvideAddressYourself;
            if (this.checkoutForm.invalid || (this.order.countryCode == '') || (this.order.delivery.country == '') ||
                (fullAddress && this.order.delivery.hasCounty() && (this.order.delivery.county == '')))
            {
                this.message = "Please correct the form errors";
                return false;
            }
            this.order.delivery.phoneNumber = PhoneNumberModel.GetNormalised(this.phone, this.order.countryCode);
            this.save.emit();
            this.message = 'Processing...';
            this.register();
            this.isProcessing = true;
        }
        return false;
    }

    public onChangeName() {
        let fullNameControl = this.checkoutForm.form.get('fullName');
        if ((!fullNameControl) || fullNameControl.touched) return;
        this.order.delivery.fullName = this.regData.firstName + ' ' + this.regData.lastName;
    }

    public changeCountry(event) {
        this.onCountryChange.emit(event);
        this.order.delivery.county = '';
        if (this.order.delivery.hasPostal() && this.checkoutForm.controls['zipCode'].dirty)
            this.checkoutForm.controls['zipCode'].reset();

        if (this.checkoutForm.controls['phoneNumber'].dirty)
            this.checkoutForm.controls['phoneNumber'].reset();
    }

    private createOrder(): void {
        if (this.order.isGiftRedemption) {
            this.processDelivery();
            return;
        }
        if (!this.auth.isAuth()) {
            this.message = "Please register or login";
            this.isProcessing = false;
            return;
        }
        this.api.createOrder(this.order).subscribe(
            data => {
                if (Object.keys(data).length != 0) {
                    this.order.id = data.orderId;
                    this.onDone.emit();
                }
                else {
                    this.isProcessing = false;
                    this.message = data.message || "Unknown Error";
                }
            },
            error => {
                this.isProcessing = false;
                this.message = error && error.message;
                if (!this.message || this.message == "") {
                    this.message = "Unknown error";
                }
            });
    };

    private register(): void {
        if (this.auth.isAuth()) {
            this.createOrder();
            return;
        }
        else this.auth.register(this.regData).subscribe(
            data => {
                if (data.success) {
                    this.createOrder();
                } else {
                    this.message = data.message || 'Unknown Error';
                    this.isProcessing = false;
                }
            },
            error => {
                this.message = 'Unknown Error';
                this.isProcessing = false;
            });
    }

    private processDelivery(): void {
        this.api.saveGiftDelivery(this.order).subscribe(
            data => {
                if (Object.keys(data).length != 0) {
                    this.onDone.emit();
                }
                else {
                    this.message = data.message || 'Unknown Error';
                    this.isProcessing = false;
                }
            },
            error => {
                this.message = 'Unknown Error';
                this.isProcessing = false;
            });
    }
}