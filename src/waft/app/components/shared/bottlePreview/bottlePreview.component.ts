import {Component, Input} from '@angular/core';

@Component({
    selector: 'bottle-preview',
    templateUrl: 'bottlePreview.template.html',
    styleUrls: ['./bottlePreview.style.css', '../bottleFonts.style.css']
})
export class BottlePreviewComponent {
    @Input() style: string;
    @Input() name: string;
    @Input() message: string;
    @Input() initials: string;
    @Input() mobile: boolean;

    public processText(s: string): string {
        return s;
    }
}