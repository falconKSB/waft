import {Component, Input, Output, EventEmitter} from '@angular/core';

import {AddressModel} from "../../../models/address.model";
import {StateModel} from "../../../models/country.model";

@Component({
    selector: 'state-select',
    templateUrl: 'stateSelect.template.html',
    styleUrls: ['../forms.style.css']
})
export class StateSelectComponent {
    @Input() address: AddressModel;
    @Input() showErrors: boolean;
    @Output() onStateChange = new EventEmitter();

    public states: Array<StateModel>;
    public isStateSelection: boolean = false;

    constructor() {
        this.states = StateModel.GetAll();
    }
}