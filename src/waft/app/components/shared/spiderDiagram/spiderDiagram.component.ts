import {Component, Input, ViewChild, OnChanges, SimpleChange} from '@angular/core';

import {WizardService} from "../../../services/wizard.service";
import {SpideRenderingService} from "../../../services/spide.service"
import {SpideModel} from '../../../models/spide.model'


@Component({
    selector: 'spider-diagram',
    templateUrl: 'spiderDiagram.template.html',
    styleUrls: ['spiderDiagram.style.css']
})
export class SpiderDiagramComponent implements OnChanges {
    constructor(public wizard: WizardService, public spide: SpideRenderingService) {
    }

    @Input() accords: any;
    @Input() fontSize: string;
    @Input() lineHeight: string;

    @ViewChild('layout') canvasRef;

    public spideType: string = "";
    public fontStyles: any;

    ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
        if (this.accords) {
            setTimeout(() => {
                this.fontStyles = {
                    'font-size': this.fontSize,
                    'line-height': this.lineHeight
                };
                this.spideType = SpideModel.GetSpideType(this.accords.length);
                this.spide.renderSpide(this.accords, this.canvasRef.nativeElement);
            });
        }
    }
}