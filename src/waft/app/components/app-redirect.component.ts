import { Component } from '@angular/core';
import {LocalStorageService} from "../services/localStorage.service";
import {Router} from "@angular/router";

@Component({
    template: ''
})
export class AppRedirectComponent {
    constructor(localStorage: LocalStorageService, router: Router) {
        router.navigate([localStorage.get('redirectUrl','/app/wizard/type')]);
    }
}