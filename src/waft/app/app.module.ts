import {NgModule, ApplicationRef} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule, Router} from '@angular/router';
// import {removeNgStyles, createNewHosts} from '@angularclass/hmr';
import {HttpModule, Http, XHRBackend, RequestOptions} from "@angular/http";
import {ModalModule} from "ng2-modal";

import {AuthConfig, AuthHttp} from 'angular2-jwt';

import {Cookie} from 'ng2-cookies/cookie';

import {AuthService} from './services/auth.service';
import {WizardService} from './services/wizard.service';
import {PreloaderService} from './services/preloader.service';

import {LayoutService} from './services/layout.service';
import {LocalStorageService} from './services/localStorage.service';

import {RemainingTimePipe} from './pipes/remaningTime.pipe'

import {PasswordFocusDirective} from './directives/passwordFocus.directive'



/*
 * Platform and Environment providers/directives/pipes
 */
import {ENV_PROVIDERS} from './environment';
import {ROUTES} from './app.routes';
// App is our top level component
import {AppComponent} from './app.component';

//import {WaftHeaderComponent} from './components/layout/waft-header/waft-header.component';
import {KioskComponent} from "./components/kiosk/kiosk.component";
import {MyJourneysComponent}    from "./components/profile/myJourneys/myJourneys.component";
import {JourneyCardComponent}   from "./components/profile/journeyCard/journeyCard.component";
import {OrderCardComponent}     from "./components/profile/orderCard/orderCard.component";
import {MyOrdersComponent}      from "./components/profile/myOrders/myOrders.component";
import {ReferralComponent}      from "./components/profile/referral/referral.component";
import {SharedModule} from './components/shared/index';
import {ProfileComponent} from './components/profile/profile.component';
import {ChangePrimaryFragranceComponent} from'./components/profile/orderCard/changePrimaryFragrance/changePrimaryFragrance.component';
import {CancelingPopupComponent} from'./components/profile/orderCard/canceling/cancelingPopup.component';
//import { SorryModalComponent } from './components/gift/landing/sorryModal/sorryModal.component';

import {APP_RESOLVER_PROVIDERS} from './app.resolver';
import {AppState} from './app.service';
import {GiftService} from './services/gift.service';
import {ProfileService} from './services/profile.service';
import {AuthGuard} from './guards/auth.guard';

import {WaftAPIService} from "./services/waftapi.service";

// static loading
import {WizardModule} from './components/wizard';
import {GiftModule} from './components/gift';
import {AccountModule} from './components/account';
import {AppRedirectComponent} from './components/app-redirect.component';
import {GTMService} from "./services/gtm.service";
import {LayoutModule} from "./components/layout/index";
import {KioskService} from "./services/kiosk.service";
import {ABTestService} from "./services/abtest.service";
import {ClipboardService} from "./services/clipboard.service";
import {HttpService} from "./services/http.service";
import {WizardGuard} from "./guards/wizard.guard";
import {GiftGuard} from "./guards/gift.guard";

// Application wide providers
const APP_PROVIDERS = [
    ...APP_RESOLVER_PROVIDERS,
    AppState
];

export function getToken() {
    var session = (new LocalStorageService()).getObject('waft_token');
    if (session)
        return session.accessToken;
    else
        return '';
}

export function authFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(new AuthConfig({
      headerName: 'Authorization',
      headerPrefix: '',
      tokenName: 'waft-token',
      tokenGetter: getToken,
      globalHeaders: [{'Content-Type': 'application/json'}],
      noJwtError: true,
      noTokenScheme: true
  }), http, options);
};

// Include this in your ngModule providers
export const authProvider = {
  provide: AuthHttp,
  deps: [Http, RequestOptions],
  useFactory: authFactory
};

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
    bootstrap: [AppComponent],
    declarations: [
        AppComponent,
        AppRedirectComponent,
        KioskComponent,
       // WaftHeaderComponent,
        ProfileComponent,
        MyOrdersComponent,
        MyJourneysComponent,
        ReferralComponent,
        JourneyCardComponent,
        OrderCardComponent,
        ChangePrimaryFragranceComponent,
        CancelingPopupComponent,
        PasswordFocusDirective,
        RemainingTimePipe
    ],
    imports: [ // import Angular's modules
        SharedModule,
        LayoutModule,
        BrowserModule,
        HttpModule,
        ModalModule,
        RouterModule.forRoot(ROUTES, {useHash: false}),
        WizardModule,
        GiftModule,
        AccountModule
    ],
    providers: [ // expose our Services and Providers into Angular's dependency injection
        AuthGuard,
        WizardGuard,
        GiftGuard,
        Cookie,
        LocalStorageService,
        LayoutService,
        WaftAPIService,
        WizardService,
        KioskService,
        HttpService,
        ABTestService,
        PreloaderService,
        GiftService,
        ProfileService,
        authProvider,
        AuthService,
        GTMService,
        ClipboardService,

        ENV_PROVIDERS,
        APP_PROVIDERS
    ]
})
export class AppModule {
    constructor(public appRef: ApplicationRef, public appState: AppState) {
    }
/* TODO ngc
    hmrOnInit(store) {
        if (!store || !store.state) return;
        this.appState._state = store.state;
        this.appRef.tick();
        delete store.state;
    }

    hmrOnDestroy(store) {
        const cmpLocation = this.appRef.components.map(cmp => cmp.location.nativeElement);
        // recreate elements
        const state = this.appState._state;
        store.state = state;
        store.disposeOldHosts = createNewHosts(cmpLocation);
        // remove styles
        removeNgStyles();
    }

    hmrAfterDestroy(store) {
        // display new elements
        store.disposeOldHosts();
        delete store.disposeOldHosts;
    }
*/
}
