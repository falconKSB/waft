export class CustomBottleModel {
    public initials: string = '';
    public name: string = '';
    public message: string = '';
    public style: string = 'modern';
}
