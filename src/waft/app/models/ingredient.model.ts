
export class IngredientModel {
    private static INGREDIENTS = [
        "fig",
        "chocolate",
        "rose",
        "powder",
        "white musk",
        "peach",
        "strawberry",
        "encens",
        "pink pepper",
        "vetiver",
        "ocean",
        "sesame",
        "mint",
        "leather",
        "green tea",
        "vanilla",
        "citrus",
        "caramel",
        "muguet",
        "jasmin",
        "violet",
        "orange blossom",
        "orris",
        "lily",
        "frangipani",
        "lavender",
        "pear",
        "black pepper",
        "cinnamon",
        "milk",
        "sandalwood",
        "patchouli",
        "cedarwood",
        "amber",
        "oud"
    ];

    public id: string;
    public description: string;
    public imageUrl: string;
    public name: string;
    public isSelect: boolean = false;

    public static GetAll() {
        var res = new Array<IngredientModel>();
        for (let i of IngredientModel.INGREDIENTS) {
            var m = new IngredientModel();
            m.name = i;
            m.imageUrl = "/assets/img/ingredients/" + i.replace(" ", "_") + ".jpg";
            res.push(m);
        }
        return res;
    }

    public static GetAllImageURLs() {
        var res = [];
        for (let i of IngredientModel.INGREDIENTS) {
            res.push("/assets/img/ingredients/" + i.replace(" ", "_") + ".jpg");
        }
        return res;
    }
}