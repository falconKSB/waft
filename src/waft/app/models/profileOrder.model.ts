export class ProfileOrderModel {
    constructor() { }

    public _id: string;
    public createdAt: Date;
    public status: string;
    public orderSize: string;
    public type: string;
    public amount: number;
    public currencySymbol: string;
}