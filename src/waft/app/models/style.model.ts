export class StyleModel {
    public static getAllStyles(): any {
        return [
            {
                name: "Modern",
                style: "modern"
            },
            {
                name: "Classic",
                style: "classic"
            },
            {
                name: "Handwritten",
                style: "handwritten"
            }
        ]
    }
}