export class OrderSizesByGenderModel {
    public man: string = "medium";
    public woman: string = "big";
    public unisex: string = "medium";
    public man_unisex: string = "medium";
    public woman_unisex: string = "big";

    constructor(sizes?: any) {
        if (sizes) {
            this.man = sizes.man;
            this.woman = sizes.woman;
            this.unisex = sizes.unisex;
            this.man_unisex = sizes.man_unisex;
            this.woman_unisex = sizes.woman_unisex;
        }
    }
}

export class ShippingModel {
    constructor(shipping?: any) {
        if (shipping) {
            this.big = shipping.big;
            this.medium = shipping.medium;
            this.small = shipping.small;
            this.samples = shipping.samples;
            this.min_days = shipping.min_days;
            this.max_days = shipping.max_days;
        }
    }

    public big: number;
    public medium: number;
    public small: number;
    public samples: number;
    public min_days: number;
    public max_days: number;

    public getShippingText(): string {
        return `Arrives in approx. ${this.min_days} business days`;
    }
}

export class PriceModel {
    public currency: string;
    public currencySymbol: string;
    public exchangeRate: number;
    public paymentAccount: string = "Stripe US";
    public big: number;
    public medium: number;
    public small: number;
    public samples: number;
    public shipping: ShippingModel;
    public orderSizesByGender: OrderSizesByGenderModel;

    constructor(price?: any) {
        if (price) {
            this.currency = price.currency;
            this.currencySymbol = price.currencySymbol;
            this.exchangeRate = price.exchangeRate;
            this.big = price.big;
            this.medium = price.medium;
            this.small = price.small;
            this.samples = price.samples;
            this.paymentAccount = price.paymentAccount;
            this.shipping = new ShippingModel(price.shipping);
            this.orderSizesByGender = new OrderSizesByGenderModel(price.orderSizesByGender)
        } else {
            this.shipping = new ShippingModel();
            this.orderSizesByGender = new OrderSizesByGenderModel();
        }
    }

    public toJSON() : any {
        return {
            currency: this.currency,
            currencySymbol: this.currencySymbol,
            exchangeRate: this.exchangeRate,
            paymentAccount: this.paymentAccount,
            big: this.big,
            medium: this.medium,
            small: this.small,
            samples: this.samples,
            shipping: this.shipping,
            orderSizesByGender: this.orderSizesByGender
        }
    }

    public getPriceString(size: string, decimals = true): string {
        return this.format(<number> this[size], decimals);
    }

    public getMainOrderSize(gender: string) : string {
        return this.orderSizesByGender[gender];
    }

    public getMainOrderVolume(size: string) : string {
        if (size == 'big') return '100ml';
        if (size == 'medium') return '50ml';
        return '';
    }

    public getShippingPriceString(size: string, decimals = true): string {
        if (this.isFreeShipping(size))
            return "FREE";
        else
            return this.format(<number> this.shipping[size], decimals);
    }

    public getShippingPrice(size: string): number {
        return <number> this.shipping[size];
    }

    public isFreeShipping(size: string): boolean {
        return this.shipping[size] == 0;
    }

    public getPromoDiscount(size: string, promoData): number {
        if (size == 'samples' || !promoData) return 0;
        else if (promoData.value_unit == 'value') return Number(promoData.value);
        else return this.getTotalPrice(size) * Number(promoData.value) / 100;
    }

    public getPromoDiscountString(size: string, promoData, decimals = true): string {
        return this.format(this.getPromoDiscount(size, promoData), decimals);
    }

    public getGrandTotal(size: string, promoData): number {
        return this.getTotalPrice(size) - this.getPromoDiscount(size, promoData);
    }

    public getGrandTotalUSD(size: string, promoData): number {
        return this.getGrandTotal(size, promoData) * this.exchangeRate;
    }

    public getGrandTotalString(size: string, promoData, decimals = true): string {
        return this.format(this.getGrandTotal(size, promoData));
    }

    public getTotalPrice(size: string): number {
        return <number> this[size] + <number> this.shipping[size];
    }

    public getTotalPriceString(size: string, decimals = true): string {
        return this.format(this.getTotalPrice(size), decimals);
    }

    public format(n: number, decimals = true): string {
        const places = ((!decimals) && (n % 1 == 0)) ? 0 : 2;
        return (n != null) ? this.currencySymbol + n.toFixed(places) : '';
    }
}

export class StateModel {
    public state: string;
    public stateCode: string;

    public static GetAll(): Array<StateModel> {
        const res = [];
        for (let i of StateModel.STATES) {
            const m = new StateModel();
            m.state = i.state;
            m.stateCode = i.stateCode;
            res.push(m);
        }
        return res;
    }

    private static STATES = [
        {
            stateCode: 'AL',
            state: 'Alabama'
        },
        // {
        //     stateCode: 'AK',
        //     state: 'Alaska'
        // },
        // {
        //     stateCode: 'AS',
        //     state: 'American Samoa'
        // },
        {
            stateCode: 'AZ',
            state: 'Arizona'
        },
        {
            stateCode: 'AR',
            state: 'Arkansas'
        },
        {
            stateCode: 'CA',
            state: 'California'
        },
        {
            stateCode: 'CO',
            state: 'Colorado'
        },
        {
            stateCode: 'CT',
            state: 'Connecticut'
        },
        {
            stateCode: 'DE',
            state: 'Delaware'
        },
        {
            stateCode: 'DC',
            state: 'Dist. of Columbia'
        },
        {
            stateCode: 'FL',
            state: 'Florida'
        },
        {
            stateCode: 'GA',
            state: 'Georgia'
        },
        // {
        //     stateCode: 'GU',
        //     state: 'Guam'
        // },
        // {
        //     stateCode: 'HI',
        //     state: 'Hawaii'
        // },
        {
            stateCode: 'ID',
            state: 'Idaho'
        },
        {
            stateCode: 'IL',
            state: 'Illinois'
        },
        {
            stateCode: 'IN',
            state: 'Indiana'
        },
        {
            stateCode: 'IA',
            state: 'Iowa'
        },
        {
            stateCode: 'KS',
            state: 'Kansas'
        },
        {
            stateCode: 'KY',
            state: 'Kentucky'
        },
        {
            stateCode: 'LA',
            state: 'Louisiana'
        },
        {
            stateCode: 'ME',
            state: 'Maine'
        },
        {
            stateCode: 'MD',
            state: 'Maryland'
        },
        // {
        //     stateCode: 'MH',
        //     state: 'Marshall Islands'
        // },
        {
            stateCode: 'MA',
            state: 'Massachusetts'
        },
        {
            stateCode: 'MI',
            state: 'Michigan'
        },
        // {
        //     stateCode: 'FM',
        //     state: 'Micronesia'
        // },
        {
            stateCode: 'MN',
            state: 'Minnesota'
        },
        {
            stateCode: 'MS',
            state: 'Mississippi'
        },
        {
            stateCode: 'MO',
            state: 'Missouri'
        },
        {
            stateCode: 'MT',
            state: 'Montana'
        },
        {
            stateCode: 'NE',
            state: 'Nebraska'
        },
        {
            stateCode: 'NV',
            state: 'Nevada'
        },
        {
            stateCode: 'NH',
            state: 'New Hampshire'
        },
        {
            stateCode: 'NJ',
            state: 'New Jersey'
        },
        {
            stateCode: 'NM',
            state: 'New Mexico'
        },
        {
            stateCode: 'NY',
            state: 'New York'
        },
        {
            stateCode: 'NC',
            state: 'North Carolina'
        },
        {
            stateCode: 'ND',
            state: 'North Dakota'
        },
        // {
        //     stateCode: 'MP',
        //     state: 'Northern Marianas'
        // },
        {
            stateCode: 'OH',
            state: 'Ohio'
        },
        {
            stateCode: 'OK',
            state: 'Oklahoma'
        },
        {
            stateCode: 'OR',
            state: 'Oregon'
        },
        // {
        //     stateCode: 'PW',
        //     state: 'Palau'
        // },
        {
            stateCode: 'PA',
            state: 'Pennsylvania'
        },
        // {
        //     stateCode: 'PR',
        //     state: 'Puerto Rico'
        // },
        {
            stateCode: 'RI',
            state: 'Rhode Island'
        },
        {
            stateCode: 'SC',
            state: 'South Carolina'
        },
        {
            stateCode: 'SD',
            state: 'South Dakota'
        },
        {
            stateCode: 'TN',
            state: 'Tennessee'
        },
        {
            stateCode: 'TX',
            state: 'Texas'
        },
        {
            stateCode: 'UT',
            state: 'Utah'
        },
        {
            stateCode: 'VT',
            state: 'Vermont'
        },
        {
            stateCode: 'VA',
            state: 'Virginia'
        },
        // {
        //     stateCode: 'VI',
        //     state: 'Virgin Islands'
        // },
        {
            stateCode: 'WA',
            state: 'Washington'
        },
        {
            stateCode: 'WV',
            state: 'West Virginia'
        },
        {
            stateCode: 'WI',
            state: 'Wisconsin'
        },
        {
            stateCode: 'WY',
            state: 'Wyoming'
        },
    ]
}

export class CountryModel {
    constructor(country?: any) {
        if (country && country.countryCode) {
            this.countryCode = country.countryCode.toUpperCase();
            this.country = CountryModel.countryName(country.countryCode);
            this.price = new PriceModel(country.price);
        } else {
            this.price = new PriceModel();
        }
    }

    public country: string;
    public countryCode: string;
    public price: PriceModel;

    public toJSON() : any {
        return {
            countryCode: this.countryCode,
            country: this.country,
            price: this.price.toJSON()
        }
    }

    public static GetAll(): Array<CountryModel> {
        CountryModel.COUNTRIES.sort(function (a, b) { // TODO: presort the countries and store sorted list in COUNTRIES
            const nameA = a.country.toLowerCase(), nameB = b.country.toLowerCase();
            return (nameA < nameB) ? -1 : (nameA == nameB ? 0 : 1);
        });
        const res = [];
        for (let i of CountryModel.COUNTRIES) {
            const m = new CountryModel();
            m.country = i.country;
            m.countryCode = i.countryCode;
            res.push(m);
        }
        return res;
    }

    public static GetByTerm(term: string): Array<CountryModel> {
        CountryModel.COUNTRIES.sort(function (a, b) { // TODO: presort the countries and store sorted list in COUNTRIES
            let nameA = a.country.toLowerCase(), nameB = b.country.toLowerCase();
            return (nameA < nameB) ? -1 : (nameA == nameB ? 0 : 1);
        });
        const res = [];
        for (let i of CountryModel.COUNTRIES) {
            if (i.country.toLowerCase().indexOf(term.toLowerCase()) != -1 || (!term)) {
                const m = new CountryModel();
                m.country = i.country;
                m.countryCode = i.countryCode;
                res.push(m);
            }
        }
        return res;
    }

    public static countryName(countryCode: string) {
        const s: string = countryCode.toLowerCase();
        for (let i of CountryModel.COUNTRIES) {
            if (i.countryCode.toLowerCase() == s)
                return i.country;
        }
        return "";
    }

    private static COUNTRIES = [
        // {
        //     countryCode: 'AD',
        //     country: 'Andorra'
        // },
        // {
        //     countryCode: 'AE',
        //     country: 'United Arab Emirates'
        // },
        // {
        //     countryCode: 'AF',
        //     country: 'Afghanistan'
        // },
        // {
        //     countryCode: 'AG',
        //     country: 'Antigua and Barbuda'
        // },
        // {
        //     countryCode: 'AI',
        //     country: 'Anguilla'
        // },
        // {
        //     countryCode: 'AL',
        //     country: 'Albania'
        // },
        // {
        //     countryCode: 'AM',
        //     country: 'Armenia'
        // },
        // {
        //     countryCode: 'AO',
        //     country: 'Angola'
        // },
        // {
        //     countryCode: 'AP',
        //     country: 'Asia/Pacific Region'
        // },
        // {
        //     countryCode: 'AQ',
        //     country: 'Antarctica'
        // },
        // {
        //     countryCode: 'AR',
        //     country: 'Argentina'
        // },
        // {
        //     countryCode: 'AS',
        //     country: 'American Samoa'
        // },
        {
            countryCode: 'AT',
            country: 'Austria'
        },
        {
            countryCode: 'AU',
            country: 'Australia'
        },
        // {
        //     countryCode: 'AW',
        //     country: 'Aruba'
        // },
        // {
        //     countryCode: 'AX',
        //     country: 'Aland Islands'
        // },
        // {
        //     countryCode: 'AZ',
        //     country: 'Azerbaijan'
        // },
        // {
        //     countryCode: 'BA',
        //     country: 'Bosnia and Herzegovina'
        // },
        // {
        //     countryCode: 'BB',
        //     country: 'Barbados'
        // },
        // {
        //     countryCode: 'BD',
        //     country: 'Bangladesh'
        // },
        {
            countryCode: 'BE',
            country: 'Belgium'
        },
        // {
        //     countryCode: 'BF',
        //     country: 'Burkina Faso'
        // },
        // {
        //     countryCode: 'BG',
        //     country: 'Bulgaria'
        // },
        // {
        //     countryCode: 'BH',
        //     country: 'Bahrain'
        // },
        // {
        //     countryCode: 'BI',
        //     country: 'Burundi'
        // },
        // {
        //     countryCode: 'BJ',
        //     country: 'Benin'
        // },
        // {
        //     countryCode: 'BL',
        //     country: 'Saint Bartelemey'
        // },
        // {
        //     countryCode: 'BM',
        //     country: 'Bermuda'
        // },
        // {
        //     countryCode: 'BN',
        //     country: 'Brunei Darussalam'
        // },
        // {
        //     countryCode: 'BO',
        //     country: 'Bolivia'
        // },
        // {
        //     countryCode: 'BQ',
        //     country: 'Bonaire, Saint Eustatius and Saba'
        // },
        // {
        //     countryCode: 'BR',
        //     country: 'Brazil'
        // },
        // {
        //     countryCode: 'BS',
        //     country: 'Bahamas'
        // },
        // {
        //     countryCode: 'BT',
        //     country: 'Bhutan'
        // },
        // {
        //     countryCode: 'BV',
        //     country: 'Bouvet Island'
        // },
        // {
        //     countryCode: 'BW',
        //     country: 'Botswana'
        // },
        // {
        //     countryCode: 'BY',
        //     country: 'Belarus'
        // },
        // {
        //     countryCode: 'BZ',
        //     country: 'Belize'
        // },
        // {
        //     countryCode: 'CA',
        //     country: 'Canada'
        // },
        // {
        //     countryCode: 'CC',
        //     country: 'Cocos (Keeling) Islands'
        // },
        // {
        //     countryCode: 'CD',
        //     country: 'Congo, The Democratic Republic of the'
        // },
        // {
        //     countryCode: 'CF',
        //     country: 'Central African Republic'
        // },
        // {
        //     countryCode: 'CG',
        //     country: 'Congo'
        // },
        {
            countryCode: 'CH',
            country: 'Switzerland'
        },
        // {
        //     countryCode: 'CI',
        //     country: 'Cote d\'Ivoire'
        // },
        // {
        //     countryCode: 'CK',
        //     country: 'Cook Islands'
        // },
        // {
        //     countryCode: 'CL',
        //     country: 'Chile'
        // },
        // {
        //     countryCode: 'CM',
        //     country: 'Cameroon'
        // },
        // {
        //     countryCode: 'CN',
        //     country: 'China'
        // },
        // {
        //     countryCode: 'CO',
        //     country: 'Colombia'
        // },
        // {
        //     countryCode: 'CR',
        //     country: 'Costa Rica'
        // },
        // {
        //     countryCode: 'CU',
        //     country: 'Cuba'
        // },
        // {
        //     countryCode: 'CV',
        //     country: 'Cape Verde'
        // },
        // {
        //     countryCode: 'CW',
        //     country: 'Curacao'
        // },
        // {
        //     countryCode: 'CX',
        //     country: 'Christmas Island'
        // },
        // {
        //     countryCode: 'CY',
        //     country: 'Cyprus'
        // },
        {
            countryCode: 'CZ',
            country: 'Czech Republic'
        },
        {
            countryCode: 'DE',
            country: 'Germany'
        },
        // {
        //     countryCode: 'DJ',
        //     country: 'Djibouti'
        // },
        {
            countryCode: 'DK',
            country: 'Denmark'
        },
        // {
        //     countryCode: 'DM',
        //     country: 'Dominica'
        // },
        // {
        //     countryCode: 'DO',
        //     country: 'Dominican Republic'
        // },
        // {
        //     countryCode: 'DZ',
        //     country: 'Algeria'
        // },
        // {
        //     countryCode: 'EC',
        //     country: 'Ecuador'
        // },
        // {
        //     countryCode: 'EE',
        //     country: 'Estonia'
        // },
        // {
        //     countryCode: 'EG',
        //     country: 'Egypt'
        // },
        // {
        //     countryCode: 'EH',
        //     country: 'Western Sahara'
        // },
        // {
        //     countryCode: 'ER',
        //     country: 'Eritrea'
        // },
        {
            countryCode: 'ES',
            country: 'Spain'
        },
        // {
        //     countryCode: 'ET',
        //     country: 'Ethiopia'
        // },
        // {
        //     countryCode: 'EU',
        //     country: 'Europe'
        // },
        {
            countryCode: 'FI',
            country: 'Finland'
        },
        // {
        //     countryCode: 'FJ',
        //     country: 'Fiji'
        // },
        // {
        //     countryCode: 'FK',
        //     country: 'Falkland Islands (Malvinas)'
        // },
        // {
        //     countryCode: 'FM',
        //     country: 'Micronesia, Federated States of'
        // },
        // {
        //     countryCode: 'FO',
        //     country: 'Faroe Islands'
        // },
        {
            countryCode: 'FR',
            country: 'France'
        },
        // {
        //     countryCode: 'GA',
        //     country: 'Gabon'
        // },
        {
            countryCode: 'GB',
            country: 'United Kingdom'
        },
        // {
        //     countryCode: 'GD',
        //     country: 'Grenada'
        // },
        // {
        //     countryCode: 'GE',
        //     country: 'Georgia'
        // },
        // {
        //     countryCode: 'GF',
        //     country: 'French Guiana'
        // },
        // {
        //     countryCode: 'GG',
        //     country: 'Guernsey'
        // },
        // {
        //     countryCode: 'GH',
        //     country: 'Ghana'
        // },
        // {
        //     countryCode: 'GI',
        //     country: 'Gibraltar'
        // },
        // {
        //     countryCode: 'GL',
        //     country: 'Greenland'
        // },
        // {
        //     countryCode: 'GM',
        //     country: 'Gambia'
        // },
        // {
        //     countryCode: 'GN',
        //     country: 'Guinea'
        // },
        // {
        //     countryCode: 'GP',
        //     country: 'Guadeloupe'
        // },
        // {
        //     countryCode: 'GQ',
        //     country: 'Equatorial Guinea'
        // },
        // {
        //     countryCode: 'GR',
        //     country: 'Greece'
        // },
        // {
        //     countryCode: 'GS',
        //     country: 'South Georgia and the South Sandwich Islands'
        // },
        // {
        //     countryCode: 'GT',
        //     country: 'Guatemala'
        // },
        // {
        //     countryCode: 'GU',
        //     country: 'Guam'
        // },
        // {
        //     countryCode: 'GW',
        //     country: 'Guinea-Bissau'
        // },
        // {
        //     countryCode: 'GY',
        //     country: 'Guyana'
        // },
        {
            countryCode: 'HK',
            country: 'Hong Kong'
        },
        // {
        //     countryCode: 'HM',
        //     country: 'Heard Island and McDonald Islands'
        // },
        // {
        //     countryCode: 'HN',
        //     country: 'Honduras'
        // },
        {
            countryCode: 'HR',
            country: 'Croatia'
        },
        // {
        //     countryCode: 'HT',
        //     country: 'Haiti'
        // },
        {
            countryCode: 'HU',
            country: 'Hungary'
        },
        {
            countryCode: 'ID',
            country: 'Indonesia'
        },
        // {
        //     countryCode: 'IE',
        //     country: 'Ireland'
        // },
        // {
        //     countryCode: 'IL',
        //     country: 'Israel'
        // },
        // {
        //     countryCode: 'IM',
        //     country: 'Isle of Man'
        // },
        {
            countryCode: 'IN',
            country: 'India'
        },
        // {
        //     countryCode: 'IO',
        //     country: 'British Indian Ocean Territory'
        // },
        // {
        //     countryCode: 'IQ',
        //     country: 'Iraq'
        // },
        // {
        //     countryCode: 'IR',
        //     country: 'Iran, Islamic Republic of'
        // },
        // {
        //     countryCode: 'IS',
        //     country: 'Iceland'
        // },
         {
            countryCode: 'IT',
            country: 'Italy'
        },
        // {
        //     countryCode: 'JE',
        //     country: 'Jersey'
        // },
        // {
        //     countryCode: 'JM',
        //     country: 'Jamaica'
        // },
        // {
        //     countryCode: 'JO',
        //     country: 'Jordan'
        // },
        // {
        //     countryCode: 'JP',
        //     country: 'Japan'
        // },
        // {
        //     countryCode: 'KE',
        //     country: 'Kenya'
        // },
        // {
        //     countryCode: 'KG',
        //     country: 'Kyrgyzstan'
        // },
        // {
        //     countryCode: 'KI',
        //     country: 'Kiribati'
        // },
        // {
        //     countryCode: 'KM',
        //     country: 'Comoros'
        // },
        // {
        //     countryCode: 'KN',
        //     country: 'Saint Kitts and Nevis'
        // },
        // {
        //     countryCode: 'KP',
        //     country: 'Korea, Democratic People\'s Republic of'
        // },
        // {
        //     countryCode: 'KR',
        //     country: 'Korea, Republic of'
        // },
        // {
        //     countryCode: 'KW',
        //     country: 'Kuwait'
        // },
        // {
        //     countryCode: 'KY',
        //     country: 'Cayman Islands'
        // },
        // {
        //     countryCode: 'KZ',
        //     country: 'Kazakhstan'
        // },
        // {
        //     countryCode: 'LA',
        //     country: 'Lao People\'s Democratic Republic'
        // },
        // {
        //     countryCode: 'LB',
        //     country: 'Lebanon'
        // },
        // {
        //     countryCode: 'LC',
        //     country: 'Saint Lucia'
        // },
        {
            countryCode: 'LI',
            country: 'Liechtenstein'
        },
        // {
        //     countryCode: 'LK',
        //     country: 'Sri Lanka'
        // },
        // {
        //     countryCode: 'LR',
        //     country: 'Liberia'
        // },
        // {
        //     countryCode: 'LS',
        //     country: 'Lesotho'
        // },
        // {
        //     countryCode: 'LT',
        //     country: 'Lithuania'
        // },
        {
            countryCode: 'LU',
            country: 'Luxembourg'
        },
        // {
        //     countryCode: 'LV',
        //     country: 'Latvia'
        // },
        // {
        //     countryCode: 'LY',
        //     country: 'Libyan Arab Jamahiriya'
        // },
        // {
        //     countryCode: 'MA',
        //     country: 'Morocco'
        // },
        {
            countryCode: 'MC',
            country: 'Monaco'
        },
        // {
        //     countryCode: 'MD',
        //     country: 'Moldova, Republic of'
        // },
        // {
        //     countryCode: 'ME',
        //     country: 'Montenegro'
        // },
        // {
        //     countryCode: 'MF',
        //     country: 'Saint Martin'
        // },
        // {
        //     countryCode: 'MG',
        //     country: 'Madagascar'
        // },
        // {
        //     countryCode: 'MH',
        //     country: 'Marshall Islands'
        // },
        // {
        //     countryCode: 'MK',
        //     country: 'Macedonia'
        // },
        // {
        //     countryCode: 'ML',
        //     country: 'Mali'
        // },
        // {
        //     countryCode: 'MM',
        //     country: 'Myanmar'
        // },
        // {
        //     countryCode: 'MN',
        //     country: 'Mongolia'
        // },
        // {
        //     countryCode: 'MO',
        //     country: 'Macao'
        // },
        // {
        //     countryCode: 'MP',
        //     country: 'Northern Mariana Islands'
        // },
        // {
        //     countryCode: 'MQ',
        //     country: 'Martinique'
        // },
        // {
        //     countryCode: 'MR',
        //     country: 'Mauritania'
        // },
        // {
        //     countryCode: 'MS',
        //     country: 'Montserrat'
        // },
        // {
        //     countryCode: 'MT',
        //     country: 'Malta'
        // },
        // {
        //     countryCode: 'MU',
        //     country: 'Mauritius'
        // },
        // {
        //     countryCode: 'MV',
        //     country: 'Maldives'
        // },
        // {
        //     countryCode: 'MW',
        //     country: 'Malawi'
        // },
        // {
        //     countryCode: 'MX',
        //     country: 'Mexico'
        // },
        {
            countryCode: 'MY',
            country: 'Malaysia'
        },
        // {
        //     countryCode: 'MZ',
        //     country: 'Mozambique'
        // },
        // {
        //     countryCode: 'NA',
        //     country: 'Namibia'
        // },
        // {
        //     countryCode: 'NC',
        //     country: 'New Caledonia'
        // },
        // {
        //     countryCode: 'NE',
        //     country: 'Niger'
        // },
        // {
        //     countryCode: 'NF',
        //     country: 'Norfolk Island'
        // },
        // {
        //     countryCode: 'NG',
        //     country: 'Nigeria'
        // },
        // {
        //     countryCode: 'NI',
        //     country: 'Nicaragua'
        // },
        {
            countryCode: 'NL',
            country: 'Netherlands'
        },
        // {
        //     countryCode: 'NO',
        //     country: 'Norway'
        // },
        // {
        //     countryCode: 'NP',
        //     country: 'Nepal'
        // },
        // {
        //     countryCode: 'NR',
        //     country: 'Nauru'
        // },
        // {
        //     countryCode: 'NU',
        //     country: 'Niue'
        // },
        {
            countryCode: 'NZ',
            country: 'New Zealand'
        },
        // {
        //     countryCode: 'OM',
        //     country: 'Oman'
        // },
        // {
        //     countryCode: 'PA',
        //     country: 'Panama'
        // },
        // {
        //     countryCode: 'PE',
        //     country: 'Peru'
        // },
        // {
        //     countryCode: 'PF',
        //     country: 'French Polynesia'
        // },
        // {
        //     countryCode: 'PG',
        //     country: 'Papua New Guinea'
        // },
        // {
        //     countryCode: 'PH',
        //     country: 'Philippines'
        // },
        // {
        //     countryCode: 'PK',
        //     country: 'Pakistan'
        // },
        {
            countryCode: 'PL',
            country: 'Poland'
        },
        // {
        //     countryCode: 'PM',
        //     country: 'Saint Pierre and Miquelon'
        // },
        // {
        //     countryCode: 'PN',
        //     country: 'Pitcairn'
        // },
        // {
        //     countryCode: 'PR',
        //     country: 'Puerto Rico'
        // },
        // {
        //     countryCode: 'PS',
        //     country: 'Palestinian Territory'
        // },
        {
            countryCode: 'PT',
            country: 'Portugal'
        },
        // {
        //     countryCode: 'PW',
        //     country: 'Palau'
        // },
        // {
        //     countryCode: 'PY',
        //     country: 'Paraguay'
        // },
        // {
        //     countryCode: 'QA',
        //     country: 'Qatar'
        // },
        // {
        //     countryCode: 'RE',
        //     country: 'Reunion'
        // },
        // {
        //     countryCode: 'RO',
        //     country: 'Romania'
        // },
        // {
        //     countryCode: 'RS',
        //     country: 'Serbia'
        // },
        // {
        //     countryCode: 'RU',
        //     country: 'Russian Federation'
        // },
        // {
        //     countryCode: 'RW',
        //     country: 'Rwanda'
        // },
        // {
        //     countryCode: 'SA',
        //     country: 'Saudi Arabia'
        // },
        // {
        //     countryCode: 'SB',
        //     country: 'Solomon Islands'
        // },
        // {
        //     countryCode: 'SC',
        //     country: 'Seychelles'
        // },
        // {
        //     countryCode: 'SD',
        //     country: 'Sudan'
        // },
        {
            countryCode: 'SE',
            country: 'Sweden'
        },
        {
            countryCode: 'SG',
            country: 'Singapore'
        },
        // {
        //     countryCode: 'SH',
        //     country: 'Saint Helena'
        // },
        {
            countryCode: 'SI',
            country: 'Slovenia'
        },
        // {
        //     countryCode: 'SJ',
        //     country: 'Svalbard and Jan Mayen'
        // },
        {
            countryCode: 'SK',
            country: 'Slovakia'
        },
        // {
        //     countryCode: 'SL',
        //     country: 'Sierra Leone'
        // },
        // {
        //     countryCode: 'SM',
        //     country: 'San Marino'
        // },
        // {
        //     countryCode: 'SN',
        //     country: 'Senegal'
        // },
        // {
        //     countryCode: 'SO',
        //     country: 'Somalia'
        // },
        // {
        //     countryCode: 'SR',
        //     country: 'Suriname'
        // },
        // {
        //     countryCode: 'SS',
        //     country: 'South Sudan'
        // },
        // {
        //     countryCode: 'ST',
        //     country: 'Sao Tome and Principe'
        // },
        // {
        //     countryCode: 'SV',
        //     country: 'El Salvador'
        // },
        // {
        //     countryCode: 'SX',
        //     country: 'Sint Maarten'
        // },
        // {
        //     countryCode: 'SY',
        //     country: 'Syrian Arab Republic'
        // },
        // {
        //     countryCode: 'SZ',
        //     country: 'Swaziland'
        // },
        // {
        //     countryCode: 'TC',
        //     country: 'Turks and Caicos Islands'
        // },
        // {
        //     countryCode: 'TD',
        //     country: 'Chad'
        // },
        // {
        //     countryCode: 'TF',
        //     country: 'French Southern Territories'
        // },
        // {
        //     countryCode: 'TG',
        //     country: 'Togo'
        // },
        // {
        //     countryCode: 'TH',
        //     country: 'Thailand'
        // },
        // {
        //     countryCode: 'TJ',
        //     country: 'Tajikistan'
        // },
        // {
        //     countryCode: 'TK',
        //     country: 'Tokelau'
        // },
        // {
        //     countryCode: 'TL',
        //     country: 'Timor-Leste'
        // },
        // {
        //     countryCode: 'TM',
        //     country: 'Turkmenistan'
        // },
        // {
        //     countryCode: 'TN',
        //     country: 'Tunisia'
        // },
        // {
        //     countryCode: 'TO',
        //     country: 'Tonga'
        // },
        // {
        //     countryCode: 'TR',
        //     country: 'Turkey'
        // },
        // {
        //     countryCode: 'TT',
        //     country: 'Trinidad and Tobago'
        // },
        // {
        //     countryCode: 'TV',
        //     country: 'Tuvalu'
        // },
        // {
        //     countryCode: 'TW',
        //     country: 'Taiwan'
        // },
        // {
        //     countryCode: 'TZ',
        //     country: 'Tanzania, United Republic of'
        // },
        // {
        //     countryCode: 'UA',
        //     country: 'Ukraine'
        // },
        // {
        //     countryCode: 'UG',
        //     country: 'Uganda'
        // },
        // {
        //     countryCode: 'UM',
        //     country: 'United States Minor Outlying Islands'
        // },
        {
            countryCode: 'US',
            country: 'United States'
        },
        // {
        //     countryCode: 'UY',
        //     country: 'Uruguay'
        // },
        // {
        //     countryCode: 'UZ',
        //     country: 'Uzbekistan'
        // },
        // {
        //     countryCode: 'VA',
        //     country: 'Holy See (Vatican City State)'
        // },
        // {
        //     countryCode: 'VC',
        //     country: 'Saint Vincent and the Grenadines'
        // },
        // {
        //     countryCode: 'VE',
        //     country: 'Venezuela'
        // },
        // {
        //     countryCode: 'VG',
        //     country: 'Virgin Islands, British'
        // },
        // {
        //     countryCode: 'VI',
        //     country: 'Virgin Islands, U.S.'
        // },
        // {
        //     countryCode: 'VN',
        //     country: 'Vietnam'
        // },
        // {
        //     countryCode: 'VU',
        //     country: 'Vanuatu'
        // },
        // {
        //     countryCode: 'WF',
        //     country: 'Wallis and Futuna'
        // },
        // {
        //     countryCode: 'WS',
        //     country: 'Samoa'
        // },
        // {
        //     countryCode: 'YE',
        //     country: 'Yemen'
        // },
        // {
        //     countryCode: 'YT',
        //     country: 'Mayotte'
        // },
        // {
        //     countryCode: 'ZA',
        //     country: 'South Africa'
        // },
        // {
        //     countryCode: 'ZM',
        //     country: 'Zambia'
        // },
        // {
        //     countryCode: 'ZW',
        //     country: 'Zimbabwe'
        // }
    ]
}
