export class FragranceCardModel {

    constructor(id: string, code: string) {
        this.id = id;
        this.code = code;
    }

    public id: string;
    public code: string;
}