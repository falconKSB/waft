export class GiftRecipientModel {
    public firstName: string = '';
    public lastName: string = '';
    public email: string = '';
}
