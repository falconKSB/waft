import {AddressModel} from "./address.model";
import {CustomBottleModel} from "./customBottleModel";
import {GiftRecipientModel} from "./giftRecepient.model";

export class ReceivePageModel {
    key: string;
    customBottle: CustomBottleModel;
    recipient: GiftRecipientModel;
    giftSenderName: string;
    delivery: AddressModel;
    countryCode: string;
}
