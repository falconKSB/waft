export class PaymentModel {
    public constructor(obj?: any) {
        if (!obj) return;
        this.promoCode = obj.promoCode;
    }

    public cardNumber: string = '';
    public expireDate: string = '';
    public cvCode: string = '';
    public zipCode: string = '';
    public promoCode: string = '';
    public promoData: any = null;

    public isValidPromoCode() : boolean {
        return !((this.promoCode != '') && (this.promoData == null))
    }
}
