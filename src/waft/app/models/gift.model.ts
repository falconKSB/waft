import {GiftRecipientModel} from "./giftRecepient.model";

export class GiftModel {
    public senderName: string = '';
    public note: string = '';
    public inviteEmailDate: Date = new Date();
    public emailFromUser: boolean = false;
    public recipient: GiftRecipientModel = new GiftRecipientModel();
 }
