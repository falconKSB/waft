export class PhoneNumberModel {
    public static GetNormalised(phoneNumber, countryCode): string {
        let result;
        switch (countryCode) {
            case 'US':
                let phoneDigit = phoneNumber.replace(/\D/g, '');
                if (phoneDigit[0] != 1) {
                    phoneDigit = '1' + phoneDigit;
                }
                result = '+' + phoneDigit;
                break;
            default:
                result = phoneNumber
                break;
        }
        return result;
    }

    public static Validate(phoneNumber, countryCode): boolean {
        let phoneRe: RegExp = /^[\d\\ +\-()\s]+$/
        if (phoneNumber == '') return null;
        let phoneDigit = phoneNumber.replace(/\D/g, '');
        let result;
        switch (countryCode) {
            case 'US':

                if (phoneDigit[0] != 1) {
                    phoneDigit = '1' + phoneDigit;
                }
                let y = phoneDigit[1];
                let x = phoneDigit.substring(2, phoneDigit.length - 1);

                result = phoneRe.test(phoneDigit) && phoneDigit.length == 11 && /^[2-9]*$/.test(y) && /^[0-9]*$/.test(x);
                break;
            default:
                result = phoneRe.test(phoneDigit) && phoneDigit.length >= 8;
                break;
        }
        return result;
    }
}