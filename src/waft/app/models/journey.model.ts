import {CustomBottleModel} from "./customBottleModel";

export class JourneyModel {

    constructor() {
        this.ingredients = new Array<string>();
        this.baseFragrances = new Array<string>();
        this.customBottle = new CustomBottleModel();
    }

    public _id: string;

    public step: string;

    public email: string;

    public recipientGender: string;
    public gender: string;
    public timeOfDay: string;
    public activity: string;
    public mood: string;
    public personalStyle: string;
    public waft: string;

    public gift: boolean|undefined;

    public baseFragrances: Array<string>;
    public primaryFragrance: string;

    public ingredients: Array<string>;

    public customBottle: CustomBottleModel;

    public user: string;

    public createdAt: Date;
}