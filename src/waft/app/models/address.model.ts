import {CountryModel} from "./country.model";

export class AddressModel {
    public fullName: string = '';
    public addressLine1: string = '';
    public addressLine2: string = '';
    public city: string = '';
    public county: string = '';
    public zipCode: string = '';
    public phoneNumber: string = '';
    public country: string = '';
    public countryObj: CountryModel;

    constructor(obj?: any) {
        if (!obj) obj = {};
        this.fullName = obj.fullName || '';
        this.addressLine1 = obj.addressLine1 || '';
        this.addressLine2 = obj.addressLine2 || '';
        this.city = obj.city || '';
        this.county = obj.county || '';
        this.zipCode = obj.zipCode || '';
        this.phoneNumber = obj.phoneNumber || '';
        this.countryObj = new CountryModel(obj.countryObj) || new CountryModel();
        this.country = this.countryObj.country;
    }

    public setCountry(countryObj: CountryModel): void {
        let previousCountryCode = this.countryObj.countryCode;
        this.countryObj = countryObj;
        this.country = countryObj.country;
        if (previousCountryCode == countryObj.countryCode) return;
        if (previousCountryCode == 'SG') {
            this.county = '';
            this.city = '';
        }
        if (countryObj.countryCode == 'SG') {
            this.county = 'Singapore';
            this.city = 'Singapore';
        }
        if (!this.hasPostal())
            this.zipCode = '';
        if (!this.hasCounty())
            this.county = '';
    }

    public getCityLabel(): string {
        if (this.countryObj.countryCode == 'HK')
            return 'District';
        if (this.countryObj.countryCode == 'SG')
            return null;
        return 'City / Town'
    }

    public getCountyLabel(): string {
        if ((this.countryObj.countryCode == 'AU') || (this.countryObj.countryCode == 'MY') ||
            (this.countryObj.countryCode == 'US') || (this.countryObj.countryCode == 'IN'))
            return 'State';
        if (this.countryObj.countryCode == 'NZ')
            return 'Suburb';
        if (this.countryObj.countryCode == 'CA')
            return 'Province';
        return 'Region';
    }

    public getPostal(): string {
        if (this.countryObj.countryCode == 'US')
            return 'Zip Code';
        return 'Postal Code';
    }

    public hasCity(): boolean {
        return this.countryObj.countryCode != 'SG';
    }

    public hasCounty(): boolean {
        return (this.countryObj.countryCode != 'GB') && (this.countryObj.countryCode != 'SG');
    }

    public hasPostal(): boolean {
        return this.countryObj.countryCode != 'HK';
    }

    public hasBillingPostal(): boolean {
        return (this.countryObj.countryCode == 'US') || (this.countryObj.countryCode == 'GB') ||
            (this.countryObj.countryCode == 'CA')
    }

    public getPaymentAccount(): string {
        if (this.countryObj && this.countryObj.price.paymentAccount && (this.countryObj.price.paymentAccount !== ''))
            return this.countryObj.price.paymentAccount;
        else return "Stripe US"
    }

    public toAPIObject() : any {
        return {
            fullName: this.fullName,
            addressLine1: this.addressLine1,
            addressLine2: this.addressLine2,
            city: this.city,
            county: this.county,
            zipCode: this.zipCode,
            phoneNumber: this.phoneNumber,
            country: this.country
        };
    }
}