import {CustomPackagingModel} from "./customPackagingModel";
import {GiftModel} from "./gift.model";
import {AddressModel} from "./address.model";
import {CustomBottleModel} from "./customBottleModel";
import {JourneyModel} from "./journey.model";
import {PaymentModel} from "./payment.model";
import {CountryModel} from "./country.model";
import {WaftAPIService} from "../services/waftapi.service";

const VALID_GENDERS = ['man', 'woman', 'unisex', 'man_unisex', 'woman_unisex'];

export class OrderModel {
    public id: string = '';
    public type: string;
    public orderSize: string;
    public countryCode: string = '';
    public gift: GiftModel;
    public customBottle = new CustomBottleModel();
    public customPackaging: CustomPackagingModel;
    public promoCode: string = '';
    public delivery: AddressModel;
    public journey: JourneyModel;
    public payment = new PaymentModel();
    public gender: string;
    public isGiftRedemption: boolean = false;
    public salesRep: string;
    public preferredSample: string;
    public primaryFragrance: any;
    public yourFragrances: Array<any> = [];
    public repeatOrderRef: string;

    public constructor(private api: WaftAPIService, obj?: any) {
        if (!obj) return;
        this.id = obj.id;
        this.type = obj.type;
        this.orderSize = obj.orderSize;
        this.countryCode = obj.countryCode;
        this.gift = obj.gift;
        this.customBottle = obj.customBottle;
        this.customPackaging = obj.customPackaging;
        this.promoCode = obj.promoCode;
        this.delivery = new AddressModel(obj.delivery);
        this.gender = obj.gender;
        this.isGiftRedemption = obj.isGiftRedemption;
        this.salesRep = obj.salesRep;
        this.preferredSample = obj.preferredSample;
        this.primaryFragrance = obj.primaryFragrance;
        this.yourFragrances = obj.yourFragrances;
        this.repeatOrderRef = obj.repeatOrderRef;
        this.payment = new PaymentModel(obj.payment);
        this.setJourney(obj.journey);
    }

    public static CreateLabOrder(api: WaftAPIService) : OrderModel {
        let order = new OrderModel(api);
        order.journey = new JourneyModel();
        order.delivery = new AddressModel();
        order.customPackaging = new CustomPackagingModel();
        order.type = 'WaftLab';
        order.yourFragrances = [];
        return order;
    }

    public static CreateGiftOrder(api: WaftAPIService) : OrderModel {
        let order = new OrderModel(api);
        order.gift = new GiftModel();
        order.delivery = new AddressModel();
        order.customPackaging = new CustomPackagingModel();
        order.type = 'WaftGift';
        order.orderSize = 'medium';
        order.yourFragrances = [];
        return order;
    }

    public isGift() : boolean {
        return this.type == 'WaftGift'
    }

    public isLab() : boolean {
        return this.type == 'WaftLab'
    }

    public static IsValidGender(gender : string) : boolean {
        return VALID_GENDERS.indexOf(gender) !== -1;
    }
    
    public setCountry(country: CountryModel) {
        this.countryCode = country.countryCode;
        this.delivery.setCountry(country);
        this.setOrderSize();
    }

    public setGender() {
        this.gender = this.getGender();
    }

    public getGender() : string {
        if (!this.journey) return null;
        if (this.journey.recipientGender == "None")
            this.journey.recipientGender = null;
        let gender = this.journey.recipientGender || this.journey.gender;
        if (this.primaryFragrance && OrderModel.IsValidGender(this.primaryFragrance.gender)) {
            if (this.primaryFragrance.gender == 'unisex') {
                return gender == 'unisex'? gender : (gender+'_unisex');
            }
            else return this.primaryFragrance.gender;
        }
        if ((this.journey.gender == 'unisex') && (gender != 'unisex'))
            gender += '_unisex';
        return gender;
    }

    setOrderSize() {
        if (this.type == 'WaftGift') {
            this.orderSize = 'medium';
            return;
        }
        if (this.orderSize == 'samples') return;
        this.orderSize = this.getMainOrderSize();
    }

    public getMainOrderSize() : string {
        return this.delivery.countryObj.price.getMainOrderSize(this.getGender());
    }

    public getMainOrderVolume() : string {
        return this.delivery.countryObj.price.getMainOrderVolume(this.getMainOrderSize());
    }

    public setJourney(journey: JourneyModel) {
        if (!journey) {
            journey = new JourneyModel();
        }
        this.journey = journey;
        this.setOrderSize();
        this.setGender();
        this.initYourFragrances(journey.baseFragrances);
        this.initPrimaryFragrance(journey.primaryFragrance);
    }

    getFragranceObj(fragrance: any) : any {
        return {
            gender: fragrance.gender || (fragrance.male?(fragrance.female?'unisex':'man'):'woman'),
            name: fragrance.name,
            accords: fragrance.accords,
            _id: fragrance._id
        }
    }

    public setPrimaryFragrance(fragrance: any) {
        if (!fragrance) {
            this.journey.primaryFragrance = null;
            this.primaryFragrance = null;
            return;
        }
        this.journey.primaryFragrance = fragrance._id;
        this.primaryFragrance = this.getFragranceObj(fragrance);
        this.setGender();
    }

    private initYourFragrances(listOfIds: Array<string>): void {
        listOfIds = listOfIds.filter(id => this.findFragranceById(id) == -1, this);
        if (listOfIds.length == 0) return;
        this.api.getFragrances(null, listOfIds).subscribe(
            fragrances => {
                this.yourFragrances = [];
                this.journey.baseFragrances = [];
                fragrances.forEach(this.addFragrance, this);
            });
    }

    private initPrimaryFragrance(fragranceId?: string): void {
        if (!fragranceId) return;
        if (this.primaryFragrance && (fragranceId == this.primaryFragrance._id)) return;
        this.api.getFragrances(null, [fragranceId]).subscribe(
            fragrances => {
                this.setPrimaryFragrance(fragrances[0]);
            });
    }

    public toJSON() : any {
        return {
            id: this.id,
            type: this.type,
            orderSize: this.orderSize,
            countryCode: this.countryCode,
            gift: this.gift,
            customBottle: this.customBottle,
            customPackaging: this.customPackaging,
            promoCode: this.promoCode,
            delivery: this.delivery,
            journey: this.journey,
            payment: this.payment,
            gender: this.gender,
            isGiftRedemption: this.isGiftRedemption,
            salesRep: this.salesRep,
            preferredSample: this.preferredSample,
            primaryFragrance: this.primaryFragrance,
            yourFragrances: this.yourFragrances,
            repeatOrderRef: this.repeatOrderRef
        };
    }

    public addFragrance(fragrance: any) {
         if (this.findFragranceById(fragrance._id) == -1)
            this.yourFragrances.push(this.getFragranceObj(fragrance));
        if (this.journey.baseFragrances.indexOf(fragrance._id) == -1)
            this.journey.baseFragrances.push(fragrance._id);
    }

    public removeFragrance(fragrance: any) {
        let idx = this.findFragranceById(fragrance._id);
        if (idx != -1)
            this.yourFragrances.splice(idx, 1);
        idx = this.journey.baseFragrances.indexOf(fragrance._id);
        if (idx != -1)
            this.journey.baseFragrances.splice(idx, 1);
    }

    findFragranceById(id: any) {
        return this.yourFragrances.findIndex(f => f._id == id);
    }

    public getPriceString() : string {
        return this.delivery.countryObj.price.getPriceString(this.orderSize);
    }

    public getGiftURL() : string {
        let url = (<any>window).location.origin;
        return url + "/app/gift/receive/" + this.id;
    }
}

