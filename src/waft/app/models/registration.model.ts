export class RegistrationModel {
    public firstName: string = "";
    public lastName: string = "";
    public email: string = "";
    public password: string = "";

    public toBody() : string {
        return JSON.stringify({
            firstName : this.firstName,
            lastName : this.lastName,
            email : this.email,
            password: this.password })
    }
}