export class User {
    constructor() {
        this.id = '';
        this.email = '';
        this.firstName = '';
        this.lastName = '';
    }

    public id: string;
    public email: string;
    public firstName: string;
    public lastName: string;
}


export class SessionModel {
    constructor() {
        this.isAuth = false;
        this.currentUser = new User();
    }

    accessToken: string;
    tokenType: string;
    currentUser: User;
    isAuth: boolean;
}