export class SpideModel {

    private static TriangleSpide = [{x: 77, y: 15},
        {x: 155, y: 133},
        {x: 4, y: 133}];

    private static SquareSpide = [
        {x: 78, y: 13},
        {x: 155, y: 91},
        {x: 76, y: 167},
        {x: 4, y: 91}];

    private static PentagonSpide = [{x: 78, y: 6},
        {x: 154, y: 65},
        {x: 126, y: 154},
        {x: 30, y: 154},
        {x: 4, y: 65}];

    private static HexagonSpide = [
        {x: 78.5, y: 4},
        {x: 153, y: 45},
        {x: 153, y: 135},
        {x: 74.5, y: 176},
        {x: 4, y: 135},
        {x: 4, y: 45},
    ];

    private pointsOfPeaks;

    private getPointsOfPeaks(numbersOfNotes: number) {

        switch (numbersOfNotes) {
            case 6:
                this.pointsOfPeaks = SpideModel.HexagonSpide;
                break;
            case 5:
                this.pointsOfPeaks = SpideModel.PentagonSpide;
                break;
            case 4:
                this.pointsOfPeaks = SpideModel.SquareSpide;
                break;
            case  3:
                this.pointsOfPeaks = SpideModel.TriangleSpide;
                break;
            case  2:
                this.pointsOfPeaks = SpideModel.TriangleSpide;
                break;
        }
    }


    private getCharPoint(i: number, weight: number) {

        var commonLength = Math.sqrt((Math.pow(78.5 - this.pointsOfPeaks[i].x, 2) + Math.pow(90 - this.pointsOfPeaks[i].y, 2)));
        var length = weight * commonLength;
        var ratio;
        weight == 1 ? ratio = commonLength : ratio = length / (commonLength - length);

        var x = (78.5 + (ratio * this.pointsOfPeaks[i].x)) / (1 + ratio);
        var y = (90 + (ratio * this.pointsOfPeaks[i].y)) / (1 + ratio);

        return {x: x, y: y};
    }

    public static GetChartLineСoordinates(accords: Array<any>): any {

        var m = new SpideModel();
        m.getPointsOfPeaks(accords.length);

        var result = [];
        for (var i = 0; i < accords.length; i++) {
            var weight = (i >= accords.length) ? 0 : accords[i].weight;
            result.push(m.getCharPoint(i, weight));
        }
        if (accords.length == 2) result.push(m.getCharPoint(2, 0));
        return result;
    }

    public static GetSpideType(numbersOfNotes: number): string {
        switch (numbersOfNotes) {
            case 6:
                return 'Hexagon';
            case 5:
                return 'Pentagon';
            case 4:
                return 'Square';
            case 3:
                return 'Triangle';
            case 2:
                return 'Triangle';
        }
    }
}