/**
 * Created by Alex on 10/20/2016.
 */
import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'remainingTime'
})

export class RemainingTimePipe implements PipeTransform {
    transform(value: any, args?: any[]): string {
        if (!value) return "";

        return this.showRemaining(value);
    }

    showRemaining(value: any): string {
        let initialDate = new Date(value);
        let currentDate = new Date();

        let _second = 1000;
        let _minute = _second * 60;
        let _hour = _minute * 60;
        let _day = _hour * 24;

        let dateDiff = (+initialDate + (10 * _day)) - (+currentDate);

        if (dateDiff < 0) return '0 HOURS';

        let days = Math.floor(dateDiff / _day);
        let hours = Math.floor((dateDiff % _day) / _hour);
        let minutes = Math.floor((dateDiff % _hour) / _minute);

        if (dateDiff < _hour) return minutes + ' ' + (minutes == 1?'MINUTE':'MINUTES');
        if (dateDiff < _day) return hours + ' ' + (hours == 1?'HOUR':'HOURS');
        return days + ' ' + (days == 1 ?'DAY':'DAYS');
    }
}
