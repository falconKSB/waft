import {Injectable} from '@angular/core';
import {IngredientModel} from "../models/ingredient.model";
import {Router} from "@angular/router";

declare var CDN: string;

@Injectable()
export class PreloaderService {
    preloadedAssets: any = {};
    paths = {
        '/app/wizard/landing': {
            resources: [
                '/assets/img/wizard/orderType/for_gift.jpg',
                '/assets/img/wizard/orderType/for_me.jpg'
            ],
            preload: [
                '/app/wizard/order-design',
                '/app/wizard/gender'
            ]
        },
        '/app/wizard/type': {
            resources: [
                '/assets/img/wizard/orderType/for_gift.jpg',
                '/assets/img/wizard/orderType/for_me.jpg'
            ],
            preload: [
                '/app/wizard/order-design',
                '/app/wizard/gender'
            ]
        },
        '/app/wizard/order-design': {
            resources: [
                '/assets/img/wizard/orderDesign/design_yourself.jpg',
                '/assets/img/wizard/orderDesign/let_them_design.jpg'
            ],
            preload: [
                '/app/wizard/gender',
                '/app/gift/customise-bottle',
                '/app/wizard/type',
                '/app/wizard/fragrance-type'
            ]
        },
        '/app/wizard/gender': {
            resources: [
                '/assets/img/wizard/recipientGender/man.jpg',
                '/assets/img/wizard/recipientGender/woman.jpg'
            ],
            preload: [
                '/app/wizard/type',
                '/app/gift/customise-bottle',
                '/app/wizard/time-of-day',
                '/app/wizard/order-design'
            ]
        },
        '/app/wizard/fragrance-type' : {
            resources: [
                '/assets/img/wizard/gender/man.jpg',
                '/assets/img/wizard/gender/woman.jpg',
                '/assets/img/wizard/gender/unisex.jpg'
            ],
            preload: [
                '/app/wizard/time-of-day',
                '/app/wizard/activity',
                '/app/wizard/gender'
            ]
        },
        '/app/wizard/time-of-day' : {
            resources: [
                '/assets/img/wizard/timeofday/day.jpg',
                '/assets/img/wizard/timeofday/night.jpg'
            ],
            preload: [
                '/app/wizard/activity',
                '/app/wizard/mood',
                '/app/wizard/type'
            ]
        },
        '/app/wizard/activity' : {
            resources: [
                '/assets/img/wizard/activity/sport-mobile.jpg',
                '/assets/img/wizard/activity/social-mobile.jpg',
                '/assets/img/wizard/activity/work-mobile.jpg',
                '/assets/img/wizard/activity/dating-mobile.jpg',
                '/assets/img/wizard/activity/sport.jpg',
                '/assets/img/wizard/activity/social.jpg',
                '/assets/img/wizard/activity/work.jpg',
                '/assets/img/wizard/activity/dating.jpg'
            ],
            preload: [
                '/app/wizard/mood',
                '/app/wizard/personalstyle',
                '/app/wizard/time-of-day'
            ]
        },
        '/app/wizard/mood' : {
            resources: [
                '/assets/img/wizard/mood/elegant-mobile.jpg',
                '/assets/img/wizard/mood/fresh-mobile.jpg',
                '/assets/img/wizard/mood/relaxed-mobile.jpg',
                '/assets/img/wizard/mood/sexy-mobile.jpg',
                '/assets/img/wizard/mood/elegant.jpg',
                '/assets/img/wizard/mood/fresh.jpg',
                '/assets/img/wizard/mood/relaxed.jpg',
                '/assets/img/wizard/mood/sexy.jpg'
            ],
            preload: [
                '/app/wizard/personalstyle',
                '/app/wizard/waft',
                '/app/wizard/activity'
            ]
        },
        '/app/wizard/personalstyle' : {
            resources: [
                '/assets/img/wizard/personalstyle/classic.jpg',
                '/assets/img/wizard/personalstyle/trendy.jpg'
            ],
            preload: [
                '/app/wizard/waft',
                '/app/wizard/ingredients',
                '/app/wizard/mood'
            ]
        },
        '/app/wizard/waft' : {
            resources: [
                '/assets/img/wizard/waft/balanced-mobile.jpg',
                '/assets/img/wizard/waft/daring-mobile.jpg',
                '/assets/img/wizard/waft/subtle-mobile.jpg',
                '/assets/img/wizard/waft/balanced.jpg',
                '/assets/img/wizard/waft/daring.jpg',
                '/assets/img/wizard/waft/subtle.jpg'
            ],
            preload: [
                '/app/wizard/ingredients',
                '/app/wizard/fragrances-choice',
                '/app/wizard/personalstyle'
            ]
        },
        '/app/wizard/ingredients' : {
            resources: IngredientModel.GetAllImageURLs(),
            preload: [
                '/app/wizard/fragrances-choice',
                '/app/wizard/customise-bottle',
                '/app/wizard/waft'
            ]
        },
        '/app/wizard/fragrances-choice' : {
            resources: [
                '/assets/img/wizard/labels/small/classic.png',
                '/assets/img/wizard/labels/small/dating.png',
                '/assets/img/wizard/labels/small/day.png',
                '/assets/img/wizard/labels/small/elegant.png',
                '/assets/img/wizard/labels/small/fresh.png',
                '/assets/img/wizard/labels/small/man.png',
                '/assets/img/wizard/labels/small/night.png',
                '/assets/img/wizard/labels/small/relaxed.png',
                '/assets/img/wizard/labels/small/sexy.png',
                '/assets/img/wizard/labels/small/social.png',
                '/assets/img/wizard/labels/small/sport.png',
                '/assets/img/wizard/labels/small/trendy.png',
                '/assets/img/wizard/labels/small/unisex.png',
                '/assets/img/wizard/labels/small/woman.png',
                '/assets/img/wizard/labels/small/work.png',
                '/assets/img/wizard/fragrancesChoice/search.svg',
                '/assets/img/close.svg',
                '/assets/img/wizard/fragrancesChoice/spide6.png',
                '/assets/img/wizard/fragrancesChoice/spide5.png',
                '/assets/img/wizard/fragrancesChoice/spide4.png',
                '/assets/img/wizard/fragrancesChoice/spide3.png'
            ],
            preload: [
                '/app/wizard/customise-bottle',
                '/app/wizard/the-new-you',
                '/app/wizard/ingredients'
            ]
        },
        '/app/wizard/customise-bottle' : {
            resources: [
                '/assets/img/bottle/big.jpg',
                '/assets/img/close.svg',
                '/assets/img/wizard/theNewYou3/hand.jpg',
                '/assets/img/wizard/theNewYou3/mini_bottles.jpg',
                '/assets/img/wizard/theNewYou3/val.jpg',
                '/assets/img/wizard/theNewYou3/wheel.jpg'
            ],
            preload: [
                '/app/wizard/the-new-you',
                '/app/wizard/fragrances-choice'
            ]
        },
        '/app/wizard/the-new-you' : {
            resources: [
                '/assets/img/wizard/theNewYou3/bottle-15ml.png',
                '/assets/img/wizard/theNewYou3/bottle-100ml.png',
                '/assets/img/wizard/theNewYou3/white_bg.jpg',
                '/assets/img/wizard/theNewYou3/parallax/cap.png',
                '/assets/img/wizard/theNewYou3/parallax/spray.png',
                '/assets/img/wizard/theNewYou3/parallax/inner-crew.png',
                '/assets/img/wizard/theNewYou3/parallax/water.png',
                '/assets/img/wizard/theNewYou3/parallax/bottle.png',
                '/assets/img/wizard/theNewYou3/play.svg',
                '/assets/img/wizard/theNewYou3/leader.png',
                '/assets/img/wizard/theNewYou3/xoxo.png',
                '/assets/img/wizard/theNewYou3/star.svg',
                '/assets/img/wizard/theNewYou3/bbottle.svg',
                '/assets/img/wizard/theNewYou3/sbottle.svg',
                '/assets/img/wizard/theNewYou3/payment.png',
                '/assets/img/wizard/theNewYou3/order-bg.jpg'
            ],
            preload: [
                '/app/wizard/login',
                '/app/wizard/checkout',
                '/app/wizard/customise-bottle'
            ]
        },
        '/app/wizard/login' : {
            resources: [
                '/assets/img/logo/waft.png',
                '/assets/img/elements/after-element.svg',
                '/assets/img/elements/facebook.svg',
                '/assets/img/elements/red-arr.svg'
            ],
            preload: [
                '/app/wizard/checkout',
                '/app/wizard/payment',
                '/app/wizard/the-new-you'
            ]
        },
        '/app/wizard/checkout' : {
            resources: [
                '/assets/img/wizard_checkout/bag-white.svg',
                '/assets/img/wizard_checkout/gift-gray.svg',
                '/assets/img/wizard_checkout/bag-gray.svg',
                '/assets/img/wizard_checkout/gift-white.svg'
            ],
            preload: [
                '/app/wizard/payment',
                '/app/wizard/thank-you'
            ]
        },
        '/app/gift/customise-bottle' : {
            resources: [
                '/assets/img/bottle/big.jpg',
                '/assets/img/close.svg'
            ],
            preload: [
                '/app/gift/personalisation',
                '/app/wizard/login',
                '/app/wizard/gender'
            ]
        },
        '/app/gift/personalisation' : {
            resources: [
                '/assets/img/close.svg',
                '/assets/img/logo/waftgift_white.png',
                '/assets/img/logo/waft.png',
                '/assets/img/gift/emailSetup/bottles.png',
                '/assets/img/gift/emailSetup/email-bg.jpg',
                '/assets/img/gift/emailSetup/email-bg2.jpg',
                '/assets/img/gift/emailSetup/left-sh.jpg',
                '/assets/img/gift/emailSetup/right-sh.jpg'
            ],
            preload: [
                '/app/wizard/login',
                '/app/wizard/payment',
                '/app/gift/thank-you'
            ]
        },
        '/app/wizard/payment' : {
            resources: [
                '/assets/img/wizard_checkout/payment.jpg',
                '/assets/img/logo/guarantee.png',
                '/assets/img/wizard_checkout/checkbox.svg',
                '/assets/img/wizard_checkout/checkbox_checked.svg'
            ],
            preload: [
                '/app/wizard/thank-you'
            ]
        },
        '/app/wizard/gift-delivery' : {
            resources: [
            ],
            preload: [
                '/app/wizard/type',
                '/app/wizard/time-of-day'
            ]
        },
        '/app/profile' : {
            resources: [
            ],
            preload: [
                '/app/gift/customise-bottle',
                '/app/wizard/type'
            ]
        }
    };

    constructor(router: Router) {
        router.events.subscribe((path) => {
            let p = this.paths[path.url];
            if (!p) return;
            if (p.resources && (p.resources.length > 0)) p.resources = [];
            if (p.preload && (p.preload.length > 0)) {
                for (let pp of p.preload) {
                    this.preload(pp);
                }
                p.preload = []
            }
        });
    }

    preload(path: string) {
        let p = this.paths[path];
        if (!p) return;
        if ((!p.resources) || (p.resources.length == 0)) return;
        for (let url of p.resources) {
            if (!(url in this.preloadedAssets)) {
                this.preloadedAssets[url] = new Image();
                this.preloadedAssets[url].src = this.cdn(url);
            }
        }
    }

    public cdn(path: string) {
        return CDN + path;
    }

    public loadCDNScript(path: string) {
        let node = document.createElement('script');
        node.src = this.cdn(path);
        node.type = 'text/javascript';
        node.async = true;
        node.charset = 'utf-8';
        document.getElementsByTagName('head')[0].appendChild(node);
    }
}
