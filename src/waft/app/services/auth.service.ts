import {Injectable, Inject, ReflectiveInjector} from '@angular/core';
import {Http, Headers, Response, RequestOptions, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Observable';

import {Cookie} from 'ng2-cookies/cookie';
import {LocalStorageService} from './localStorage.service';
import {SessionModel, User} from '../models/session.model';
import {AuthHttp, JwtHelper} from 'angular2-jwt';
import {Router} from "@angular/router";
import {RegistrationModel} from "../models/registration.model";

@Injectable()
export class AuthService {

    constructor(private localStorage: LocalStorageService, public http: AuthHttp, private router: Router) {
        this.session = new SessionModel();
        this.sessionKey = "waft_token";
        this.restoreSession();
    }

    sessionKey: string;

    session: SessionModel;

    private jwtHelper: JwtHelper = new JwtHelper();

    public updateSession(principal) {
        this.session.isAuth = true;
        this.session.accessToken = principal.data.accessToken;
        this.session.currentUser = principal.data.currentUser;
        this.localStorage.setObject(this.sessionKey, this.session);
    }

    public getCurrentUser(): User {
        return this.isAuth() ? this.session.currentUser : null;
    }

    public getCurrentUserId(): string {
        return this.isAuth() ? this.session.currentUser.id : null;
    }

    private destroy() {
        this.session = new SessionModel();
        this.localStorage.setObject(this.sessionKey, null);
    };

    public getToken() {
        try {
            let session = this.localStorage.getObject('waft_token');
            if (session) {
                return session.accessToken;
            }
        }
        catch (e) {
        }
        return false;
    }

    isAuth(): boolean {
        return this.getToken() && !this.jwtHelper.isTokenExpired(this.getToken());
    }

    logout() {
        this.destroy();
    }

    // TODO: move to API service
    login(email: string, password: string): Observable<any> {
        let body = JSON.stringify({email, password});
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});

        return this.http.post('/api/auth/login', body, options)
            .map((res: Response) => {
                let body = res.json();
                if (body.success) {
                    this.updateSession(body)
                }
                return body;
            })
    }

    register(regData : RegistrationModel): Observable<any> {
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});

        return this.http.post('/api/auth/register', regData.toBody(), options)
            .map((res: Response) => {
                let body = res.json();
                if (body.success) {
                    this.updateSession(body)
                }
                return body;
            })
    }

    registerationCheck(email: string) {
        let headers = new Headers({'Content-Type': 'application/json'});
        let params: URLSearchParams = new URLSearchParams();
        params.set('email', email);
        let options = new RequestOptions({headers: headers, search: params});

        return this.http.get('/api/auth/register/check', options)
            .map((res: Response) => {
                return res.json();
            })
    }

    restorePassword(email: string): Observable<any> {
        let body = JSON.stringify({email});
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});

        return this.http.post('/api/auth/password/restore', body, options)
            .map((res: Response) => {
                return res.json();
            })
    }

    restoreSession() {
        try {
            let c = Cookie.get('authData');
            if (c) {
                Cookie.delete('authData');
                let authData = JSON.parse(c);
                this.updateSession(authData);
                return;
            }
            let storedSession = this.localStorage.getObject(this.sessionKey);
            if (storedSession && storedSession != 'null') this.session = storedSession;
        }
        catch (e) {
        }
    }

    private extractData(res: Response) {
        let body = res.json();
        return body.data || {};
    }

    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

    public facebookConnect() {
        let url = this.router.url;
        if (url.indexOf("/app/wizard/login") == 0) {
            this.localStorage.set('redirectUrl', "/app/wizard/checkout");
        }
        else if (url.indexOf("/app/wizard/the-new-you") == 0) {
            this.localStorage.set('redirectUrl', url);
        }
        else if (url.indexOf("/app/gift/login") == 0) {
            this.localStorage.set('redirectUrl', "/app/gift/checkout");
        }
        else if (url.indexOf("/app/account/login") == 0) {
            url = this.localStorage.get('redirectUrl', '/app/profile');
            if (url.indexOf("/app/profile") != 0)
                url = '/app/profile';
            this.localStorage.set('redirectUrl', url);
        }
        (<any>window).location.href = '/auth/facebook';
    }
}
