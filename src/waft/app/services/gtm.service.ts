import {Injectable} from '@angular/core';
import {Router} from "@angular/router";
import {CountryModel} from "../models/country.model";
import {AuthService} from "./auth.service";
import {OrderModel} from "../models/order.model";
import {KioskService} from "./kiosk.service";
import {HttpService} from "./http.service";

@Injectable()
export class GTMService {
    constructor(router: Router, private auth: AuthService, private kiosk: KioskService, private http: HttpService) {
        router.events.subscribe((path) => {
            this.gtmVirtualPageView(path.url);
        });
    }

    private lastGTMURL = null;
    private images: Array<any> = [];

    gtmVirtualPageView(url) : void {
        if (this.lastGTMURL == url) return;
        if (this.lastGTMURL == null) {
            this.lastGTMURL = url; // skip first page load
            return;
        }
        if (this.kiosk.isEnabled() && (url == '/app/wizard/type')) {
            this.send({
                'event' : 'KioskNewSession',
                'salesRep' : this.kiosk.getSalesRep(),
                'virtualPageURL': url
            });
            return;
        }
        this.lastGTMURL = url;
        this.send({
            'event':'VirtualPageview',
            'virtualPageURL': url
        });
    }

    kioskNewSession() : void {
    }

    send(data: any) : void {
        if (this.auth.isAuth()) {
            let email = this.auth.getCurrentUser().email;
            if (email.endsWith('waft.com'))
                return; // Don't send Waft.com staff orders
            data['userId'] = this.auth.getCurrentUserId();
            data['userEmail'] = email;
        }
        this.http.post('/api/event', data).subscribe(data => {}, error => {});
        let dataLayer = (<any>window).dataLayer;
        if (dataLayer) dataLayer.push(data);
    }

    ecommerceEvent(event: string, ecommerceEvent: string, country: CountryModel, productCategory: string,
                   orderSizes: string[], actionField: any) : void {
        let data = {
            event: event,
            ecommerce: {
                currencyCode: country.price.currency ? country.price.currency.toUpperCase() : '',
            }
        };
        let products = [];
        for (let i = 0; i < orderSizes.length; i++) {
            let orderSize = orderSizes[i];
            products.push({
                name: orderSize,
                category: productCategory,
                price: country.price.getTotalPrice(orderSize),
                quantity: 1
            })
        }
        data.ecommerce[ecommerceEvent] = {
            products: products,
            actionField: actionField
        };
        this.send(data);
    }

    public event(event: string) : void {
        this.send({event: event});
    }

    public addToCart(event: string, country: CountryModel, productCategory: string, orderSize: string) : void {
        this.ecommerceEvent(event, 'add', country, productCategory, [orderSize], { })
    }

    public productDetail(event: string, country: CountryModel, productCategory: string, orderSizes: string[]) : void {
        this.ecommerceEvent(event, 'detail', country, productCategory, orderSizes, { list: event })
    }

    public checkout(event: string, country: CountryModel, productCategory: string, orderSize: string, checkoutStep = 1) : void {
        this.ecommerceEvent(event, 'checkout', country, productCategory, [orderSize], {
            step: checkoutStep,
            option: event
        })
    }

    public purchase(event: string, country: CountryModel, order: OrderModel) : void {
        let grandTotal = country.price.getGrandTotal(order.orderSize, order.payment.promoData);
        if (grandTotal == 0) return; // don't send orders that are free of charge
        this.ecommerceEvent(event, 'purchase', country, order.isGift()?'gift':'lab', [order.orderSize], {
            id: order.id,
            revenue: grandTotal,
            shipping: country.price.getShippingPrice(order.orderSize),
            coupon: order.payment.promoCode
        })
    }

    public trackReferralRock(orderId : string) {
        let user = this.auth.getCurrentUser();
        let name = user.firstName + ' ' + user.lastName;
        let i = new Image();
        i.src = 'https://waft.referralrock.com/webcallback/?refID=' + encodeURIComponent(orderId)
            + '&name=' + encodeURIComponent(name) + '&email=' + encodeURIComponent(user.email);
        this.images.push(i);
    }
}
