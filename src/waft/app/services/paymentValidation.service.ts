import { Injectable } from '@angular/core';
// import * as $ from 'jQuery';

declare var $:any;

@Injectable()
export class PaymentValidationService {

    constructor() { }

    public isValidCardholderName: boolean = true;
    public isValidCardNumber: boolean = true;
    public isValidExpireDate: boolean = true;
    public isValidCvCode: boolean = true;

    public CardholderError: string = '';
    public CardNumberError: string = '';
    public ExpireDateError: string = '';
    public CvCodeError: string = '';

    private defaultFormat: RegExp = /(\d{1,4})/g;

    private cards: any = [
        {
            type: 'elo',
            patterns: [401178, 401179, 431274, 438935, 451416, 457393, 457631, 457632, 504175, 506699, 5067, 509, 627780, 636297, 636368, 650, 6516, 6550],
            format: this.defaultFormat,
            length: [16],
            cvcLength: [3],
            luhn: true
        }, {
            type: 'maestro',
            patterns: [5018, 502, 503, 506, 56, 58, 639, 6220, 67],
            format: this.defaultFormat,
            length: [12, 13, 14, 15, 16, 17, 18, 19],
            cvcLength: [3],
            luhn: true
        }, {
            type: 'forbrugsforeningen',
            patterns: [600],
            format: this.defaultFormat,
            length: [16],
            cvcLength: [3],
            luhn: true
        }, {
            type: 'dankort',
            patterns: [5019],
            format: this.defaultFormat,
            length: [16],
            cvcLength: [3],
            luhn: true
        }, {
            type: 'visa',
            patterns: [4],
            format: this.defaultFormat,
            length: [13, 16],
            cvcLength: [3],
            luhn: true
        }, {
            type: 'mastercard',
            patterns: [51, 52, 53, 54, 55, 22, 23, 24, 25, 26, 27],
            format: this.defaultFormat,
            length: [16],
            cvcLength: [3],
            luhn: true
        }, {
            type: 'amex',
            patterns: [34, 37],
            format: /(\d{1,4})(\d{1,6})?(\d{1,5})?/,
            length: [15],
            cvcLength: [3, 4],
            luhn: true
        }, {
            type: 'dinersclub',
            patterns: [30, 36, 38, 39],
            format: /(\d{1,4})(\d{1,6})?(\d{1,4})?/,
            length: [14],
            cvcLength: [3],
            luhn: true
        }, {
            type: 'discover',
            patterns: [60, 64, 65, 622],
            format: this.defaultFormat,
            length: [16],
            cvcLength: [3],
            luhn: true
        }, {
            type: 'unionpay',
            patterns: [62, 88],
            format: this.defaultFormat,
            length: [16, 17, 18, 19],
            cvcLength: [3],
            luhn: false
        }, {
            type: 'jcb',
            patterns: [35],
            format: this.defaultFormat,
            length: [16],
            cvcLength: [3],
            luhn: true
        }
    ];

    private __slice = [].slice;
    private __indexOf = [].indexOf || function (item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

    public validateCardHolder(name) {

        if (name == '') {
            this.isValidCardholderName = false;
            return false;
        }
        this.isValidCardholderName = true;
        return true;

    }

    public validateCardNumber(num) {

        let card, _ref;

        if (num == '') {
            this.isValidCardNumber = false;
        }

        num = (num + '').replace(/\s+|-/g, '');
        if (!/^\d+$/.test(num)) {
            this.isValidCardNumber = false;
            return false;
        }
        card = this.cardFromNumber(num);
        if (!card) {
            this.isValidCardNumber = false;
            return false;
        }
        this.isValidCardNumber = (_ref = num.length, this.__indexOf.call(card.length, _ref) >= 0) && (card.luhn === false || this.luhnCheck(num));
        return this.isValidCardNumber;
    };

    public validateCardExpiry(month, year) {
        let currentTime, expiry, _ref;
        if (typeof month === 'object' && 'month' in month) {
            _ref = month, month = _ref.month, year = _ref.year;
        }
        if (!(month && year)) {
            this.isValidExpireDate = false;
            return false;
        }
        month = month.trim();
        year = year.trim();
        if (!/^\d+$/.test(month)) {
            this.isValidExpireDate = false;
            return false;
        }
        if (!/^\d+$/.test(year)) {
            this.isValidExpireDate = false;
            return false;
        }
        if (!((1 <= month && month <= 12))) {
            this.isValidExpireDate = false;
            return false;
        }
        if (year.length === 2) {
            if (year < 70) {
                year = "20" + year;
            } else {
                year = "19" + year;
            }
        }
        if (year.length !== 4) {
            this.isValidExpireDate = false;
            return false;
        }
        expiry = new Date(year, month);
        currentTime = new Date;
        expiry.setMonth(expiry.getMonth() - 1);
        expiry.setMonth(expiry.getMonth() + 1, 1);
        this.isValidExpireDate = expiry > currentTime;
        return this.isValidExpireDate;
    };

    public validateCardCVC(cvc, type) {
        var card, _ref;
        cvc = cvc.trim();
        if (!/^\d+$/.test(cvc)) {
            this.isValidCvCode = false;
            return false;
        }
        card = this.cardFromType(type);
        if (card != null) {
            _ref = cvc.length;
            this.isValidCvCode = this.__indexOf.call(card.cvcLength, _ref) >= 0;
            return this.isValidCvCode;
        } else {
            this.isValidCvCode = cvc.length >= 3 && cvc.length <= 4;
            return this.isValidCvCode; 
        }
    };


    public cardFromNumber(num) {
        let card, p, pattern, _i, _j, _len, _len1, _ref;
        num = (num + '').replace(/\D/g, '');
        for (_i = 0, _len = this.cards.length; _i < _len; _i++) {
            card = this.cards[_i];
            _ref = card.patterns;
            for (_j = 0, _len1 = _ref.length; _j < _len1; _j++) {
                pattern = _ref[_j];
                p = pattern + '';
                if (num.substr(0, p.length) === p) {
                    return card;
                }
            }
        }
    };

    private cardFromType(type) {
        var card, _i, _len;
        for (_i = 0, _len = this.cards.length; _i < _len; _i++) {
            card = this.cards[_i];
            if (card.type === type) {
                return card;
            }
        }
    };

    public formatBackCardNumber(e) {
        let $target, value;
        $target = $(e.currentTarget);
        value = $target.val();
        if (e.which !== 8) {
            return;
        }
        if (($target.prop('selectionStart') != null) && $target.prop('selectionStart') !== value.length) {
            return;
        }
        if (/\d\s$/.test(value)) {
            e.preventDefault();
            return setTimeout(function () {
                return $target.val(value.replace(/\d\s$/, ''));
            });
        } else if (/\s\d?$/.test(value)) {
            e.preventDefault();
            return setTimeout(function () {
                return $target.val(value.replace(/\d$/, ''));
            });
        }
    };

    public formatBackExpiry(e) {
        let $target, value;
        $target = $(e.currentTarget);
        value = $target.val();
        if (e.which !== 8) {
            return;
        }
        if (($target.prop('selectionStart') != null) && $target.prop('selectionStart') !== value.length) {
            return;
        }
        if (/\d\s\/\s$/.test(value)) {
            e.preventDefault();
            return setTimeout(function () {
                return $target.val(value.replace(/\d\s\/\s$/, ''));
            });
        }
    };

    public formatCardNumber(e) {
        let $target, card, digit, length, re, upperLength, value;
        digit = String.fromCharCode(e.which);
        if (!/^\d+$/.test(digit)) {
            return;
        }
        $target = $(e.currentTarget);
        value = $target.val();
        card = this.cardFromNumber(value + digit);
        length = (value.replace(/\D/g, '') + digit).length;
        upperLength = 16;
        if (card) {
            upperLength = card.length[card.length.length - 1];
        }
        if (length >= upperLength) {
            return;
        }
        if (($target.prop('selectionStart') != null) && $target.prop('selectionStart') !== value.length) {
            return;
        }
        if (card && card.type === 'amex') {
            re = /^(\d{4}|\d{4}\s\d{6})$/;
        } else {
            re = /(?:^|\s)(\d{4})$/;
        }
        if (re.test(value)) {
            e.preventDefault();
            return setTimeout(function () {
                return $target.val(value + ' ' + digit);
            });
        } else if (re.test(value + digit)) {
            e.preventDefault();
            return setTimeout(function () {
                return $target.val(value + digit + ' ');
            });
        }
    };

    private luhnCheck(num) {
        let digit, digits, odd, sum, _i, _len;
        odd = true;
        sum = 0;
        digits = (num + '').split('').reverse();
        for (_i = 0, _len = digits.length; _i < _len; _i++) {
            digit = digits[_i];
            digit = parseInt(digit, 10);
            if ((odd = !odd)) {
                digit *= 2;
            }
            if (digit > 9) {
                digit -= 9;
            }
            sum += digit;
        }
        return sum % 10 === 0;
    };
    /*
    private hasTextSelected($target) {
            let _ref;
            if (($target.prop('selectionStart') != null) && $target.prop('selectionStart') !== $target.prop('selectionEnd')) {
                return true;
            }
            if ((typeof document !== "undefined" && document !== null ? (_ref = document.selection) != null ? _ref.createRange : void 0 : void 0) != null) {
                if (document.selection.createRange().text) {
                    return true;
                }
            }
            return false;
        };*/

    private safeVal(value, $target) {
        let currPair, cursor, digit, error, last, prevPair;
        try {
            cursor = $target.prop('selectionStart');
        } catch (_error) {
            error = _error;
            cursor = null;
        }
        last = $target.val();
        $target.val(value);
        if (cursor !== null && $target.is(":focus")) {
            if (cursor === last.length) {
                cursor = value.length;
            }
            if (last !== value) {
                prevPair = last.slice(cursor - 1, +cursor + 1 || 9e9);
                currPair = value.slice(cursor - 1, +cursor + 1 || 9e9);
                digit = value[cursor];
                if (/\d/.test(digit) && prevPair === ("" + digit + " ") && currPair === (" " + digit)) {
                    cursor = cursor + 1;
                }
            }
            $target.prop('selectionStart', cursor);
            return $target.prop('selectionEnd', cursor);
        }
    };

    private replaceFullWidthChars(str) {
        let chars, chr, fullWidth, halfWidth, idx, value, _i, _len;
        if (str == null) {
            str = '';
        }
        fullWidth = '\uff10\uff11\uff12\uff13\uff14\uff15\uff16\uff17\uff18\uff19';
        halfWidth = '0123456789';
        value = '';
        chars = str.split('');
        for (_i = 0, _len = chars.length; _i < _len; _i++) {
            chr = chars[_i];
            idx = fullWidth.indexOf(chr);
            if (idx > -1) {
                chr = halfWidth[idx];
            }
            value += chr;
        }
        return value;
    };

    private reFormatNumeric(e) {
        let $target;
        let self = this;
        $target = $(e.currentTarget);
        return setTimeout(function () {
            var value;
            value = $target.val();
            value = self.replaceFullWidthChars(value);
            value = value.replace(/\D/g, '');
            return self.safeVal(value, $target);
        });
    };

    public reFormatCardNumber(e) {
        let $target;
        let self = this;
        $target = $(e.currentTarget);
        return setTimeout(function () {
            var value;
            value = $target.val();
            value = self.replaceFullWidthChars(value);
            value = self.formatCardNumber(value);
            return self.safeVal(value, $target);
        });
    };


    private reFormatExpiry(e) {
        var $target;
        let self = this;
        $target = $(e.currentTarget);
        return setTimeout(function () {
            var value;
            value = $target.val();
            value = self.replaceFullWidthChars(value);
            value = self.formatExpiry(value);
            return self.safeVal(value, $target);
        });
    };

    public formatExpiry(e) {
        let $target, digit, val;
        digit = String.fromCharCode(e.which);
        if (!/^\d+$/.test(digit)) {
            return;
        }
        $target = $(e.currentTarget);
        val = $target.val() + digit;
        if (/^\d$/.test(val) && (val !== '0' && val !== '1')) {
            e.preventDefault();
            return setTimeout(function () {
                return $target.val("0" + val + " / ");
            });
        } else if (/^\d\d$/.test(val)) {
            e.preventDefault();
            return setTimeout(function () {
                var m1, m2;
                m1 = parseInt(val[0], 10);
                m2 = parseInt(val[1], 10);
                if (m2 > 2 && m1 !== 0) {
                    return $target.val("0" + m1 + " / " + m2);
                } else {
                    return $target.val("" + val + " / ");
                }
            });
        }
    };

    public formatForwardExpiry(e) {
        let $target, digit, val;
        digit = String.fromCharCode(e.which);
        if (!/^\d+$/.test(digit)) {
            return;
        }
        $target = $(e.currentTarget);
        val = $target.val();
        if (/^\d\d$/.test(val)) {
            return $target.val("" + val + " / ");
        }
    };

    public formatForwardSlashAndSpace(e) {
        let $target, val, which;
        which = String.fromCharCode(e.which);
        if (!(which === '/' || which === ' ')) {
            return;
        }
        $target = $(e.currentTarget);
        val = $target.val();
        if (/^\d$/.test(val) && val !== '0') {
            return $target.val("0" + val + " / ");
        }
    };

    public reFormatCVC(e) {
        var $target;
        let self = this;
        $target = $(e.currentTarget);
        return setTimeout(function () {
            var value;
            value = $target.val();
            value = self.replaceFullWidthChars(value);
            value = value.replace(/\D/g, '').slice(0, 4);
            return self.safeVal(value, $target);
        });
    };

    public restrictNumeric(e) {
        var input;
        if (e.metaKey || e.ctrlKey) {
            return true;
        }
        if (e.which === 32) {
            return false;
        }
        if (e.which === 0) {
            return true;
        }
        if (e.which < 33) {
            return true;
        }
        input = String.fromCharCode(e.which);
        return !!/[\d\s]/.test(input);
    };

    public restrictCardNumber(e) {
        let $target, card, digit, value;
        $target = $(e.currentTarget);
        digit = String.fromCharCode(e.which);
        if (!/^\d+$/.test(digit)) {
            return;
        }
        // if (this.hasTextSelected($target)) {
        //     return;
        // }
        value = ($target.val() + digit).replace(/\D/g, '');
        card = this.cardFromNumber(value);
        if (card) {
            return value.length <= card.length[card.length.length - 1];
        } else {
            return value.length <= 16;
        }
    };

    public restrictExpiry(e) {
        let $target, digit, value;
        $target = $(e.currentTarget);
        digit = String.fromCharCode(e.which);
        if (!/^\d+$/.test(digit)) {
            return;
        }
        // if (hasTextSelected($target)) {
        //return;
        // }
        value = $target.val() + digit;
        value = value.replace(/\D/g, '');
        if (value.length > 4) {
            return false;
        }
        return true;
    };

    public restrictCVC(e) {
        var $target, digit, val;
        $target = $(e.currentTarget);
        digit = String.fromCharCode(e.which);
        if (!/^\d+$/.test(digit)) {
            return;
        }
        //if (hasTextSelected($target)) {
        //    return;
        //}
        val = $target.val() + digit;
        return val.length <= 4;
    };

    public setCardType(e) {
        let $target, allTypes, card, cardType, val;
        let cards = this.cards;
        $target = $(e.currentTarget);
        val = $target.val();
        cardType = this.cardType(val) || 'unknown';
        if (!$target.hasClass(cardType)) {
            allTypes = (function () {
                var _i, _len, _results;
                _results = [];
                for (_i = 0, _len = cards.length; _i < _len; _i++) {
                    card = cards[_i];
                    _results.push(card.type);
                }
                return _results;
            })();
            $target.removeClass('unknown');
            $target.removeClass(allTypes.join(' '));
            $target.addClass(cardType);
            $target.toggleClass('identified', cardType !== 'unknown');
            return $target.trigger('payment.cardType', cardType);
        }
    };

    public cardType(num) {
        var _ref;
        if (!num) {
            return null;
        }
        return ((_ref = this.cardFromNumber(num)) != null ? _ref.type : void 0) || null;
    };
}
