import {Injectable} from '@angular/core';
import {WizardService} from "./wizard.service";
import {LocalStorageService} from "./localStorage.service";
import {PreloaderService} from "./preloader.service";

@Injectable()
export class KioskService {
    private isKioskModeEnabled: boolean = false;
    private salesRep: string = '';
    private scriptLoaded: boolean = false;

    constructor(private storage: LocalStorageService, private preloader: PreloaderService) {
        this.restore()
    }

    public switchKioskMode(salesRep: string = '') {
        this.isKioskModeEnabled = !this.isKioskModeEnabled;
        this.salesRep = salesRep;
        this.store();
        if (this.isEnabled()) {
            this.loadKioskScript();
        }
    }

    public isEnabled() {
        return this.isKioskModeEnabled;
    }

    public getSalesRep() : string {
        return this.salesRep;
    }

    private store() {
        this.storage.setObject("kiosk", {
            salesRep: this.salesRep,
            isKioskModeEnabled: this.isKioskModeEnabled
        })
    }

    private restore() {
        try {
            let restoredState = this.storage.getObject('kiosk');
            if (restoredState) {
                if ((restoredState.isKioskModeEnabled === true) && restoredState.salesRep && (restoredState.salesRep != '')) {
                    this.salesRep = restoredState.salesRep;
                    this.isKioskModeEnabled = restoredState.isKioskModeEnabled;
                    this.loadKioskScript();
                }
            }
        }
        catch (e) {
            if (console) console.log(e);
        }
    }

    private loadKioskScript() {
        if (this.scriptLoaded) return;
        this.preloader.loadCDNScript(''); // TODO script name
        this.scriptLoaded = true;
    }
}