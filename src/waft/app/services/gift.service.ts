import {Injectable} from '@angular/core';
import {Location} from '@angular/common';
import {Response} from "@angular/http";
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable'
import {LocalStorageService} from './localStorage.service';
import {AuthService} from './auth.service';
import {JourneyModel} from '../models/journey.model';
import {CountryModel} from '../models/country.model';
import {WaftAPIService} from "./waftapi.service";

import {PaymentModel} from "../models/payment.model";
import {AddressModel} from "../models/address.model";
import {OrderModel} from "../models/order.model";

@Injectable()
export class GiftService {
    public order: OrderModel;
    public isLockedThankYou: boolean = true;

    constructor(private storage: LocalStorageService, private auth: AuthService,
                private router: Router, private api: WaftAPIService, private location: Location) {
        this.getFromStorage();
    }

    private setDefauts() {
        this.isLockedThankYou = true;
        this.order = OrderModel.CreateGiftOrder(this.api);
        this.initCountry();
    }

    private getFromStorage() {
        try {
            let restoredGift = this.storage.getObject('gift');
            if (restoredGift && (typeof restoredGift.order === 'object')) {
                this.order = new OrderModel(this.api, restoredGift.order);
                this.initCountry(this.order.countryCode);
                if ((!this.order.orderSize) || (this.order.orderSize === ''))
                    this.order.orderSize = 'medium';
                return;
            }
        }
        catch (e) {
            if (console) console.log(e);
        }
        this.reset();
    }

    public putToStorage() {
        let gift = {
            order: this.order.toJSON()
        };
        this.storage.setObject('gift', gift);
    }

    public reset() {
        this.setDefauts();
        this.putToStorage();
    }

    public initCountry(countryCode?: string) {
        this.api.getCountry(countryCode)
            .subscribe(
                (data) => {
                    let country = new CountryModel(data);
                    this.order.setCountry(country);
                    this.putToStorage();
                });
    }
}
