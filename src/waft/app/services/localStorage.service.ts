import { Injectable } from '@angular/core';
import { CookieStorage } from 'cookie-storage';

@Injectable()
export class LocalStorageService {
    public localStorage:any;
    version: number = 1;

    constructor() {
        try {
            let testKey = 'test', storage = window.localStorage;
            storage.setItem(testKey, '1');
            storage.removeItem(testKey); // Success
            this.localStorage = localStorage;
            return;
        } catch (error) {
            if (console) console.log('Current browser does not support Local Storage');
            this.localStorage = new CookieStorage();
        }
    }

    public set(key:string, value:string):void {
        this.localStorage.setItem(key, value);
    }

    public get(key:string, default_value : string):string {
        return this.localStorage.getItem(key) || default_value;
    }

    public setObject(key:string, value:any):void {
        if (value) value._version = this.version;
        this.set(key, JSON.stringify(value));
    }

    public getObject(key:string) : any {
        let val = this.get(key, null);
        if (!val) return null;
        let obj = JSON.parse(val);
        if (obj && obj._version && (obj._version == this.version))
            return obj;
        else return null;
    }

    public remove(key:string):any {
        this.localStorage.removeItem(key);
    }

    public static DefaultValue(arg1: any, arg2: any) {
        return (typeof arg1 === 'undefined') ? arg2 : arg1;
    }
}