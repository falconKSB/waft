import {Injectable} from '@angular/core';
import {JourneyModel} from "../models/journey.model";
import {OrderModel} from "../models/order.model";
import {CountryModel} from "../models/country.model";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {LocalStorageService} from './localStorage.service';
import {AuthService} from './auth.service';
import {WaftAPIService} from "./waftapi.service";
import {GTMService} from "./gtm.service";
import {KioskService} from "./kiosk.service";

@Injectable()
export class WizardService {
    public order: OrderModel;
    public country: CountryModel = new CountryModel();

    public currentStepN: number = 0;
    public currentStep: any;
    public previousStep: any;
    public lastStep: any;
    public routes: any;
    public allSteps: Array<any>;
    public isLetThemDesign: boolean = false;
    paramsInitialized: boolean = false;
    skipGender: boolean = false;
    skipGift: boolean = false;

    constructor(public router: Router, private route: ActivatedRoute,
                private storage: LocalStorageService, private auth: AuthService,
                private api: WaftAPIService, private gtm: GTMService, private kiosk: KioskService) {
        this.allSteps = WizardService.GetAllSteps();
        this.routes = this.getRoutes();
        this.getFromStorage();
        this.toStep(0, false, true);
        this.router.events.subscribe((path) => {
            this.toStepFromRoute(path.url);
        });
    }

    public isEmpty(): boolean {
        return (this.lastStep == null) || (this.lastStep.index == 0);
    }

    public back() {
        if (this.currentStepN == 0) return;
        if (this.currentStepN == 2 && !this.order.journey.gift) {
            this.toStep(0, true);
            return;
        }
        this.toStep(this.currentStepN - 1, true);
    }

    public disableAllSteps() {
        for (let i = 1; i < this.allSteps.length; i++) {
            if (this.currentStepN != i)
                this.allSteps[i].canEnable = false;
        }
    }

    public next(navigate = false) {
        this.toStep(this.currentStepN + 1, navigate, true);
    }

    public navToStart() {
        this.toStep(0, true);
    }

    public getMaxStep() {
        for (let i = 1; i < this.allSteps.length; i++) {
            if (!this.allSteps[i].canEnable) {
                return i - 1;
            }
        }
    }

    public enableStep(url: string): void {
        this.routes[url].canEnable = true;
    }

    public setMaxStep(max: number) {
        for (let i = 0; i <= max; i++) {
            this.allSteps[i].canEnable = true;
        }
    }

    public toMaxStep() {
        this.toStep(this.getMaxStep(), true);
    }

    toStep(n: number, navigate = false, enable = false): void {
        const newRoute = this.allSteps[n].route;
        if (
            (this.order.isGiftRedemption && (newRoute == '/app/wizard/the-new-you')) ||
            (this.auth.isAuth() && (newRoute == '/app/wizard/login')) ||
            ((!this.order.journey.gift) && (newRoute == '/app/wizard/order-design'))) {
            if (this.currentStepN <= n) {
                // going forward
                this.toStep(n + 1, navigate, enable);
                return;
            }
            if ((this.currentStepN > n) && (this.currentStep)) {
                // going back
                this.toStep(n - 1, navigate, enable);
                return;
            }
        }
        if (this.currentStep && (this.currentStep.route == '/app/wizard/gender') && this.isLetThemDesign && (this.currentStepN == n-1)) {
            this.router.navigate(['/app/gift/customise-bottle']);
            return;
        }

        if (this.currentStepN != n) this.lastStep = this.allSteps[n];

        this.currentStepN = n;
        this.currentStep = this.allSteps[n];
        if (n >= 0) this.previousStep = this.allSteps[n - 1];
        else this.previousStep = {};
        if (enable) this.currentStep.canEnable = true;
        if (navigate) this.router.navigate([this.currentStep.route]);
        this.putToStorage();
    }

    public toStepFromRoute(url?: string, navigate = false, enable = false) {
        if (!url) url = this.router.url;
        if (this.currentStep && (url == this.currentStep.route) && (!this.currentStep.canEnable) && (!enable)) {
            //this.toMaxStep();
            return;
        }
        if ((!this.currentStep) || (url != this.currentStep.route)) {
            if (url in this.routes) {
                this.toStep(this.routes[url].index, navigate, enable);
            }
            else {
                this.currentStep = null;
                this.currentStepN = -1;
            }
        }
    }

    private getRoutes(): any {
        let routes = {};
        let i = 0;
        for (let r of this.allSteps) {
            r.index = i;
            routes[r.route] = r;
            i++;
        }
        return routes;
    }

    public initCountry(countryCode?: string, toNewYou = false) {
        this.api.getCountry(countryCode)
            .subscribe(
                (data) => {
                    if (!(this.country && this.country.countryCode && (this.country.countryCode !== '') &&
                        (this.country.countryCode != countryCode))) {
                        // no change of country
                        toNewYou = false;
                    }
                    this.country = new CountryModel(data);
                    this.order.setCountry(this.country);
                    let samples_allowed = this.country.price && (this.country.price.samples !== undefined) &&
                        (this.country.price.samples !== null);
                    if (toNewYou && (this.order.orderSize == "samples") && (!samples_allowed)) {
                        this.toStepFromRoute('/app/wizard/the-new-you', true, true);
                    }
                })
    }

    public setDefauts(): void {
        this.order = OrderModel.CreateLabOrder(this.api);
        this.initCountry();
        this.lastStep = null;
        this.isLetThemDesign = false;
        this.skipGender = false;
        this.skipGift = false;
        this.storage.remove('emailForm');
    }

    public restoreWizard(journey?: JourneyModel): void {
        this.setDefauts();
        this.storage.remove('wizard');
        this.order.setJourney(journey);
        if (['/app/wizard/checkout', '/app/wizard/payment'].indexOf(this.order.journey.step) != -1)
            this.order.journey.step = '/app/wizard/the-new-you';
        if (!this.order.journey.step || (this.order.journey.step === ''))
            this.order.journey.step = '/app/wizard/type';
        this.router.navigate([this.order.journey.step]);
        this.putToStorage();
    }

    private getFromStorage(): void {
        try {
            let restoredWizard = this.storage.getObject('wizard');
            if (restoredWizard && (typeof restoredWizard.order === 'object')) {
                this.order = new OrderModel(this.api, restoredWizard.order);
                this.initCountry(this.order.countryCode);
                this.lastStep = restoredWizard.lastStep;
                this.setMaxStep(restoredWizard.maxStep || 0);
                return;
            }
        }
        catch (e) {
            if (console) console.log(e);
        }
        this.setDefauts();
    }

    public putToStorage(): void {
        let wizard = {
            order: this.order.toJSON(),
            lastStep: this.lastStep,
            maxStep: this.getMaxStep()
        };
        this.storage.setObject('wizard', wizard);
    }

    public buyFragrance(orderSize: string): void {
        this.order.orderSize = orderSize;
        this.putToStorage();
        this.gtm.addToCart('lab_say_hello_select', this.country, 'lab', orderSize);

        if (this.kiosk.isEnabled()) {
            this.toStepFromRoute('/app/wizard/checkout', true, true);
        } else {
            this.toStepFromRoute('/app/wizard/login', true, true);
        }
    }

    protected tryGender(gender: string): boolean {
        if (OrderModel.IsValidGender(gender)) {
            this.order.journey.gender = gender;
            this.order.setGender();
            this.putToStorage();
            this.skipGender = true;
            return true;
        }
        return false;
    }

    public initParams(params: Params) {
        if (this.paramsInitialized)
            return;
        this.paramsInitialized = true;
        let promo = params['promo'];
        if (promo) {
            this.order.payment.promoCode = promo;
            this.putToStorage();
        }
        if (!this.tryGender(params['gender']))
            if (this.order.isGiftRedemption) {
                if (this.tryGender(this.order.gender))
                    this.toStepFromRoute('/app/wizard/fragrance-type', true, true);
                else
                    this.toStepFromRoute('/app/wizard/gender', true, true);
            }
        if (params['gift']) {
            this.order.journey.gift = true;
            this.skipGift = true;
        }
    }

    public canSkipGift(): boolean {
        if (this.skipGift) {
            this.skipGift = false;
            return true;
        }
        return false;
    }

    public canSkipGender(): boolean {
        if (this.skipGender) {
            this.skipGender = false;
            return true;
        }
        return false;
    }

    public saveJourney(): void {
        // if (!(this.auth.isAuth() || (this.order.journey.email && this.order.journey.email !== ''))) return;
        this.order.journey.step = this.router.url;
        this.api.saveJourney(this.order.journey).subscribe(
            data => {
                this.order.journey = data.journey;
                this.putToStorage();
            });
    }

    public getMatchingScent() {
        if (!this.auth.isAuth()) return;
        this.order.journey.step = this.router.url;
        this.order.journey.user = this.auth.getCurrentUserId();
        return this.api.matchScent(this.order.journey);
    }

    public resetWizard(): void {
        this.setDefauts();
        this.disableAllSteps();
        this.putToStorage();
    }

    public showLoginOnCheckout() {
        return !this.auth.isAuth();
    }

    private static GetAllSteps(): Array<any> {
        let allSteps = [];
        allSteps.push({
            route: '/app/wizard/type',
            canEnable: true
        });
        allSteps.push({
            route: '/app/wizard/order-design'
        });
        allSteps.push({
            route: '/app/wizard/gender'
        });
        allSteps.push({
            route: '/app/wizard/fragrance-type'
        });
        allSteps.push({
            route: '/app/wizard/time-of-day',
            preload: ['/assets/img/wizard/timeofday/day.jpg',
                '/assets/img/wizard/timeofday/night.jpg']
        });
        allSteps.push({
            route: '/app/wizard/activity'
        });
        allSteps.push({
            route: '/app/wizard/mood'
        });
        allSteps.push({
            route: '/app/wizard/personalstyle'
        });
        allSteps.push({
            route: '/app/wizard/waft'
        });
        allSteps.push({
            route: '/app/wizard/ingredients'
        });
        allSteps.push({
            route: '/app/wizard/fragrances-choice'
        });
        allSteps.push({
            route: '/app/wizard/customise-bottle'
        });
        allSteps.push({
            route: '/app/wizard/the-new-you'
        });
        allSteps.push({
            route: '/app/wizard/login'
        });
        allSteps.push({
            route: '/app/wizard/checkout'
        });
        allSteps.push({
            route: '/app/wizard/payment'
        });
        allSteps.push({
            route: '/app/wizard/gift-delivery'
        });
        allSteps.push({
            route: '/app/wizard/thank-you'
        });
        return allSteps;
    }

    public selectAlternativeCheckout(): boolean {
        if (this.order.isGiftRedemption) {
            this.toStepFromRoute("/app/wizard/gift-delivery", true, true);
            return true;
        }
        // let opt = (<any> window)['optimizely'];
        // if (!opt) return;
        // let variations = opt.data.state.variationNamesMap;
        // for (let key in variations) {
        //     if (variations[key] == "Split Checkout flow") {
        //         this.toStepFromRoute("/app/wizard/checkoutSplit", true, true);
        //         return true;
        //     }
        // }
        return false;
    }
}
