import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

@Injectable()
export class LayoutService {
    constructor(public router: Router) {
        this.router.events.subscribe((path) => {
            if (path.url != this.currentUrl) {
                this.currentUrl = path.url;
                window.scrollTo(0, 0);
            }
        });
    }

    private currentUrl: String;
}