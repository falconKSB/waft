import {Injectable} from '@angular/core';
import {Response} from "@angular/http";
import {Router} from '@angular/router';
import {AuthHttp, JwtHelper} from 'angular2-jwt';
import {Observable} from 'rxjs/Observable'
import {LocalStorageService} from './localStorage.service';
import {AuthService} from './auth.service';
import {WizardService} from './wizard.service';
import {WaftAPIService} from "./waftapi.service";
import {JourneyModel} from '../models/journey.model';
import {ProfileOrderModel} from '../models/profileOrder.model';
import {FragranceCardModel} from '../models/fragranceCard.model';
import {OrderModel} from "../models/order.model";

@Injectable()
export class ProfileService {
    constructor(private http: AuthHttp, private storage: LocalStorageService,
                private api: WaftAPIService, private auth: AuthService,
                private router: Router, public wizard: WizardService,) {
        this.setDefaults();
    }

    public journeys: Array<JourneyModel>;
    public orders: Array<ProfileOrderModel>;
    public samples: Array<ProfileOrderModel>;

    public firstName: string;
    public lastName: string;
    public email: string;
    public createdAt: Date;

    public activeSample: any;
    public mainScent: string;

    public fragrancesCards: Array<FragranceCardModel>;

    private setDefaults() {
        this.journeys = [];
        this.orders = [];
        this.samples = [];
    }

    public toLastJourney(journey?: JourneyModel) {
        if (!journey) {
            journey = this.journeys[this.journeys.length - 1];
        }
        this.wizard.restoreWizard(journey);
    }

    public orderAgain(order: any) {
        order.journey._id = null;
        this.wizard.restoreWizard(order.journey);
        this.wizard.order.repeatOrderRef = order._id;
        this.wizard.buyFragrance(order.orderSize);
    }

    public deleteJourney(journey: JourneyModel) {
        this.api.deleteJourney(journey._id)
            .subscribe(
                () => {
                    this.getJourney()
                })
    }

    public getJourney() {
        return this.api.getUncompletedJoyrneys()
            .subscribe(
                data => {
                    this.journeys = data;
                })

    }

    public getOrders() {
        return this.api.getOrders()
            .subscribe(
                data => {
                    this.orders = data;
                })
    }

    public getSamples() {
        return this.api.getSamples()
            .subscribe(
                data => {
                    this.samples = data;
                })
    }

    public setCurrentUser() {
        return this.api.getUser()
            .subscribe(
                data => {
                    this.firstName = data.firstName;
                    this.lastName = data.lastName;
                    this.email = data.email;
                    this.createdAt = data.createdAt;
                })
    }

    public initFragrancesCards() {
        this.fragrancesCards = [];

        this.mainScent = this.activeSample.journey.scents.main._id;

        this.fragrancesCards.push(new FragranceCardModel(
            this.activeSample.journey.scents.main._id,
            this.activeSample.journey.customBottle.initials + this.activeSample.journey.scents.main.code));
        this.fragrancesCards.push(new FragranceCardModel(
            this.activeSample.journey.scents.add1.scent._id,
            this.activeSample.journey.customBottle.initials + this.activeSample.journey.scents.add1.scent.code
        ));
        this.fragrancesCards.push(new FragranceCardModel(
            this.activeSample.journey.scents.add2.scent._id,
            this.activeSample.journey.customBottle.initials + this.activeSample.journey.scents.add2.scent.code
        ));
    }

    public savePrimary() {
        return this.api.savePrimaryScent(this.activeSample._id, this.mainScent);
    }

    public makePrimary(scent: string) {
        this.mainScent = scent;
    }

}