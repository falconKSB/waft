import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import {AuthService} from './auth.service';
import {JourneyModel} from "../models/journey.model";
import {OrderModel} from "../models/order.model";
import {AddressModel} from "../models/address.model";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {ABTestService} from "./abtest.service";
import {KioskService} from "./kiosk.service";
import {GiftModel} from "../models/gift.model";
import {HttpService} from "./http.service";

@Injectable()
export class WaftAPIService {
    constructor(private auth: AuthService,
                private http: HttpService,
                private abtest: ABTestService,
                private kiosk: KioskService) {
    }

    public getCountry(countryCode?: string): Observable<any> {
        return this.http.get('/api/price', countryCode ? {country_code: countryCode.toLowerCase()} : {});
    }

    public saveJourney(journey: JourneyModel): Observable<any> {
        journey.user = this.auth.getCurrentUserId();
        return this.http.post('/api/journeys', {
            journey: journey
        })
    }

    public deleteJourney(journeyId: string): Observable<any> {
        return this.http.post("/api/profile/journeys/delete/", {journeyId: journeyId});
    }

    public matchScent(journey: JourneyModel): Observable<any> {
        return this.http.post('/api/journeys/scent', {
            journey: journey
        })
    }

    public getJourney(journeyId: string): Observable<any> {
        return this.http.get("/api/journeys/"+journeyId);
    }

    public getFragrances(filter?: string, listOfIds?: Array<string>): Observable<any[]> {
        let params = {};
        if (listOfIds) {
            for (let i = 0; i < listOfIds.length; i++) {
                params["id" + (i + 1)] = listOfIds[i];
            }
        }
        else params = {term: filter};
        return this.http.get('/api/fragrances', params);
    }

    public createOrder(order: OrderModel, token?: string, immediatePayment = false) {
        let sender = this.auth.getCurrentUserId();
        if(order.journey) order.journey.user = sender;

        let newOrder = {
            token: token,
            order: {
                id: order.id,
                type: order.type,
                sender: sender,
                orderSize: order.orderSize,
                countryCode: order.countryCode,
                gift: order.gift,
                customBottle: order.customBottle,
                customPackaging: order.customPackaging,
                promoCode: order.promoCode,
                gender: order.gender,
                isGiftRedemption: order.isGiftRedemption,
                salesRep: order.salesRep,
                preferredSample: order.preferredSample,
                repeatOrderRef: order.repeatOrderRef,
                optimizelyVariations: this.abtest.getOptimizelyVariations()
            },
            delivery: order.delivery.toAPIObject(),
            journey: order.journey,
            immediatePayment: immediatePayment
        };
        if (this.kiosk.isEnabled())
            newOrder.order.salesRep = this.kiosk.getSalesRep();
        return this.http.post('/api/orders', newOrder);
    }

    public placeOrder(orderId: string, paymentAccount:string, token: string, promoCode: string, preferredSample: string): Observable<any> {
        return this.http.post('/api/orders/' + orderId + '/pay', {
            token: token,
            promoCode: promoCode,
            paymentAccount: paymentAccount,
            preferredSample: preferredSample
        });
    }

    public getPromoCodeStatus(code: string, currency: string): Observable<any> {
        let sender = this.auth.getCurrentUserId();
        return this.http.get("/api/promoCode/" + code, {currency: currency, sender: sender})
    }

    public saveGiftDelivery(order : OrderModel): Observable<any> {
        if (!this.auth.isAuth()) return;
        let userId = this.auth.getCurrentUserId();
        order.journey.user = userId;

        return this.http.post('/api/gift/delivery', {
            journey: order.journey,
            delivery: order.delivery,
            orderId: order.id,
            orderGender: order.gender,
            giftReceiver: userId
        });
    }

    public saveGiftEmail(orderId: string, gift: GiftModel): Observable<any> {
        if (!this.auth.isAuth()) return;
        let userId = this.auth.getCurrentUserId();

        return this.http.post('/api/gift/' + orderId + '/gift-email', {
            orderId: orderId,
            gift: gift,
            giftReceiver: userId
        });
    }

    public getGiftDetails(journeyId: string): Observable<any> {
        return this.http.get('/api/gift/' + journeyId);
    }

    public getUncompletedJoyrneys(): Observable<any> {
        return this.http.get("/api/profile/journeys");
    }

    public getOrders(): Observable<any> {
        return this.http.get("/api/profile/orders");
    }

    public getSamples(): Observable<any> {
        return this.http.get("/api/profile/orders/trials");
    }

    public getUser(): Observable<any> {
        return this.http.get("/api/profile/user/");
    }

    public savePrimaryScent(orderId: string, mainScent: string): Observable<any> {
        return this.http.post("/api/profile/orders/" + orderId + "/changePrimaryScent/", {
            newPrimary: mainScent
        });
    }
}
