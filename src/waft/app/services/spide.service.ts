/**
 * Created by alexl on 02.12.2016.
 */
import {Injectable} from '@angular/core';
import {SpideModel} from '../models/spide.model';

declare var CDN: string;

@Injectable()
export class SpideRenderingService {

    constructor() {
    }

    public cdn(path: string) {
        return CDN + path;
    }

    private drawAccords(context: any, coordinates: any, numbersOfNotes: number) {

        for (var i = 0; i < numbersOfNotes; i++) {
            //drow line
            context.beginPath();
            context.moveTo(coordinates[i].x, coordinates[i].y);
            i != numbersOfNotes - 1 ? context.lineTo(coordinates[i + 1].x, coordinates[i + 1].y) : context.lineTo(coordinates[0].x, coordinates[0].y);
            context.lineCap = 'round';
            context.lineWidth = 2;
            context.strokeStyle = "Red";
            context.stroke();

            //drow dot
            context.beginPath();
            context.arc(coordinates[i].x, coordinates[i].y, 3, 0, 2 * Math.PI, false);
            context.fillStyle = 'Red';
            context.fill();
        }

    }

    public renderSpide(accords: Array<any>, canvas: any) {

        let numbersOfNotes;
        accords.length >= 3 ? numbersOfNotes = accords.length : numbersOfNotes = 3;

        let image = this.cdn("/assets/img/wizard/fragrancesChoice/spide" + numbersOfNotes + ".png");

        let context = canvas.getContext('2d');
        let source: HTMLImageElement = new Image();

        source.crossOrigin = 'Anonymous';

        source.onload = () => {

            canvas.height = source.height;
            canvas.width = source.width;
            context.drawImage(source, 0, 0);

            var chartLineСoordinates = SpideModel.GetChartLineСoordinates(accords);
            this.drawAccords(context, chartLineСoordinates, numbersOfNotes);

            image = canvas.toDataURL();
        };

        source.src = image;
    }

}