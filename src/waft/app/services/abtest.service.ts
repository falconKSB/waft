import {Injectable} from '@angular/core';
import {KioskService} from "./kiosk.service";

@Injectable()
export class ABTestService {
    constructor(private kiosk: KioskService) {
    }

    getOptimizelyVariations(): string[] {
        if (this.kiosk.isEnabled()) return [];
        let variations = (<any> window)['_vwo_exp'];
        if (!variations) return [];
        let result = [];
        for (let key in variations) {
            if (variations.hasOwnProperty(key)) {
                let v = variations[key];
                if (v.combination_chosen)
                    result.push([v.name] + ':' + v.combination_chosen);
            }
        }
        return result;
    }

    public isABVariation(name: string, variation = 2): boolean {
        if (this.kiosk.isEnabled()) return false;
        let variations = (<any> window)['_vwo_exp'];
        if (!variations) return false;
        for (let key in variations) {
            if (variations.hasOwnProperty(key)) {
                if ((variations[key].name == name) || (variations[key].name == 'TEST ' + name))
                    return variations[key].combination_chosen == variation;
            }
        }
        return false;
    }
}