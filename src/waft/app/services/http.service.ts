
import {Injectable} from '@angular/core';
import {
    Response,
    Headers,
    URLSearchParams
} from '@angular/http';
import {AuthHttp} from 'angular2-jwt';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import { isDevMode } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class HttpService {
    defaultHeaders: Headers;

    constructor(private http: AuthHttp) {
        this.defaultHeaders = new Headers({'Content-Type': 'application/json'});
    }

    public get(path: string, params?: any): Observable<any> {
        let urlParams = HttpService.ObjToParams(params);
        return this.http.get(path, {
            search: urlParams,
            headers: this.defaultHeaders
        })
            .map(this.extractData)
            .catch(this.log)
    }

    public post(path: string, body: any): Observable<any> {
        return this.http.post(path, body, {
            headers: this.defaultHeaders
        })
            .map(this.extractData)
            .catch(this.log)
    }

    static ObjToParams(obj: any, urlParams?: URLSearchParams, prefix = '') {
        if (!urlParams) urlParams = new URLSearchParams();
        if (!obj) return urlParams;
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                let value = obj[key];
                if (typeof value == 'object')
                    HttpService.ObjToParams(value, urlParams, prefix+key+'_');
                else
                    urlParams.set(prefix+key, value);
            }
        }
        return urlParams;
    }

    extractData(res: Response): any {
        let data = res.json();
        if (!data.success) throw(data);
        return data.data || {};
    }

    log(err: any) : Observable<any> {
        if (console && isDevMode()) console.log(err);
        let newrelic = (<any> window).newrelic;
        if (newrelic)
            newrelic.noticeError(err);
        return Observable.throw(err);
    }
}
