import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {WizardService} from "../services/wizard.service";
import {OrderModel} from "../models/order.model";
import {JourneyModel} from "../models/journey.model";
import {GiftService} from "../services/gift.service";

@Injectable()
export class GiftGuard implements CanActivate {

    constructor(private gift: GiftService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot) : boolean {
        let order: OrderModel = this.gift.order;
        if (!order || !order.id) return this.reset();

        return true;
    }

    reset() : boolean {
        this.gift.reset();
        this.router.navigate(['/app/gift']);
        return false;
    }
}
