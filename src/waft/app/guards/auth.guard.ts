import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LocalStorageService } from '../services/localStorage.service';
import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router, private localStorage: LocalStorageService, private auth: AuthService) { }

    canActivate(route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot) {
        if (!this.auth.getToken()) this.auth.restoreSession(); //when session restored later than checking route

        if (this.auth.isAuth()) return true;

        this.localStorage.set('redirectUrl', state.url);
        this.router.navigate(['/app/account/login']);
        return false;
    }
}
