import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {WizardService} from "../services/wizard.service";
import {OrderModel} from "../models/order.model";
import {JourneyModel} from "../models/journey.model";

@Injectable()
export class WizardGuard implements CanActivate {

    constructor(private wizard: WizardService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot) : boolean {
        let order: OrderModel = this.wizard.order;
        if (!order) return this.reset();
        let journey: JourneyModel = this.wizard.order.journey;
        if (!journey) return this.reset();
        if (typeof journey.gift === 'undefined') return this.reset();

        let url: string = state.url;

        if (url.startsWith('/app/wizard/order-design')) return true;
        if (url.startsWith('/app/wizard/gender')) return true;

        if (!journey.recipientGender) return this.reset();
        if (url.startsWith('/app/wizard/fragrance-type')) return true;

        if (!journey.gender) return this.reset();
        
        return true;
    }

    reset() : boolean {
        this.wizard.resetWizard();
        this.wizard.navToStart();
        return false;
    }
}
