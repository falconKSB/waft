import { Component, ViewEncapsulation } from '@angular/core';

import { AppState } from './app.service';
import { LayoutService } from './services/layout.service';
import { AuthHttp } from 'angular2-jwt';
import {PreloaderService} from "./services/preloader.service";


@Component({
    selector: 'app',
    encapsulation: ViewEncapsulation.None,
    template: `<div id="page" landing>
               <router-outlet></router-outlet>
               <waft-footer></waft-footer>
               </div>`

})

export class AppComponent {
    constructor(public appState: AppState, public layout: LayoutService, public http: AuthHttp, private preloader: PreloaderService) {}
}