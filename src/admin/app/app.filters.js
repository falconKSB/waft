(function () {
    'use strict';

    angular
        .module('adminApp')
        .filter('badwords', function($sce) {
            return function(input) {
                let search = /(?:p.?\s*[o0]\.?|post\s+office)\s+b[o0]x|fuck|pussy|cunt|ass|shit|arse|bitch|bastard|bollocks|crap|damn|waft|nigger|nigga|whore|twat|piss/;
                return $sce.trustAsHtml(input.replace(new RegExp(search, 'gi'), '<span class="bad-text">$&</span>'));
            }
        });
})();