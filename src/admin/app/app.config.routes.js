(function () {
    'use strict';

    angular
        .module('adminApp')
        .config(RouteConfigure);

    RouteConfigure.$inject =
        [
            '$stateProvider',
            '$urlRouterProvider'
        ];

    function RouteConfigure
    ($stateProvider,
     $urlRouterProvider) {
        $urlRouterProvider.otherwise('/orders');

        // set up the states
        $stateProvider

        // Index pages section

            .state('app', {
                abstract: true,
                private: false,
                url: '/',
                views: {
                    'header@': {
                        templateUrl: appHelper.templatePath('header/header')
                    },
                    'footer@': {
                        templateUrl: appHelper.templatePath('footer/footer')
                    }
                }
            })
            .state('app.login', {
                url: 'login',
                private: false,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('login/login'),
                        controller: 'LoginCtrl as login'
                    },
                    'header@': {template: ''},
                    'footer@': {template: ''}
                }
            })
            .state('app.orders', {
                url: 'orders',
                private: true,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('orders/orders'),
                        controller: 'OrdersCtrl as orders'
                    }
                }
            })
            .state('app.users', {
                url: 'users',
                private: true,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('users/users'),
                        controller: 'UsersCtrl as users'
                    }
                }
            })
            .state('app.scents', {
                url: 'scents',
                private: true,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('scents/scents'),
                        controller: 'ScentsCtrl as scents'
                    }
                }
            })
            .state('app.scents.create', {
                url: '/create',
                private: true,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('createScent/createScent'),
                        controller: 'CreateScentCtrl as createScent'
                    }
                }
            })
            .state('app.scents.edit', {
                url: '/edit/:scentId',
                private: true,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('createScent/createScent'),
                        controller: 'EditScentCtrl as editScent'
                    }
                }
            })
            .state('app.journeys', {
                url: 'journeys',
                private: true,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('journeys/journeys'),
                        controller: 'JourneysCtrl as journeys'
                    }
                }
            })
            .state('app.dashboard', {
                url: 'dashboard',
                private: true,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('dashboard/dashboard'),
                        controller: 'DashboardCtrl as dashboard'
                    }
                }
            })
            .state('app.conversions', {
                url: 'conversions',
                private: true,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('conversions/conversions'),
                        controller: 'ConversionsCtrl as conversions'
                    }
                }
            })
            .state('app.prices', {
                url: 'prices',
                private: true,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('prices/prices'),
                        controller: 'PricesCtrl as prices'
                    }
                }
            })
            .state('app.fragrances', {
                url: 'fragrances',
                private: true,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('fragrances/fragrances'),
                        controller: 'FragrancesCtrl as fragrances'
                    }
                }
            })
            .state('app.adminsAccounts', {
                url: 'adminsAccounts',
                private: true,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('admins/admins'),
                        controller: 'AdminsCtrl as admins'
                    }
                }
            })
            .state('app.adminsAccounts.create', {
                url: '/create',
                private: true,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('createAdmin/createAdmin'),
                        controller: 'CreateAdminCtrl as createAdmin'
                    }
                }
            })
            .state('app.adminsAccounts.edit', {
                url: '/edit/:adminId',
                private: true,
                views: {
                    'content@': {
                        templateUrl: appHelper.templatePath('createAdmin/createAdmin'),
                        controller: 'EditAdminCtrl as editAdmin'
                    }
                }
            })
    }
})();
