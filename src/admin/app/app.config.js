(function () {
	
    'use strict';

	angular
		.module('adminApp')
		.config(configure);

	configure.$inject = 
	[
		'$httpProvider', 
		'localStorageServiceProvider'
	];

	function configure
	(
		$httpProvider, 
		localStorageServiceProvider
	) {	

		// LOCAL STORAGE CONFIGURATION
		localStorageServiceProvider.setPrefix('adminApp');

		$httpProvider.interceptors.push('tokenInjector');
        $httpProvider.interceptors.push('tokenRestorer');
		
	}
})();