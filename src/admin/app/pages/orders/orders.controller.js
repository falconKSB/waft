(function () {
  'use strict';

  angular
    .module('adminApp')
    .controller('OrdersCtrl', OrdersCtrl);

  OrdersCtrl.$inject = ['$rootScope', '$scope', '$state', 'uiGridConstants', 'order', 'scent', 'country', 'modalService', 'warehouse'];

  function OrdersCtrl($root, $scope, $state, uiGridConstants, order, scent, country, modalService, warehouse) {

    $scope.term = '';
    $scope.warehouses = '';

    $scope.order = null;
    $scope.nextOrder = null;
    $scope.prevOrder = null;
    $scope.message = '';
    $scope.isEditing = false;
    $scope.isProcessing = false;
    $scope.searchWarehouse = null;
    $scope.searchStatus = null;
    $scope.searchDate = null;
    $scope.searchSamplesStatus = null;
    $scope.countries = [];

    $scope.orderDeliveryDate = '';

    $scope.otherOrders = [];

    function processDeliveryDate() {
      var d;
      if ($scope.order && $scope.order.deliveredOn)
        d = new Date($scope.order.deliveredOn);
      else
        d = new Date();
      d.setSeconds(0, 0);
      return d;
    }

    function refreshScents() {
      scent.getList(0, 1, $scope.order.warehouse).then(
        function (data) {
          $scope.scents = data.docs;
        },
        function (errResponse) {
        }
      );
    }

    $scope.genders = ['man', 'woman', 'unisex'];
    $scope.statuses = ['Gift Invitation', 'Created', 'Payment Failed', 'Received', 'On Hold', 'Completed Trial', 'Processing', 'Processed', 'Shipped', 'Delivered', 'Returned', 'Refunded'];
    $scope.orderSizes = ['big', 'medium', 'small', 'samples', '100ml', '50ml', '15ml', '5ml'];
    $scope.orderSizesMain = ['big', 'medium'];
    $scope.searchStatuses = ['All Statuses', 'Received-Refunded'].concat($scope.statuses);
    $scope.searchWarehouses = ['All Warehouses'];
    $scope.searchDates = ['All Dates', 'Today', 'Yesterday', 'This Month', 'Last Month', 'Last 7 Days', 'Last 30 Days', 'Last 60 Days'];
    $scope.samplesStatuses = ['Pending', 'Cancelled', 'Completed'];
    $scope.samplesStatuses = ['Pending', 'Cancelled', 'Completed'];
    $scope.searchSamplesStatuses = ['All Samples'].concat($scope.samplesStatuses);
    $scope.types = ['Intensify', 'Freshen', 'Dare'];
    $scope.styles = ['modern', 'classic', 'handwritten'];

    function formatQuantity(scent, size) {
      return (scent.availableQuantity && scent.availableQuantity[size]) ?
        scent.availableQuantity[size] : 0
    }

    function getInvQuantity(scent, isBase) {
      var size = $scope.order.orderSize;
      if (!size) return [0];
      if (!$scope.scents) return [0];
      switch (size) {
        case '100ml':
          size = 'big';
          break;
        case '50ml':
          size = 'medium';
          break;
        case '15ml':
          size = 'small';
          break;
        case '5ml':
          size = 'samples';
          break;
        case 'big':
          size = isBase ? 'big' : 'small';
          break;
        case 'medium':
          size = isBase ? 'medium' : 'small';
          break;
      }
      var s = getScentByCode(scent.code);
      if (!s) return [0];
      if ($scope.order.orderSize != 'samples')
        return [formatQuantity(s, size)];
      var mainOrderSize = $scope.order.orderSizeMain || 'big';
      if (!isBase) mainOrderSize = 'small';
      return [formatQuantity(s, size), formatQuantity(s, mainOrderSize)];
    }

    $scope.hasOtherImporantOrders = function () {
      for (var i = 0; i < $scope.otherOrders.length; i++) {
        var o = $scope.otherOrders[i];
        if ((o.status == 'Gift Invitation') ||
          (o.status == 'Created') ||
          (o.status == 'Payment Failed')) continue;
        return true;
      }
      return false;
    };

    $scope.warehouseName = function (warehouseId) {
      return warehouseId ? $scope.warehouses.find(e => e._id == warehouseId).name : '';
    };

    $scope.warehouseId = function (name) {
      return name ? $scope.warehouses.find(e => e.name == name)._id : null;
    };

    $scope.canCancelTrial = function () {
      return $root.canEdit() && ($scope.order.orderSize == 'samples') && ($scope.order.status != 'Payment Failed') &&
        ($scope.order.status != 'Created') && ($scope.order.samplesStatus == 'Pending');
    };

    $scope.cancelTrial = function () {
      if (!$scope.canCancelTrial()) return;
      $scope.selectSamplesStatus('Cancelled');
      $scope.save();
    };

    $scope.canExpediteTrial = function () {
      return $root.canEdit() && ($scope.order.orderSize == 'samples') && $scope.hasScents() &&
        ($scope.order.status != 'Created') && ($scope.order.status != 'Payment Failed');
    };

    $scope.expediteTrial = function () {
      if (!$scope.canExpediteTrial()) return;
      var modalOptions = {
        closeButtonText: 'Cancel',
        actionButtonText: 'Expedite',
        headerText: 'Expedite Order?',
        bodyText: 'Are you sure you want to expedite this order?'
      };
      if ($scope.order.samplesStatus == 'Completed')
        modalOptions.bodyText = 'This trial is already complted. Are you sure you want to expedite this order AGAIN?';

      if (confirm(modalOptions.bodyText)) {
        if ($scope.order.samplesStatus == 'Completed') {
          $scope.order.expediteAgain = true;
        }
        $scope.selectSamplesStatus('Completed');
        $scope.save();
      }
      // TODO: replace with below
      // modalService.showModal({}, modalOptions)
      //     .then(function (result) {
      //         $scope.selectSamplesStatus('Complated');
      //         $scope.save();
      //     });
    };

    $scope.canHold = function () {
      return ($scope.order.status == 'Received') || ($scope.order.status == 'Processing') || ($scope.order.status == 'Completed Trial')
    };

    $scope.putOnHold = function () {
      if (!$scope.canHold()) return;
      var reason = prompt("Reason?", "");
      if (reason == null) return;
      $scope.order.onHoldReason = reason;
      $scope.selectStatus('On Hold');
      $scope.save();
    };

    $scope.hasScents = function () {
      return ($scope.order.journey.scents && $scope.order.journey.scents.main && $scope.order.journey.scents.main.code) &&
        ($scope.order.journey.scents && $scope.order.journey.scents.add1.scent && $scope.order.journey.scents.add1.scent.code) &&
        ($scope.order.journey.scents && $scope.order.journey.scents.add2.scent && $scope.order.journey.scents.add2.scent.code);
    };

    $scope.canProcess = function () {
      return $root.canEdit() && ($scope.order.status == 'Received') && $scope.hasScents();
    };

    $scope.setToProcessing = function () {
      if (!$scope.canProcess()) return;
      $scope.selectStatus('Processing');
      $scope.save();
    };

    $scope.canConfirmRecommendation = function () {
      return $root.canEdit() && ($scope.order.status == 'Received') && $scope.order.recommendations &&
        $scope.order.recommendations.main && (!($scope.order.journey.scents && $scope.order.journey.scents.main && $scope.order.journey.scents.main.code)) &&
        $scope.order.recommendations.add1 && (!($scope.order.journey.scents && $scope.order.journey.scents.add1.scent && $scope.order.journey.scents.add1.scent.code)) &&
        $scope.order.recommendations.add2 && (!($scope.order.journey.scents && $scope.order.journey.scents.add2.scent && $scope.order.journey.scents.add2.scent.code))
    };

    $scope.confirmRecommendation = function () {
      if (!$scope.canConfirmRecommendation()) return;
      $scope.selectScent($scope.order.recommendations.main.code, 'main');
      $scope.selectScent($scope.order.recommendations.add1.code, 'add1');
      $scope.selectScent($scope.order.recommendations.add2.code, 'add2');
      if ($scope.order.status == 'Received') $scope.selectStatus('Processing');
      $scope.save();
    };

    $scope.getScentFullName = function (scent, isBase, isQuantity) {
      if (!(scent && scent.code)) return '';
      var res = scent.code + ' - ' + scent.name;
      if (isQuantity)
        res += ' [' + getInvQuantity(scent, isBase).join(',') + ']';
      return res;
    };

    $scope.getJourneyDesc = function (journey) {
      if (!journey) return 'n/a';
      var gift = journey.gift;
      if (typeof gift === 'undefined')
        gift = ($scope.order.customPackaging.firstName === $scope.order.sender.firstName);
      if (gift === true) gift = 'gift';
      else gift = 'not a gift';
      var result = [gift, journey.recipientGender, journey.gender, journey.timeOfDay,
        journey.activity, journey.mood, journey.personalStyle, journey.waft];
      return result.join(' / ');
    };

    $scope.next = function () {
      $scope.showMore($scope.nextOrder);
    };

    $scope.prev = function () {
      $scope.showMore($scope.prevOrder);
    };

    $scope.editOrder = function () {
      $scope.isEditing = true;
      $scope.message = '';
      $scope.orderDeliveryDate = processDeliveryDate();
      if (!$scope.order.journey) return;
      if (!$scope.order.journey.scents)
        initScents();
    };

    $scope.selectStyle = function (style) {
      $scope.order.customBottle.style = style;
    };

    $scope.selectStatus = function (status) {
      $scope.order.status = status;
      if (status == 'Delivered') {
        if ((!$scope.order.deliveredOn) || ($scope.order.deliveredOn == ''))
          $scope.order.deliveredOn = (new Date()).toISOString();
      }
    };

    $scope.selectSamplesStatus = function (status) {
      $scope.order.samplesStatus = status;
    };

    $scope.selectGender = function (gender) {
      $scope.order.gender = gender;
    };

    $scope.selectWarehouse = function (warehouseId) {
      $scope.order.warehouse = warehouseId;
      refreshScents();
    };

    $scope.selectOrderSize = function (orderSize) {
      $scope.order.orderSize = orderSize;
    };

    $scope.selectMainOrderSize = function (orderSize) {
      $scope.order.orderSizeMain = orderSize;
    };

    $scope.refreshTable = function () {
      getPage(paginationOptions.pageNumber, paginationOptions.pageSize)
    };

    $scope.getTrackingUrl = function () {
      return "http://track.waft.com/" + $scope.order.shippingInfo.trackingNumber.replace(/\s/g, '');
    };

    $scope.save = function () {

      if ($scope.isProcessing) return;
      startProcessing();

      order.post($scope.order).then(
        function (data) {
          if (data.success) {
            $scope.message = data.message;
            $scope.isEditing = false;
            $scope.showMore($scope.order._id, true);
            $scope.refreshTable();
          } else {
            $scope.message = data.message;
          }
          endProcessing();
        },
        function (err) {
          if (err && err.data && err.data.message)
            $scope.message = err.data.message;
          else
            $scope.message = "Unknown Server Error";
          endProcessing();
        }
      );
    }

    $scope.cancel = function () {
      $scope.isEditing = false;
      $scope.message = '';

      $scope.showMore($scope.order._id, true);
    };

    function getScentByCode(code) {
      for (var i = 0; i < $scope.scents.length; i++) {
        var scent = $scope.scents[i];
        if (scent.code == code) return scent;
      }
      return null;
    }

    $scope.selectScent = function (code, type) {
      if (!$scope.order.journey.scents) initScents();
      var scent = getScentByCode(code);
      switch (type) {
        case 'main':
          $scope.baseScent = scent.code;
          $scope.order.journey.scents.main = scent;
          break;
        case 'add1':
          $scope.add1Scent = scent.code;
          $scope.order.journey.scents.add1.scent = scent;
          break;
        case 'add2':
          $scope.add2Scent = scent.code;
          $scope.order.journey.scents.add2.scent = scent;
          break;
      }
    };

    $scope.selectAddType = function (addType, type) {
      if (!$scope.order.journey.scents) initScents();
      switch (type) {
        case 'add1':
          $scope.order.journey.scents.add1.addType = addType;
          break;
        case 'add2':
          $scope.order.journey.scents.add2.addType = addType;
          break;
      }
    };

    function startProcessing() {
      $scope.message = 'Processing...';
      $scope.isProcessing = true;
    }

    function endProcessing() {
      $scope.isProcessing = false;
    }

    function initScents() {
      $scope.order.journey.scents = {
        main: '',
        add1: {
          addType: '',
          scent: ''
        },
        add2: {
          addType: '',
          scent: ''
        }
      };
    }

    $scope.baseScent = '';
    $scope.add1Scent = '';
    $scope.add2Scent = '';

    $scope.baseScentConfig = {
      maxItems: 1,
      create: false,
      valueField: 'code',
      labelField: 'name',
      delimiter: '|',
      searchField: ['code', 'name'],
      placeholder: 'Select',
      onChange: function (value) {
        $scope.selectScent(value, 'main')
      },
      render: {
        item: function (item, escape) {
          return "<div>" + escape($scope.getScentFullName(item, true, true)) + "</div>";
        },
        option: function (item, escape) {
          return "<div>" + escape($scope.getScentFullName(item, true, true)) + "</div>";
        }
      }
    };

    $scope.add1ScentConfig = {
      maxItems: 1,
      create: false,
      valueField: 'code',
      labelField: 'name',
      delimiter: '|',
      searchField: ['code', 'name'],
      placeholder: 'Select',
      onChange: function (value) {
        $scope.selectScent(value, 'add1')
      },
      render: {
        item: function (item, escape) {
          return "<div>" + escape($scope.getScentFullName(item, false, true)) + "</div>";
        },
        option: function (item, escape) {
          return "<div>" + escape($scope.getScentFullName(item, false, true)) + "</div>";
        }
      }
    };

    $scope.add2ScentConfig = {
      maxItems: 1,
      create: false,
      valueField: 'code',
      labelField: 'name',
      delimiter: '|',
      searchField: ['code', 'name'],
      placeholder: 'Select',
      onChange: function (value) {
        $scope.selectScent(value, 'add2')
      },
      render: {
        item: function (item, escape) {
          return "<div>" + escape($scope.getScentFullName(item, false, true)) + "</div>";
        },
        option: function (item, escape) {
          return "<div>" + escape($scope.getScentFullName(item, false, true)) + "</div>";
        }
      }
    };

    function defineScents(_order) {
      $scope.baseScent = '';
      $scope.add1Scent = '';
      $scope.add2Scent = '';

      if (_order.journey && _order.journey.scents) {
        if (_order.journey.scents.main)
          $scope.baseScent = _order.journey.scents.main.code;
        if (_order.journey.scents.add1.scent)
          $scope.add1Scent = _order.journey.scents.add1.scent.code;
        if (_order.journey.scents.add2.scent)
          $scope.add2Scent = _order.journey.scents.add2.scent.code;
      }
    }

    var paginationOptions = {
      pageNumber: 1,
      pageSize: 50,
      sort: null
    };

    $scope.searchByStatus = function (st) {
      $scope.searchStatus = (st === $scope.searchStatuses[0]) ? null : st;
      $scope.search();
    };

    $scope.searchByWarehouse = function (st) {
      $scope.searchWarehouse = (st === $scope.searchWarehouses[0]) ? null : st;
      $scope.search();
    };

    $scope.searchByDate = function (d) {
      $scope.searchDate = (d === $scope.searchStatuses[0]) ? null : d;
      $scope.search();
    };

    $scope.getExportLink = function (fields) {
      var url = '/api/admin/orders/export.csv';
      var params = [];
      if ($scope.searchStatus)
        params.push({ name: 'status', value: $scope.searchStatus });
      if ($scope.searchDate)
        params.push({ name: 'dates', value: $scope.searchDate });
      if ($scope.searchWarehouse)
        params.push({ name: 'warehouse', value: $scope.warehouseId($scope.searchWarehouse) });
      if (fields)
        params.push({ name: 'fields', value: fields});
      let query = params.map( v => v.name + '=' + encodeURIComponent(v.value)).join('&');
      if (query)
        url += '?' + query;
      return url;
    };

    $scope.searchBySamplesStatus = function (st) {
      $scope.searchSamplesStatus = st;
      $scope.search();
    };

    $scope.search = function () {
      getPage(1, paginationOptions.pageSize);
    };

    var dateFormat = "yyyy-MM-dd HH:mm:ss Z";

    $scope.gridOptions = {
      paginationPageSizes: [10, 50, 100],
      paginationPageSize: 50,
      useExternalPagination: true,
      enableColumnMenus: false,
      columnDefs: [
        {
          field: '_id',
          displayName: 'ID',
          width: 200,
          cellTemplate: '<div class="ui-grid-cell-contents"><a href="" ng-click="grid.appScope.showMore(row.entity._id)">{{row.entity._id}}</a></div>'
        },
        {
          field: 'countryCode',
          displayName: 'NAME',
          width: 200,
          cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.sender.firstName + " " + row.entity.sender.lastName}}</div>'
        },
        {
          field: 'sender.email',
          displayName: 'EMAIL',
          width: 240
        },
        {
          field: 'countryCode',
          displayName: 'CC',
          cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.countryCode | uppercase}}</div>'
        },
        {
          field: 'createdAt',
          displayName: 'DATE',
          width: 130,
          type: 'date',
          cellFilter: 'date:\'yyyy-MM-dd HH:mm\''
        },
        {
          field: 'type',
          displayName: 'TYPE',
          width: 150,
          cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.type}} {{row.entity.orderSize}}</div>'
        },
        {
          field: 'amount',
          displayName: 'AMOUNT',
          width: 90,
          cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.currency|uppercase}}{{row.entity.amount}}</div>'
        },
        {field: 'status', displayName: 'STATUS', width: 100}
      ],

      onRegisterApi: function (gridApi) {
        $scope.gridApi = gridApi;

        gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
          paginationOptions.pageNumber = newPage;
          paginationOptions.pageSize = pageSize;
          getPage(newPage, pageSize);
        });
      }
    };

    $scope.showMore = function (orderId, refresh) {
      if (!orderId) return;
      order.get(orderId).then(
        function (data) {
          $scope.order = data.order;
          refreshScents();
          $scope.countries = country.getList();
          if (!refresh) $scope.message = '';
          defineScents($scope.order);
          $scope.isEditing = false;
          $scope.orderDeliveryDate = processDeliveryDate();
          $scope.getOtherOrders(data.order.sender._id, orderId);
          var i = getOrderIndex(orderId);
          if (i != -1) {
            var all_orders = $scope.gridOptions.data;
            $scope.nextOrder = (i != 0) ? all_orders[i - 1]._id : null;
            $scope.prevOrder = (i != (all_orders.length - 1)) ? all_orders[i + 1]._id : null;
          }
        },
        function (errResponse) {
        }
      );
    };

    function getOrderIndex(orderId) {
      var all_orders = $scope.gridOptions.data;
      for (var i = 0; i < all_orders.length; i++)
        if (all_orders[i]._id == orderId)
          return i;
      return -1;
    }

    $scope.getOtherOrders = function (userid, excludedOrder) {
      order.getList({ userid, excludedOrder}).then(
        function (data) {
          $scope.otherOrders = data.docs;
        },
        function (errResponse) {
          $scope.otherOrders = [];
        }
      );
    };

    $scope.selectCountry = function (country) {
      $scope.order.delivery.country = country.country;
    };

    $scope.getFragranceDescription = function (f, main_order) {
      if (!f) return '';
      var sex = (f.male && f.female) ? 'unisex' : (f.male ? "men's" : "women's");
      var accords = [];
      f.accords = f.accords.sort(function (a, b) {
        return b.weight - a.weight;
      });
      for (var i = 0; i < f.accords.length; i++) {
        accords.push(f.accords[i].name)
      }
      var scents = '';
      if (f.scents.main && f.scents.main.length > 0) {
        scents = ' ' + f.scents.main.join('-');
        if (f.scents.unisexwomen && f.scents.unisexwomen.length > 0)
          scents += ' UW:' + f.scents.unisexwomen.join('-');
        if (f.scents.unisexmen && f.scents.unisexmen.length > 0)
          scents += ' UM:' + f.scents.unisexmen.join('-');
      } else if (main_order && $scope.order.recommendations && $scope.order.recommendations.history) {
        scents = ' History: ' + $scope.order.recommendations.history.join('-');
      }
      return f.name + ' (' + sex + ' / ' + (f.family || 'Unknown family ') + ' / ' + f.main_accord +
        ' / ' + accords.join(', ') + ')' + scents;
    };

    function getSearchHash(page, take) {
      return $scope.term + $scope.searchStatus + $scope.searchSamplesStatus + $scope.searchDate
    }

    var getPage = function (page, take) {
      var searchHash = getSearchHash(page, take);
      order.getList({
        take,
        page,
        term : $scope.term,
        status: $scope.searchStatus,
        samplesStatus: $scope.searchSamplesStatus,
        warehouse: $scope.warehouseId($scope.searchWarehouse),
        dates: $scope.searchDate
      })
        .then(
          function (data) {
            if (searchHash != getSearchHash(page, take)) return;
            $scope.gridOptions.data = data.docs;
            $scope.gridOptions.totalItems = data.count;
          },
          function (errResponse) {
          }
      );
    };

    function readWarehouses() {
      warehouse.getList().then(
        function (data) {
          $scope.warehouses = data.warehouses;
          $scope.searchWarehouses = ['All Warehouses'].concat(data.warehouses.map(w => w.name));
        },
        function (errResponse) {
        }
      );
    }

    readWarehouses();
    getPage(paginationOptions.pageNumber, paginationOptions.pageSize);
  }
})();
