(function () {
    'use strict';

    angular
        .module('adminApp')
        .controller('FragrancesCtrl', FragrancesCtrl);

    FragrancesCtrl.$inject = ['$scope', 'fileUpload', 'fragrances'];

    function FragrancesCtrl($scope, fileUpload, fragrances) {

        $scope.newFile = {};
        $scope.message = '';
        $scope.details = '';
        $scope.term = '';
        $scope.fragrance = null;
        $scope.journeys = [];

        var paginationOptions = {
            pageNumber: 1,
            pageSize: 50,
            sort: null
        };

        $scope.gridOptions = {
            paginationPageSizes: [10, 50, 100],
            paginationPageSize: 50,
            useExternalPagination: true,
            enableColumnMenus: false,
            columnDefs: [
                {
                    field: 'brand',
                    displayName: 'Brand',
                    width: 150
                },
                {
                    field: 'short_name',
                    displayName: 'Name',
                    width: 150,
                    cellTemplate: '<div class="ui-grid-cell-contents"><a href="" ng-click="grid.appScope.showMore(row.entity._id)">{{row.entity.short_name}}</a></div>'
                },
                {
                    field: 'description',
                    displayName: 'Description',
                    minWidth: 600
                },
                {
                    field: 'scents.main',
                    displayName: 'Main',
                    width: 100,
                    cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.scents.main.join("-")}}</div>'
                },
                {
                    field: 'scents.unisexwomen',
                    displayName: 'UW',
                    width: 100,
                    cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.scents.unisexwomen.join("-")}}</div>'
                },
                {
                    field: 'scent.unisexmen',
                    displayName: 'UM',
                    width: 100,
                    cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.scents.unisexmen.join("-")}}</div>'
                }
            ],

            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;

                gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
                    paginationOptions.pageNumber = newPage;
                    paginationOptions.pageSize = pageSize;
                    getPage(newPage, pageSize);
                });
            }
        };

        $scope.showMore = function (fragranceId) {
            fragrances.get(fragranceId).then(
                function (data) {
                    $scope.fragrance = data.fragrance;
                    getJourneys(fragranceId);
                },
                function (errResponse) {
                }
            );
        };

        $scope.search = function() {
            getPage(1, paginationOptions.pageSize);
        };

        $scope.getJourneyDesc = function (journey) {
            if (!journey) return 'n/a';
            var gift = journey.gift;
            if (typeof gift === 'undefined')
                gift = ($scope.order.customPackaging.firstName === $scope.order.sender.firstName);
            if (gift === true) gift = 'gift';
            else gift = 'not a gift';
            var result = [gift, journey.recipientGender, journey.gender, journey.timeOfDay,
                journey.activity, journey.mood, journey.personalStyle, journey.waft];
            return result.join(' / ');
        };

        var element = document.getElementById('newFile');

        element.addEventListener('change', function (e) {
            $scope.newFile = e.target.files[0];
        });

        $scope.uploadFile = function () {
            var file = $scope.newFile;
            if (file == undefined) {
                return;
            }

            var uploadUrl = "/api/admin/fragrances";

            $scope.message = "Uploading...";
            $scope.details = '';

            fileUpload.uploadFileToUrl(file, uploadUrl, 'fragranceFile')
                .then(function ok(result) {
                        $scope.message = result.message;
                        $scope.details = JSON.stringify(result, null, '    ');
                        document.getElementById('newFile').value = "";
                    },
                    function err(err) {
                        $scope.message = err.message;
                        $scope.details = JSON.stringify(err, null, '    ');
                    });
        };

        getPage(paginationOptions.pageNumber, paginationOptions.pageSize);

        function getPage(page, take) {

            fragrances.getList($scope.term, take, page).then(
                function (data) {
                    $scope.gridOptions.data = data.docs;
                    $scope.gridOptions.totalItems = data.count;
                },
                function (errResponse) {
                }
            );
        }

        function getJourneys (fragranceId) {
            fragrances.getJourneys(fragranceId).then(
                function (data) {
                    $scope.journeys = data.journeys;
                },
                function (errResponse) {
                    $scope.journeys = [];
                });
        };
    }

})();
