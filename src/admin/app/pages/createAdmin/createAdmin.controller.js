(function () {
    'use strict';

    angular
        .module('adminApp')
        .controller('CreateAdminCtrl', CreateAdminCtrl);

    CreateAdminCtrl.$inject = ['$scope', '$state', '$location', 'admin'];

    function CreateAdminCtrl($scope, $state, $location, admin) {

        if ($scope.getAccessLevel() != 'admin')
            $location.path('/orders');

        $scope.header = 'CREATE NEW ADMIN ACCOUNT';
        $scope.message = '';
        $scope.isEditing = false;

        $scope.admin = {
            email: '',
            password1: '',
            password2: '',
            status: '',
            accessLevel: ''
        }

        $scope.statuses = ['Active', 'Inactive'];
        $scope.accessLevels = ['admin', 'operator', 'readonly'];

        $scope.selectStatus = function (status) {
            $scope.admin.status = status;
        }
        $scope.selectAccessLevel = function (accessLevel) {
            $scope.admin.accessLevel = accessLevel;
        }

        $scope.save = function () {
            admin.post($scope.admin).then(
                function (data) {
                    $scope.message = data.message;

                    if (data.success) {
                        $state.go('app.adminsAccounts');
                    }
                },
                function (errResponse) {
                }
            );
        }
    }

})();
