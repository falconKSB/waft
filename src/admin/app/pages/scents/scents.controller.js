(function () {
    'use strict';

    angular
        .module('adminApp')
        .controller('ScentsCtrl', ScentsCtrl);

    ScentsCtrl.$inject = ['$rootScope', '$scope', '$state', '$location', 'uiGridConstants', 'scent', 'authService', 'fileUpload', 'warehouse'];

    function ScentsCtrl($root, $scope, $state, $location, uiGridConstants, scent, authService, fileUpload, warehouse) {

        $scope.warehouses = '';
        $scope.warehouse = '';
        $scope.newFile = {};
        $scope.message = '';
        $scope.details = '';
        $scope.warehouseToUpload = 'US';

        var paginationOptions = {
            pageNumber: 1,
            pageSize: 50,
            sort: null
        };

        var baseNameTemplate = "<div class='ui-grid-cell-contents'><a href='' ng-click='grid.appScope.editScent(row.entity._id)'>{{row.entity.name}}</a><div>";
        var readonlyNameTemplate = "<div class='ui-grid-cell-contents'>{{row.entity.name}}<div>";

        $scope.setWarehouse = function (warehouse) {
            if ($scope.warehouse != warehouse) {
                $scope.warehouse = warehouse;
                getPage(paginationOptions.pageNumber, paginationOptions.pageSize, $scope.warehouse);
            }
        };

        $scope.gridOptions = {
            paginationPageSizes: [50, 100],
            paginationPageSize: 50,
            useExternalPagination: true,
            enableColumnMenus: false,
            columnDefs: [
                {
                    field: 'code', displayName: 'CODE'
                },
                {
                    field: 'name', displayName: 'NAME',
                    cellTemplate: isReadOnly() ? readonlyNameTemplate : baseNameTemplate
                },
                {
                    field: 'family', displayName: 'FAMILY'
                },
                {
                    field: 'scentGender', displayName: 'GENDER'
                },
                {
                    field: 'availableQuantity.big', displayName: '100ml',
                    cellTemplate: "<div class='ui-grid-cell-contents'><span >{{row.entity.availableQuantity.big || 0}}</span></div>"
                },
                {
                    field: 'availableQuantity.medium', displayName: '50ml',
                    cellTemplate: "<div class='ui-grid-cell-contents'><span >{{row.entity.availableQuantity.medium || 0}}</span></div>"
                },
                {
                    field: 'availableQuantity.small', displayName: '15ml',
                    cellTemplate: "<div class='ui-grid-cell-contents'><span >{{row.entity.availableQuantity.small || 0}}</span></div>"
                },
                {
                    field: 'availableQuantity.samples', displayName: '5ml',
                    cellTemplate: "<div class='ui-grid-cell-contents'><span >{{row.entity.availableQuantity.samples || 0}}</span></div>"
                },
                {field: 'status', displayName: 'STATUS'}],

            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;

                gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
                    paginationOptions.pageNumber = newPage;
                    paginationOptions.pageSize = pageSize;
                    getPage(newPage, pageSize);
                });
            }
        };

        $scope.editScent = function (scentId) {
            $location.path('/scents/edit/' + scentId);
        };

        var element = document.getElementById('newFile');

        element.addEventListener('change', function (e) {
            $scope.newFile = e.target.files[0];
        });

        $scope.uploadFile = function () {
            var file = $scope.newFile;
            if (file == undefined) {
                return;
            }

            var uploadUrl = "/api/admin/inventory?warehouse=" + $scope.warehouseToUpload;

            $scope.message = "Uploading...";
            $scope.details = '';

            fileUpload.uploadFileToUrl(file, uploadUrl, 'inventoryFile')
                .then(function ok(result) {
                        $scope.message = result.message;
                        $scope.details = JSON.stringify(result, null, '    ');
                        document.getElementById('newFile').value = "";
                    },
                    function err(err) {
                        $scope.message = err.message;
                        $scope.details = JSON.stringify(err, null, '    ');
                    });
        };

        var getPage = function (page, take, warehouse) {

            scent.getList(take, page, warehouse).then(
                function (data) {
                    $scope.gridOptions.data = data.docs;
                    $scope.gridOptions.totalItems = data.count;
                },
                function (errResponse) {
                }
            );
        };

        function isReadOnly() {
            return authService.getAccessLevel() == 'readonly';
        }

        function readWarehouses() {
            warehouse.getList().then(
                function (data) {
                    $scope.warehouses = data.warehouses;
                },
                function (errResponse) {
                }
            );
        }

        readWarehouses();
        getPage(paginationOptions.pageNumber, paginationOptions.pageSize, $scope.warehouse);

    }

})();
