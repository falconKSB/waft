(function () {
    'use strict';

    angular
        .module('adminApp')
        .controller('LoginCtrl', LoginCtrl);

    LoginCtrl.$inject =
        [
            '$rootScope',
            '$state',
            'authService',
        ];

    function LoginCtrl($root, $state, auth) {

        var vm = this;

        vm.message = "";

        vm.loginData = {
            username: "",
            password: ""
        };

        vm.login = function () {
            fixAutofill();

            auth.login(vm.loginData).then(
                function (result) {
                    $state.go('app.orders');
                },
                function (err) {
                    vm.message = err.message;
                }
            );
        };

        function fixAutofill() {
            var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);

            if (iOS) {
                vm.loginData.username = $('input[name="username"]').val();
                vm.loginData.password = $('input[name="password"]').val();
            }
        }
    }

})();