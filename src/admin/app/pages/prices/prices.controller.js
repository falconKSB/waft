(function () {
    'use strict';

    angular
        .module('adminApp')
        .controller('PricesCtrl', PricesCtrl);

    PricesCtrl.$inject = ['$rootScope', '$scope', '$state', '$location', 'uiGridConstants', 'prices'];

    function PricesCtrl($root, $scope, $state, $location, uiGridConstants, prices) {

        var getPage = function () {

            prices.getPrices().then(
                function (data) {
                    $scope.data = data;
                },
                function (errResponse) {
                }
            );
        };

        $scope.getPage = getPage;

        getPage();

    }

})();
