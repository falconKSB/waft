(function () {
    'use strict';

    angular
        .module('adminApp')
        .controller('AdminsCtrl', AdminsCtrl);

    AdminsCtrl.$inject = ['$rootScope', '$scope', '$state', '$location', 'uiGridConstants', 'admin'];

    function AdminsCtrl($root, $scope, $state, $location, uiGridConstants, admin) {

        if ($scope.getAccessLevel() != 'admin')
            $location.path('/orders');

        var paginationOptions = {
            pageNumber: 1,
            pageSize: 50,
            sort: null
        };

        $scope.gridOptions = {
            paginationPageSizes: [50, 100],
            paginationPageSize: 50,
            useExternalPagination: true,
            enableColumnMenus: false,
            columnDefs: [
                {
                    field: 'email', displayName: 'EMAIL',
                    cellTemplate: '<div class="ui-grid-cell-contents"> <a href="" ng-click="grid.appScope.editAdmin(row.entity._id)">{{row.entity.email}}</a><div>'
                },
                {field: 'status', displayName: 'STATUS'}],

            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;

                gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
                    paginationOptions.pageNumber = newPage;
                    paginationOptions.pageSize = pageSize;
                    getPage(newPage, pageSize);
                });
            }
        };

        $scope.editAdmin = function (adminId) {
            $location.path('/adminsAccounts/edit/' + adminId);
        };

        var getPage = function (page, take) {

            admin.getList(take, page).then(
                function (data) {
                    $scope.gridOptions.data = data.docs;
                    $scope.gridOptions.totalItems = data.count;
                },
                function (errResponse) {
                }
            );
        };

        getPage(paginationOptions.pageNumber, paginationOptions.pageSize);

    }

})();