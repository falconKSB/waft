(function () {
    'use strict';

    angular
        .module('adminApp')
        .controller('EditAdminCtrl', EditAdminCtrl);

    EditAdminCtrl.$inject = ['$scope', '$state', '$location', '$stateParams', 'admin'];

    function EditAdminCtrl($scope, $state, $location, $stateParams, admin) {

        if ($scope.getAccessLevel() != 'admin')
            $location.path('/orders');

        $scope.adminId = $stateParams.adminId;
        $scope.isEditing = true;

        $scope.header = 'EDIT';
        $scope.message = '';

        $scope.admin = {
            email: '',
            password1: '',
            password2: '',
            status: '',
            accessLevel: ''
        }

        $scope.statuses = ['Active', 'Inactive'];
        $scope.accessLevels = ['admin', 'operator', 'readonly'];

        admin.get($scope.adminId).then(
            function (data) {
                $scope.admin = data.admin;
                $scope.header = 'EDIT ' + $scope.admin.email;
            },
            function (errResponse) {
            }
        );

        $scope.selectStatus = function (status) {
            $scope.admin.status = status;
        }
        $scope.selectAccessLevel = function (accessLevel) {
            $scope.admin.accessLevel = accessLevel;
        }

        $scope.save = function () {
            admin.put($scope.admin).then(
                function (data) {
                    $scope.message = data.message;

                    if (data.success) {
                        $state.go('app.adminsAccounts');
                    }
                },
                function (errResponse) {
                }
            );
        }
    }

})();
