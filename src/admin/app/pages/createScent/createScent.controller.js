(function () {
    'use strict';

    angular
        .module('adminApp')
        .controller('CreateScentCtrl', CreateScentCtrl);

    CreateScentCtrl.$inject = ['$rootScope', '$scope', '$state', '$location', 'uiGridConstants', 'scent'];

    function CreateScentCtrl($root, $scope, $state, $location, uiGridConstants, scent) {

        if ($scope.getAccessLevel() == 'readonly')
            $location.path('/orders');

        $scope.header = 'CREATE NEW SCENT';

        $scope.scent = {
            name: '',
            code: '',
            iffName: '',
            iffCode: '',
            family: '',
            subFamily: '',
            subGroup: '',
            subGroup2: '',
            subGroup3: '',
            subGroup4: '',
            marketReference: '',
            keyIngredients: '',
            otherIngredients: '',
            description: '',
            scentMood: '',
            scent: '',

            timeOfDay: [],
            mood: [],
            personalStyle: [],
            gender: [],
            scentGender: '',
            activity: [],
            waft: '',
            status: '',
            inStock: false
        };

        $scope.statuses = ['Active', 'Inactive'];
        $scope.activities = ['sport', 'social', 'work', 'dating'];
        $scope.wafts = ['subtle', 'balanced', 'daring'];
        $scope.genders = ['man', 'woman', 'unisex'];
        $scope.moods = ['fresh', 'relaxed', 'elegant', 'sexy'];
        $scope.personalStyles = ['classic', 'trendy'];
        $scope.timeOfDays = ['day', 'night'];

        $scope.save = function () {

            scent.post($scope.scent).then(
                function (data) {
                    if (data.success) {
                        $state.go('app.scents');
                    } else {
                        $scope.message = data.message;
                    }
                },
                function (err) {
                    if (err && err.data && err.data.message)
                        $scope.message = err.data.message;
                    else
                        $scope.message = "Unknown Server Error"
                }
            );
        }

        $scope.sync = function (bool, item, resultList) {

            if (bool) {
                // add item
                resultList.push(item);

            } else {
                // remove item
                for (var i = 0; i < resultList.length; i++) {
                    if (resultList[i] == item) {
                        resultList.splice(i, 1);
                    }
                }
            }
        };

        $scope.isChecked = function (item, resultList) {
            var match = false;
            for (var i = 0; i < resultList.length; i++) {
                if (resultList[i] == item) {
                    match = true;
                }
            }
            return match;
        };

    }

})();
