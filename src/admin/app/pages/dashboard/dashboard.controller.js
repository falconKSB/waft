(function () {
    'use strict';

    angular
        .module('adminApp')
        .controller('DashboardCtrl', DashboardCtrl);

    DashboardCtrl.$inject = ['$rootScope', '$scope', '$interval', '$state', '$location', 'uiGridConstants', 'dashboard', 'prices'];

    function DashboardCtrl($root, $scope, $interval, $state, $location, uiGridConstants, dashboard, prices) {

        $scope.country = "";

        $scope.countries = [{countryCode: "", name: "Global"}];
        
        prices.getPrices().then(function(prices) {
            $scope.countries = $scope.countries.concat(prices.countries);
        });

        $scope.countriesConfig = {
            maxItems: 1,
            create: false,
            valueField: 'countryCode',
            labelField: 'name',
            delimiter: '|',
            searchField: ['name'],
            placeholder: 'Select',
            onChange: function (value) {
                getPage($scope.country)
            }
        };
        
        var getPage = function (country) {

            dashboard.get(country).then(
                function (data) {
                    $scope.data = data;
                },
                function (errResponse) {
                }
            );
        };

        $scope.getPage = getPage;

        getPage($scope.country);

        var refresh = $interval(function() {
            getPage($scope.country);
        }, 60*1000);

        $scope.$on('$destroy', function() {
            $interval.cancel(refresh);
        });
        
    }

})();
