(function () {
    'use strict';

    angular
        .module('adminApp')
        .controller('UsersCtrl', UsersCtrl);

    UsersCtrl.$inject = ['$rootScope', '$scope', 'user', 'order', 'journeys'];

    function UsersCtrl($root, $scope, user, order, journeys) {

        $scope.term = '';

        $scope.user = null;
        $scope.logs = null;
        $scope.message = '';
        $scope.isEditing = false;
        $scope.isProcessing = false;
        $scope.orders = [];
        $scope.journeys = [];
        $scope.showjson = false;

        var oldEmail = '';

        $scope.refreshTable = function () {
            getPage(paginationOptions.pageNumber, paginationOptions.pageSize)
        };

        $scope.cancel = function () {
            $scope.isEditing = false;
            $scope.message = '';
            $scope.user.email = oldEmail;
        };

        function startProcessing() {
            $scope.message = 'Processing...';
            $scope.isProcessing = true;
        }

        function endProcessing() {
            $scope.isProcessing = false;
        }

        var paginationOptions = {
            pageNumber: 1,
            pageSize: 50,
            sort: null
        };

        $scope.search = function () {
            getPage(1, paginationOptions.pageSize);
        };

        $scope.gridOptions = {
            paginationPageSizes: [10, 50, 100],
            paginationPageSize: 50,
            useExternalPagination: true,
            enableColumnMenus: false,
            columnDefs: [
                {
                    field: '_id',
                    displayName: 'ID',
                    width: 300,
                    cellTemplate: '<div class="ui-grid-cell-contents"><a href="" ng-click="grid.appScope.showMore(row.entity._id)">{{row.entity._id}}</a></div>'
                },
                {
                    field: 'email',
                    displayName: 'EMAIL',
                    width: 300
                },
                {
                    field: 'firstName',
                    displayName: 'NAME',
                    cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.firstName + " " + row.entity.lastName}}</div>'
                }
            ],
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;

                gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
                    paginationOptions.pageNumber = newPage;
                    paginationOptions.pageSize = pageSize;
                    getPage(newPage, pageSize);
                });
            }
        };

        $scope.showMore = function (userId, refresh) {
            user.get(userId).then(
                function (data) {
                    oldEmail = data.user.email;

                    $scope.user = data.user;
                    $scope.logs = data.logs;
                    if (!refresh) $scope.message = '';
                    $scope.isEditing = false;
                    $scope.getOrders($scope.user._id);
                    $scope.getJourneys($scope.user._id);
                },
                function (errResponse) {
                }
            );
        };

        $scope.save = function () {
            if ($scope.isProcessing) return;
            startProcessing();

            user.update($scope.user).then(
                function (data) {
                    if (data.success) {
                        oldEmail = '';

                        $scope.message = data.message;
                        $scope.isEditing = false;
                        $scope.showMore($scope.user._id, true);
                        $scope.refreshTable();
                    } else {
                        $scope.message = data.message;
                    }
                    endProcessing();
                },
                function (err) {
                    if (err && err.data && err.data.message)
                        $scope.message = err.data.message;
                    else
                        $scope.message = "Unknown Server Error";
                    endProcessing();
                }
            );
        };

        $scope.getOrders = function (userid) {
            order.getList({userid}).then(
                function (data) {
                    $scope.orders = data.orders;
                },
                function (errResponse) {
                    $scope.orders = [];
                });
        };

        $scope.getJourneys = function (userId) {
            journeys.getUserJourneys(userId).then(
                function (data) {
                    $scope.journeys = data.journeys;
                },
                function (errResponse) {
                    $scope.journeys = [];
                });
        };

        $scope.editEmail = function () {
            $scope.isEditing = true;
            $scope.message = '';
        };

        var getPage = function (page, take) {
            user.getList($scope.term, take, page).then(
                function (data) {
                    $scope.gridOptions.data = data.docs;
                    $scope.gridOptions.totalItems = data.count;
                },
                function (errResponse) {
                }
            );
        };

        getPage(paginationOptions.pageNumber, paginationOptions.pageSize);
    }
})();
