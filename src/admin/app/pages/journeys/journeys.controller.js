(function () {
    'use strict';

    angular
        .module('adminApp')
        .controller('JourneysCtrl', JourneysCtrl);

    JourneysCtrl.$inject = ['$rootScope', '$scope', '$state', '$location', 'uiGridConstants', 'journeys'];

    function JourneysCtrl($root, $scope, $state, $location, uiGridConstants, journeys) {

        var paginationOptions = {
            pageNumber: 1,
            pageSize: 50,
            sort: null
        };

        $scope.gridOptions = {
            paginationPageSizes: [50, 100],
            paginationPageSize: 50,
            useExternalPagination: true,
            enableColumnMenus: false,
            columnDefs: [
                {
                    field: 'createdAt',
                    displayName: 'DATE',
                    width: 130,
                    type: 'date',
                    cellFilter: 'date:\'yyyy-MM-dd HH:mm\''
                },
                {
                    field: '_id',
                    displayName: 'ID',
                    width: 200
                },
                {
                    field: 'email',
                    displayName: 'EMAIL',
                    cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.user?row.entity.user.email:(row.entity.email+" (email)")}}</div>',
                    width: 200
                },
                {
                    field: 'status',
                    displayName: 'STATUS',
                    width: 100
                },
                {
                    field: 'step',
                    displayName: 'LAST STEP',
                    width: 200
                },
                {
                    field: 'answers',
                    displayName: 'ANSWERS',
                    cellTemplate: "<div class='ui-grid-cell-contents'>"
                        +"<span>{{row.entity.gender? row.entity.recipientGender+' / ':''}}</span>"
                        +"<span>{{row.entity.gender? row.entity.gender+' / ':''}}</span>"
                        +"<span>{{row.entity.timeOfDay? row.entity.timeOfDay+' / ':''}}</span>"
                        +"<span>{{row.entity.activity? row.entity.activity+' / ':''}}</span>"
                        +"<span>{{row.entity.mood? row.entity.mood+' / ':''}}</span>"
                        +"<span>{{row.entity.personalStyle? row.entity.personalStyle+' / ':''}}</span>"
                        +"<span>{{row.entity.waft? row.entity.waft:''}}</span></div>"
                }
            ],

            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;

                gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
                    paginationOptions.pageNumber = newPage;
                    paginationOptions.pageSize = pageSize;
                    getPage(newPage, pageSize);
                });
            }
        };

        var getPage = function (page, take) {

            journeys.getList(take, page).then(
                function (data) {
                    $scope.gridOptions.data = data.docs;
                    $scope.gridOptions.totalItems = data.count;
                },
                function (errResponse) {
                }
            );
        };

        getPage(paginationOptions.pageNumber, paginationOptions.pageSize);

    }

})();