(function () {
    'use strict';

    angular
        .module('adminApp')
        .controller('EditScentCtrl', EditScentCtrl);

    EditScentCtrl.$inject = ['$rootScope', '$scope', '$state', '$location', 'uiGridConstants', 'scent', '$stateParams'];

    function EditScentCtrl($root, $scope, $state, $location, uiGridConstants, scent, $stateParams) {

        if ($scope.getAccessLevel() == 'readonly')
            $location.path('/orders');

        $scope.scentId = $stateParams.scentId;

        $scope.header = 'EDIT SCENT';

        $scope.scent = {
            name: '',
            code: '',
            iffName: '',
            iffCode: '',
            family: '',
            subFamily: '',
            subGroup: '',
            subGroup2: '',
            subGroup3: '',
            subGroup4: '',
            marketReference: '',
            keyIngredients: '',
            otherIngredients: '',
            description: '',
            scentMood: '',
            scent: '',

            timeOfDay: [],
            mood: [],
            personalStyle: [],
            activity: [],
            gender: [],
            scentGender: '',
            waft: '',
            status: '',
            inStock: false
        };

        scent.get($scope.scentId).then(
            function (data) {
                $scope.scent = data.scent;
            },
            function (errResponse) {
            }
        );

        $scope.statuses = ['Active', 'Inactive'];
        $scope.activities = ['sport', 'social', 'work', 'dating'];
        $scope.wafts = ['subtle', 'balanced', 'daring'];
        $scope.genders = ['man', 'woman', 'unisex'];
        $scope.moods = ['fresh', 'relaxed', 'elegant', 'sexy'];
        $scope.personalStyles = ['classic', 'trendy'];
        $scope.timeOfDays = ['day', 'night'];

        $scope.save = function () {
            scent.post($scope.scent).then(
                function (data) {
                    if (data.success) {
                        $state.go('app.scents');
                    }
                },
                function (errResponse) {
                }
            );
        };

        $scope.sync = function (bool, item, resultList) {

            if (bool) {
                // add item
                resultList.push(item);

            } else {
                // remove item
                for (var i = 0; i < resultList.length; i++) {
                    if (resultList[i] == item) {
                        resultList.splice(i, 1);
                    }
                }
            }
        };

        $scope.isChecked = function (item, resultList) {
            var match = false;
            for (var i = 0; i < resultList.length; i++) {
                if (resultList[i] == item) {
                    match = true;
                }
            }
            return match;
        };

    }

})();
