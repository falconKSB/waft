(function () {
    'use strict';

    angular
        .module('adminApp.services')
        .factory('dashboard', dashboard);

    dashboard.$inject = ['$http', '$q'];

    function dashboard($http, $q) {
        var service = {};

        var crudServiceBaseUrl = "/api/admin/dashboard";

        service.get = function(country) {
            return $http.get(
                crudServiceBaseUrl + (country ? "?country=" + country : '')
            ).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        service.getConversions = function(country) {
            return $http.get(
                crudServiceBaseUrl + '/conversions' + (country ? "?country=" + country : '')
            ).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        return service;
    }
})();
