(function () {
    'use strict';

    angular
        .module('adminApp.services')
        .factory('authService', authentication);

    authentication.$inject = ['$http', '$q', '$state', 'localStorageService', 'CnstApiPath'];

    function authentication($http, $q, $state, storage, apiPath) {

        var TOKENT_STORAGE = "REFRESH_TOKEN";

        var grant = {
            password: 'password',
            refresh: 'refresh_token'
        };

        var principal = {
            authenticated: false,
            remembered: true,

            account: {
                username: null,
                accessLevel: null
            },

            OAuth2: {
                access_token: null,
                refresh_token: undefined,
                token_type: null,
            }
        };

        return {
            login: login,
            logout: logout,
            refresh: refresh,
            getToken: getAccessString,
            getAccount: getAccount,
            getAccessLevel: getAccessLevel,
            isAuthenticated: isAuthenticated,
            isSessionInStore: isSessionInStore
        };
        ////////////////////////////////

        function login(model) {
            var deferred = $q.defer(),

            request = {
                method: 'POST',
                url: apiPath.login,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: getFormData(grant.password, model)
            };

            $http(request).success(
                function (response, status, headers, config) {
                    setCredentials(model, response);
                    deferred.resolve(response);
                }
            )
            .error(deferred.reject);

            return deferred.promise;
        }

        function logout() {
            principal.authenticated = false;
            principal.remembered = false;
            principal.account.username = null;
            principal.account.accessLevel = null;
            principal.OAuth2.access_token = null;
            principal.OAuth2.refresh_token = null;
            principal.OAuth2.token_type = null;
            principal.OAuth2.expires_in = null;

            storage.clearAll();

            $state.go('app.login');
        }

        function refresh() {
            var deferred = $q.defer();

            var request = {
                method: 'POST',
                url: apiPath.token,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: getFormData(grant.refresh),
                options: {'ignoreLoadingBar': true}
            };

            $http(request).success(
                function (response, status, headers, config) {
                    setCredentials(null, response);
                    deferred.resolve(response);
                }
            )
            .error(deferred.reject);

            return deferred.promise;
        }

        function setCredentials(account, oauth2) {
            if (oauth2 == undefined || oauth2 == null) return;

            if (account && account != null) {
                principal.account.email = account.email != undefined ? account.email : principal.account.email;
            }

            if (oauth2 && oauth2 != null) {
                principal.OAuth2.access_token = oauth2.access_token;
                principal.OAuth2.refresh_token = oauth2.refresh_token;
                principal.OAuth2.token_type = oauth2.token_type;

                storage.set(TOKENT_STORAGE, principal.OAuth2.refresh_token);

                principal.account.username = oauth2.currentUser.username;
                principal.account.accessLevel = oauth2.currentUser.accessLevel;
            }

            principal.authenticated = true;
        }

        function getAccessString() {
            return principal.OAuth2.token_type + ' ' + principal.OAuth2.access_token;
        }

        function getRefreshString() {
            return principal.OAuth2.token_type + ' ' + principal.OAuth2.refresh_token;
        }

        function getAccessToken() {
            return principal.OAuth2.access_token;
        }

        function getAccount() {
            return principal.account;
        }

        function getAccessLevel() {
            return principal.account.accessLevel;
        }

        function isAuthenticated() {
            return principal.authenticated;
        }

        function getFormData(grantType, model) {
            switch (grantType) {
                case grant.password: {
                    return "grant_type=" + grantType + "&username="
                        + model.username + "&password=" + model.password;
                }
                case grant.refresh: {
                    return "grant_type=" + grantType + "&refresh_token="
                        + storage.get(TOKENT_STORAGE) || principal.OAuth2.refresh_token;
                }
            }
        }

        function isSessionInStore() {
            return storage.get(TOKENT_STORAGE) ? true : false;
        }
    }

})();