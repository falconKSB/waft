(function () {
    'use strict';

    angular
        .module('adminApp.services')
        .factory('fileUpload', fileUpload);

    fileUpload.$inject = ['$http', '$q'];

    function fileUpload($http, $q) {
        var service = {};

        service.uploadFileToUrl = function (file, uploadUrl, fieldName) {
            var deferred = $q.defer();

            var fd = new FormData();
            fd.append(fieldName, file);

            $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .success(function (result) {
                    if (result.success)
                        deferred.resolve(result);
                    else
                        deferred.reject(result);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        }

        return service;
    }
})();
