(function () {
    'use strict';

    angular
        .module('adminApp.services')
        .factory('admin', admin);

    admin.$inject = ['$http', '$q'];

    function admin($http, $q) {
        var service = {};

        var crudServiceBaseUrl = "/api/admin/accounts";

        service.getList = function (take, page) {
            return $http.get(crudServiceBaseUrl + '?&take=' + take + '&page=' + page).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        service.get = function (adminId) {
            return $http.get(crudServiceBaseUrl + '/' + adminId).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        service.post = function (admin) {
            return $http.post(crudServiceBaseUrl, admin).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        service.put = function (admin) {
            return $http.put(crudServiceBaseUrl, admin).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        return service;
    }
})();