(function () {
    'use strict';

    angular
        .module('adminApp.services')
        .factory('tokenInjector', injector);

    angular
        .module('adminApp.services')
        .factory('tokenRestorer', restorer);

    injector.$inject = ['$injector'];
    restorer.$inject = ['$injector', '$q'];

    function injector($injector) {
        return {
            request: function (config) {
                config.headers = config.headers || {};
                config.headers['Authorization'] = $injector.get('authService').getToken();
                return config;
            }
        };
    }

    function restorer($injector, $q) {
        return {
            responseError: function (response) {
                var auth = $injector.get('authService');

                if (((response.status === 403) || (response.status === 401)) && auth.isAuthenticated()) {

                    if (response.config.url=='/api/admin/token'||!auth.isSessionInStore()) {
                        $injector.get('$state').go('app.login');
                        return $q.reject(response);
                    }
                    // avoid circular dependency
                    var $http = $injector.get('$http');

                    var deferred = $q.defer();

                    // Create a new session (recover the session)
                    // refresh method that logs the user in using the current credentials and
                    // returns a promise
                    auth.refresh().then(deferred.resolve, deferred.reject);

                    // When the session recovered, make the same backend call again and chain the request
                    return deferred.promise.then(function () {
                        return $http(response.config);
                    }, function () {
                        $injector.get('$state').go('app.login');
                    });
                }
                if (((response.status === 403) || (response.status === 401)) && !auth.isAuthenticated()) {
                    $injector.get('$state').go('app.login');
                }

                if ((response.status === 403) || (response.status === 401)) {
                    $injector.get('$state').go('app.login');
                }

                return $q.reject(response);
            }
        };
    }
})();