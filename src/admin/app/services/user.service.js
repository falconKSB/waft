(function () {
    'use strict';

    angular
        .module('adminApp.services')
        .factory('user', user);

    user.$inject = ['$http', '$q'];

    function user($http, $q) {
        var service = {};
        var crudServiceBaseUrl = "/api/admin/users";

        service.get = function (userId) {
            return $http.get(crudServiceBaseUrl + '/' + userId).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        service.getList = function (term, take, page) {
            return $http.get(crudServiceBaseUrl + "?term=" + term + '&take=' + take + '&page=' + page).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        service.update = function (_user) {
            return $http.put(crudServiceBaseUrl, {email: _user.email, id: _user._id}).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        return service;
    }
})();