(function () {
  'use strict';

  angular
    .module('adminApp.services')
    .factory('order', order);

  order.$inject = ['$http', '$q'];

  function order($http, $q) {
    var service = {};

    var crudServiceBaseUrl = "/api/admin/orders";

    service.getList = function (filter) {
      return $http.get(crudServiceBaseUrl, {
        params: filter
      })
        .then(
          function (response) {
            return response.data;
          },
          function (errResponse) {
            return $q.reject(errResponse);
          }
        );
    };

    service.get = function (orderId) {
      return $http.get(crudServiceBaseUrl + '/' + orderId).then(
        function (response) {
          return response.data;
        },
        function (errResponse) {
          return $q.reject(errResponse);
        }
      );
    };

    service.post = function (order) {
      return $http.post(crudServiceBaseUrl, {order: order}).then(
        function (response) {
          return response.data;
        },
        function (errResponse) {
          return $q.reject(errResponse);
        }
      );
    };

    return service;
  }
})();