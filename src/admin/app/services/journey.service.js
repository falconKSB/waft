(function () {
    'use strict';

    angular
        .module('adminApp.services')
        .factory('journeys', journeys);

    journeys.$inject = ['$http', '$q'];

    function journeys($http, $q) {
        var service = {};

        var crudServiceBaseUrl = "/api/admin/journeys";

        service.getList = function (take, page) {
            return $http.get(crudServiceBaseUrl + '?&take=' + take + '&page=' + page).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        service.getUserJourneys = function (userId) {
            return $http.get(crudServiceBaseUrl + '/' + userId).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        return service;
    }
})();