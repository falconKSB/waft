(function () {
    'use strict';

    angular
        .module('adminApp.services')
        .factory('prices', prices);

    prices.$inject = ['$http', '$q'];

    function prices($http, $q) {
        var service = {};

        var crudServiceBaseUrl = "/api/admin/prices";

        service.getPrices = function() {
            return $http.get(
                crudServiceBaseUrl
            ).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        return service;
    }
})();
