(function () {
    'use strict';

    angular
        .module('adminApp.services')
        .factory('warehouse', warehouse);

    warehouse.$inject = ['$http', '$q'];

    function warehouse($http, $q) {
        var service = {};

        var crudServiceBaseUrl = "/api/admin/inventory";

        service.getList = function () {
            return $http.get(crudServiceBaseUrl).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        return service;
    }
})();
