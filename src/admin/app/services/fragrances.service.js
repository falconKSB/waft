(function () {
    'use strict';

    angular
        .module('adminApp.services')
        .factory('fragrances', fragrances);

    fragrances.$inject = ['$http', '$q'];

    function fragrances($http, $q) {
        var service = {};

        var crudServiceBaseUrl = "/api/admin/fragrances";

        service.getList = function (term, take, page) {
            return $http.get(crudServiceBaseUrl + '?term=' + term + '&take=' + take + '&page=' + page).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        service.get = function (id) {
            return $http.get(crudServiceBaseUrl + '/' + id).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        service.getJourneys = function (fragranceId) {
            return $http.get(crudServiceBaseUrl + '/journeys/' + fragranceId).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        return service;
    }
})();