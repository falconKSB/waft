(function () {
    'use strict';

    angular
        .module('adminApp.services')
        .factory('scent', scent);

    scent.$inject = ['$http', '$q'];

    function scent($http, $q) {
        var service = {};

        var crudServiceBaseUrl = "/api/admin/scents";

        service.getList = function (take, page, warehouse) {
            return $http.get(crudServiceBaseUrl + '?&take=' + take + '&page=' + page
                             + (warehouse ? '&warehouse=' + warehouse : '')).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        service.get = function (scentId) {
            return $http.get(crudServiceBaseUrl + '/' + scentId).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        service.post = function (scent) {
            return $http.post(crudServiceBaseUrl, { scent: scent }).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        service.put = function (scent) {
            return $http.get(crudServiceBaseUrl, { scent: scent }).then(
                function (response) {
                    return response.data;
                },
                function (errResponse) {
                    return $q.reject(errResponse);
                }
            );
        };

        return service;
    }
})();
