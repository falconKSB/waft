(function () {
    'use strict';

    // register main app module
    angular.module('adminApp',
        [
            'ngResource',
            'LocalStorageModule',

            'ui.router',
            'ui.bootstrap',
            'ui.grid',
            'ui.grid.pagination',
            'ui.select',

            'selectize',

            'adminApp.layout',
            'adminApp.pages',
            'adminApp.services'

        ]);

    // register all app custom modules
    angular.module('adminApp.services', []);
    angular.module('adminApp.pages', []);
    angular.module('adminApp.layout', []);
})();