(function () {
    'use strict';

    angular
        .module('adminApp')
        .controller('ApplicationCtrl', ApplicationCtrl);

    ApplicationCtrl.$inject =
        [
            '$rootScope',
            '$injector',
            'authService',
            '$state'
        ];

    function ApplicationCtrl($root, $injector, authService, $state) {
        $root.logout = function () {
            authService.logout();
        };

        $root.isLogin = function () {
            return $state.includes('app.login');
        };

        $root.getAccessLevel = function () {
            return authService.getAccessLevel();
        };

        $root.canEdit = function () {
            return authService.getAccessLevel() != 'readonly';
        };

        $root.showMobileMenu = false;
    }

})();