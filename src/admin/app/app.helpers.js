var public_vars = public_vars || {};
var appHelper = {
    // Vars (paths without trailing slash)
    templatesDir: '/admin-app/views',

    // Methods
    templatePath: function (view_name) {
        return this.templatesDir + '/' + view_name + '.template.html';
    },
};

function run() {
    setTimeout(function(){ angular.bootstrap(document, ["adminApp"]);}, 1000);
}

