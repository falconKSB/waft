(function () {
    'use strict';

    angular
        .module('adminApp')

        .constant('CnstApiPath', 
        {
            token: '/api/admin/token',
            login: '/api/admin/login'
        })

})();