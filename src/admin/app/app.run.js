(function () {
    'use strict';

    angular
        .module('adminApp')
        .run(AppRun);

    AppRun.$inject =
        [
            '$rootScope',
            '$state',
            'authService'
        ];

    function AppRun($root, $state, auth) {

        var isCredentialsChecked = false;

        auth.isSessionInStore() ? auth.refresh().then(appLoaded, appLoaded) : appLoaded();

        $root.$on('$stateChangeStart', function (event, toState, toParams) {

            if (toState.url=='/'){$state.go('app.scents'); return; } 
            if (!isCredentialsChecked) return;
            if (toState.private && !auth.isAuthenticated()) {
                $state.go('app.login');
            }
        
        });

        function appLoaded() { isCredentialsChecked = true; }

    };
})();