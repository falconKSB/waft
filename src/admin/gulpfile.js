/* 
Gulp dependencies: 

npm install --save-dev gulp gulp-ngmin gulp-concat gulp-order gulp-filter 
gulp-ignore main-bower-files gulp-rename gulp-autoprefixer gulp-livereload 
gulp-uglify gulp-concat-css gulp-minify-css

*/

var 
  gulp           = require('gulp'),
  ignore         = require('gulp-ignore'),
  ngmin          = require('gulp-ngmin'),
  concat         = require('gulp-concat'),
  order          = require('gulp-order'),
  filter         = require('gulp-filter'),
  mainBowerFiles = require('main-bower-files'),
  rename         = require('gulp-rename'),
  autoprefixer   = require('gulp-autoprefixer'),
  livereload     = require('gulp-livereload'),
  uglify         = require('gulp-uglify'),
  concatCss      = require('gulp-concat-css'),
  minifyCss      = require('gulp-minify-css');

  app = 
  [
    'app/app.module.js',
    'app/app.config.js',
    'app/app.config.routes.js',
    'app/app.constants.js',
    'app/app.controller.js',
    'app/app.run.js',
	  'app/**/**/*.js',
		'app/**/*.js',
    'app/app.helpers.js',
  ],

  dest =
  {
    scripts: '../../public/admin-app/scripts',
    styles:  '../../public/admin-app/styles',
    views:   '../../public/admin-app/views',
    fonts:   '../../public/admin-app/fonts',
    images:  '../../public/admin-app/images'
  },

  task = {
  dev: 
    {
      'vendors': function () {
        var jsFilter = filter('**.js', { restore: true }),
            cssFilter = filter('**.css', { restore: true }),
            vendors = mainBowerFiles();
        
        return gulp.src(vendors)
          .pipe(jsFilter)
          .pipe(concat('vendors.min.js'))
          //.pipe(ngmin())
          //.pipe(uglify())
          .pipe(gulp.dest(dest.scripts))
          .pipe(jsFilter.restore)
          .pipe(cssFilter)
          .pipe(order(vendors))
          .pipe(concat('vendors.min.css'))
          //.pipe(minifyCss())
          .pipe(gulp.dest(dest.styles));
      },

      'css': function () {
        gulp.src('app/styles/*.css')
        /*
          .pipe(concatCss('style.min.css'))
          .pipe(minifyCss({ compatibility: 'ie8' }))
          .pipe(autoprefixer({
            browsers: ['last 10 versions'],
            cascade: false
          }))
          */
          .pipe(gulp.dest(dest.styles));
      },

      'html': function () {
        gulp.src(['app/pages/**/*.template.html', 'app/layout/**/*.template.html'])
          .pipe(gulp.dest(dest.views));
      },

      'js': function () {
        gulp.src(app)
          .pipe(concat('app.js'))
          //.pipe(ngmin({dynamic: true}))
          //.pipe(uglify())
          .pipe(gulp.dest(dest.scripts));
      },

      'watch': function () {
        gulp.watch('app/styles/*.css', ['dev:css']);
        gulp.watch(['app/pages/**/*.template.html', 'app/layout/**/*.template.html'], ['dev:html']);
        gulp.watch(app, ['dev:js']);
        gulp.watch('bower.json', ['dev:vendors']);
      }

    },

    // TODO
    prod: 
    {
      'vendors': function() { },
      'css': function() { },
      'html': function() { },
      'js': function() { },
      'watch': function() { },
    }
  
  } 

gulp.task('dev:vendors', task.dev.vendors);
gulp.task('dev:css', task.dev.css);
gulp.task('dev:html', task.dev.html);
gulp.task('dev:js', task.dev.js);
gulp.task('dev:watch', task.dev.watch);

// Default
gulp.task('default', ['dev:html', 'dev:css', 'dev:js', 'dev:vendors', 'dev:watch']);