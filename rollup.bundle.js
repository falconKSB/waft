import rollup      from 'rollup'
import nodeResolve from 'rollup-plugin-node-resolve'
import commonjs    from 'rollup-plugin-commonjs';
import uglify      from 'rollup-plugin-uglify';
import replace from 'rollup-plugin-replace';

var ENV = process.env.ENV || 'dev';
var config = require("./build/config." + ENV + ".js");

function generateIntro() {
  return Object.keys(config).map(prop => 'var ' + prop + '="' + config[prop] + '";')
    .reduce((a, b) => a + b);
}

export default {
  entry: 'dist/unbundled-aot/src/waft/main.aot.js',
  dest: 'dist/bundle.js', // output a single application bundle
  sourceMap: ENV == 'dev',
  treeshake: true,
  format: 'iife',
  moduleName: 'main',
  context: 'window',
  intro: "\
    " + generateIntro(),
  plugins: [
    nodeResolve({jsnext: true, module: true}),
    commonjs({
      include: [
                'node_modules/rxjs/**',
                'node_modules/angular2-jwt/**',
                'node_modules/ng2-cookies/cookie.js',
                'node_modules/cookie-storage/**',
                'node_modules/ng2-modal/**'
      ]
    }),
    uglify()
  ]
}
