const fs = require('fs-extra');
const md5File = require('md5-file/promise');
const ejs = require('ejs');

const PARTIALS_DIR = "src/waft/partials";
const TARGET_DIR = 'public/assets/gen/';

var ENV = process.env.ENV || 'dev';
var config = require('./config.' + ENV + '.js');

// originalPath = 'dist/'
// TARGET_DIR
function processFile(name, suffix, srcDir, dstDir) {
  var originalPath = srcDir + name + '.' + suffix;
  return md5File(originalPath).then(hash => {
    var processedName = name + '.' + hash + '.' + suffix;
    var processedPath = dstDir + processedName;
    return new Promise((resolve, reject) => {
      fs.rename(originalPath, processedPath, err => {
        if (err) reject(err);
        else resolve(processedName);
      });
    });
  });
}

function generateScriptIncludes(files) {
  return files.map(f => '<script type="text/javascript" src="' +
                   config.CDN +
                   '/assets/gen/' + f + '"></script>')
    .reduce((a, b) => a + b);
}

function renderFile(originalName, targetName) {
  return new Promise((resolve, reject) => {
    console.log("Rendering '" + originalName + "' to '" + targetName + "'");
    ejs.renderFile(originalName, config, {}, (err, str) => {
      if (err) reject("Error occured on file '" + originalName + "' rendering: " + err);
      fs.writeFile(targetName, str, err => {
        if (err) reject("Error occured on file '" + targetName + "' saving: " + err);
        else resolve();
      });
    });
  });
}

function listFiles(path) {
  return new Promise((resolve, reject) => {
    fs.readdir(path, (err, files) => {
      if (err) reject(err);
      else resolve(files.filter(f => f.endsWith(".hbs")));
    });
  });
}

// process files
Promise.all(
  ['polyfill', 'bundle'].map(f => processFile(f, 'js', 'dist/', TARGET_DIR))
).then(result => {
  config.POLYFILL_JS = result[0];
  config.BUNDLE_JS = result[1];
  config.SCRIPTS = generateScriptIncludes(result);
  return processFile('app.style', 'css', TARGET_DIR + 'assets/css/', TARGET_DIR + 'assets/css/');
}).then(name => {
  config.APP_STYLE_CSS = name;
  return name;
}).then(() => listFiles(PARTIALS_DIR)).then(files => {
  var tasks = files.map(f => [PARTIALS_DIR + "/" + f, "dist/partials/" + f]);
  tasks.push(['src/waft/index.rollup.html', TARGET_DIR + 'app.html']);
  return Promise.all(tasks.map(f => renderFile(f[0], f[1])));
}).then(() => {
  console.log("Success");
}).catch(err => {
  console.log("Error occured: " + err);
});
