var config = require("../config/config");
const mongoose = require('mongoose');
const rest = require('restler');
const Order = require("../models/order");
const logger = require('./logging');

var that = module.exports;

function validateVauchar(promoCode, currency, userId) {
    return new Promise((resolve, reject) => {
        rest.get(
            'https://api.vauchar.com/validate',
            {
                username: config.vaucharMerchantId,
                password: config.vaucharApiKey,
                query: {
                    code: promoCode.toUpperCase(),
                    user_id: userId
                }
            }).on('complete', function (result) {
                if (result instanceof Error) return reject(result.message);
                if ((!result.validation_status) || (!result.validation_status.status) ||
                    (result.validation_status.status != 1000)) return reject(result.validation_status);
                if (currency && result.data.value_unit == "value" &&
                    result.data.currency.toUpperCase() != currency.toUpperCase())
                    return reject("Currencies don't match: Promo code currency is " + result.data.currency
                                  + ". Order currency is " + currency);
                result.data.promoService = "Vauchar";
                return resolve(result.data);
            });
    });  
}

function ensureUsedOncePerUser(promoCode, userId) {
    return new Promise((resolve, reject) => {
        if (!userId) return resolve();
        let id = mongoose.Types.ObjectId(userId.toString());
        Order.findOne({
            promoCode: promoCode,
            sender: id,
            status: { $nin : ['Created', 'Payment Failed'] }
        }).then(order => {
            if (order) {
                reject("This promo code was already used in order: " + order._id);
            } else {
                resolve();
            }
        }).catch(err => {
            logger.warn("Error in ensureUsedOncePerUser()", err);
            resolve();
        });
    });    
}

function validateReferralRock(promoCode, userId) {
    return new Promise((resolve, reject) => {
        rest.get(
            'https://api.referralrock.com/api/Members',
            {
                username: config.referralRockUser,
                password: config.referralRockPassword,
                query: {
                    query: promoCode
                }
            }).on('complete', function (result) {
                if (result instanceof Error) return reject(result.message);
                if (!result.total || result.total < 1) {
                    return resolve({
                        promoService: "ReferralRock",
                        result: "NotFound"
                    });
                } else {
                    ensureUsedOncePerUser(promoCode, userId).then(() => {
                        resolve({
                            promoService: "ReferralRock",
                            result: "Valid",
                            coupon_code: promoCode,
                            value: 10,
                            value_unit: "percentage",
                            promoMemberEmail: result.members[0].email
                        });
                    }).catch(reject);
                }
            });
    });
}

that.validate = function (promoCode, currency, userId) {
    return validateReferralRock(promoCode, userId)
        .then(data => {
            switch (data.result) {
            case "NotFound":
                return validateVauchar(promoCode, currency, userId);
            case "Valid":
                return data;
            }
        });
};

function createOrUpdateReferral(orderId, amount, promoCode, email, firstName, lastName) {
    return new Promise((resolve, reject) => {
        rest.get(
            'https://api.referralrock.com/api/Referrals',
            {
                username: config.referralRockUser,
                password: config.referralRockPassword,
                query: {
                    query: orderId.toString()
                }
            }).on('complete', function (result) {
                if (result instanceof Error) return reject(result.message);
                logger.debug('updateReferral query result', result);
                if (!result.total || result.total != 1) {
                    // referral not found, creating
                    rest.postJson(
                        'https://api.referralrock.com/api/Referrals',
                        {
                            "referralCode": promoCode,
                            "firstName": firstName,
                            "lastName": lastName,
                            "email": email,
                            "externalIdentifier": orderId,
                            "amount": amount
                        },
                        {
                            username: config.referralRockUser,
                            password: config.referralRockPassword
                        })
                        .on('complete', function (result) {
                            logger.debug('updateReferral create result', result);
                            if (result instanceof Error) return reject(result.message);
                            resolve();
                        });
                }
                else {
                    const referralId = result.referrals[0].id;
                    rest.postJson(
                        'https://api.referralrock.com/api/referral/update',
                        [{
                            query: {
                                primaryInfo: {
                                    referralId: referralId
                                }
                            },
                            referral: {
                                amount: amount
                            }
                        }],
                        {
                            username: config.referralRockUser,
                            password: config.referralRockPassword
                        })
                        .on('complete', function (result) {
                            logger.debug('updateReferral update result', result);
                            if (result instanceof Error) return reject(result.message);
                            resolve();
                        });
                }
            });
    });
}

function setReferralQualified(orderId) {
    return new Promise((resolve, reject) => {
        rest.postJson(
            'https://api.referralrock.com/api/referrals/status',
            {
                referralId: orderId.toString(),
                status: "qualified"
            },            
            {
                username: config.referralRockUser,
                password: config.referralRockPassword
            }).on('complete', function (result) {
                // console.log("setReferralQualified response: " + JSON.stringify(result));
                if (result instanceof Error) return reject(result.message);
                resolve();
            });        
    });    
}

function redeemReferralRock(orderId, amount, promoCode, email, firstName, lastName) {
    return createOrUpdateReferral(orderId, amount, promoCode, email, firstName, lastName)
        .then(() => setReferralQualified(orderId));
}

function redeemVauchar(type, promoCodeId, userId, email, orderId) {
    return new Promise((resolve, reject) => {
        let url = "https://api.vauchar.com/" + type + "s/" + promoCodeId + "/redemptions";
        rest.postJson(
            url,
            {
                user_id: userId,
                user_email: email,
                transaction_id: orderId
            },
            {
                username: config.vaucharMerchantId,
                password: config.vaucharApiKey
            })
            .on('complete', function (result, response) {
                if (result instanceof Error) return reject(result.message);
                if (response.statusCode != 201) return reject(result);
                else return resolve(result);
            });
    });
}

function round(num) {
    return Math.round((num + 0.00001) * 100) / 100;
}

that.redeem = function (amount, promoCode, currency, userId, email, orderId, firstName, lastName) {
    return that.validate(promoCode, currency, userId)
        .then(data => {
            if (data.promoService == "ReferralRock") {
                let discount = round(amount / 10);
                let discountedAmount = amount - discount;
                return redeemReferralRock(orderId, discountedAmount, promoCode, email, firstName, lastName)
                    .then(() => {
                    return {
                        promoData: data,
                        discount: discount
                    };
                });
            } else {
                return redeemVauchar(data.type, data.id, userId, email, orderId)
                    .then(() => {
                        let discount = 0;
                        if (data.value_unit == "percentage") {
                            discount = round(amount * Number(data.value) / 100);
                        } else {
                            discount = round(Math.min(amount, Number(data.value)));
                        }
                        return {
                            promoData: data,
                            discount: discount
                        };
                    });
            }
        });
}
