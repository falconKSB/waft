const Order = require('../models/order');
const Zapier = require('./zapier');
const Payment = require('./payment');
const Mailer = require("./mailer/mailer");
const Config = require("../config/config");

const receivedRefunded = ['Completed Trial', 'Received', 'On Hold', 'Processing', 'Processed', 'Shipped',
  'Delivered', 'Returned', 'Refunded'];

const processingDelivered = ['Processing', 'Processed', 'Shipped', 'Delivered'];

const expediteMainOrder = function expediteMainOrder(samples_order, who, setCompletedTrial) {
  let order = samples_order.generateMainOrderForSamples();
  let paymentAccount;
  return order.save()
    .then(_order => {
      return Order.findById(_order._id).populate('sender');
    })
    .then(_order => {
      order = _order;
      samples_order.mainOrderRef = order;
      samples_order.setSamplesStatus('Completed');
      samples_order.appendChangelog(`Expedited Main Order by ${who}`);
      return samples_order.save();
    })
    .then(_order => {
      let chargeDescription = order.type + " " + order.sender.email + " " + order._id;
      if (order.payment && order.payment.account && order.payment.sourceId && order.payment.customerId) {
        paymentAccount = order.payment.account;
        return Payment.chargeCustomerSource(order.payment.account, order.payment.customerId,
          order.payment.sourceId, order.currency, order.amount, chargeDescription);
      }
      else {
        paymentAccount = order.sender.paymentAccount;
        return Payment.chargeCustomer(order.sender.paymentAccount, order.sender.customerId,
          order.currency, order.amount, chargeDescription);
      }
    })
    .catch(err => {
      order.status = 'Payment Failed';
      order.payment_status = err.payment_status;
      if (!order.payment) order.payment = {};
      order.payment.account = paymentAccount;
      order.payment_id = err.charge;
      order.save();
      throw err;
    })
    .then(stripeCharge => {
      order.payment_status = stripeCharge.status;
      order.payment_id = stripeCharge.id;
      order.receipt_number = stripeCharge.receipt_number;
      order.payment = {
        account: paymentAccount,
        customerId: stripeCharge.source.customer,
        sourceId: stripeCharge.source.id
      };
      order.status = setCompletedTrial ? 'Completed Trial' : 'Processing';
      return order.save();
    })
    .then(_order => {
      Zapier.orderLab(_order._id);
      return Promise.resolve(_order);
    })
};

const getGiftURL = function getGiftURL(order) {
  return Config.homepath + "/app/gift/receive/" + order._id
};

const sendGiftEmail = function sendGiftEmail(order) {
  const url = getGiftURL(order);
  const senderName = order.gift.senderName;
  const senderEmail = order.sender.email;
  const locals = {
    email: order.gift.emailFromUser ? senderEmail : order.gift.recipient.email,
    subject: `A Waft Personalised Fragrance Commissioned by ${senderName}`,
    recipient: order.gift.recipient.firstName,
    sender: senderName,
    customBottle: order.customBottle,
    note: order.gift.note,
    url: url,
    staticBase: Config.cdnbase
  };

  if (!order.gift.inviteEmailSentOn) {
    Mailer.sendMail('gift', locals)
      .catch(err => {
        logger.error(`Error sending gift invitation from ${senderEmail}`, err);
      });
    Order.update({_id: order._id}, {$set: {"gift.inviteEmailSentOn": new Date()}}).exec();
  }
};

module.exports.receivedRefunded = receivedRefunded;
module.exports.processingDelivered = processingDelivered;
module.exports.getGiftURL = getGiftURL;
module.exports.sendGiftEmail = sendGiftEmail;
module.exports.expediteMainOrder = expediteMainOrder;