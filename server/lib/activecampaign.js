"use strict";

const config = require('../config/config');
const logger = require('./logging');
const Journey = require('../models/journey');
const Order = require('../models/order');

const ActiveCampaign = require("activecampaign");
const ac = new ActiveCampaign("https://waftperfume.api-us1.com", config.ACTIVECAMPAIGN_KEY);

const listIds = {
  'Newsletter': 10,
  'Waft Main Mailing List': 7,
  'Waft Investors': 9,
  'Prospects': 11,
  'Customers': 12,
  'Active Trial': 13,
  'Cancelled Trial': 14,
  'Full Customers': 15,
  'Abandoned': 16,
  'Burda': 6,
  'Gift Receivers': 17,
  'Gift Senders': 18
};

function serializeLists(existing) {
  let result = {};
  for (let l in existing) {
    result['p[' + l + ']'] = l;
    result['status[' + l + ']'] = existing[l].status;
  }
  return result;
}

function addLists(lists, status, existing) {
  let result = {};
  lists.forEach(l => {
    let id = listIds[l];
    if (!id) {
      throw new Error("There is no such list: " + l);
    }
    if (!existing || existing.hasOwnProperty(id)) {
      result['p[' + id + ']'] = id;
      result['status[' + id + ']'] = status || 1;
    }
  });
  return result;
}

function createOrUpdateContact(contact, listsToAdd, listsToRemove, tagsToAdd, tagsToRemove) {
  if (!config.prod) return true;
  if (contact.ip4 && contact.ip4 == "::1") {
    contact.ip4 = "127.0.0.1";
  }
  ac.api("contact/view?email=" + contact.email, {}).then(result => {
    if (result.success) { // contact exists
      let lists = serializeLists(result.lists);
      if (listsToRemove && listsToRemove.length) {
        Object.assign(lists, addLists(listsToRemove, 2, result.lists));
      }
      if (listsToAdd && listsToAdd.length) {
        Object.assign(lists, addLists(listsToAdd), 1);
      }

      let tags = result.tags;
      if (tagsToRemove && tagsToRemove.length) {
        tags = tags.filter(tag => tagsToRemove.indexOf(tag) < 0);
      }

      if (tagsToAdd && tagsToAdd.length) {
        tags = tags.concat(tagsToAdd);
      }

      let data = Object.assign({
        id: result.id,
        tags: tags.join(',')
      }, lists, contact);
      logger.debug("ActiveCampaign Contact Update", data);
      return ac.api("contact/edit", data);
    } else { // new contact
      let data = Object.assign({
        tags: tagsToAdd.join(',')
      }, addLists(listsToAdd, 1), contact);
      logger.debug("ActiveCampaign New Contact", data);
      return ac.api("contact/add", data);
    }
  }).catch(err => {
    logger.error('ActiveCampaign Error', {contact: contact, response: err.toString()});
    return err;
  });
}

function getBottleImageUrl(journey) {
  return 'https://waft.com/bottle/' + journey._id + '/' + journey.getHash();
}

function findOrder(orderId) {
  return Order.findById(orderId)
    .populate('sender', 'email firstName lastName')
    .populate('delivery')
    .populate({
      path: 'journey',
      model: 'Journey',
      populate: {
        path: 'primaryFragrance baseFragrances',
        select: 'name female male family main_accord accords',
        model: 'Fragrance'
      }
    })
    .populate({
      path: 'journey',
      model: 'Journey',
      populate: {
        path: 'scents.main scents.add1.scent scents.add2.scent',
        select: 'code name',
        model: 'Scent'
      }
    });
}

function contactFromOrder(order) {
  return {
    email: order.sender.email,
    first_name: order.sender.firstName,
    last_name: order.sender.lastName,
    phone: order.delivery.phoneNumber,
    "field[%ORDER_ID%,0]": order._id.toString(),
    "field[%BOTTLE_NAME%,0]": order.customBottle.name,
    "field[%BOTTLE_MESSAGE%,0]": order.customBottle.message,
    "field[%CUSTOM_BOTTLE_NAME%,0]": order.customBottle.name,
    "field[%CUSTOM_BOTTLE_STYLE%,0]": order.customBottle.style,
    "field[%CUSTOM_BOTTLE_MESSAGE%,0]": order.customBottle.message,
    "field[%ORDER_AMOUNT%,0]": order.amount,
    "field[%ORDER_CURRENCY%,0]": order.currencySymbol,
    "field[%DELIVERY_ADDRESS_LINE_1%,0]": order.delivery.addressLine1,
    "field[%DELIVERY_ADDRESS_LINE_2%,0]": order.delivery.addressLine2,
    "field[%DELIVERY_CITY%,0]": order.delivery.city,
    "field[%DELIVERY_COUNTRY%,0]": order.delivery.county + ' ' + order.delivery.country,
    "field[%DELIVERY_ZIP_CODE%,0]": order.delivery.zipCode,
    "field[%BOTTLE_IMAGE_URL%,0]": getBottleImageUrl(order.journey)
  };
}

module.exports.onTrialOperation = function (orderId, operation) {
  return Order.findById(orderId)
    .populate('sender', 'email firstName lastName')
    .then(order => {
      if (operation == 'expedite' || operation == 'expediteAdmin') {
        return createOrUpdateContact(
          {
            email: order.sender.email,
            first_name: order.sender.firstName,
            last_name: order.sender.lastName
          },
          ['Full Customers'],
          ['Active Trial', 'Cancelled Trial'],
          ['PURCHASE: Bottle'],
          ['PURCHASE: Trial', 'STATUS: Cancelled Trial']);
      } else if (operation == 'cancel' || operation == 'cancelAdmin') {
        return createOrUpdateContact(
          {
            email: order.sender.email,
            first_name: order.sender.firstName,
            last_name: order.sender.lastName
          },
          ['Cancelled Trial'],
          ['Active Trial'],
          ['STATUS: Cancelled Trial'],
          ['PURCHASE: Trial']);
      }
    });
};

module.exports.onPayLab = function (orderId) {
  return findOrder(orderId).then(order => {
    if (order.orderSize == 'samples') {
      return createOrUpdateContact(
        contactFromOrder(order), ['Customers', 'Active Trial'],
        ['Prospects', 'Abandoned', 'Cancelled Trial', 'Gift Receivers'],
        ['PURCHASE: Trial'], ['LEAD: Gift', 'LEAD: Lab', 'STATUS: Cancelled Trial']);
    } else if (order.orderSize == 'big' || order.orderSize == 'medium') {
      return createOrUpdateContact(
        contactFromOrder(order), ['Customers', 'Full Customers'],
        ['Prospects', 'Abandoned', 'Cancelled Trial', 'Gift Receivers'],
        ['PURCHASE: Bottle'],
        ['LEAD: Gift', 'LEAD: Lab', 'PURCHASE: Trial', 'STATUS: Cancelled Trial']);
    }
  });
};

module.exports.onCreateGift = function (orderId) {
  return findOrder(orderId).then(order => {
    return createOrUpdateContact(
      {
        email: order.sender.email,
        first_name: order.sender.firstName,
        last_name: order.sender.lastName,
        "field[%ORDER_ID%,0]": order._id.toString(),
        "field[%BOTTLE_NAME%,0]": order.customBottle.name,
        "field[%BOTTLE_MESSAGE%,0]": order.customBottle.message,
        "field[%CUSTOM_BOTTLE_NAME%,0]": order.customBottle.name,
        "field[%CUSTOM_BOTTLE_STYLE%,0]": order.customBottle.style,
        "field[%CUSTOM_BOTTLE_MESSAGE%,0]": order.customBottle.message,
        "field[%RECIPIENT%,0]": order.gift.recipient.firstName,
        "field[%RECIPIENT_EMAIL%,0]": order.gift.recipient.email
      },
      ['Abandoned'],
      [],
      ['LEAD: Gift']);
  });
};

module.exports.onPayGift = function (order, claim_url) {
  if (!order.gift) return true;
  let promises = [createOrUpdateContact(
    {
      email: order.sender.email,
      first_name: order.sender.firstName,
      last_name: order.sender.lastName,
      "field[%ORDER_ID%,0]": order._id.toString(),
      "field[%BOTTLE_NAME%,0]": order.customBottle.name,
      "field[%BOTTLE_MESSAGE%,0]": order.customBottle.message,
      "field[%CUSTOM_BOTTLE_NAME%,0]": order.customBottle.name,
      "field[%CUSTOM_BOTTLE_STYLE%,0]": order.customBottle.style,
      "field[%CUSTOM_BOTTLE_MESSAGE%,0]": order.customBottle.message,
      "field[%RECIPIENT%,0]": order.gift.recipient.firstName,
      "field[%RECIPIENT_EMAIL%,0]": order.gift.recipient.email
    },
    ['Customers', 'Gift Senders'],
    ['Prospects', 'Abandoned', 'Cancelled Trial', 'Gift Receivers'],
    ['PURCHASE: Gift'],
    ['LEAD: Gift', 'LEAD: Lab', 'PURCHASE: Trial', 'STATUS: Cancelled Trial'])];
  if (order.gift.emailFromUser !== true)
    promises.push(createOrUpdateContact(
      {
        email: order.gift.recipient.email,
        first_name: order.gift.recipient.firstName,
        last_name: order.gift.recipient.lastName,
        "field[%ORDER_ID%,0]": order._id.toString(),
        "field[%BOTTLE_NAME%,0]": order.customBottle.name,
        "field[%BOTTLE_MESSAGE%,0]": order.customBottle.message,
        "field[%CUSTOM_BOTTLE_NAME%,0]": order.customBottle.name,
        "field[%CUSTOM_BOTTLE_STYLE%,0]": order.customBottle.style,
        "field[%CUSTOM_BOTTLE_MESSAGE%,0]": order.customBottle.message,
        // "field[%%,0]": order.gift.recipient.firstName + '' + order.gift.recipient.lastName, TODO: %% - WTF?
        "field[%RECIPIENT_EMAIL%,0]": order.gift.recipient.email,
        "field[%SENDER%,0]": order.sender.firstName,
        "field[%SENDER_EMAIL%,0]": order.sender.email,
        "field[%SENDER_LAST_NAME%,0]": order.sender.lastName,
        "field[%CLAIM_URL%,0]": claim_url
      },
      ['Gift Receivers'],
      [],
      ['STATUS: Gift Receiver']));
  return Promise.all(promises);
};

module.exports.onJourney = function (journey) {
  return Journey.findById(journey._id)
    .populate('user')
    .then(journey => {
      if (
        !journey ||
        (!journey.email && (!journey.user || !journey.user.email || journey.user.email === ''))
      ) {
        if (journey.cookie) return true;
        logger.warn('Wrong journey in Active Campaign', journey.toObject() || {});
        return true;
      }
      let contact = {
        "field[%ABANDONED_URL%,0]": 'https://waft.com/app/wizard/journey/' + journey._id,
        "field[%BOTTLE_IMAGE_URL%,0]": getBottleImageUrl(journey)
      };
      if (journey.user) {
        contact.first_name = journey.user.firstName;
        contact.last_name = journey.user.lastName;
        contact.email = journey.user.email;
      } else {
        contact.email = journey.email;
      }
      if (journey.gender && journey.gender != 'None') {
        contact['field[%JOURNEY_GENDER%,0]'] = journey.gender;
      }
      return createOrUpdateContact(contact, ['Abandoned'], [], ['LEAD: Lab']);
    });
};

module.exports.onRegistration = function (contact) {
  return createOrUpdateContact(contact, ['Prospects', 'Waft Main Mailing List'], [], []);
};

module.exports.onRegistrationFacebook = function (contact) {
  return createOrUpdateContact(contact, ['Prospects', 'Waft Main Mailing List'], [], []);
};

module.exports.onNewsletter = function (contact) {
  return createOrUpdateContact(contact, ['Prospects', 'Waft Main Mailing List'], [], ['LM: Newsletter']);
};

module.exports.onCorporateForm = function (contact) {
  return createOrUpdateContact(
    contact, ['Waft Main Mailing List'], [], ['LEAD: Corporate']);
};
