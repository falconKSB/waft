var path = require('path');

var nodemailer = require('nodemailer');
var EmailTemplate = require('email-templates').EmailTemplate;

var config = require('../../config/config');

var templatesDir = path.join(__dirname, 'templates');

exports.sendMail = function (templateName, locals) {
    return new Promise((resolve, reject) => {
        let template = new EmailTemplate(path.join(templatesDir, templateName));
        let transport = nodemailer.createTransport(config.mailer.transport);
        template.render(locals, function (err, results) {
            if (err) {
                return reject(err)
            }

            transport.sendMail({
                from: config.mailer.defaultFromAddress,
                to: locals.email,
                subject: locals.subject,
                html: results.html,
                text: results.text
            }, function (err, responseStatus) {
                if (err) {
                    return reject(err)
                }
                return resolve(responseStatus);
            })
        })
    })
};
