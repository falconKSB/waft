"use strict";

const mcache = require('memory-cache');

module.exports = function(duration) {
    return function(key, notFound) {
        let cachedValue = mcache.get(key);
        if (cachedValue) {
            return cachedValue;
        } else {
            return notFound(key)
                .then(res => {
                    mcache.put(key, res, duration * 1000);
                    return res;
                });
        }
    }
};
