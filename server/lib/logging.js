const Winston = require('winston');
const config = require('../config/config');
const Geoip = require('./geoip');

const transports = [
  new (Winston.transports.Console)({
    timestamp: function () {
      return (new Date()).toISOString();
    },
    formatter: function (options) {
      // Return string will be passed to logger.
      return options.timestamp() + ' ' + options.level.toUpperCase() + ' ' + (options.message || '') +
        (options.meta && Object.keys(options.meta).length ? '\n\t' + JSON.stringify(options.meta) : '' );
    },
    level: config.LOGLEVEL
  })];


/**
 * Production Log File Monitoring
 */

if (config.prod) {
  const Bugsnag = require("bugsnag");
  Bugsnag.register("df25a8f16e04d0d87c57e8903bd9bfd8");
  const WinstonBugsnag = require('winston-bugsnag');
  transports.push(new (WinstonBugsnag)({
      level: 'error'
    })
  );
}

const logger = new (Winston.Logger)({transports: transports});

logger.rewriters.push(function (level, msg, meta) {
  if (meta && meta.req && meta.req.body && meta.req.body.password) {
    meta.req.body.password = Array(meta.req.body.password.length + 1).join("*")
  }
  return meta;
});


logger.debugReq = function (message, req) {
  this.debug(message,
    {
      url: req.originalUrl,
      method: req.method,
      ip: Geoip.getIP(req),
      ua: req.headers['user-agent'] || '',
      cookies: req.cookies,
      body: req.body
    });
};

logger.logErrorReq = function (loglevel, err, req) {
  this.log(loglevel, err.message,
    {
      err: {
        message: err.message,
        stack: err.stack
      },
      req: {
        url: req.originalUrl,
        method: req.method,
        ip: Geoip.getIP(req),
        ua: req.headers['user-agent'] || '',
        body: req.body
      }
    });
};

module.exports = logger;
