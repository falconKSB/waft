var Order = require('../models/order');
var Scent = require('../models/scent');
var Journey = require('../models/journey');
var Fragrance = require('../models/fragrance');
var logger = require('./logging');
const Cache = require('./cache');
var Warehouse = require('../models/warehouse');


function matchByOrderID(orderID) {
    return Order.findById(orderID).populate('journey')
        .then(function (_order) {
           return matchByOrder(_order);
        });
}

function matchCount(a, b) {
    if (a instanceof Array) {
        return a.reduce((sum, ai) => sum + (b.indexOf(ai) == -1 ? 0 : 1), 0);
    } else {
        return (b.indexOf(a) == -1) ? 0 : 1
    }
}

function matchCountMin(a, b, min) {
    let match = matchCount(a, b);
    return match >= min;
}

function matchExact(a, b) {
    if (a.length != b.length) return false;
    for (let i = 0; i < a.length; i++) {
        let ai = a[i];
        let found = false;
        for (let j = 0; j < b.length; j++) {
            if (ai == b[j]) {
                found = true;
                break;
            }
        }
        if (!found) return false;
    }
    return true;
}


function filterRecommendations(list, field, values, min, exact) {
    for (let i = list.length - 1; i >= 0; i--) {
        if ( (exact && (!matchExact(list[i][field], values))) ||
            ((!exact) && (!matchCountMin(list[i][field], values, min)))) {
            list.splice(i, 1);
        }
    }
}

function topRecommendations(list, field, values, n) {
    for (let i = 0; i < list.length; i++) {
        list[i].rank = list[i].rank*10 + matchCount(list[i][field], values);
    }
    list.sort((a,b) => {
        return b.rank - a.rank;
    });
    let cutoff = list[n-1];
    for (let i = 0; i < list.length; i++) {
        if (list[i].rank < cutoff) {
            list.splice(i);
            break;
        }
    }
}

function getScentsCodes(list) {
    let result = [];
    for (let i = 0; i < list.length; i++) {
        result.push(list[i].code + (list[i].rank?('('+list[i].rank+')'):''));
    }
    return result.join(', ');
}

function checkAndAdd(value, list) {
    if (!value || (value.trim() == '')) return;
    list.push(value.toLowerCase().trim());
}

function findSimilar(journey) {
    if (!journey.primaryFragrance)
        return Promise.resolve(null);
    let where = {
        gender: journey.gender, // TODO: move to recipientGender?
        primaryFragrance: journey.primaryFragrance,
        'scents.main' : { $exists: true }
    };
    if (journey._id)
        where._id = { $ne: journey._id };
    return Journey.findOne(where)
        .sort('-createdAt')
        .populate({
            path: 'scents.main scents.add1.scent scents.add2.scent',
            model: 'Scent'
        })
        .exec()
}

function getElementsWithRank(a, rank) {
    let result = [];
    for (let i = 0; i < a.length; i++) {
        if(a[i].rank == rank) result.push(a[i]);
    }
    return result;
}

function top2(a) {
    let el1, el2;
    let group = getElementsWithRank(a, a[0].rank);
    if (group.length == 1) {
        el1 = group[0];
        if (a.length == 1) return [el1, null];
        group = getElementsWithRank(a, a[1].rank);
        return [el1, randomElement(group)];
    } else {
        el1 = randomElement(group);
        removeFromArray(group, el1);
        return [el1, randomElement(group)]
    }
}

function top1(a) {
    if (a.length == 0) return null;
    return randomElement(getElementsWithRank(a, a[0].rank));
}

function randomElement(a) {
    if (a.length == 0) return null;
    if (a.length == 1) return a[0];
    return a[Math.floor(Math.random() * a.length)]
}

function removeFromArray(a, el, field) {
    if (el === null) return;
    if (el instanceof Array) {
        for (let i = a.length-1; i >= 0; i--)
            for (let j = 0; j < el.length; j++)
                if ((field && (a[i][field] === el[j][field])) ||
                    ((!field) && (a[i] === el[j]))) {
                    a.splice(i, 1);
                    break
                }
    }
    else {
        for (let i = a.length-1; i >= 0; i--)
            if ((field && (a[i][field] === el[field])) ||
                ((!field) && (a[i] === el[j]))) {
                a.splice(i, 1);
                break
            }
    }
}

function concatUniques(arr1, arr2) {
    for (let i = 0; i < arr2.length; i++) {
        let el = arr2[i];
        if (!arr1.includes(el)) {
            arr1.push(el);
        }
    }
    return arr1;
}

function matchByOrder(order) {
    return matchByJourney(order.journey);
}

function getScents() {
    return Cache(300)("AllScents", function () {
        return Scent.find({
            status: 'Active',
            isDeleted: false
        });
    })
}

function getWarehouses() {
    return Cache(300)("AllWarehouses", function () {
        return Warehouse.find({});
    })
}

function getWarehouseByName(name) {
    return getWarehouses().find(w => w.name === name);
}

function filterStock(scents, warehouse) {
    let w = getWarehouseByName(warehouse);
    return scents.filter(s => w.isScentInStock(s.code));
}

function findScentByCode(scents, code) {
    return scents.find(s => s.code === code);
}

function matchByJourney(journey) {
    if (journey == null) return Promise.resolve({});
    return Promise.all([getScents(),findSimilar(journey)]) // TODO: active, in stock
        .then(result => {
            let main, add1, add2, history_recommendations;
            let explain = [];
            let short_list = [];
            let short_list_add2 = [];
            for (let i = 0; i < result[0].length; i++) {
                let s = result[0][i];
                short_list.push(s.toScentObject());
                short_list_add2.push(s.toScentObject());
            }
            let journey_ingredients = journey.getInternalIngredients();
            let gender = null;
            if (journey.primaryFragrance) {
                if (journey.primaryFragrance.scents && journey.primaryFragrance.scents.main
                    && (journey.primaryFragrance.scents.main.length > 0)) {
                    let alloc = journey.primaryFragrance.scents.main;
                    if (journey.primaryFragrance.isUnisex()) {
                        if ((journey.recipientGender == 'man') && (journey.primaryFragrance.scents.unisexmen) &&
                            (journey.primaryFragrance.scents.unisexmen.length > 0))
                            alloc = journey.primaryFragrance.scents.unisexmen;
                        if ((journey.recipientGender == 'woman') && (journey.primaryFragrance.scents.unisexwomen) &&
                            (journey.primaryFragrance.scents.unisexwomen.length > 0))
                            alloc = journey.primaryFragrance.scents.unisexwomen;
                    }
                    main = findScentByCode(short_list, alloc[0]);
                    if (alloc.length > 1)
                        add1 = findScentByCode(short_list, alloc[1]);
                    if (alloc.length > 2)
                        add2 = findScentByCode(short_list, alloc[2]);
                    explain.push("Found direct allocations for primary fragrance "+journey.primaryFragrance.name);
                    explain.push("Direct allocations "+alloc.join(', '));
                }
                else if (result[1] != null) {
                    let history = result[1].scents;
                    explain.push("Found history of allocations for primary fragrance "+journey.primaryFragrance.name);
                    explain.push("Primary fragrance main recommendation: "
                        + history.main.toShortString());
                    main = history.main.toScentObject();
                    history_recommendations = [history.main.code];
                    if (history.add1.scent) {
                        explain.push("Primary fragrance add1 recommendation: " + history.add1.scent.toShortString());
                        add1 = history.add1.scent.toScentObject();
                        history_recommendations.push(history.add1.scent.code);
                    }
                    if (history.add2.scent) {
                        explain.push("Primary fragrance add2 recommendation: " + history.add2.scent.toShortString());
                        add2 = history.add2.scent.toScentObject();
                        history_recommendations.push(history.add2.scent.code);
                    }

                } else {
                    gender = journey.primaryFragrance.getGenderString();
                    explain.push("No history of allocations for primary fragrance "+journey.primaryFragrance.name)
                }
            } else {
                explain.push("No primary fragrance");
            }
            // Take user's Gender (implied from perfumes selected, or a wizard choice) and select all perfumes that match the gender or are unisex
            if (gender !== null) {
                if (gender === 'unisex') {
                    if (journey.recipientGender === 'man')
                        gender = ['man'];
                    else
                        gender = ['woman', 'unisex'];
                } else {
                    gender = [gender];
                }
            } else {
                if ((journey.recipientGender === 'man') || (journey.recipientGender === 'woman')) {
                    gender = [journey.recipientGender];
                    if ((journey.recipientGender === 'woman') && (journey.gender === 'unisex'))
                        gender.push('unisex');
                }
                else
                    gender = journey.getGender();
            }
            explain.push("Algorithm gender: " + gender);
            filterRecommendations(short_list, 'gender', gender, 1);
            filterRecommendations(short_list_add2, 'gender', gender, 1);
            explain.push("Gender shortlist: "+getScentsCodes(short_list));
            let primaryFragranceIngredients = journey.primaryFragrance?journey.primaryFragrance.getIngredientsArray():[];
            if ((!main) || (!add1)) {
                if (journey.primaryFragrance) {
                    // Take Fragrantica Family and match against Family Choice 1 or Family Choice 2 => shortlist
                    let family = journey.primaryFragrance.family.toLowerCase().trim();
                    explain.push("Primary fragrance family: " + family);
                    topRecommendations(short_list, 'families', [family], 2);
                    explain.push("Shortlist after fragrance family ranking: " + getScentsCodes(short_list));
                    explain.push('Primary fragrance ingredients: ' + primaryFragranceIngredients.join(', '));
                    topRecommendations(short_list, 'ingredients', primaryFragranceIngredients, 2);
                    explain.push("Shortlist after ingredients ranking: " + getScentsCodes(short_list));
                } else {
                    explain.push("No primary fragrance");
                    explain.push("Journey ingredients: " + journey_ingredients.join(', '));
                    topRecommendations(short_list, 'ingredients', journey_ingredients, 2);
                    explain.push("Shortlist after ingredients ranking: " + getScentsCodes(short_list));
                    let params = journey.getParametersArray();
                    explain.push("Journey parameters: " + params.join(', '));
                    topRecommendations(short_list, 'params', params, 2);
                    explain.push("Shortlist after parameters ranking: " + getScentsCodes(short_list));
                }
                if (!main) main = top1(short_list);
                removeFromArray(short_list, main, 'code');
                if (!add1) add1 = top1(short_list);
            }
            removeFromArray(short_list_add2, main, 'code');
            removeFromArray(short_list_add2, add1, 'code');

            concatUniques(journey_ingredients, primaryFragranceIngredients);
            explain.push('Journey and primary fragrance ingredients: ' + journey_ingredients.join(', '));
            if (main) removeFromArray(journey_ingredients, main.ingredients);
            if (add1) removeFromArray(journey_ingredients, add1.ingredients);
            explain.push('After removal of main and add1: ' + journey_ingredients.join(', '));
            if ((journey_ingredients.length !== 0) || (!add2)) {
                topRecommendations(short_list_add2, 'ingredients', journey_ingredients, 1);
                explain.push("Shortlist for add2 based on ingredients: " + getScentsCodes(short_list_add2));
                add2 = top1(short_list_add2);
            }

            //
            //     Then take Fragrantica Sub-Family, Accord 1 - 5 (6 in total) and score against Sub Family 1 - 3 => score A between 0 and 3
            // If exactly one perfume has the top score A, then we recommend it
            // If more than one perfume have the same score A (e.g. 2 perfumes with score of 3) => second shortlist
            // Score user-selected Time of Day, Activity, Mood, Positioning, Waft against Time of Day, Activity, Mood, Positioning, Waft of each perfume in second shortlist => score B between 0-5
            // If exactly one perfume has the top score B, then we recommend it
            // If more than one perfume have the same score B, then select the one with most number of ingredient matches
            // If still more than one, then select a random one
            //
            // Accessory
            //
            // Each perfume has 4 accessories predefined
            // Time of day (Day/Night) + Positioning (Timeless / Trendy)
            // If user answer is Day and main perfume is Day then we select Night
            return Promise.resolve({
                main: main,
                add1: add1,
                add2: add2,
                history: history_recommendations,
                explain: explain
            });
        })
}

module.exports.matchByOrderID = matchByOrderID;
module.exports.matchByOrder = matchByOrder;
module.exports.matchByJourney = matchByJourney;