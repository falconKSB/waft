var config = require("../config/config");
var stripeSG = require("stripe")(config.stripeSecretKeySG);
var stripeUS = require("stripe")(config.stripeSecretKeyUS);

function getStripe(account) {
    return account == "Stripe SG"?stripeSG:stripeUS;
}

module.exports.charge = function(account, token, currency, amount, description) {
    // Create a charge: this will charge the user's card
    return stripe.charges.create({
        amount: Math.floor(amount * 100), // Amount in cents
        currency: currency,
        source: token,
        description: description
    });
};

module.exports.createSource = function(account, token, email) {
    return getStripe(account).sources.create({
        type: 'card',
        token: token,
        owner: {
            email: email
        }
    })
};

function processStripeError(err) {
    let bank_decline_code = (err.raw && err.raw.decline_code) || 'N/A';
    let stripe_decline_code = (err.code) || 'N/A';
    err.payment_status = err.message +
        ' Bank code: '+bank_decline_code+
        '. Stripe code: '+stripe_decline_code+'.';
    if (err.message == 'Your card was declined.') {
        err.message += ' Please contact your bank to complete the payment.'
    }
    if (err.raw && (err.raw.type == "card_error"))
        err.status = 402;
    throw err;
}

function updateCustomer(account, customerId, token) {
    const stripe = getStripe(account);
    return stripe.customers.createSource(customerId, {
            source: token
        })
        .then(source => {
            return stripe.customers.update(customerId, {
                default_source: source.id
            });
        })
        .catch(processStripeError)
}

function createCustomer(account, token, email) {
    return getStripe(account).customers.create({
            source: token,
            email: email,
            description: email
        })
        .catch(processStripeError);
}

module.exports.createCustomer = createCustomer;
module.exports.updateCustomer = updateCustomer;

module.exports.createOrUpdateCustomer = function(account, customerId, token, email) {
    if (customerId && (customerId != ''))
        return updateCustomer(account, customerId, token);
    else
        return createCustomer(account, token, email)
}


module.exports.chargeCustomer = function(account, customerId, currency, amount, description) {
    return getStripe(account).charges.create({
            customer: customerId,
            amount: Math.floor(amount * 100), // Amount in cents
            currency: currency,
            description: description
        })
        .catch(processStripeError)
};

module.exports.chargeCustomerSource = function(account, customerId, sourceId, currency, amount, description) {
    return getStripe(account).charges.create({
        customer: customerId,
        source: sourceId,
        amount: Math.floor(amount * 100), // Amount in cents
        currency: currency,
        description: description
    })
        .catch(processStripeError)
};

