var config = require('../config/config');
var jwt = require('jsonwebtoken');

module.exports.authenticateUser = function (req, res, next) {
    // check header or url parameters or post parameters for token
    let token = req.body.token || req.query.token || req.headers['authorization'];

    // decode token
    if (token) {
        // verifies secret and checks exp
        jwt.verify(token, config.secret, function (err, user) {
            if (err) {
                next({status: 403, success: false, message: 'Failed to authenticate token'});
            } else {
                // if everything is good, save to request for use in other routes
                req.currentUser = user;
                next();
            }
        });

    } else {
        // if there is no token
        // return an error
        next({
            status: 401,
            message: 'No token provided'
        });
    }
};

module.exports.authenticateAdmin = function (req, res, next) {
    let token = req.body.access_token || req.query.access_token || req.headers['authorization'] || req.cookies['admintoken'];

    if (!token) {
        return next({
            status: 401,
            message: 'No token provided'
        });

    }
    if (token.indexOf('jwt ') === 0) {
        token = token.split(' ')[1];
    }
    jwt.verify(token, config.secret, function (err, user) {
        if (err) {
            next({
                status: 403,
                message: "Invalid token, please Log in first"
            });
        } else {
            req.currentAdmin = user;
            next();
        }
    });
};