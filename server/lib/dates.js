const dateformat = require( 'dateformat');

const extractDate = function (field) {
  if ((!field) || (field === 'All Dates') || (field === 'Dates')) return null;
  let from = new Date();
  let to = new Date();
  from.setHours(-8, 0, 0, 0);
  if (field === 'Today') {
  }
  else if (field === 'Yesterday') {
    from.setDate(from.getDate() - 1);
    to.setHours(-8, 0, 0, 0);
  }
  else if (field === 'This Month') {
    from.setDate(1);
  }
  else if (field === 'Last Month') {
    from.setMonth(from.getMonth() - 1, 1);
    to.setDate(1);
    to.setHours(-8, 0, 0, 0);
  }
  else if (field === 'Last 7 Days') {
    from.setDate(from.getDate() - 7);
  }
  else if (field === 'Last 30 Days') {
    from.setDate(from.getDate() - 30);
  }
  else if (field === 'Last 60 Days') {
    from.setDate(from.getDate() - 60);
  }
  else return null;
  return {"$gte": from, "$lt": to};
};

const formatDateForCSV = function (fieldName, subfieldName) {
  return function (row, field, data) {
    let d = row[fieldName];
    if (!d) return '';
    if (subfieldName) {
      d = d[subfieldName];
      if (!d) return '';
    }
    d.setHours(d.getHours() + 8); // SGT adjustment (GMT+8)
    return dateformat(d, 'yyyy-mm-dd HH:MM:ss')
  }
};

module.exports.extractDate = extractDate;
module.exports.formatDateForCSV = formatDateForCSV;

