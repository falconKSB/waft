var i18n = require('i18n');

var helpers = {
  section(name, options) {
    if(!this._sections) this._sections = {};
    this._sections[name] = options.fn(this);
    return null;
  },
  staticBase() {
    return process.env.CDN_BASE_URL;
  },
  isProduction() {
    return process.env.NODE_ENV == 'production';
  }
};

module.exports = function(hbs) {
  Object.keys(helpers).forEach(function(name) {
    hbs.registerHelper(name, helpers[name]);
  });
}
