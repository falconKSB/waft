var geoiplite = require('geoip-lite');

var that = module.exports;

const countries = {
  'us': 'en_US',
  'default': 'en'
};

module.exports.getIP = function(req) {
    return (req.ips && req.ips[0]) || req.ip;
}

module.exports.lookup = function(ip) {
    var geo = geoiplite.lookup(ip);
    var countryCode = geo && geo.country && geo.country.toLowerCase();
    var city = geo && geo.city;
    return { countryCode: countryCode, city: city }
}

module.exports.getLang = function(req) {
  var result = that.lookup(that.getIP(req));
  if (result && result.countryCode && countries[result.countryCode]) {
    return countries[result.countryCode];
  } else {
    return countries['default'];
  }
}
