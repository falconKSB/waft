/**
 * Created by anton on 10/4/17.
 */

const AWS = require('aws-sdk');
const Config = require('../config/config');

cloudwatchlogs = new AWS.CloudWatchLogs({
    accessKeyId: Config.AWSKEY,
    secretAccessKey: Config.AWSSECRET,
    region: 'us-west-2'
});

function filterLogs(patterns) {
    return cloudwatchlogs
        .describeLogGroups({
            logGroupNamePrefix: '/aws/elasticbeanstalk/waft/'
        })
        .promise()
        .then(data => {
            let promises = [];
            patterns.forEach(pattern => {
                Array.prototype.push.apply(promises,
                    data.logGroups.map(group => cloudwatchlogs.filterLogEvents({
                        logGroupName: group.logGroupName,
                        filterPattern: pattern
                    }).promise()));
            });
            return Promise.all(promises);
        })
        .then(results => {
            return results.reduce((val, result) => result.events.concat(val), [])
                .filter((elem, index, self) => index == self.indexOf(elem))
                .sort((a, b) => a.timestamp - b.timestamp)
                .map(a => {
                    let json_idx = a.message.indexOf('\n\t{');
                    let message = a.message;
                    let ext = null;
                    if (json_idx != -1) {
                        message = a.message.substr(0, json_idx);
                        ext = a.message.substr(json_idx+2);
                        ext = JSON.parse(ext);
                    }
                    return {
                        timestamp: new Date(a.timestamp),
                        message: message,
                        ext: ext
                    }});
        });
}

module.exports = function(user) {
    let filter = [];
    user.orders.forEach(o => {
        filter.push(o.userInfo.ip);
    });
    user.journeys.forEach(j => {
        filter.push(j._id.toString());
    });
    filter.push(user.email);
    filter.push(user._id.toString());
    filter.filter(a =>  (typeof a == 'string') && (a !== ''));
    filter = filter.map(a => '"' + a + '"');
    return filterLogs(filter);
};


// filterLogs(['"aroslov@hotmail.com"','"57989472cf821d0574788cfb"','"175.156.232.222"']).then(l => l.forEach(a => console.log(a.message)));

// .then(data => {
//     return Promise.all(data.logGroups.map(group => cloudwatchlogs.describeLogStreams({
//         logGroupName: group.logGroupName,
//         orderBy: "LastEventTime",
//         descending: true
//     }).promise()));
// })
//     .then(streams => {
//         streams = streams.map(s => s.splice(5));
//         streams.forEach(stream => {
//             console.log(stream);
//         })
//     });

