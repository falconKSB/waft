"use strict";

const crypto = require('crypto');
const mcache = require('memory-cache');
const geoip = require('./geoip');
const isCallerMobile = require('./mobileCheck');

module.exports = function(duration) {
    return function(req, res, next) {
        if (req.method != 'GET') return next();
        
        let key = '__express__' +
            crypto.createHash("md5")
            .update(req.path || "")
            .update(geoip.lookup(geoip.getIP(req)).countryCode || "")
            .update(geoip.getLang(req) || "")
            .update(isCallerMobile(req)?'mobile':'desktop')
            .digest("base64");
        
        // let key = '__express__' + (req.originalUrl || req.url);
        let cachedBody = mcache.get(key);
        if (cachedBody) {
            res.send(cachedBody);
            return;
        } else {
            if (!res.sendResponse) {
                res.sendResponse = res.send;
                res.send = function(body) {
                    mcache.put(key, body, duration * 1000);
                    res.sendResponse(body);
                }
            }
            next();
        }
    }
}
