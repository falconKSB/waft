"use strict";

const rp = require("request-promise-native");
const config = require('../config/config');
const geoip = require('./geoip');
const logger = require('./logging');
const orderModel = require('../models/order');
const journeyModel = require('../models/journey');

function send(hook, data) {
  var options = {
    method: 'POST',
    uri: hook,
    body: data,
    json: true
  };
  return rp(options)
    .then(response => {
      logger.info('Zapier OK', {hook: hook, sent: data, response: response});
      return true;
    })
    .catch(err => {
      logger.error('Zapier Error', {hook: hook, sent: data, response: err});
      return false;
    });
}

module.exports.orderGift = function (order) {
  if (!config.prod) return true;
  return send('https://hooks.zapier.com/hooks/catch/1556371/6zd911/', order);
};

module.exports.journey = function (journey) {
  if (!config.prod) return true;
  return journeyModel.findById(journey._id)
    .populate('user')
    .then(journey => {
      if ((!journey) || (!journey.user) || (!journey.user.email) || (journey.user.email == '')) return;
      send('https://hooks.zapier.com/hooks/catch/1556371/tuctff/', {
        email: journey.user.email,
        url: 'https://waft.com/app/wizard/journey/' + journey._id,
        journey: {
          gender: journey.gender,
          timeOfDay: journey.timeOfDay,
          activity: journey.activity,
          mood: journey.mood,
          personalStyle: journey.personalStyle,
          waft: journey.waft
        }
      });
      return journey;
    });
};

function findTrialOrder(orderID) {
  return orderModel.findById(orderID)
    .populate('sender', 'email firstName lastName')
    .then(order => {
      if (!order) {
        throw new Error(`Order ${orderID} not found`);
      }
      let zapierOrder = {
        sender: {
          firstName: order.sender.firstName,
          lastName: order.sender.lastName,
          email: order.sender.email
        },
        order_id: order._id.toString(),
        cancellationReasons: order.cancellationReasons
      };
      return zapierOrder;
    });
}

const TRIAL_OPERATIONS = {
  "expedite": "https://hooks.zapier.com/hooks/catch/1556371/t5vqpi/",
  "expediteAdmin": "https://hooks.zapier.com/hooks/catch/1556371/h9g35z/",
  "cancel": "https://hooks.zapier.com/hooks/catch/1556371/h9gocm/",
  "cancelAdmin": "https://hooks.zapier.com/hooks/catch/1556371/h9g26z/"
};

module.exports.processTrial = function processTrial(orderID, operation) {
  if (!config.prod) return true;
  let url = TRIAL_OPERATIONS[operation];
  if (!url) {
    logger.error(`Zapier processTrial error. Operation not supported: ${operation}`);
    return;
  }
  return findTrialOrder(orderID)
    .then(order => {
      return send(url, order);
    })
    .catch(err => {
      logger.error(`Zapier processTrial(${operation}) error`, err);
    })
};

function findOrder(orderID) {
  return orderModel.findById(orderID)
    .populate('sender', 'email firstName lastName')
    .populate('delivery')
    .populate({
      path: 'journey',
      model: 'Journey',
      populate: {
        path: 'primaryFragrance baseFragrances',
        select: 'name female male family main_accord accords',
        model: 'Fragrance'
      }
    })
    .populate({
      path: 'journey',
      model: 'Journey',
      populate: {
        path: 'scents.main scents.add1.scent scents.add2.scent',
        select: 'code name',
        model: 'Scent'
      }
    })
    .then(order => {
      if (!order) throw new Error(`Order ${orderID} not found`);
      return {
        recepient: {
          firstName: order.sender.firstName,
          lastName: order.sender.lastName,
          email: order.sender.email
        },
        order_id: order._id.toString(),
        type: order.type,
        isExpedite: order.hasOwnProperty("samplesOrderRef"),
        amount: order.amount,
        currencySymbol: order.currencySymbol,
        customBottle: {
          style: order.customBottle.style,
          message: order.customBottle.message,
          name: order.customBottle.name,
          initials: order.customBottle.initials
        },
        orderSize: order.orderSize,
        promoCode: order.promoCode,
        optimizely: order.getOptimizelyVariationsString(),
        journey: {
          journey: order.journey.getJourneyString(),
          primaryFragrance: order.journey.primaryFragrance ? order.journey.primaryFragrance.getDescriptionString() : 'None'
        },
        delivery: {
          fullName: order.delivery.fullName,
          addressLine1: order.delivery.addressLine1,
          addressLine2: order.delivery.addressLine2,
          city: order.delivery.city,
          country: order.delivery.country,
          county: order.delivery.county,
          zipCode: order.delivery.zipCode,
          phoneNumber: order.delivery.phoneNumber
        }
      };
    });
}

module.exports.orderLab = function orderLab(orderID) {
  if (!config.prod) return true;
  findOrder(orderID)
    .then(order => {
      let url = 'https://hooks.zapier.com/hooks/catch/1556371/6kgib9/'; // lab main
      if (order.type == 'WaftGift')
        url = 'https://hooks.zapier.com/hooks/catch/1556371/t5vkbc/'; // gift complete
      if (!order.isExpedite) {
        // Expedited orders processed in separate hook (see above)
        return send(url, order);
      }
    })
    .catch(err => {
      logger.error('Zapier orderLab Error', err);
    })
};

module.exports.orderDelivered = function orderDelivered(orderID) {
  if (!config.prod) return true;
  findOrder(orderID)
    .then(order => {
      return send("https://hooks.zapier.com/hooks/catch/1556371/h9e0ng/", order);
    })
    .catch(err => {
      logger.error('Zapier orderDelivered Error', err);
    })
};
