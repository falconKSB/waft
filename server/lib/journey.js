const JourneyModel = require('../models/journey');
const activeCampaign = require('../lib/activecampaign');

function getNewJourney(id) {
  if (id) {
    return JourneyModel.findById(id);
  }
  else {
    return Promise.resolve(new JourneyModel())
  }
}

function addOrUpdateJourney(newJourney, cookie) {
  let journey;
  return getNewJourney(newJourney._id)
    .then(function (_journey) {
      _journey.mapping(newJourney);
      if (cookie) _journey.cookie = cookie;
      _journey.processRecommendation();
      return _journey.save()
    })
    .then(_journey => {
      journey = _journey;
      activeCampaign.onJourney(journey);
    })
    .then(() => {
      return journey;
    })
}

module.exports.addOrUpdateJourney = addOrUpdateJourney;