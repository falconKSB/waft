const Json2csv = require('json2csv');
const DateFormat = require('dateformat');

function deepCompare( x, y ) {
    if ( x === y ) return true;
    // if both x and y are null or undefined and exactly the same

    if ( ! ( x instanceof Object ) || ! ( y instanceof Object ) ) return false;
    // if they are not strictly equal, they both need to be Objects

    for ( let p in x ) {
        if ( ! x.hasOwnProperty( p ) ) continue;

        if ( x[ p ] === y[ p ] ) continue;
        // if they have the same strict value or identity then they are equal

        if ( typeof( x[ p ] ) !== "object" ) return false;
        // Numbers, Strings, Functions, Booleans must be strictly equal

        if ( ! deepCompare( x[ p ],  y[ p ] ) ) return false;
        // Objects and Arrays must be tested recursively
    }
    return true;
}

module.exports.deepCompare = deepCompare;

// Reverses array of Mongo documents into a map
module.exports.getDocumentsMap = function(documents) {
    const result = {};
    for (let i = 0; i < documents.length; i++) {
        let document = documents[i];
        result[document._id] = document;
    }
    return result;
};

module.exports.sendCSV = function (res, fileNamePrefix, data, fields) {
    const now = new Date();
    const fileName = fileNamePrefix + '-' + DateFormat(now, 'yyyy-mm-dd-HH-MM-ss') + '.csv';
    res.setHeader('Content-disposition', 'attachment; filename=' + fileName);
    res.set('Content-Type', 'application/octet-stream');
    res.send(Json2csv({data: data, fields: fields}));
};
