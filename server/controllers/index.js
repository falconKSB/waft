const router = require('express').Router();
module.exports = router;

const nocache = function (req, res, next) {
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.header('Expires', '-1');
    res.header('Pragma', 'no-cache');
    next();
};

router.use('/auth', require('./auth/index'));
router.use("/", require("./landing"));
router.use("/bottle", require("./bottle"));
router.use("/investors", require("./investors"));
router.use("/password-recovery", nocache, require("./passwordRecovery"));
router.use("/api", nocache, require("./api/index"));
router.use("/app", nocache, require("./app"));
router.use("/admin", nocache, require("./adminApp"));

