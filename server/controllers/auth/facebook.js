const express = require('express');
const config = require('../../config/config');

const User = require('../../models/user');
const graph = require('fbgraph');
const activecampaign = require("../../lib/activecampaign");
const geoip = require("../../lib/geoip");

const logger = require('../../lib/logging');

const authenticationRouter = express.Router();
module.exports = authenticationRouter;

function fb_authorise(code) {
    return new Promise((resolve, reject) => {
        graph.authorize({
            "client_id": config.facebookAuth.clientID
            , "redirect_uri": 'https://' + config.domain + '/auth/facebook'
            , "client_secret": config.facebookAuth.clientSecret
            , "code": code
        }, function (err, result) {
            if (err) {
                if (err.type == 'OAuthException' && err.code == 100) {
                    // Token already used (possibly user refreshed the page
                    err.status = 400;
                }
                let new_message = "Facebook authentication error";
                if (err.message) new_message += " (" + err.message + ")";
                new_message += ". Please go back and try again.";
                err.message = new_message;
                return reject(err);
            }
            return resolve(result);
        });
    });
}

function fb_me(token, fields) {
    return new Promise((resolve, reject) => {
        graph.setAccessToken(token);
        graph.get('me?fields='+fields, function (err, result) {
            return err ? reject(err) : resolve(result);
        });
    });
}


authenticationRouter.get('/', function (req, res, next) {
    let email;
    let user;
    let facebookProfile;

    if (!req.query.code) {
        if (!req.query.error) {
            let authUrl = graph.getOauthUrl({
                "client_id": config.facebookAuth.clientID
                , "redirect_uri": 'https://' + config.domain + '/auth/facebook'
                , "scope": 'email'
            });
            res.redirect(authUrl);
        } else {
            logger.warn('Facebook login error', req.query.error);
            res.redirect('/app');
        }
        return;
    }

    fb_authorise(req.query.code)
        .then(result => {
            return fb_me(result.access_token, 'id,email,first_name,last_name,gender')
        })
        .then(_facebookProfile => {
            facebookProfile = _facebookProfile;
            if (!facebookProfile.email) throw { no_facebook_email: true };
            email = facebookProfile.email.toLowerCase();
            return User.findOne({ email: email });
        })
        .then(_user => {
            if (_user) { // update existing user
                user = _user;
                if (user.facebookUserId) {
                    if (user.facebookUserId != facebookProfile.id)
                        throw { message: 'This email address is linked to a different Facebook User ID' }
                } else user.facebookUserId = facebookProfile.id;

                if (!user.firstName) user.firstName = facebookProfile.first_name;
                if (!user.lastName) user.lastName = facebookProfile.last_name;
                if (!user.gender) user.gender = facebookProfile.gender;

                if (!user.isModified()) return Promise.resolve(user);
            }
            else {
                user = new User();
                user.email = email;
                user.facebookUserId = facebookProfile.id;
                user.firstName = facebookProfile.first_name;
                user.lastName = facebookProfile.last_name;
                user.gender = facebookProfile.gender;
            }
            return user.save()
                .then(_user => {
                    user = _user;
                    let contact = {
                        email: email,
                        ip4: geoip.getIP(req),
                        firstName: user.firstName,
                        lastName: user.lastName
                    };
                    if (user.gender) {
                        contact["field[%GENDER%,0]"] = user.gender;
                    }
                    return activecampaign.onRegistrationFacebook(contact);
                })
        })
        .then(() => {
            res.cookie('authData', JSON.stringify({ data: user.getCurrentUser()}),
                { expires: new Date(Date.now() + config.auth_cookie_age) })
                .redirect('/app');
        })
        .catch(err => {
            if (err.no_facebook_email) {
                logger.warn('No e-mail address in Facebook profile', { req: { body: req.body, ip: req.ip, method: req.method, url: req.originalUrl }});
                res.redirect('/app');
            }
            else return next(err);
        });
});
