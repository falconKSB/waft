"use strict";

var router = require("express").Router();
var Subscription = require("../../models/subscription");
var activecampaign = require("../../lib/activecampaign");

var GeoIP = require('../../lib/geoip');

module.exports = router;

router.post("/", function (req, res, next) {
    let ip = GeoIP.getIP(req);
    let geo = GeoIP.lookup(ip);


    let newSubscription = new Subscription({
        ip: ip,
        email: req.body.email,
        country: geo.countryCode,
        city: geo.city
    });

    newSubscription.save()
        .then(subscription => {
            return activecampaign.onNewsletter({
                email: req.body.email,
                ip4: ip
            });
        })
        .then(result => {
            res.json({success: true});
        })
        .catch(next)
});
