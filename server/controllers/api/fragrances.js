"use strict";

var express = require("express");
var router = express.Router();
var path = require("path");
var jsonwebtoken = require("jsonwebtoken");
var jwt = require('jwt-simple');

var Fragrance = require('../../models/fragrance');
var search_util = require('../../lib/search');

module.exports = router;

function Index(min_word_length, field) {
    return {
        index: {},
        min_word_length: min_word_length,
        field: field,

        find: function (term, max) {
            let words = search_util.getWords(term);
            let documents = {};
            for (let i = 0; i < words.length; i++) {
                let added_documents = [];
                let res = this.findWord(words[i]);
                if (!res) continue;
                for (let j = 0; j < res.length; j++) {
                    let indexedDocument = res[j];
                    let document = indexedDocument.document;
                    let key = document[field];
                    if (added_documents.indexOf(key) != -1) continue;
                    added_documents.push(key);
                    if (key in documents) {
                        documents[key].score += indexedDocument.score;
                    } else {
                        documents[key] = {
                            document: document,
                            score: indexedDocument.score
                        }
                    }
                }
            }
            let temp_result = [];
            for (let key in documents) {
                temp_result.push(documents[key]);
            }
            temp_result = temp_result.sort(function (a, b) {
                let result =  b.score - a.score;
                if (result == 0) {
                    result = a.document.secondary_score - b.document.secondary_score;
                }
                return result;
            });
            const result = [];
            for (let i = 0; i < Math.min(max, temp_result.length); i++) {
                result.push(temp_result[i].document)
            }
            return result;
        },

        findWord: function (term) {
            return this.index[term];
        },

        updateScore: function (key, indexedDocument) {
            let docIndex = this.index[key];
            for (let i = 0; i < docIndex.length; i++) {
                let doc = docIndex[i];
                if (doc.id == indexedDocument.id) {
                    if (indexedDocument.score > doc.score)
                        doc.score = indexedDocument.score;
                    return true;
                }
            }
            return false;
        },

        append: function (key, document, exact) {
            let id = ''+document._id;
            let str = document[this.field].toLowerCase();

            let indexedDocument = {
                id: id,
                document: document,
                score: exact?1:0.4
            };
            if (key in this.index) {
                if (!this.updateScore(key, indexedDocument))
                    this.index[key].push(indexedDocument);
            }
            else
                this.index[key] = [indexedDocument]
        },

        appendAllKeysForWord: function (document, word) {
            let current_key = word.slice(0, this.min_word_length);
            this.append(current_key, document);
            for (let i = this.min_word_length; i < word.length; i++) {
                current_key += word[i];
                this.append(current_key, document, i == word.length-1);
            }
        },

        add: function (document) {
            const words = search_util.getWords(document[this.field]);
            for (let i = 0; i < words.length; i++) {
                this.appendAllKeysForWord(document, words[i]);
            }
        }
    }
}

const index = Index(1, 'name');
let defaultReady = false;
let defaultList = [];

function isASCII(str) {
    return /^[\x00-\x7F]*$/.test(str);
}

const cursor = Fragrance.find({main: true}).select('name accords female male').cursor();
cursor.on('data', function (doc) {
    doc = doc.toObject();
    let str = doc.name.toLowerCase();
    doc.secondary_score = str.length - (str.endsWith(" for men")?8:0) - (str.endsWith(" for women")?10:0);
    if (doc.accords.length > 2)
        index.add(doc);
    if ((doc.name[0].toLowerCase() == 'a') &&
        isASCII(doc.name) && (doc.accords.length == 6))
            defaultList.push(doc);
}).on('close', function () {
    defaultList = defaultList.sort(function (a, b) {
        return (a.name > b.name)? 1: -1;
    });
    const result = [];
    for (let i = 0; i < Math.min(300, defaultList.length); i++) {
        result.push(defaultList[i])
    }
    defaultList = result;
    defaultReady = true;
});

//Gets a list of fragrances by term or list of ids.
router.get("/", function (req, res) {
    const term = req.query["term"];

    if (term != null) {
        let fragrances;
        if (term == "") {
            fragrances = defaultReady?defaultList:[];
        }
        else {
            fragrances = index.find(term, 100)
        }
        res.json({ success: true, term: term, data: fragrances });
        return;
    }
    const listOfIds = [];
    if (req.query["id1"]) listOfIds.push(req.query["id1"]);
    if (req.query["id2"]) listOfIds.push(req.query["id2"]);
    if (req.query["id3"]) listOfIds.push(req.query["id3"]);

    if (listOfIds.length > 0) {
        const query = { _id: { $in: listOfIds } };

        Fragrance.find(query)
            .then(function (fragrances) {
                if (!fragrances) {
                    let err = new Error('Not Found');
                    err.status = 404;
                    throw err;
                }
                res.json({ success: true, data: fragrances });

            })
            .catch(function (err) {
                return next(err);
            });
    }
    else {
        res.status(400);
        res.send('Bad Request');
    }
});


