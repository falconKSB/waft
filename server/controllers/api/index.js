var router = require('express').Router();
module.exports = router;

router.use('/journeys', require('./journeys'));

router.use('/webhook', require('./webhook'));
router.use('/admin', require('./admin/index'));
router.use('/gift', require('./gift/index'));

router.use('/fragrances', require('./fragrances'));
router.use('/newsletter', require('./newsletter'));
router.use('/corporate', require('./corporate'));
router.use('/price', require('./price'));
router.use('/orders', require('./orders'));
router.use('/klipfolio', require('./klipfolio'));
router.use('/promoCode', require('./promoCode'));

router.use('/profile', require('./profile/index'));

router.use('/auth', require('./auth/index'));

router.use('/event', require('./event'));