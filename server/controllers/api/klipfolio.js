"use strict";

var router = require('express').Router();
var Order = require('../../models/order');

module.exports = router;

router.get("/kliporders260120171201", function (req, res, next) {
    const stm = Order.find();
    if (req.query.paidOnly) {
        stm.where('payment_status').equals('succeeded')
            .where('amount').gt(0);
    }
    stm.populate('sender', 'email firstName lastName')
        .populate('delivery')
        .populate({
            path: 'journey',
            model: 'Journey',
            populate: {
                path: 'primaryFragrance baseFragrances',
                select: 'name female male family main_accord accords',
                model: 'Fragrance'
            }
        })
        .populate({
            path: 'journey',
            model: 'Journey',
            populate: {
                path: 'scents.main scents.add1.scent scents.add2.scent',
                select: 'name code',
                model: 'Scent'
            }
        })
        .sort('-createdAt')
        .exec()
        .then(function (orders) {
            res.json({
                success: true,
                data: orders
            })
        })
        .catch(next)
});

