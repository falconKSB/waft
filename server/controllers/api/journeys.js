const router = require("express").Router();
module.exports = router;

const JourneyModel = require('../../models/journey');
const journeyHelper = require('../../lib/journey');
const scentMatching = require("../../lib/scentMatching");
const logger = require('../../lib/logging');
const crypto = require("crypto");

function findJourneyByIdOrCookie({id, cookie}) {
  if (id === 'saved') {
    return JourneyModel.find({ cookie })
      .sort({ createdAt: -1 })
      .populate('primaryFragrance')
      .limit(1)
      .then(journeys => {
        if (!journeys || journeys.length === 0)
          return null;
        else return journeys[0];
      })
  } else {
    return JourneyModel.findById(id)
      .populate('primaryFragrance')
  }
}

//Gets a journey by id.
router.get("/:id", function (req, res, next) {
  findJourneyByIdOrCookie({id: req.params.id, cookie: req.cookies['_p']})
    .then(function (journey) {
      if (!journey) {
        let err = new Error('Not Found');
        err.status = 404;
        throw err;
      }
      res.json({success: true, data: journey});
    })
    .catch(next);
});

const CookieAge = 1000*60*60*24*365;

/**
 * Adds or updates a journey
 */
router.post("/", function (req, res, next) {
  let newJourney = req.body.journey;
  logger.debugReq(`Journey ${newJourney._id ? 'Update' : 'Create'}`, req);

  let cookie = req.cookies['_p'];
  if (!cookie)
    cookie = crypto.randomBytes(32).toString('base64');

  journeyHelper.addOrUpdateJourney(newJourney, cookie)
    .then(journey => {
      res.cookie('_p', cookie, { maxAge: CookieAge });
      res.json({
        success: true,
        data: {journey: journey}
      });
    })
    .catch(next);
});

router.post("/scent", function (req, res, next) {
  let journey = new JourneyModel();
  journey.mapping(req.body.journey);
  journey.populate({
      path: 'primaryFragrance baseFragrances',
      model: 'Fragrance'
    })
    .execPopulate()
    .then(journey => {
      return scentMatching.matchByJourney(journey)
    })
    .then(result => {
      res.json({
        success: true,
        data: {
          main: {
            code: result.main.code,
            description: result.main.scent.description,
            accords: result.main.scent.getAsAccords()
          }
        }
      });
    })
    .catch(next);
});