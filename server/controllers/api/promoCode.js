"use strict";

var express = require("express");
var router = express.Router();
var promoCode = require("../../lib/promoCode");

module.exports = router;

router.get("/:code", function(req, res, next) {
    let code = req.params.code;
    let currency = req.query.currency;
    let sender = req.query.sender;
    
    if (!code || !currency) {
        return res.status(404).json({success: false});
    }

    promoCode.validate(code, currency, sender)
        .then(data => {
            return res.status(200).json({
                success: true,
                data: data
            });
        })
        .catch(err => {
            if (err == null)
                err = {};
            else if (typeof err == "string") {
                err = {
                    message: err
                }
            }
            err.status = 404;
            err.details = err.message;
            err.message = 'Invalid promo code';
            return next(err);
        });
});
