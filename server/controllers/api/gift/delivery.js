const router = require("express").Router();

const Order = require('../../../models/order');
const Delivery = require('../../../models/delivery');
const User = require('../../../models/user');

const JourneyHelper = require('../../../lib/journey');
const Zapier = require("../../../lib/zapier");
const logger = require('../../../lib/logging');

module.exports = router;

router.post("/delivery", function (req, res, next) {
  logger.debugReq('Gift Delivery', req);
  let delivery = new Delivery(req.body.delivery);
  let orderId = req.body.orderId;
  let userId = req.body.giftReceiver;
  let order, user;
  let journey = req.body.journey;

  if (!orderId) {
    let err = new Error('Order Not Found');
    err.status = 404;
    throw err;
  }

  Promise.all([User.findById(userId), Order.findById(orderId)])
    .then(result => {
      user = result[0];
      if (!user) throw new Error("User not found");
      order = result[1];
      if ((!order) || (order.type !== 'WaftGift') || (order.gift.status !== 'paid')) {
        let err = new Error('Gift reference not found');
        err.status = 200;
        throw err;
      }
      journey.order = orderId;
      journey.status = 'Completed';
      return Promise.all([delivery.save(), JourneyHelper.addOrUpdateJourney(journey)]);
    })
    .then(result => {
      delivery = result[0];
      journey = result[1];
      order.journey = journey._id;
      order.delivery = delivery._id;
      order.gift.receiver = userId;
      order.customBottle = journey.customBottle.toObject();
      order.status = 'Received';
      order.gift.status = 'complete';
      order.customPackaging.firstName = user.firstName;
      if(req.body.orderGender) order.gender = req.body.orderGender;
      order.processRecommendation();
      return order.save();
    })
    .then(_order => {
      return Zapier.orderLab(orderId);
    })
    .then(result => {
      res.json({
        success: true,
        data: {orderId: orderId}
      });
    }).catch(next);
});

