const router = require('express').Router();

router.use('/', require('./delivery'));
router.use('/', require('./giftEmail'));
router.use('/', require('./get'));

module.exports = router;
