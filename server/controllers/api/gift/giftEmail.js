const express = require("express");
const router = express.Router();
const orderModel = require('../../../models/order');
const userModel = require('../../../models/user');
const activeCampaign = require("../../../lib/activecampaign");
const orderHelper = require("../../../lib/orderHelper");

router.post("/:id/gift-email", function (req, res, next) {
  let orderId = req.body.orderId;
  let userId = req.body.giftReceiver;
  let order, user;
  let gift = req.body.gift;

  if (!orderId)
    return next({status: 404, message: 'Order Not Found'});

  Promise.all([userModel.findById(userId), orderModel.findById(orderId)])
    .then(result => {
      user = result[0];
      if (!user) throw new Error("User not found");
      order = result[1];
      if ((!order) || (order.type !== 'WaftGift') || (order.gift.status !== 'paid'))
        throw {status: 200, message: "Gift reference not found"};

      order.gift.recipient = gift.recipient;
      order.gift.emailFromUser = gift.emailFromUser;
      order.gift.inviteEmailDate = gift.inviteEmailDate;
      order.gift.note = gift.note;
      order.gift.senderName = gift.senderName;

      return order.save();
    })
    .then(result => {
      if (order.gift.emailFromUser || !order.gift.inviteEmailDate ||
        order.gift.inviteEmailDate <= new Date()) {
        orderHelper.sendGiftEmail(order);
      }
      res.json({
        success: true,
        data: {orderId: orderId}
      });
      activeCampaign.onPayGift(order, orderHelper.getGiftURL(order));
    }).catch(next);
});

module.exports = router;
