"use strict";

var express = require("express");
var router = express.Router();

var Order = require('../../../models/order');
var async = require('async');

module.exports = router;
router.get('/:id', function (req, res, next) {
    Order.findById(req.params.id).populate('sender delivery')
        .then(function (order) {
            if (!order || (order.type !== 'WaftGift') || (order.gift.status !== 'paid') ) {
                let err = new Error('Gift reference not found');
                err.status = 200;
                throw err;
            }
            res.json({
                success: true, data: {
                    id: order._id,
                    customBottle: order.customBottle,
                    gift: order.gift,
                    delivery: order.delivery,
                    countryCode: order.countryCode,
                    gender: order.gender
                }
            })
        })
        .catch(next);
});
