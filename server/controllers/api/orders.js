"use strict";

var router = require('express').Router();
var Order = require('../../models/order');
var config = require("../../config/config");
var geoip = require('../../lib/geoip');

var User = require('../../models/user');
var Journey = require('../../models/journey');
var Delivery = require('../../models/delivery');
var Warehouse = require('../../models/warehouse');
var Prices = require('../../models/prices');
var UserPaymentMethod = require('../../models/userPaymentMethod');

var Zapier = require("../../lib/zapier");
var activecampaign = require("../../lib/activecampaign");
var Payment = require("../../lib/payment");
var PromoCode = require("../../lib/promoCode");
var JourneyHelper = require('../../lib/journey');
var logger = require('../../lib/logging');

module.exports = router;

function getGiftURL(order) {
    return config.homepath + "/app/gift/receive/" + order._id
}

function isFirstPaidOrder(user_id) {
    return Order
        .count({sender: user_id, payment_status: 'succeeded'})
        .then(result => result == 0);
}

function createOrder(user_id, order, delivery, journey, price) {
    let user;
    return User.findById(user_id)
        .then(_user => {
            if (!_user) throw new Error("User not found");
            user = _user;
            let promises = [
                delivery.save(),
                Warehouse.findOne({name: price.warehouse}),
                isFirstPaidOrder(user_id)
            ];
            if (order.type == 'WaftLab')
                promises.push(JourneyHelper.addOrUpdateJourney(journey));
            return Promise.all(promises);
        })
        .then(result => {
            delivery = result[0];
            order.warehouse = result[1]._id;
            order.isFirstOrder = result[2];
            if (order.type == 'WaftLab') {
                journey = result[3];
                order.journey = journey._id;
                order.customBottle = journey.customBottle.toObject();
              if (!order.gender) {
                order.gender = journey.recepientGender;
              }
            }
            else {
                order.gift.status = 'created';
            }
            order.delivery = delivery._id;
            order.sender = user_id;
            order.currencySymbol = price.currencySymbol;
            order.currency = price.currency;
            order.amount = price[order.orderSize] + price.shipping[order.orderSize];
            order.status = 'Created';
            if (order.orderSize == 'samples') {
                order.orderSizeMain = price.orderSizesByGender[order.gender];
                order.amountMain = price[order.orderSizeMain] + price.shipping[order.orderSizeMain];
                order.samplesStatus = 'Pending';
            }
            if ((user.email == 'paula@waft.com') || (user.email == 'cassandra@waft.com') ||
                (user.email == 'allie@waft.com'))
            {
                order.amountMain = 0;
                order.amount = 0;
            }
            return order.save();
        })
        .then(_order => {
            if (order.type == 'WaftGift') {
                activecampaign.onCreateGift(_order._id);
            }
            return order;
        })
        .catch(err => {
            if (order) {
                order.payment_status = err.message;
                order.status = 'Processing Failed';
                order.save();
            }
            err.loglevel = 'error';
            err.status = 200;
            throw err;
        })
}

function applyDiscount(amount, discount) {
    return (100 * amount - 100 * discount) / 100; // to address float binary storage issues
}

function charge(user, order, paymentAccount, token) {
    let amount = ((order.orderSize != 'samples') && order.promoDiscount) ?
        applyDiscount(order.amount, order.promoDiscount) : order.amount;
    if (amount === 0) {
        return Promise.resolve({id: '', status: 'Free of Charge'});
    }
    let method = user.paymentMethods ?
        ((paymentAccount == 'Stripe US') ? user.paymentMethods.stripe_us : user.paymentMethods.stripe_sg) : null;
    return Payment.createOrUpdateCustomer(paymentAccount, method?method.customerId:null, token, user.email)
        .then(customer => {
            let createMethod = !method;
            if (createMethod) {
                method = new UserPaymentMethod();
                method.user = user;
                method.customerId = customer.id;
                method.account = paymentAccount;
            }
            method.default_source = customer.default_source;
            method.sources = [];
            for (let i = 0; i < customer.sources.data.length; i++) {
                let source = customer.sources.data[i];
                method.sources.push({
                    id: source.id,
                    card: {
                        brand: source.brand,
                        country: source.country,
                        exp_month: source.exp_month,
                        exp_year: source.exp_year,
                        last4: source.last4,
                        funding: source.funding,
                        cvc_check: source.cvc_check,
                        address_zip_check: source.address_zip_check
                    }
                });
            }
            return method.save().then(_method => {
                method = _method;
                if (createMethod) {
                    if (!user.paymentMethods)
                        user.paymentMethods = {};
                    if (paymentAccount == 'Stripe US')
                        user.paymentMethods.stripe_us = _method;
                    else
                        user.paymentMethods.stripe_sg = _method;
                }
                user.customerId = method.customerId;
                user.paymentAccount = paymentAccount;
                return user.save();
            });
        })
        .then(_user => {
            return Payment.chargeCustomer(paymentAccount, method.customerId, order.currency, amount,
                order.type + " " + user.email + " " + order._id)
        })
}

function redeemPromoCode(promoCode, order, sender) {
    if ((!promoCode) || (promoCode == '')) return order;
    let amount = (order.orderSize == 'samples') ? order.amountMain : order.amount;
    return PromoCode.redeem(amount, promoCode, order.currency, sender._id, sender.email, order._id, sender.firstName, sender.lastName)
        .then(function (result) {
            order.promoCode = promoCode;
            order.promoDiscount = result.discount;
            if (result.promoData.promoMemberEmail) {
                order.promoMemberEmail = result.promoData.promoMemberEmail;
            }
            return order;
        })
        .catch(err => {
            if (err == null)
                err = new Error('Unable to redeem the promo code '+promoCode);
            else if (typeof err == 'string')
                err = new Error(err);
            err.payment_status = 'Unable to redeem the promo code '+promoCode;
            throw err;
        });
}

function pay(order_id, paymentAccount, token, promoCode, preferredSample) {
    if ((!paymentAccount) || (paymentAccount == ''))
        paymentAccount = 'Stripe US';
    let order, journey, sender;
    return Order.findOne({
            _id: order_id,
            status: {$in: ['Created', 'Payment Failed']}
        })
        .populate('journey')
        .populate({
            path: 'sender',
            model: 'User',
            populate: {
                path: 'paymentMethods.stripe_sg paymentMethods.stripe_us',
                model: 'UserPaymentMethod'
            }
        })
        .then(_order => {
            if (!_order) {
                let err = new Error('Invalid Order');
                err.status = 200;
                throw err;
            }
            order = _order;
            if(preferredSample) order.preferredSample = preferredSample;
            if (order.type == 'WaftLab') {
                journey = order.journey;
                order.processRecommendation();
            }
            sender = order.sender;
            return redeemPromoCode(promoCode, order, sender);
        })
        .then(_order => {
            order = _order;
            return order.save();
        })
        .then(_order => {
            order = _order;
            return charge(sender, order, paymentAccount, token)
        })
        .then(stripeCharge => {
            order.payment_status = stripeCharge.status;
            order.payment_id = stripeCharge.id;
            if (stripeCharge.status != 'Free of Charge') {
                order.receipt_number = stripeCharge.receipt_number;
                order.payment = {
                    account: paymentAccount
                };
                if (stripeCharge.source) {
                    order.payment.customerId = stripeCharge.source.customer;
                    order.payment.sourceId = stripeCharge.source.id;
                }
            }
            order.placedOn = new Date();
            order.status = 'Received';
            if (order.promoDiscount) {
                if (order.orderSize == 'samples')
                    order.amountMain = applyDiscount(order.amountMain, order.promoDiscount);
                else
                    order.amount = applyDiscount(order.amount, order.promoDiscount);
            }
            if (order.type == 'WaftGift') {
                order.status = 'Gift Invitation';
                order.gift.status = 'paid';
            }
            return order.save();
        })
        .catch(err => {
            if (order) {
                order.status = 'Payment Failed';
                order.payment_status = err.payment_status;
                if (!order.payment) order.payment = {};
                order.payment.account = paymentAccount;
                order.payment_id = err.charge;
                order.save();
            }
            err.status = 200;
            throw(err);
        })
        .then(_order => {
            if (!journey) return true;
            journey.order = _order._id;
            journey.status = 'Completed';
            return journey.save();
        })
        .then(() => {
            if (order.type == 'WaftLab') {
                activecampaign.onPayLab(order._id);
                Zapier.orderLab(order._id);
            }
            else {
                let orderToReport = {
                    sender: {
                        firstName: sender.firstName,
                        lastName: sender.lastName,
                        email: sender.email
                    },
                    recepient: order.gift.recipient,
                    order_id: order._id,
                    claim_url: getGiftURL(order),
                    customBottle: order.customBottle,
                    note: order.gift.note
                };
                Zapier.orderGift(orderToReport);
            }
            return order._id;
        });
}

router.post("/", function (req, res, next) {
    logger.debugReq('New Order', req);
    if (!req.body.order)
        next({ status: 400, message: 'Missing the order object', loglevel: 'error' });
    if (req.body.order._id == "") {
        delete req.body.order._id;
    }
    let order = new Order(req.body.order);
    let price = Prices.getPrice(order.countryCode);
    order.userInfo.ip = geoip.getIP(req);
    order.userInfo.location = geoip.lookup(order.userInfo.ip);
    order.userInfo.ua = req.headers['user-agent'];

    createOrder(req.body.order.sender, order, new Delivery(req.body.delivery),
        req.body.journey, price)
        .then(function (_order) {
            if (req.body.immediatePayment) {
                return pay(_order._id, req.body.token, req.body.promoCode);
            }
            else return _order._id
        })
        .then(function (order_id) {
            res.json({
                success: true,
                data: {orderId: order_id}
            });
            return order_id;
        })
        .catch(next)
});

router.post("/:id/pay", function (req, res, next) {
    logger.debugReq('Order Payment', req);
    pay(req.params.id, req.body.paymentAccount, req.body.token, req.body.promoCode, req.body.preferredSample)
        .then(function (order_id) {
            res.json({
                success: true,
                data: {orderId: order_id}
            });
            return order_id;
        })
        .catch(next)
});
