"use strict";

var express = require("express");
var router = express.Router();

var Journey = require('../../../models/journey');

module.exports = router;

router.get("/", function (req, res, next) {
    let query = {
        user: req.currentUser.id,
        order: {$exists: false}
    };

    Journey.find(query)
        .sort({createdAt: -1})
        .limit(5)
        .populate('user', 'email firstName lastName') // TODO: check why is this needed?
        .then(journeys => {
            if (!journeys)  {
                let err = new Error('Not Found');
                err.status = 404;
                throw err;
            }

            let journeysWithoutDeleted = journeys.filter(journey => journey.status !== 'Deleted');
            res.json({success: true, data: journeysWithoutDeleted});
        })
        .catch(function (err) {
            return next(err);
        });
});

router.post("/delete", function (req, res, next) {
    let journeyId = req.body.journeyId;

    Journey.findById(journeyId)
        .then(journey => {
            if (!journey)  {
                let err = new Error('Not Found');
                err.status = 404;
                throw err;
            }

            journey.status = 'Deleted';
            return journey.save();
        })
        .then(() => {
            res.json({
                success: true
            });
        })
        .catch(err => next(err));
});
