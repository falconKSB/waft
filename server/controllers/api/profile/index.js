var router = require('express').Router();

router.use(require('../../../lib/auth').authenticateUser);

router.use('/user', require('./user'));
router.use('/orders', require('./orders/index'));
router.use('/journeys', require('./journeys'));

module.exports = router;


