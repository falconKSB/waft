"use strict";

var express = require("express");
var router = express.Router();
var config = require("../../../config/config");
var path = require("path");
var jsonwebtoken = require("jsonwebtoken");

var User = require('../../../models/user');
var Journey = require('../../../models/journey');

module.exports = router;

//Gets a user.
router.get("/", function (req, res, next) {
    User.findById(req.currentUser.id, 'email firstName lastName createdAt')
        .then(function (user) {
            if (!user)  {
                let err = new Error('Not Found');
                err.status = 404;
                throw err;
            }
            res.json({
                success: true, data: {
                    email: user.email,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    createdAt: user.createdAt
                }
            });
        })
        .catch(next);
});
