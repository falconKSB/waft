"use strict";

const express = require("express");
const router = express.Router();

const Order = require('../../../../models/order');

module.exports = router;


function getOrdersBySize(size) {
    return function(req,res,next) {
        const query = {
            sender: req.currentUser.id,
            orderSize: size,
            status: {$nin: ['Payment Failed', 'Created']}
        };
        Order.find(query, 'createdAt journey status orderSize currencySymbol amount promoCode delivery customBottle shippingInfo shippedOn type gift customPackaging samplesStatus')
            .populate({
                path: 'journey',
                model: 'Journey',
                populate: {
                    path: 'user',
                    model: 'User',
                    select: 'firstName lastName'
                }
            })
            .populate({
                path: 'journey',
                model: 'Journey',
                populate: {
                    path: 'scents.main scents.add1.scent scents.add2.scent',
                    select: 'code',
                    model: 'Scent'
                }
            })
            .populate('delivery')
            .sort({createdAt: -1})
            .limit(100)
            .then(function (orders) {
                if (!orders) {
                    let err = new Error('Not Found');
                    err.status = 404;
                    throw err;
                }
                res.json({success: true, data: orders});
            })
            .catch(next);
    }
}

//Gets a list of users orders.
router.get("/", getOrdersBySize({$nin: ['samples']}));

router.get("/trials", getOrdersBySize('samples'));

