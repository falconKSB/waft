var router = require('express').Router();

router.use('/', require('./list'));
router.use('/', require('./one'));

module.exports = router;


