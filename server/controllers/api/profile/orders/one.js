"use strict";

var express = require("express");
var router = express.Router();

var Order = require('../../../../models/order');
var activecampaign = require("../../../../lib/activecampaign");
var Zapier = require("../../../../lib/zapier");
var OrderHelper = require("../../../../lib/orderHelper");

module.exports = router;

router.post("/:id/cancelTrial", function (req, res, next) {
    let orderId = req.params.id;
    Order.findOne({
            _id: orderId,
            sender: req.currentUser.id,
            orderSize: 'samples',
            samplesStatus: { $ne: 'Completed' }
        })
        .then(function (samples_order) {
            if (!samples_order) throw new Error("Order not found");
            samples_order.setSamplesStatus('Cancelled');
            samples_order.cancellationReasons = req.body.reasonsCancel;
            return samples_order.save();
        })
        .then(function (samples_order) {
            res.json({
                success: true,
                message: 'Samples Order is Cancelled'
            });
        })
        .then(() => {
            activecampaign.onTrialOperation(orderId, "cancel");
            Zapier.processTrial(orderId, "cancel");
        })
        .catch(function(err) {
            return next(err);
        });
});

router.post("/:id/expediteTrial", function (req, res, next) {
    var samples_order, order;
    let orderId = req.params.id;
    Order.findOne({
            _id: orderId,
            sender: req.currentUser.id,
            orderSize: 'samples',
            samplesStatus: { $ne: 'Completed' }
        })
        .then(function (samples_order) {
            if (!samples_order) throw new Error("Order not found");
            return OrderHelper.expediteMainOrder(samples_order, "customer", false);
        })
        .then(function (order) {
            res.json({
                success: true,
                message: 'Main Order is Created',
                data: {
                    order_id : order._id
                }
            });
        })
        .then(() => {
            activecampaign.onTrialOperation(orderId, "expedite");
            Zapier.processTrial(orderId, "expedite");
        })
        .catch(function(err) {
            if(order) {
                order.payment_status = err.message;
                order.status = 'Payment Failed';
                order.save();
            }
            err.status = 200;
            return next(err);
        });
});

router.post("/:id/changePrimaryScent", function (req, res, next) {
    var id = req.params.id;
    req.checkBody('newPrimary').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        return next({ status: 200, message: 'Invalid '+errors[0].param});
    }

    Order.findOne({
        _id: req.params.id,
        sender: req.currentUser.id
        })
        .populate('journey')
        .populate({
            path: 'journey',
            model: 'Journey',
            populate: {
                path: 'scents.main scents.add1.scent scents.add2.scent',
                model: 'Scent'
            }
        })
        .then(function (samples_order) {
            if (!samples_order) throw new Error("Order not found");
            samples_order.assertCanChangeSamplesOrder();
            var old_code = samples_order.journey.scents.main.code;
            samples_order.journey.changePrimaryScent(req.body.newPrimary);
            var new_code = samples_order.journey.scents.main.code;
            samples_order.appendChangelog('Changing Primary Scent. Old: '+
                +old_code+'. New: '+ new_code);
            return Promise.all([
                samples_order.journey.save(),
                samples_order.save()]);
        })
        .then(function () {
            res.json({
                success: true,
                message: 'Scents Changed'
            });
        })
        .catch(function(err) {
            return next(err);
        });
});

