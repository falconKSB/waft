var router = require("express").Router();
module.exports = router;

var config = require("../../config/config");
const logger = require('../../lib/logging');

var mongoose = require('mongoose');
var Zapier = require('../../lib/zapier');
var Order = require('../../models/order');

router.post("/delivery/:token", function (req, res, next) {
    if (!req.params.token || req.params.token != config.aftershipToken) {
        throw { status: 401, loglevel: 'error', message: "Delivery token is not provided" };
    }
    logger.info("Delivery webhook", req.body);
    if (!req.body.msg || !req.body.msg.order_id ||
        !(req.body.msg.tag == "Delivered")) {
        throw { status: 200, loglevel: 'error', message: "Preconditions for delivery webhook is not fulfilled" };
    }

    let orderId = mongoose.Types.ObjectId(req.body.msg.order_id);
    Order.update(
        { $and : [
            { "_id": orderId },
            {"shippingInfo.trackingNumber": req.body.msg.tracking_number},
            {status: {$in: ['Shipped', 'Processed']}}
        ]},
        { status: "Delivered", deliveredOn: new Date(req.body.msg.updated_at)
        })
        .then((result) => {
            logger.info("Delivery webhook result", result);
            if (result.n != 1 || result.nModified != 1)
                throw { status: 200, loglevel: 'error', message: "Order was not updated" };
            res.sendStatus(200);
        })
        .then(() => {
            Zapier.orderDelivered(orderId);
        })
        .catch(err => {
            err.status = 200;
            err.loglevel = 'error';
            err.message = "Delivery webhook error: " + err.message;
            next(err);
        });
});
