"use strict";

const router = require('express').Router();

const User = require('../../../models/user');
const CloudWatchLogsHelper = require('../../../lib/cloudwatchlogs');

module.exports = router;

function defineQuery(term) {
    if (term) {
        let rexp = new RegExp(term, 'i');
        return {email: rexp};
    }
    return {};
}

router.get("/", function (req, res, next) {
    const page = +req.query.page - 1 || 0;
    const take = +req.query.take || 100;

    let term = (req.query.term || '').trim();

    var query = User.find(defineQuery(term),
        {
            '_id': 1,
            'email': 1,
            'firstName': 1,
            'lastName': 1
        })
        .sort('-createdAt')
        .limit(100);

    query.count(function (err, count) {
        query.skip(page * take).limit(take).exec('find', function (err, docs) {
            if (err) return next(err);

            res.json({
                count: count,
                docs: docs
            });
        });
    });
});

router.get("/:id", function (req, res, next) {
    const userId = req.params['id'];

    const query = User.findOne({_id: userId}, '-password -passwordResetToken')
        .populate('orders', '_id createdAt type status userInfo.ip')
        .populate('journeys auditlog')
        .populate({
            path: 'paymentMethods.stripe_sg paymentMethods.stripe_us',
            model: 'UserPaymentMethod'
        });

    let user;
    query.exec()
        .then(function (_user) {
            user = _user;
            return CloudWatchLogsHelper(user);
        })
        .then(logs => {
            res.json({
                user: user,
                logs: logs
            });
        })
        .catch(next)
});

router.put("/", function (req, res, next) {
    req.checkBody('email').notEmpty().isEmail();
    let errors = req.validationErrors();
    if (errors) {
        if (errors[0].param == 'email')
            return next({ status: 200, message: 'Invalid E-mail Address. Please try again' });
        return next(errors);
    }

    let userId = req.body.id;
    let newEmail = req.body.email.toLowerCase();

    let admin = req.currentAdmin;

    User.findOne({email: newEmail})
        .then(function (_user) {
            if (_user &&_user._id != userId) {
                return next({
                    message: "User with same email already exists"
                });
            } else {
                return User.findOne({_id: userId});
            }
        })
        .then(function (_user) {
            _user.appendChangelogChange(admin.email, _user.email, newEmail);
            _user.email = newEmail;
            return _user.save();
        })
        .then(function (_user) {
            res.json({
                //user: _user,
                success: true,
                message: "Email Updated Successfully"
            });
        })
        .catch(next)
});