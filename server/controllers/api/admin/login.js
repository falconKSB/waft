"use strict";
var config = require("../../../config/config");

var express = require("express");
var router = express.Router();
var Admin = require('../../../models/admin');

module.exports = router;

router.post("/login", function (req, res, next) {
    req.checkBody('username').notEmpty().isEmail();
    let errors = req.validationErrors();
    if (errors) {
        if (errors[0].param == 'username')
            return next({status: 400, message: 'Invalid E-mail Address'});
    }

    let username = req.body.username.toLowerCase();
    let password = req.body.password;
    let user;

    Admin.findOne({email: username})
        .then(_user => {
                if (!_user) {
                    let err = new Error('Wrong username or password!');
                    err.status = 401;
                    throw err;
                }
                user = _user;
                return user.comparePassword(req.body.password)
            })
        .then(isMatch => {
            if (!isMatch) {
                let err = new Error('Wrong username or password!');
                err.status = 401;
                throw err;
            }
            return user.getCurrentUser();
        })
        .then(currentUser => {
            currentUser.success = true;
            res.cookie('admintoken', currentUser.access_token, {expires: new Date(Date.now() + 7 * 86400000)})
                .json(currentUser);
        })
        .catch(next)
});


router.post("/token", function (req, res, next) {
    req.checkBody('refresh_token').notEmpty();
    let errors = req.validationErrors();
    if (errors) {
        let err = new Error('Bad request');
        err.details = 'No token provided';
        err.status = 400;
        throw err;
    }

    Admin.findOne({refreshToken: req.body.refresh_token})
        .then(function (_user) {
            if (!_user) {
                let err = new Error('Invalid token');
                err.details = 'User not found';
                err.status = 401;
                throw err;
            }
            return _user.getCurrentUser();
        })
        .then(currentUser => {
            res.cookie('admintoken', currentUser.access_token, {expires: new Date(Date.now() + 7 * 86400000)})
                .json(currentUser);
        })
        .catch(next)
});
