const router = require('express').Router();
const orderHelper = require('../../../../lib/orderHelper');
const orderModel = require('../../../../models/order');
const userModel = require('../../../../models/user');
const datesHelper = require('../../../../lib/dates');

let ObjectId = require('mongoose').Types.ObjectId;
RegExp.quote = require("regexp-quote");

function getUserIDs(userid, term) {
  if (userid)
    return Promise.resolve([userid]);
  if (term) {
    let rexp = new RegExp(RegExp.quote(term), 'i');
    return userModel.find({
      $or: [{email: rexp}, {firstName: rexp},
        {lastName: rexp}]
    })
  }
  return Promise.resolve(null);
}

function extractQuery(term, prefix) {
  if (!term) return null;
  if (!term.toLowerCase().startsWith(prefix + ' ')) return null;
  return term.substring(prefix.length + 1);
}

function getOrderWhere(req) {
  let term = (req.query.term || '').trim();
  let orderID = extractQuery(term, 'order') || term;
  if (orderID.length == 24) {
    try {
      let re = /[0-9A-Fa-f]{24}/g;
      if (re.test(orderID))
        orderID = new ObjectId(orderID);
      else
        orderID = null;
    }
    catch (e) {
      orderID = null;
    }
  }
  else orderID = null;
  let batch = extractQuery(term, 'batch');
  let tracking = extractQuery(term, 'track');
  let promo = extractQuery(term, 'promo');
  let receipt = extractQuery(term, 'receipt');
  if (batch || orderID || tracking || promo || receipt)
    term = null;

  let where = {};
  if (req.query.status && (req.query.status !== 'All Statuses') && (req.query.status !== 'Status')) {
    if (req.query.status == 'Received-Refunded')
      where['status'] = {$in: orderHelper.receivedRefunded};
    else
      where['status'] = req.query.status;
  }
  if (req.query.warehouse && (req.query.warehouse !== 'Warehouse')) {
    where['warehouse'] = req.query.warehouse;
  }
  if (req.query.samplesStatus && (req.query.samplesStatus !== 'All Samples')) {
    where['orderSize'] = 'samples';
    where['samplesStatus'] = req.query.samplesStatus;
  }
  if (req.query.excludedOrder) {
    where['_id'] = {$ne: req.query.excludedOrder};
  }
  let dates = datesHelper.extractDate(req.query.dates);
  if (dates) where['createdAt'] = dates;
  if (batch) {
    where['batchNo'] = batch;
  }
  if (tracking) {
    where['shippingInfo.trackingNumber'] = tracking;
  }
  if (promo) {
    where['promoCode'] = new RegExp(RegExp.quote(promo), 'i');
  }
  if (receipt) {
    where['receipt_number'] = receipt;
  }
  if (orderID) {
    where['_id'] = orderID;
  }
  return getUserIDs(req.query.userid, term)
    .then(ids => {
      if (ids) where['$or'] = [{sender: {$in: ids}}, {'gift.receiver': {$in: ids}}];
      return where;
    })
}

router.get("/", function (req, res, next) {
  const page = +req.query.page - 1 || 0;
  const take = +req.query.take || 100;

  getOrderWhere(req)
    .then(where => {
      let query = orderModel.find(where)
        .populate('sender', 'email firstName lastName')
        .populate({
          path: 'journey',
          model: 'Journey',
          populate: {
            path: 'scents.main scents.add1.scent scents.add2.scent',
            select: 'code',
            model: 'Scent'
          }
        })
        .populate({
          path: 'journey',
          model: 'Journey',
          populate: {
            path: 'primaryFragrance',
            model: 'Fragrance'
          }
        })
        .sort('-createdAt');

      query.count(function (err, count) {
        query.skip(page * take).limit(take).exec('find', function (err, docs) {
          if (err) return next(err);

          res.json({
            count: count,
            docs: docs
          });
        });
      });
    })
  .catch(next);
});

router.get("/orders/:id", function (req, res, next) {
  const orderId = req.params['id'];

  const query = orderModel.findById(orderId)
    .populate('sender', 'email firstName lastName')
    .populate('gift.receiver', 'email firstName lastName')
    .populate('delivery')
    .populate({
      path: 'journey',
      model: 'Journey',
      populate: {
        path: 'primaryFragrance baseFragrances',
        model: 'Fragrance'
      }
    })
    .populate({
      path: 'journey',
      model: 'Journey',
      populate: {
        path: 'scents.main scents.add1.scent scents.add2.scent',
        select: 'code name',
        model: 'Scent'
      }
    });

  let order;

  query.exec()
    .then(function (_order) {
      order = _order;
      return ScentMatching.matchByOrder(order);
    })
    .then(function (r) {
      const result = order.toObject();
      result.recommendations = {
        main: r.main ? r.main.toShortObject() : null,
        add1: r.add1 ? r.add1.toShortObject() : null,
        add2: r.add2 ? r.add2.toShortObject() : null,
        explain: r.explain,
        history: r.history
      };
      res.json({order: result});
    })
    .catch(next)
});

module.exports = router;
