const router = require('express').Router();
const orderModel = require('../../../../models/order');
const scentMatching = require('../../../../lib/scentMatching');

router.get("/:id", function (req, res, next) {
  const orderId = req.params['id'];
  let order;

  orderModel.findById(orderId)
    .populate('sender', 'email firstName lastName')
    .populate('gift.receiver', 'email firstName lastName')
    .populate('delivery')
    .populate({
      path: 'journey',
      model: 'Journey',
      populate: {
        path: 'primaryFragrance baseFragrances',
        model: 'Fragrance'
      }
    })
    .populate({
      path: 'journey',
      model: 'Journey',
      populate: {
        path: 'scents.main scents.add1.scent scents.add2.scent',
        select: 'code name',
        model: 'Scent'
      }
    })
    .then(function (_order) {
      order = _order;
      return scentMatching.matchByOrder(order);
    })
    .then(function (r) {
      const result = order.toObject();
      result.recommendations = {
        main: r.main ? r.main.toShortObject() : null,
        add1: r.add1 ? r.add1.toShortObject() : null,
        add2: r.add2 ? r.add2.toShortObject() : null,
        explain: r.explain,
        history: r.history
      };
      res.json({order: result});
    })
    .catch(next)
});

module.exports = router;
