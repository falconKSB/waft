const router = require('express').Router();
const orderModel = require('../../../../models/order');
const activeCampaign = require("../../../../lib/activecampaign");
const zapier = require("../../../../lib/zapier");
const logger = require('../../../../lib/logging');
const orderHelper = require("../../../../lib/orderHelper");

router.post("/expedite", function (req, res, next) {
  let thresholdDate = new Date();
  thresholdDate.setDate(thresholdDate.getDate() - 10);
  logger.info(`expediteOrders: threshold date is: ${thresholdDate}`);
  orderModel.find({
    orderSize: "samples",
    samplesStatus: "Pending",
    deliveredOn: {$lt: thresholdDate}
  })
    .then(orders => {
      logger.info("expediteOrders: found orders: " + orders.length);
      return Promise.all(
        orders.map(order => {
          return orderHelper.expediteMainOrder(order, "automatic job", true)
            .then(() => {
              logger.info(`expediteOrders: order expedited: ${order._id}`);
              return Promise.all([
                activeCampaign.onTrialOperation(order._id, 'expediteAdmin'),
                zapier.processTrial(order._id, 'expediteAdmin')]);
            });
        }))
    })
    .then(result => {
      res.json({
        success: true,
        message: `Processed ${result.length} orders`
      })
    })
    .catch(next);
});

router.post("/processCompletedTrials", function (req, res, next) {
  let thresholdDate = new Date();
  thresholdDate.setDate(thresholdDate.getDate() - 2);
  logger.info(`processCompletedTrials: threshold date is: ${thresholdDate}`);
  orderModel.updateMany(
    {$and: [{status: "Completed Trial"}, {createdAt: {$lt: thresholdDate}}]},
    {$set: {status: 'Received'}}
  ).then(result => {
    res.json({
      success: true,
      message: `processCompletedTrials: processed ${result.nModified} orders`
    });
  }).catch(next);
});

module.exports = router;
