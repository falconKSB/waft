const router = require('express').Router();
const orderModel = require('../../../../models/order');
const orderHelper = require('../../../../lib/orderHelper');
const datesHelper = require('../../../../lib/dates');
const dateformat = require( 'dateformat');
const scentMatching = require('../../../../lib/scentMatching');
const json2csv = require('json2csv');

const DEFAULT_ORDER_FIELDS_CSV =
  [
    {
      label: "Order ID",
      value: function (row, field, data) {
        return row._id.toString();
      }
    },
    {
      label: "Customer ID",
      value: function (row, field, data) {
        return row.sender._id.toString();
      }
    },
    {
      label: "Email",
      value: 'sender.email'
    },
    {
      label: "First Name",
      value: 'sender.firstName'
    },
    {
      label: "Last Name",
      value: 'sender.lastName'
    },
    {
      label: "Country",
      value: 'countryCode'
    },
    {
      label: "Warehouse",
      value: 'warehouse.name'
    },
    {
      label: "Type",
      value: 'type'
    },
    {
      label: "Size",
      value: 'orderSize'
    },
    {
      label: "Amount",
      value: function (row, field, data) {
        return row.currencySymbol + row.amount;
      }
    },
    {
      label: "Promo Code",
      value: 'promoCode'
    },
    {
      label: "Status",
      value: 'status'
    },
    {
      label: "Samples Status",
      value: 'samplesStatus'
    },
    {
      label: "Samples Order ID",
      value: function (row, field, data) {
        let d = row['samplesOrderRef'];
        return d ? '' + d : '';
      }
    },
    {
      label: "Main Order ID",
      value: function (row, field, data) {
        let d = row['mainOrderRef'];
        return d ? d['_id'].toString() : '';
      }
    },
    {
      label: "Main Order Status",
      value: 'mainOrderRef.status'
    },
    {
      label: "Main Order Date",
      value: datesHelper.formatDateForCSV('mainOrderRef', 'createdAt')
    },
    {
      label: "Main Order Amount",
      value: function (row, field, data) {
        if (!row.mainOrderRef) return '';
        return row.currencySymbol + row.mainOrderRef.amount;
      }
    },
    {
      label: "Created",
      value: datesHelper.formatDateForCSV('createdAt')
    },
    {
      label: "Placed",
      value: datesHelper.formatDateForCSV('placedOn')
    },
    {
      label: "Gender",
      value: 'gender'
    },
    {
      label: "Batch",
      value: "batchNo"
    },
    {
      label: "AB Test Split",
      value: function (row, field, data) {
        return row.optimizelyVariations ? row.optimizelyVariations.join(', ') : ''
      }
    },
    {
      label: "Initials",
      value: "customBottle.initials"
    },
    {
      label: "Bottle Name",
      value: "customBottle.name"
    },
    {
      label: "Bottle Message",
      value: "customBottle.message"
    },
    {
      label: "Bottle Style",
      value: "customBottle.style"
    },
    {
      label: "Package Name",
      value: "customPackaging.firstName"
    },
    {
      label: "Kiosk Sales Rep",
      value: "salesRep"
    },
    {
      label: "Cancellation Reasons",
      value: function (row, field, data) {
        return row['cancellationReasons'].join(' / ');
      }
    },
  ];

const DEFAULT_ORDER_ALGO_CSV =
  [
    {
      label: "Order ID",
      value: function (row, field, data) {
        return row._id.toString();
      }
    },
    {
      label: "Email",
      value: 'sender.email'
    },
    {
      label: "Country",
      value: 'countryCode'
    },
    {
      label: "Type",
      value: 'type'
    },
    {
      label: "Size",
      value: 'orderSize'
    },
    {
      label: "Status",
      value: 'status'
    },
    {
      label: "Samples Status",
      value: 'samplesStatus'
    },
    {
      label: "Created",
      value: datesHelper.formatDateForCSV('createdAt')
    },
    {
      label: "Gender",
      value: 'gender'
    },
    {
      label: "Journey Answers",
      value: function (row, field, data) {
        if (!row.journey) return '';
        return row.journey.getJourneyString();
      }
    },
    {
      label: "Journey Ingredients",
      value: function (row, field, data) {
        if (!row.journey) return '';
        return row.journey.getInternalIngredients().join(' / ');
      }
    },
    {
      label: "Primary Fragrance",
      value: 'journey.primaryFragrance.name'
    },
    {
      label: "Primary Fragrance Details",
      value: function (row, field, data) {
        if (!(row.journey && row.journey.primaryFragrance)) return '';
        return row.journey.primaryFragrance.getDetailsString();
      }
    },
    {
      label: "Main",
      value: function (row, field, data) {
        return row.journey && row.journey.scents.main ? row.journey.scents.main.toShortString() : '';
      }
    },
    {
      label: "Add1",
      value: function (row, field, data) {
        return row.journey && row.journey.scents.add1.scent ? row.journey.scents.add1.scent.toShortString() : '';
      }
    },
    {
      label: "Add2",
      value: function (row, field, data) {
        return row.journey && row.journey.scents.add2.scent ? row.journey.scents.add2.scent.toShortString() : '';
      }
    },
    {
      label: "Main Recommendation",
      value: "recommendation.main"
    },
    {
      label: "Add1 Recommendation",
      value: "recommendation.add1"
    },
    {
      label: "Add2 Recommendation",
      value: "recommendation.add2"
    }
  ];

const DEFAULT_ORDER_ML_CSV =
  [
    {
      label: "gender",
      value: 'gender'
    },
    {
      label: "journeyGender",
      value: 'journeyGender'
    },
    {
      label: "recipientGender",
      value: 'recipientGender'
    },
    {
      label: "timeOfDay",
      value: 'timeOfDay'
    },
    {
      label: "activity",
      value: 'activity'
    },
    {
      label: "mood",
      value: 'mood'
    },
    {
      label: "personalStyle",
      value: 'personalStyle'
    },
    {
      label: "waft",
      value: 'waft'
    },
    {
      label: "ingredients",
      value: 'ingredients'
    },
    {
      label: "fragranceBrand",
      value: 'fragranceBrand'
    },
    {
      label: "fragranceName",
      value: 'fragranceName'
    },
    {
      label: "fragranceFamily",
      value: 'fragranceFamily'
    },
    {
      label: "fragranceGender",
      value: 'fragranceGender'
    },
    {
      label: "fragranceMainAccord",
      value: 'fragranceMainAccord'
    },
    {
      label: "fragranceAccords",
      value: 'fragranceAccords'
    },
    {
      label: "scentType",
      value: 'scentType'
    },
    {
      label: "code",
      value: 'code'
    },
  ];

function getWhere(req) {
  let where = {};
  if (req.query.status && (req.query.status != 'All Statuses') && (req.query.status != 'Status')) {
    if (req.query.status === 'Received-Refunded')
      where['status'] = {$in: orderHelper.receivedRefunded};
    else
      where['status'] = req.query.status;
  }
  if (req.query.fields === 'ml') {
    if (!where['status'])
      where['status'] = {$in: orderHelper.processingDelivered};
    where['amount'] = {$gt: 0};
  }
  if (req.query.warehouse && (req.query.warehouse !== 'Warehouse')) {
    where['warehouse'] = req.query.warehouse;
  }
  if ((req.query.fields === 'algo') || (req.query.fields === 'ml')) {
    where['orderSize'] = {$nin: ['100ml', '50ml', '15ml', '5ml']}
  }
  let dates = datesHelper.extractDate(req.query.dates);
  if (dates) where['createdAt'] = dates;
  return where;
}

router.get("/export\.csv", function (req, res, next) {
  const now = new Date();
  const fileName = 'orders-' + dateformat(now, 'yyyy-mm-dd-HH-MM') + '.csv';
  res.setHeader('Content-disposition', 'attachment; filename=' + fileName);
  res.set('Content-Type', 'application/octet-stream');

  let fields = req.query.fields;
  let csvFields;
  if (fields === 'algo') csvFields = DEFAULT_ORDER_ALGO_CSV;
  else if (fields === 'ml') csvFields = DEFAULT_ORDER_ML_CSV;
  else {
    fields = 'default';
    csvFields = DEFAULT_ORDER_FIELDS_CSV;
  }

  const orders = [];
  let query = orderModel.find(getWhere(req));
  if ((fields === 'algo') || (fields === 'default'))
    query.populate({
      path: 'sender',
      select: 'email firstName lastName'
    });
  if (fields === 'default')
    query.populate({
      path: 'warehouse',
      select: 'name'
    })
    .populate({
      path: 'mainOrderRef',
      model: 'Order',
      select: 'status createdAt amount'
    });
  if ((fields === 'algo') || (fields === 'ml'))
    query = query.populate({
        path: 'journey',
        model: 'Journey',
        populate: {
          path: 'primaryFragrance baseFragrances',
          model: 'Fragrance'
        }
      })
      .populate({
        path: 'journey',
        model: 'Journey',
        populate: {
          path: 'scents.main scents.add1.scent scents.add2.scent',
          select: 'code name',
          model: 'Scent'
        }
      });
  let promises = (req.query.fields === 'algo') ? [] : null;
  if (fields === 'default')
    query.sort('-createdAt');
  query.cursor()
    .on('data', function (order) {
      if (req.query.fields === 'algo') {
        promises.push(
          scentMatching.matchByOrder(order).then(r => {
            order.recommendation = {
              main: r.main ? r.main.toShortString() : null,
              add1: r.add1 ? r.add1.toShortString() : null,
              add2: r.add2 ? r.add2.toShortString() : null
            };
            return order;
          }));
      }
      if (req.query.fields === 'ml') {
        if (!order.journey) return;
        if (!order.journey.scents.main) return;
        let base = {
          _id: order._id.toString(),
          gender: order.gender,
          journeyGender: order.journey.gender,
          recipientGender: order.journey.recipientGender,
          timeOfDay: order.journey.timeOfDay,
          activity: order.journey.activity,
          mood: order.journey.mood,
          personalStyle: order.journey.personalStyle,
          waft: order.journey.waft,
          ingredients: order.journey.ingredients.join(' ')
        };
        if (order.journey.primaryFragrance) {
          base['fragranceBrand'] = order.journey.primaryFragrance.brand;
          base['fragranceName'] = order.journey.primaryFragrance.name;
          base['fragranceFamily'] = order.journey.primaryFragrance.family;
          base['fragranceGender'] = order.journey.primaryFragrance.getGenderString();
          base['fragranceMainAccord'] = order.journey.primaryFragrance.main_accord;
          base['fragranceAccords'] = order.journey.primaryFragrance.accords.map(a => a.name).join(' ');
        }
        const mainCode = order.journey.scents.main.code;
        const add1Code = order.journey.scents.add1.scent ? order.journey.scents.add1.scent.code : null;
        const add2Code = order.journey.scents.add2.scent ? order.journey.scents.add2.scent.code : null;
        const add1Type = order.journey.scents.add1.addType;
        const add2Type = order.journey.scents.add2.addType;
        orders.push(Object.assign({scentType: 'Main', code: mainCode }, base));
        if (add1Code)
          orders.push(Object.assign({scentType: add1Type, code: add1Code }, base));
        if (add2Code)
          orders.push(Object.assign({scentType: add2Type, code: add2Code }, base));
      }
      else orders.push(order)
    })
    .on('close', function () {
      if (promises) {
        Promise.all(promises)
          .then(results => {
            res.send(json2csv({data: results, fields: csvFields}));
          })
          .catch(next)
      }
      else
        res.send(json2csv({data: orders, fields: csvFields}));
    })
    .on('error', next);
});

module.exports = router;
