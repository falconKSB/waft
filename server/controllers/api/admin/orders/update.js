const router = require('express').Router();
const orderModel = require('../../../../models/order');
const orderHelper = require('../../../../lib/orderHelper');
const journeyModel = require('../../../../models/journey');
const deliveryModel = require('../../../../models/delivery');
const utils = require('../../../../lib/utils');
const activeCampaign = require("../../../../lib/activecampaign");
const zapier = require("../../../../lib/zapier");

function getShortScentsObject(value) {
  let result = {
    main: value.main && value.main.code,
  };
  if (value.add1 && value.add1.scent)
    result.add1 = value.add1.scent.code;
  if (value.add2 && value.add2.scent)
    result.add2 = value.add2.scent.code;
  return result;
}

function checkAndChange(admin, order, newOrder, field, subField) {
  let newValue = newOrder[field];
  if (!newValue) return;
  let oldValue = order[field];
  if (subField) {
    newValue = newValue[subField];
    oldValue = oldValue[subField];
  }
  if (typeof oldValue == "number") {
    newValue = Number(newValue);
  } else if ((typeof oldValue == "object") && (oldValue._bsontype == "ObjectID")) {
    oldValue = oldValue.toString();
  }
  if (!utils.deepCompare(newValue, oldValue)) {
    if (subField == 'scents') {
      let oldValue1 = getShortScentsObject(oldValue);
      let newValue1 = getShortScentsObject(newValue);
      if (utils.deepCompare(newValue1, oldValue1)) return;
      order.appendChangelogChange(admin.email, field + (subField ? ('.' + subField) : ''), oldValue1, newValue1);
    }
    else {
      order.appendChangelogChange(admin.email, field + (subField ? ('.' + subField) : ''), oldValue, newValue);
    }
    if (subField)
      order[field][subField] = newValue;
    else
      order[field] = newValue;
  }
}

router.post("/", function (req, res, next) {
  let order;
  let trialOperation;
  let newOrder = req.body.order;
  let admin = req.currentAdmin;
  if (newOrder._id) {
    orderModel.findById(req.body.order._id)
      .populate({
        path: 'journey',
        model: 'Journey',
        populate: {
          path: 'scents.main scents.add1.scent scents.add2.scent',
          select: 'code name',
          model: 'Scent'
        }
      })
      .then(function (_order) {
        order = _order;
        checkAndChange(admin, order, newOrder, "orderSize");
        checkAndChange(admin, order, newOrder, "orderSizeMain");
        checkAndChange(admin, order, newOrder, "amountMain");
        checkAndChange(admin, order, newOrder, "status");
        checkAndChange(admin, order, newOrder, "gender");
        checkAndChange(admin, order, newOrder, "warehouse");
        checkAndChange(admin, order, newOrder, "onHoldReason");
        checkAndChange(admin, order, newOrder, "refundReason");
        if ((order.orderSize === 'samples') &&
          req.body.order.samplesStatus &&
          (order.samplesStatus != req.body.order.samplesStatus)) {
          if (req.body.order.samplesStatus !== 'Completed')
            order.setSamplesStatus(req.body.order.samplesStatus);
          if (req.body.order.samplesStatus === 'Cancelled') {
            trialOperation = "cancelAdmin";
          }
        }
        if (newOrder.deliveredOn) order.deliveredOn = newOrder.deliveredOn;
        checkAndChange(admin, order, newOrder, "journey", "scents");
        checkAndChange(admin, order, newOrder, "customBottle");
        checkAndChange(admin, order, newOrder, "customPackaging");
        return order.save();
      })
      .then(function (_order) {
        order = _order;
        return deliveryModel.findById(order.delivery);
      })
      .then(function (delivery) {
        delivery.mapping(newOrder.delivery);
        return Promise.all([journeyModel.findById(order.journey), delivery.save()])
      })
      .then(function (result) {
        let journey = result[0];
        if (!journey) {
          if (order.type === 'WaftGift') return true;
          else throw new Error('Unable to update the Journey');
        }
        journey.customBottle = order.customBottle;
        journey.scents = order.journey.scents;
        return journey.save();
      })
      .then(function () {
        if (((order.samplesStatus !== 'Completed') || (newOrder.expediteAgain === true)) &&
          (newOrder.samplesStatus === 'Completed') &&
          (order.orderSize === 'samples')) {
          trialOperation = "expediteAdmin";
          return orderHelper.expediteMainOrder(order, admin.email, false);
        }
        return Promise.resolve(true);
      })
      .then(function () {
        res.json({
          success: true,
          message: "Order Updated Successfully"
        });
      })
      .then(function () {
        if (trialOperation) {
          activeCampaign.onTrialOperation(order._id, trialOperation);
          zapier.processTrial(order._id, trialOperation);
        }
      })
      .catch(next);
  } else {
    next({
      message: "Invalid Operation"
    });
  }
});

module.exports = router;
