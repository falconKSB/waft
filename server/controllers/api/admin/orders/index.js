const router = require('express').Router();

router.use('/', require('./expedite'));
router.use('/', require('./export'));
router.use('/', require('./update'));
router.use('/', require('./list'));
router.use('/', require('./get'));

module.exports = router;
