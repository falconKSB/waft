"use strict";

const router = require('express').Router();
const NeatCsv = require('neat-csv');
const ObjectId = require('mongoose').Types.ObjectId;
const Utils = require('../../../lib/utils');

const Multer  = require('multer');
const Upload = Multer({ storage: Multer.memoryStorage(), limits : { fileSize: 100*1024*1024 } });

const Fragrance = require('../../../models/fragrance');
const Journey = require('../../../models/journey');

module.exports = router;

function parseScents(scent) {
    if (!scent) return [];
    scent = scent.trim();
    if (scent == '') return [];
    let list = scent.split('-');
    for (let i = list.length - 1; i >= 0; i--) {
        list[i] = list[i].trim();
        if ((list[i] == '') || isNaN(list[i])) {
            list.splice(i, 1);
        }
    }
    return list;
}

const FRAGRANCE_ALLOCATED_FIELDS_CSV =
    [
        {
            label: "ID",
            value: "id"
        },
        {
            label: "Name",
            value: 'name'
        },
        {
            label: "Description",
            value: 'description'
        },
        {
            label: "Main",
            value: 'main'
        },
        {
            label: "Main Unisex for Women",
            value: 'unisexwomen'
        },
        {
            label: "Main Unisex for Men",
            value: 'unisexmen'
        }
    ];

const FRAGRANCE_UNALLOCATED_FIELDS_CSV = FRAGRANCE_ALLOCATED_FIELDS_CSV.concat([
    {
        label: "Recommended for Men",
        value: 'recommended_man'
    },
    {
        label: "Recommended for Women",
        value: 'recommended_woman'
    },
    {
        label: "Recommended for Unisex",
        value: 'recommended_woman'
    }
]);

function defineQuery(term) {
    if (term) {
        let rexp = new RegExp(term, 'i');
        return { name : rexp};
    }
    return {};
}

router.get("/", function (req, res, next) {
    const page = +req.query.page - 1 || 0;
    const take = +req.query.take || 100;

    let term = (req.query.term || '').trim();

    let query = Fragrance.find(defineQuery(term),
        {
            'brand': 1,
            'short_name': 1,
            'scents': 1,
            'male': 1,
            'female': 1,
            'accords': 1,
            'family': 1,
            'main_accord': 1
        }).sort('brand short_name');

    query.count(function (err, count) {
        query.skip(page * take).limit(take).exec('find', function (err, docs) {
            if (err) return next(err);

            let fragrances = docs.map((item) => {
                item.description = item.getDetailsString();
                return item;
            });

            res.json({
                count: count,
                docs: fragrances
            });
        });
    });
});

router.get("/:id", function (req, res, next) {
    const fragranceId = req.params['id'];

    const query = Fragrance.findOne({_id: fragranceId});

    query.exec()
        .then(function (_fragrance) {
            _fragrance.description = _fragrance.getDetailsString();
            res.json({fragrance: _fragrance});
        })
        .catch(next)
});

router.get("/journeys/:id", function (req, res, next) {
    const fragranceId = req.params['id'];

    Journey.find({ baseFragrances : fragranceId })
        .populate({
            path: 'scents.main scents.add1.scent scents.add2.scent',
            select: 'code',
            model: 'Scent'
        })
        .sort('-createdAt')
        .limit(20)
        .then(function (data) {
            res.json({journeys: data});
        }).catch(next);
});

router.get("/download/allocated", function (req, res, next) {
    let where = {
        'main': true,
        'scents.main': {
            $exists: true,
            $ne: []
        }
    };
    const fragrances = [];
    Fragrance.find(where)
        .cursor()
        .on('data', function (fragrance) {
            fragrances.push({
                id: ''+fragrance._id,
                name: fragrance.name,
                description: fragrance.getDetailsString(),
                main: fragrance.scents.main.join('-'),
                unisexwomen: fragrance.scents.unisexwomen.join('-'),
                unisexmen: fragrance.scents.unisexmen.join('-')
            })
        })
        .on('close', function () {
            if (req.query.format == 'csv') {
                Utils.sendCSV(res, 'fragrances-allocated', fragrances, FRAGRANCE_ALLOCATED_FIELDS_CSV);
            } else {
                res.json({
                    success: true,
                    data: fragrances
                });
            }
        })
        .on('error', next);
});

router.get("/download/unallocated", function (req, res, next) {
    const unallocatedFragrances = {};
    Journey.find({
            'status': 'Completed',
            'primaryFragrance' : {
                $exists: true,
                $ne: null
            }
        })
        .select('primaryFragrance scents order gender')
        .populate({
            path: 'primaryFragrance',
            model: 'Fragrance'
        })
        .populate({
            path: 'scents.main scents.add1.scent scents.add1.scent',
            model: 'Scent',
            select: 'code'
        })
        .cursor()
        .on('data', function (journey) {
            const fragrance = journey.primaryFragrance;
            // if (fragrance.scents && fragrance.scents.main && (fragrance.scents.main.length > 0)) return;
            let id = fragrance._id;
            if (!(id in unallocatedFragrances)) {
                unallocatedFragrances[id] = {
                    id: '' + id,
                    journey_id: '' + journey._id,
                    order_id: '' + journey.order,
                    name: fragrance.name,
                    description: fragrance.getDetailsString(),
                    main: fragrance.scents.main.join('-'),
                    unisexwomen: fragrance.scents.unisexwomen.join('-'),
                    unisexmen: fragrance.scents.unisexmen.join('-')
                };
            }
            let scents = journey.scents;
            let recommend = ((scents && scents.main)?scents.main.code:'0') +
                '-'+((scents && scents.add1 && scents.add1.scent)?scents.add1.scent.code:'0');
            unallocatedFragrances[id]['recommended_'+journey.gender] = (recommend == '0-0')?'':recommend
        })
        .on('close', function () {
            const result = [];
            for (let id in unallocatedFragrances)
                result.push(unallocatedFragrances[id]);
            if (req.query.format == 'csv') {
                Utils.sendCSV(res, 'fragrances-unallocated', result, FRAGRANCE_UNALLOCATED_FIELDS_CSV);
            } else {
                res.json({
                    success: true,
                    data: unallocatedFragrances
                });
            }
        })
        .on('error', next)
});

router.post("/", Upload.single('fragranceFile'), function (req, res, next) {
    if (!req.file) {
        return next({
            message: "Invalid Operation"
        });
    }
    let lines = 0;
    let errorLines = 0;
    let warnLines = 0;
    let details = [];
    NeatCsv(req.file.buffer)
        .then(data => {
            let bulk = Fragrance.collection.initializeOrderedBulkOp();
            lines = data.length+1;
            errorLines = 0;
            for (let i = 0; i < data.length; i++) {
                let fragrance = data[i];
                if (((!fragrance.hasOwnProperty('Name'))&&(!fragrance.hasOwnProperty('ID'))) ||
                    (!fragrance.hasOwnProperty('Main')) ||
                    (!fragrance.hasOwnProperty('Main Unisex for Women')) ||
                    (!fragrance.hasOwnProperty('Main Unisex for Men')))
                {
                    details.push("ERROR: Line "+i+": not enough fields");
                    errorLines++;
                    continue;
                }
                let main = parseScents(fragrance['Main']);
                let unisexwomen = parseScents(fragrance['Main Unisex for Women']);
                let unisexmen = parseScents(fragrance['Main Unisex for Men']);
                if (main.length == 0) {
                    details.push("WARNING: Line "+i+": no main");
                    warnLines++;
                    continue;
                }
                let where = fragrance.hasOwnProperty('ID')?
                    { _id: new ObjectId(fragrance.ID) } :
                    { name: fragrance.Name };
                bulk.find(where).update({
                    $set: {
                        scents : {
                            main: main,
                            unisexwomen: unisexwomen,
                            unisexmen: unisexmen
                        }
                    }
                });
            }
            return bulk.execute();
        })
        .then(result => {
            res.json({
                success: true,
                message: "Fragrances Updated Successfully",
                linesInFile: lines,
                errorLines: errorLines,
                warnLines: warnLines,
                result: result,
                details: details
            });
        });
});
