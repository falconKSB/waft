const router = require('express').Router();
const jwt = require('jsonwebtoken');

router.use('/', require('./login'));
router.use('/accounts', require('./accounts'));

router.use(require('../../../lib/auth').authenticateAdmin);

// TODO: check if admin can edit or readonly

router.use('/scents', require('./scents'));
router.use('/fragrances', require('./fragrances'));
router.use('/inventory', require('./inventory'));
router.use('/orders', require('./orders/index'));
router.use('/journeys', require('./journeys'));
router.use('/dashboard', require('./dashboard'));
router.use('/prices', require('./prices'));
router.use('/users', require('./users'));
router.use('/gift', require('./gift/index'));


module.exports = router;
