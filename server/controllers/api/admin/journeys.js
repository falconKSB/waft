"use strict";

var router = require('express').Router();

var Journey = require('../../../models/journey');
var Order = require('../../../models/order');
var User = require('../../../models/user');

module.exports = router;

router.get("/", function (req, res, next) {
    var page = +req.query.page - 1;
    var take = +req.query.take;

    var query = Journey.find({}).populate('user', 'email').sort('-createdAt').limit(500);

    query.count(function (err, count) {
        query.skip(page * take).limit(take).exec('find', function (err, docs) {
            if (err) return next(err);

            res.json({
                count: count,
                docs: docs
            });
        });
    });
});

router.get("/:userId", function (req, res, next) {
    const userId = req.params['userId'];

    Journey.find({user: userId})
        .then(function (data) {
            res.json({journeys: data});
        }).catch(next);
});

