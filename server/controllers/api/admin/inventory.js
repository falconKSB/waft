"use strict";

const router = require('express').Router();
const NeatCsv = require('neat-csv');
const ObjectId = require('mongoose').Types.ObjectId;
const Utils = require('../../../lib/utils');

const Multer  = require('multer');
const Upload = Multer({ storage: Multer.memoryStorage(), limits : { fileSize: 100*1024*1024 } });

const Warehouse = require('../../../models/warehouse');

module.exports = router;

router.get("/", function (req, res, next) {
    Warehouse.find({}, '_id name').sort('name').then(warehouses => {
        res.json({warehouses: warehouses});
    }).catch(next);
});

router.post("/", Upload.single('inventoryFile'), function (req, res, next) {
    if (!req.file || !req.query.warehouse) {
        return next({
            message: "Invalid Operation"
        });
    }
    let lines = 0;
    let errorLines = 0;
    let warnLines = 0;
    let details = [];
    NeatCsv(req.file.buffer).then(data => data.map(line => {
        return {
            sku: line.SKU,
            stock: Number(line.Stock),
            allocated: 0
        };
    })).then(stocks => Warehouse.update(
        {name: req.query.warehouse}, {$set: {stock: stocks}}, {upsert: true})
    ).then(result => {
        res.json({
            success: true,
            message: "Inventory Updated Successfully",
            details: result
        });
    }).catch(next);
});
