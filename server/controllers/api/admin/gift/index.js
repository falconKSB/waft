const router = require('express').Router();

router.use('/', require('./email'));

module.exports = router;
