"use strict";

const express = require("express");
const router = express.Router();

const Order = require('../../../../models/order');
const OrderLib = require('../../../../lib/orderHelper');

module.exports = router;

router.post("/sendemails", function(req, res, next) {
    let now = new Date();
    Order.find({
        'gift.status': 'paid',
        'gift.emailFromUser' : false,
        'gift.inviteEmailSentOn': null,
        'gift.inviteEmailDate': {$lte: now}
    }).populate({
        path: 'sender',
        select: 'email firstName lastName'
    }).then(orders => {
        orders.forEach(order => {
            OrderLib.sendGiftEmail(order);
        });
        res.json({
            success: true,
            data: { toBeUpdated: orders.length }
        });
    }).catch(next);
});
