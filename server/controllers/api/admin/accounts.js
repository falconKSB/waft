"use strict";

var express = require("express");
var router = express.Router();

var Admin = require('../../../models/admin');

module.exports = router;

router.get("/", function (req, res, next) {

    var page = +req.query.page - 1;
    var take = +req.query.take;

    var query = Admin.find({}).select('_id email status').sort('email');

    query.count(function (err, _count) {

        query.skip(page * take).limit(take).exec('find', function (err, _docs) {

            if (err) return next(err);

            res.json({
                count: _count,
                docs: _docs
            });
        });
    });
});


router.get('/:id', function (req, res, next) {
    Admin.findById(req.params.id)
        .then(function (_admin) {

            res.json({
                success: true, admin: {
                    id: _admin._id,
                    email: _admin.email,
                    status: _admin.status,
                    accessLevel: _admin.accessLevel
                }
            })
        })
        .catch(function (err) {
            return next(err);
        });
});

router.post('/', function (req, res, next) {
    validate(req, function (err) {
        if (err) return next(err);

        Admin.findOne({email: req.body.email})
            .then(function (_admin) {
                if (!_admin) {
                    let admin = new Admin();

                    admin.email = req.body.email;
                    admin.password = req.body.password1;
                    admin.status = req.body.status;
                    admin.accessLevel = req.body.accessLevel;

                    return admin.save();
                }
                let err = new Error('The email is already in use');
                err.status = 200;
                throw err;

            })
            .then(function () {
                res.json({
                    success: true,
                    message: "New Account Created Successfully"
                });
            })
            .catch(next);
    });
});

router.put('/', function (req, res, next) {

    var userId = req.body.id || null;
    var admin;

    validate(req, function (err) {

        if (err)  return next(err);

        Admin.findById(userId)
            .then(function (_admin) {
                if (!_admin) return next({status: 200, message: 'Account not found'});
                admin = _admin;
                return Admin.findOne({email: req.body.email});
            })
            .then(function (_admin) {
                if (_admin && _admin._id != userId) return next({
                    status: 200,
                    message: 'The email is already in use'
                });

                admin.email = req.body.email;
                if (req.body.password1 != undefined) {
                    admin.password = req.body.password1;
                }
                admin.status = req.body.status;
                admin.accessLevel = req.body.accessLevel;

                return admin.save();
            })
            .then(function () {
                res.json({
                    success: true,
                    message: "Account Edited Successfully"
                });
            }).catch(function (err) {
            next(err);
        });

    });

});

function validate(req, callback) {
    try {
        req.checkBody('email').notEmpty().isEmail();
        var errors = req.validationErrors();
        if (errors && errors[0].param == 'email') {
            throw{status: 200, message: 'Invalid e-mail address'};
        }
        if (req.body.password1 == '' || req.body.password2 == '' && req.body._id) {
            throw{status: 200, message: 'Passwords can not be empty'};
        }
        if (req.body.password1 != req.body.password2) {
            throw{status: 200, message: 'Passwords are not equal'};
        }
        if (req.body.status == '') {
            throw{status: 200, message: 'Status can not be empty'};
        }
        if (req.body.accessLevel == '') {
            throw{status: 200, message: 'Access Level can not be empty'};
        }
    }
    catch (e) {
        return callback(e);
    }

    return callback(null);
}
