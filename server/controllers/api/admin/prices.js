"use strict";

var router = require('express').Router();

var Prices = require('../../../models/prices');

module.exports = router;

router.get("/", function (req, res, next) {
    let countries = [];
    let defaultVal = null;
    for (let prop in Prices) {
        let val = Prices[prop];
        if (val.currency) {
            if (val.name == 'Default') {
                defaultVal = val;
            } else {
                countries.push(val);
            }
        }
    }
    countries.sort((a, b) => (a.name > b.name) ? 1 : ((a.name < b.name) ? -1 : 0));
    res.json({countries: countries, "default": defaultVal});
});

