"use strict";

var express = require("express");
var router = express.Router();

var Scent = require('../../../models/scent');
var Warehouse = require('../../../models/warehouse');
let ObjectId = require('mongoose').Types.ObjectId;

module.exports = router;

router.post("/", function (req, res, next) {

    if (!req.body.scent.name) {
        return next({status: 200, success: false, message: 'The scent name is required'});
    }
    if (!req.body.scent.code) {
        return next({status: 200, success: false, message: 'The scent code is required'});
    }

    if (req.body.scent._id) {
        Scent.findById(req.body.scent._id)
            .then(function (scent) {
                scent.mapping(req.body.scent);
                return scent.save();
            })
            .then(function (scent) {
                res.json({
                    success: true,
                    message: "Scent Updated Successfully"
                });
            })
            .catch(function (err) {
                return next(err);
            });
    } else {
        let newScent = new Scent();
        newScent.mapping(req.body.scent);

        newScent.save()
            .then(function (scent) {
                res.json({
                    success: true,
                    message: "Successfully Created Scent"
                });
            })
            .catch(function (err) {
                next(err);
            });
    }
});

function getStock(sku, warehouses) {
    return warehouses.reduce((acc, w) => acc + w.getStock(sku), 0);
}

function buildAvailableQuantity(scent, warehouses) {
    let availableQuantity = {};
    for (let e of ['big', 'medium', 'small', 'samples']) {
        availableQuantity[e] = getStock(scent.SKU[e], warehouses);
    }
    return availableQuantity;
}

function populateStocks(scents, warehouseId) {
    let query = Warehouse.find({});
    if (warehouseId) {
        query.where('_id').eq(new ObjectId(warehouseId));
    }
    return query.then(warehouses => {
        scents.forEach(scent => {
            scent.availableQuantity = buildAvailableQuantity(scent, warehouses);
        });
    }).then(() => scents);
}

router.get("/", function (req, res, next) {

    var page = +req.query.page - 1;
    var take = +req.query.take;
    let warehouseId = req.query.warehouse;

    if (take == 0) {
        Scent.find({status: 'Active'}).sort('code')
            .then(scents => populateStocks(scents, warehouseId))
            .then(scents => {
                res.json({
                    docs: scents
                });
            }).catch(next);
    }
    else {
        var query = Scent.find({}).sort('code');

        query.count(function (err, count) {

            query.skip(page * take).limit(take).exec('find', function (err, docs) {

                if (err) return next(err);

                populateStocks(docs, warehouseId)
                    .then(scents => {
                        res.json({
                            count: count,
                            docs: scents
                        });
                    }).catch(next);
            });
        });
    }

});

router.get("/:id", function (req, res, next) {

    var scentId = req.params['id'];

    var query = Scent.findById(scentId);

    query.exec(function (err, scent) {
        if (err) return next(err);

        res.json({scent: scent});
    });

});

router.delete("/", function (req, res, next) {

    var scentId = req.body.id;

    Scent.findById(scentId, function (err, scent) {
        if (err) return next(err);
        scent.isDeleted = true;

        scent.save(function (err) {
            if (err) return next(err);
            else res.json({
                success: true,
                message: "successful deletion"
            });
        });
    });

});
