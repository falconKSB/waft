"use strict";

var router = require('express').Router();

var moment = require('moment-timezone');

var Prices = require('../../../models/prices');
var Order = require('../../../models/order');
var FacebookStat = require('../../../models/facebookstat');

module.exports = router;

/*

db.orders.aggregate([
    { $match: { createdAt: {$gte: ISODate("2017-01-21T00:00:24.752Z")}, payment_status: "succeeded" } },
    { $group: { _id: "$countryCode", amount: { $sum: "$amount" },
                count: { $sum: 1 }, average: { $avg: "$amount" },
                conversions: {$sum: {$cond: [ "$samplesOrderRef", 1, 0 ]}} } }
]);

db.orders.aggregate([
    { $match: { createdAt: {$gte: ISODate("2017-01-21T00:00:24.752Z")}, payment_status: "succeeded" } },
    { $group: { _id: {orderSize: "$orderSize", countryCode: "$countryCode"}, count: { $sum: 1 } } }
]);

db.orders.aggregate([
    { $match: { createdAt: {$gte: ISODate("2017-01-21T00:00:24.752Z")}, payment_status: "succeeded" } },
    { $group: { _id: {gender: "$gender", countryCode: "$countryCode"}, count: { $sum: 1 } } }
]);

*/

function convertToSgd(countryCode, amount) {
    let price = Prices.getPrice(countryCode);
    let sgPrice = Prices.getPrice('sg');
    return amount * price.exchangeRate / sgPrice.exchangeRate;
}

function createCondition(country, dateStart, dateEnd, addCondition) {
    let cond = { payment_status: "succeeded", status: {$ne: "Refunded"} };
    if (country) cond.countryCode = {
        $in: [country.toLowerCase(), country.toUpperCase()]
    };
    if (dateStart || dateEnd) {
        cond.createdAt = {};
        if (dateStart) cond.createdAt.$gte = dateStart.toDate();
        if (dateEnd) cond.createdAt.$lte = dateEnd.toDate();
    }
    if (addCondition) {
        Object.assign(cond, addCondition);
    }
    
    return cond;
}

function totalStats(country, dateStart, dateEnd, addCondition) {
    return Order.aggregate([
        {
            $match: createCondition(country, dateStart, dateEnd, addCondition)
        },
        {
            $group: {
                _id: {$toLower: "$countryCode"},
                amount: { $sum: "$amount" },
                amountMain: { $sum: "$amountMain" },
                count: { $sum: 1 }
            }
        }
    ]).then(docs => docs.reduce((doc1, doc2) => {
        doc1.amount += convertToSgd(doc2._id, doc2.amount);
        doc1.amountMain += convertToSgd(doc2._id, doc2.amountMain);        
        doc1.count += doc2.count;
        return doc1;
    }, {
        amount: 0,
        amountMain: 0,
        count: 0
    })).then(doc => {
        doc.average = doc.count > 0 ? doc.amount / doc.count : 0;
        return doc;
    });
}

function statsBySize(country, dateStart, dateEnd, addCondition) {
    return Order.aggregate([
        {
            $match: createCondition(country, dateStart, dateEnd, addCondition)
        },
        {
            $group: {
                _id: "$orderSize",
                count: { $sum: 1 }
            }
        }
    ]).then(docs => docs.reduce((doc1, doc2) => {
        doc1[doc2._id] = doc2.count;
        return doc1;
    }, {}));    
}

function statsByGender(country, dateStart, dateEnd, addCondition) {
    return Order.aggregate([
        {
            $match: createCondition(country, dateStart, dateEnd, addCondition)
        },
        {
            $group: {
                _id: "$gender",
                count: { $sum: 1 }
            }
        }
    ]).then(docs => docs.reduce((doc1, doc2) => {
        doc1[doc2._id] = doc2.count;
        return doc1;
    }, {}));    
}

function statsBySamplesStatus(country, dateStart, dateEnd, addCondition) {
    return Order.aggregate([
        {
            $match: createCondition(country, dateStart, dateEnd, addCondition)
        },
        {
            $group: {
                _id: "$samplesStatus",
                count: { $sum: 1 }
            }
        }
    ]).then(docs => docs.reduce((doc1, doc2) => {
        doc1[doc2._id] = doc2.count;
        return doc1;
    }, {}));    
}

function samples(country, dateStart, dateEnd, addCondition) {
    let cond = createCondition(country, dateStart, dateEnd, addCondition);
    cond.orderSize = "samples";
    
    return Order.aggregate([
        {
            $match: cond
        },
        {
            $group: {
                _id: null,
                count: { $sum: 1 },
            }
        }
    ]).then(doc => {
        return doc[0] ? doc[0].count : 0;
    });

}

function convertedSamples(country, dateStart, dateEnd) {
    let cond = createCondition(country, dateStart, dateEnd);
    cond.samplesStatus = "Completed";
    cond.mainOrderRef = {$ne: null};
    
    return Order.aggregate([
        {
            $match: cond
        },
        {
            $lookup: {
                from: "orders",
                localField: "mainOrderRef",
                foreignField: "_id",
                as: "mainOrder"
            }
        },
        {
            $match: {
                "mainOrder.status": {$ne: "Refunded"},
                "mainOrder.payment_status": "succeeded"
            }
        },
        {
            $group: {
                _id: null,
                count: { $sum: 1 },
            }
        }
    ]).then(doc => {
        return doc[0] ? doc[0].count : 0;
    });
}

function conversions(country, dateStart, dateEnd) {
    return Order.aggregate([
        {
            $match: createCondition(country, dateStart, dateEnd)
        },
        {
            $lookup: {
                from: "orders",
                localField: "mainOrderRef",
                foreignField: "_id",
                as: "mainOrder"
            }
        },
        {
            $unwind: {
                path: "$mainOrder"
            }
        },
        {
            $match: {
                "mainOrder.status": {$ne: "Refunded"},
                "mainOrder.payment_status": "succeeded"
            }
        },
        {
            $group: {
                _id: {$toLower: "$countryCode"},
                conversions: {
                    $sum: 1
                },
                conversionsValue: {
                    $sum: "$mainOrder.amount"
                },
                conversionsWeek1: {
                    $sum: {
                        $cond: [
                            {
                                $and: [
                                    {$lte: [{$subtract: [ "$mainOrder.createdAt", "$createdAt"]}, 1*7*24*60*60*1000]}
                                ]
                            }, 1, 0]
                    }
                },
                conversionsWeek2: {
                    $sum: {
                        $cond: [
                            {
                                $and: [
                                    {$gt: [{$subtract: [ "$mainOrder.createdAt", "$createdAt"]}, 1*7*24*60*60*1000]},
                                    {$lte: [{$subtract: [ "$mainOrder.createdAt", "$createdAt"]}, 2*7*24*60*60*1000]}
                                ]
                            }, 1, 0]
                    }
                },
                conversionsWeek3: {
                    $sum: {
                        $cond: [
                            {$and: [
                                {$gt: [{$subtract: [ "$mainOrder.createdAt", "$createdAt"]}, 2*7*24*60*60*1000]},
                                {$lte: [{$subtract: [ "$mainOrder.createdAt", "$createdAt"]}, 3*7*24*60*60*1000]}
                            ]}, 1, 0]
                    }
                },
                conversionsWeek4: {
                    $sum: {
                        $cond: [
                            {$and: [
                                {$gt: [{$subtract: [ "$mainOrder.createdAt", "$createdAt"]}, 3*7*24*60*60*1000]},
                                {$lte: [{$subtract: [ "$mainOrder.createdAt", "$createdAt"]}, 4*7*24*60*60*1000]}
                            ]}, 1, 0]
                    }
                },
            }}
    ]).then(docs => docs.reduce((doc1, doc2) => {
        doc1.conversions += doc2.conversions || 0;
        doc1.conversionsValue += convertToSgd(doc2._id, doc2.conversionsValue) || 0;
        doc1.conversionsWeek1 += doc2.conversionsWeek1 || 0;
        doc1.conversionsWeek2 += doc2.conversionsWeek2 || 0;
        doc1.conversionsWeek3 += doc2.conversionsWeek3 || 0;
        doc1.conversionsWeek4 += doc2.conversionsWeek4 || 0;
        return doc1;
    }, {
        conversions: 0,
        conversionsValue: 0,
        conversionsWeek1: 0,
        conversionsWeek2: 0,
        conversionsWeek3: 0,
        conversionsWeek4: 0,
    }));
}

function conversionStats(country, todayStart) {
    let beginStart = todayStart.clone().subtract(30, 'days');

    var periods = [];
    for (let i = 0; i <= 30; i++) {
        periods.push({
            label: beginStart.format("DD/MM/YY"),
            startDate: beginStart,
            endDate: beginStart.clone().endOf('day')
        });
        beginStart = beginStart.clone().add(1, 'days');
    }
    return Promise.all(periods.map(period => Promise.all([
        conversions(country, period.startDate, period.endDate),
        statsBySize(country, period.startDate, period.endDate,
                    {samplesOrderRef: {$eq: null}}),
        statsBySamplesStatus(country, period.startDate, period.endDate)
    ]).then(results => {
        Object.assign(period, results[0]);
        period.sampleOrders = results[1].samples || 0;
        period.fullOrders = (results[1].big || 0) + (results[1].medium || 0);
        period.totalNewOrders = period.sampleOrders + period.fullOrders;
        period.pendingSamples = results[2].Pending || 0;
        period.cancelledSamples = results[1].samples - (results[2].Pending || 0) - (results[0].conversions || 0);
        period.completedSamples = results[2].Completed;
    }))).then(() => periods);
}

function facebookStats(country, dateStart, dateEnd) {
    let cond = {};
    if (country) cond.country = country.toUpperCase();
    if (dateStart || dateEnd) {
        cond.sdate = {};
        if (dateStart) cond.sdate.$gte = dateStart.format('YYYY-MM-DD');
        if (dateEnd) cond.sdate.$lte = dateEnd.format('YYYY-MM-DD');
    }
    return FacebookStat.aggregate([
        {
            $match: cond
        },
        {
            $group: {
                _id: null,
                cost: { $sum: "$cost" }
            }
        }
    ]).then(docs => {
        return {
            cost: docs.length > 0 ? docs[0].cost : 0
        };
    });
}

router.get("/", function (req, res, next) {
    let country = req.query.country;

    let today = moment().tz("Asia/Singapore");
    let todayStart = today.clone().startOf('day');
    
    let yesterday = today.clone().subtract(1, 'days');
    let yesterdayStart = yesterday.clone().startOf('day');
    let yesterdayEnd = yesterday.clone().endOf('day');

    let weekAgo = today.clone().subtract(7, 'days');
    let weekAgoStart = weekAgo.clone().startOf('day');
    let weekAgoEnd = weekAgo.clone().endOf('day');

    let threeWeekAgo = today.clone().subtract(21, 'days');
    let fourWeekAgo = today.clone().subtract(22, 'days');
    let threeWeekAgoStart = threeWeekAgo.clone().startOf('day');
    let threeWeekAgoEnd = threeWeekAgo.clone().endOf('day');

    let monthStart = today.clone().startOf('month');
    let yearStart = today.clone().startOf('year');

    var periods = [
        { name: "Today", startDate: todayStart, endDate: null },
        { name: "Yesterday, Same Time", startDate: yesterdayStart, endDate: yesterday },
        { name: "Yesterday, Total", startDate: yesterdayStart, endDate: yesterdayEnd },
        { name: "Week Ago, Same Time", startDate: weekAgoStart, endDate: weekAgo },
        { name: "Week Ago, Total", startDate: weekAgoStart, endDate: weekAgoEnd },
        { name: "Year to Date", startDate: yearStart, endDate: null },
        { name: "Month to Date", startDate: monthStart, endDate: null }
    ];

    var data = {};
    Promise.all([
        samples(country, fourWeekAgo, threeWeekAgo),
        convertedSamples(country, fourWeekAgo, threeWeekAgo)
    ]).then(conversionResults => {
        data.conversionRate = conversionResults[0] ? conversionResults[1] / conversionResults[0] : 0;
        return data;
    }).then(() => Promise.all(periods.map(period => {
        return Promise.all([
            totalStats(country, period.startDate, period.endDate, {samplesOrderRef: null, isFirstOrder: true}),
            statsBySize(country, period.startDate, period.endDate, {samplesOrderRef: null, isFirstOrder: true}),
            statsByGender(country, period.startDate, period.endDate, {samplesOrderRef: null, isFirstOrder: true}),
            totalStats(country, period.startDate, period.endDate, {samplesOrderRef: null, isFirstOrder: {$ne: true}}),
            statsBySize(country, period.startDate, period.endDate, {samplesOrderRef: null, isFirstOrder: {$ne: true}}),
            statsByGender(country, period.startDate, period.endDate, {samplesOrderRef: null, isFirstOrder: {$ne: true}}),
            totalStats(country, period.startDate, period.endDate, {samplesOrderRef: {$ne: null}}),
            statsBySize(country, period.startDate, period.endDate, {samplesOrderRef: {$ne: null}}),
            statsByGender(country, period.startDate, period.endDate, {samplesOrderRef: {$ne: null}}),
            totalStats(country, period.startDate, period.endDate),
            facebookStats(country, period.startDate, period.endDate)
        ]).then(results => {
            period.totalStatsNew = results[0];
            period.statsBySizeNew = results[1];
            period.statsByGenderNew = results[2];
            period.totalStatsRepeat = results[3];
            period.statsBySizeRepeat = results[4];
            period.statsByGenderRepeat = results[5];
            period.totalStatsConv = results[6];
            period.statsBySizeConv = results[7];
            period.statsByGenderConv = results[8];
            period.totalStats = results[9];
            period.facebookStats = results[10];
            return period;
        });
    }))).then(results => {
        data.periods = periods;
        res.json(data);
    }).catch(next);        
});

router.get("/conversions", function (req, res, next) {
    let country = req.query.country;

    let today = moment().tz("Asia/Singapore");
    let todayStart = today.clone().startOf('day');
    
    conversionStats(country, todayStart).then(result => {
        res.json({conversions: result});
    }).catch(next);        
});

