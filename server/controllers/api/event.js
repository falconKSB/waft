"use strict";

const router = require('express').Router();
const Logger = require('../../lib/logging');

module.exports = router;

router.get("/", function (req, res, next) {
    res.json({success: true});
});

router.post("/", function (req, res, next) {
    Logger.debugReq('Tracking Event', req);
    res.json({success: true});
});
