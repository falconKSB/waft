const router = require('express').Router();

router.use('/password', require('./password'));
router.use('/token', require('./token'));
router.use('/login', require('./login'));
router.use('/register', require('./register'));

module.exports = router;