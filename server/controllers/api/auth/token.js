var express = require('express');

var User = require('../../../models/user');
var authenticationRouter = express.Router();

module.exports = authenticationRouter;

authenticationRouter.post('/', function (req, res) {
    User.findOne({ refreshToken: req.body.refreshToken })
        .then(function (user) {
            if (!user)
                throw { status: 401, message: 'Invalid token'};
            res.json({success: true, data: user.getCurrentUser()});
        })
        .catch(function (err) {
            return next(err);
        })
});

