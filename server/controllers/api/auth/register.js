var express = require('express');

var User = require('../../../models/user');
var activecampaign = require("../../../lib/activecampaign");

var router = express.Router();
module.exports = router;

router.post('/', function (req, res, next) {
    req.checkBody('email').notEmpty().isEmail();
    req.sanitizeBody('email').trim();
    var errors = req.validationErrors();
    if (errors) {
        if (errors[0].param == 'email')
            return next({ status: 200, message: 'Invalid e-mail address'});
        return next(errors);
    }
    var email = req.body.email.toLowerCase();

    var firstName = req.body.firstName;
    var lastName = req.body.lastName;
    var password = req.body.password;

    checkEmailAvailable(email)
        .then(function () {
            var user = new User({
                email: email,
                password: password,
                firstName: firstName,
                lastName: lastName
            });
            return user.save();
        })
        .then(function(user) {
            activecampaign.onRegistration({
                email: email,
                ip4: req.clientIp,
                firstName: firstName,
                lastName: lastName
            });
            res.json({
                success: true,
                message: 'Successfully created new user',
                data: user.getCurrentUser()
            });
        })
        .catch(function (err) {
            return next(err);
        });
});

router.get('/check', function (req, res, next) {

    var email = req.query.email.toLowerCase();

    checkEmailAvailable(email)
        .then(function (_user) {
            res.json({
                success: true,
                message: 'Available'
            });
        })
        .catch(function (err) {
            return next(err);
        });
});

function checkEmailAvailable(email) {
    return User.findOne({ email: email })
        .then(function (_user) {
            if (_user) {
                throw { status: 200, message: "The email address is already in use" };
            }
            else return Promise.resolve(true);
        })
}
