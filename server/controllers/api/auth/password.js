var express = require('express');
var crypto = require("crypto");
const generatePassword = require('password-generator'); //generate guid for restore password

var config = require('../../../config/config');

var User = require('../../../models/user');
var mailer = require('../../../lib/mailer/mailer');
var logger = require('../../../lib/logging');
var authenticationRouter = express.Router();

module.exports = authenticationRouter;

authenticationRouter.post('/restore', function (req, res, next) {
    req.checkBody('email').notEmpty().isEmail();
    let errors = req.validationErrors();
    if (errors) {
        if (errors[0].param == 'email')
            return next({ status: 200, message: 'Invalid E-mail Address. Please try again' });
        return next(errors);
    }

    let email = req.body.email.toLowerCase();

    User.findOne({ email: email })
        .then(function (user) {
            if (!user) {
                throw {status: 200, success: false, message: 'Invalid E-mail Address. Please try again'};
            }
            user.passwordResetToken = {
                token: generatePassword(15, false),
                createdAt: new Date(),
                expiredTime: '1h'
            };
            return user.save();
        })
        .then(function (user) {
            let url = config.homepath + "/password-recovery/" + user.passwordResetToken.token;
            let locals = {
                email: email,
                subject: 'Waft Password Reset',
                url: url,
                user: user.firstName?user.firstName:'user'
            };
            mailer.sendMail('passwordRecovery', locals)
                .catch(err => {
                    logger.error('Error sending password reset', err);
                });
            res.send({ success: true, message: 'Please check your e-mail for password reset instructions' });
        })
        .catch(function (err) {
            return next(err);
        })
});

authenticationRouter.post('/change', function (req, res, next) {
    let token = req.body.token;
    let userId = req.body.userId;
    let firstPassword = req.body.firstPassword;
    let secondPassword = req.body.secondPassword;

    User.findById(userId)
        .then(function (user) {
            if (!user)
                throw { status: 403, message: 'Invalid change password link' };

            if (!user.passwordResetToken.token) {
                throw { status: 403, message: 'Invalid change password link' };
            }

            let currentDate = new Date();
            let createdAt = new Date(user.passwordResetToken.createdAt);
            let hours = user.passwordResetToken.expiredTime.match(/\d/g);
            let expiredDate = new Date(createdAt.getTime() + 3600000 * hours);

            if (token != user.passwordResetToken.token) {
                throw { status: 403, message: 'Invalid change password link' };
            }

            if (currentDate > expiredDate) {
                throw { status: 403, message: 'Invalid change password link' };
            }

            user.password = firstPassword;
            user.passwordResetToken = {};

            return user.save();
        })
        .then(function (user) {
            if (!user)
                throw { status: 403, message: 'Invalid change password link' };
            res.send({ success: true, message: 'Password changed successfully' });
        })
        .catch(function (err) {
            return next(err);
        })

});
