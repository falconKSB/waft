var express = require('express');
var User = require('../../../models/user');
var authenticationRouter = express.Router();

module.exports = authenticationRouter;

authenticationRouter.post('/', function (req, res, next) {
    req.checkBody('email').notEmpty().isEmail();
    let errors = req.validationErrors();
    if (errors) {
        if (errors[0].param == 'email')
            return next({ status: 200, message: 'Invalid E-mail Address' });
        return next(errors);
    }

    let email = req.body.email.toLowerCase();
    let user;
    User.findOne({ email: email })
        .then(_user => {
            if (!_user) throw {
                status: 200,
                message: 'Invalid email or password. Try again',
                details: 'User not found'
            };
            user = _user;
            return user.comparePassword(req.body.password)
        })
        .then(isMatch => {
            if (!isMatch) throw {
                status: 200,
                message: 'Invalid email or password. Try again',
                details: 'Password is not matching'
            };
            return res.json({ success: true, data: user.getCurrentUser() });
        })
        .catch(next)
});
