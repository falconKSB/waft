"use strict";

var express = require("express");
var router = express.Router();
var geoip = require('../../lib/geoip');
var Prices = require('../../models/prices');

router.get('/', function (req, res, next) {
    const data = {};
    if (req.query.country_code) {
        data.countryCode = req.query.country_code;
        data.price = Prices.getPrice(req.query.country_code);
    }
    else {
        const geo = geoip.lookup(geoip.getIP(req));
        if (geo.countryCode == null) {
            geo.countryCode = "us";
        }
        data.countryCode = geo.countryCode;
        data.price = Prices.getPrice(geo.countryCode);
    }
    res.json({ success: true, data: data });
});


module.exports = router;
