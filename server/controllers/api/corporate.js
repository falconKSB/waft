"use strict";

var Router = require("express").Router();
var Mailer = require('../../lib/mailer/mailer');
var GeoIP = require('../../lib/geoip');
var activecampaign = require('../../lib/activecampaign');

module.exports = Router;

Router.post("/contact", function (req, res, next) {
    let ip = GeoIP.getIP(req);
    let geo = GeoIP.lookup(ip);

    const locals = {
        email: 'bizdev@waft.com',
        subject: 'Corporate Website Contact Request',
        ip: ip,
        country: geo.countryCode,
        city: geo.city,
        contact_name: req.body.name,
        contact_email: req.body.email,
        contact_company_name: req.body.company_name,
        contact_phone_number: req.body.phone_number
    };

    Mailer.sendMail('corporateContact', locals)
        .then(response => {
            res.json({success: true});
        })
        .catch(next);
    activecampaign.onCorporateForm({
        email: req.body.email,
        firstName: req.body.name,
        ip4: ip
    });
});
