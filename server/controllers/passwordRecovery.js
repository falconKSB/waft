"use strict";

var express = require("express");
var config = require("../config/config");
var resetPasswordRouter = express.Router();
var path = require("path");
var User = require("../models/user");

module.exports = resetPasswordRouter;

resetPasswordRouter.get("/:token", function (req, res, next) {
    const token = req.params.token;
    User.findOne({ 'passwordResetToken.token': token })
        .then(function (user) {
            if (!user) {
                let err = new Error('Invalid password recovery link. Try to recover your password again via Forgot Password function on the login screen.');
                err.status = 404;
                throw err;
            }
            let currentDate = new Date();
            let createdAt = new Date(user.passwordResetToken.createdAt);
            let hours = user.passwordResetToken.expiredTime.match(/\d/g);
            let expiredDate = new Date(createdAt.getTime() + 3600000 * hours);

            if (currentDate < expiredDate) {
                res.render('resetPassword', { token: token, userId: user._id });
            }
        })
        .catch(next)
});