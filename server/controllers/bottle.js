const router = require("express").Router();
const tmp = require('tmp');
const fs = require('fs');
const phantomjs = require('phantomjs-prebuilt');
const journeyModel = require('../models/journey');

// /bottle/<journey>/<hash>/<template name>.<image_type>

router.get("/saved/:templateName.:imageType", function (req, res, next) {
  let cookie = req.cookies['_p'];
  if (!cookie)
    return serveDefaultImage(req, res);
  journeyModel.find({ cookie, 'customBottle.name' : {'$exists' : true, '$ne' : ''} })
    .sort({ createdAt: -1 })
    .limit(1)
    .then(journeys => {
      if (!journeys || journeys.length === 0)
        return serveDefaultImage(req, res);
      let id = journeys[0]._id.toString();
      let hash = journeys[0].getHash();
      res.redirect(`/bottle/${id}/${hash}/${req.params.templateName}.${req.params.imageType}`);
    })
    .catch(next);
});

router.get("/:journeyId/:templateName.:imageType", function (req, res, next) {
  let journeyId = req.params.journeyId;
  journeyModel.findById(journeyId)
    .then(journey => {
      if (!journey)
        return serveDefaultImage(req, res);
      res.redirect(`/bottle/${journeyId}/${journey.getHash()}/${req.params.templateName}.${req.params.imageType}`);
    })
    .catch(next);
});

router.get("/:journeyId/:hash/:templateName.:imageType", function (req, res, next) {
  let journeyId = req.params.journeyId;
  journeyModel.findById(journeyId)
    .then(journey => {
      if ((!journey) || (hasSensitiveWords(journey.customBottle)))
        return generateDefaultImage(req.params.templateName, req.params.imageType);
      return generateImage(`${journeyId}-${req.params.hash}`, req.params.templateName, req.params.imageType, journey.customBottle);
    })
    .then(sendFile(res))
    .catch(next);
});

const badWords = new RegExp(/fuck|pussy|cunt|shit|arse|bitch|bastard|bollocks|crap|damn|waft|nigger|nigga|whore|twat|piss/, 'gi');

function hasSensitiveWords(customBottle) {
  return badWords.test(customBottle.name) || badWords.test(customBottle.message);
}

function sendFile(res) {
  return function (filename) {
    res.sendFile(filename, {maxAge: '10d'}, (err) => {
      if (err) throw err;
    });
  }
}

function generateImage(namesuffix, templateName, imageType, customBottle) {
  return new Promise((resolve, reject) => {
    let filename = `/tmp/img-${namesuffix}-${templateName}.${imageType}`;
    if (fs.existsSync(filename))
      return resolve(filename);
    let program = phantomjs.exec(
      'phantomjs/render.js',
      templateName,
      imageType,
      filename,
      customBottle.style,
      customBottle.name,
      customBottle.message,
      customBottle.initials
    );
    program.stdout.pipe(process.stdout);
    program.stderr.pipe(process.stderr);
    program.on('exit', code => {
      if (code !== 0) return reject(new Error("Cannot generate image"));
      else return resolve(filename);
    });
  });
}

function generateDefaultImage(templateName, imageType) {
  return generateImage('default', templateName, imageType, {
    style: 'modern',
    name: 'DESTINY',
    message: '',
    initials: ''
  })
}

function serveDefaultImage(req, res) {
  generateDefaultImage(req.params.templateName, req.params.imageType)
    .then(sendFile(res))
}

module.exports = router;
