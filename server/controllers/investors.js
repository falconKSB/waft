"use strict";
const express = require('express');
var Router = require("express").Router();
var Mailer = require('../lib/mailer/mailer');
var GeoIP = require('../lib/geoip');
const logger = require('../lib/logging');

module.exports = Router;

Router.use("/hyieKvfE4ObRZmngwQKphZ2r", express.static('pdf'));

Router.get("/", function (req, res) {
    res.render("investors");
});

function sendError(req, res, errors) {
    let ctx = {
        errors: errors
    };
    if (req.body.name) ctx.name = req.body.name;
    if (req.body.email) ctx.email = req.body.email;
    
    res.render("investors", ctx);    
}

function sendMail(data) {
}

Router.post("/", function(req, res, next) {
    req.sanitizeBody('email').trim();
    req.sanitizeBody('name').trim();
    req.sanitizeBody('password').trim();
    req.checkBody('email', 'Invalid email').isEmail();
    req.checkBody('name', 'Name is required').notEmpty();
    req.checkBody('password', 'Invalid password').matches(/cedarwood/i);

    let errors = req.validationErrors(true);
    if (errors) {
        return sendError(req, res, errors);
    }
    
    let name = req.body.name;
    let email = req.body.email;
    let password = req.body.password;

    // send email
    let ip = GeoIP.getIP(req);
    let geo = GeoIP.lookup(ip);
    const emailCtx = {
        email: 'kent@waft.com, sam@waft.com, anton@applicious.today',
        subject: 'Investor Presentation Download',
        ip: ip,
        country: geo.countryCode ? geo.countryCode.toUpperCase() : '',
        city: geo.city,
        user_name: name,
        user_email: email
    };
    
    Mailer.sendMail('investorDownload', emailCtx)
        .then(response => {
            res.redirect("/investors/hyieKvfE4ObRZmngwQKphZ2r/Presentation.pdf");
        })
        .catch(err => {
            logger.error("Error sending email on investors download", err);
            res.redirect("/investors/hyieKvfE4ObRZmngwQKphZ2r/Presentation.pdf");
        });
});
