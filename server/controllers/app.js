"use strict";

var express = require("express");
var config = require("../config/config");
var appRouter = express.Router();
var path = require("path");

module.exports = appRouter;

appRouter.get("/*", function (req, res) {
    res.sendFile(path.join(__dirname, '../../public/assets/gen/app.html'));
});