"use strict";

var Router = require("express").Router();
var geoip = require('../lib/geoip');
var Prices = require('../models/prices');
var URL = require('url');
const isCallerMobile = require('../lib/mobileCheck');
const pageCache = require('../lib/pageCache');

module.exports = Router;

function redirectWithQuery(req, res, path) {
    let query = URL.parse(req.url, false).query;
    if (query && (query != '')) {
        path += '?' + query;
    }
    res.redirect(path)
}

Router.get("/gift1", pageCache(3600), function (req, res) {
  let geo = geoip.lookup(geoip.getIP(req));
  let price = geo.countryCode ? Prices.getPrice(geo.countryCode) : Prices.getDefaultPrice();
  let priceGift = Prices.format(price,'small');
  let priceLab = Prices.format(price,'small');
  res.render('giftLanding1', { layout: false, priceGift: priceGift, priceLab : priceLab })
});

Router.get(["/gift","/gift2"], pageCache(3600), function (req, res) {
    res.render('giftLanding2', { layout: false })
});

Router.get("/ingredients", pageCache(3600), function (req, res) {
    res.render('landing7', { layout: false })
});

Router.get("/gift-valentines-day", pageCache(3600), function (req, res) {
    res.render('giftLandingValentines', { layout: false })
});

Router.get("/amp", pageCache(3600), function (req, res) {
    res.render('amp', { layout: false })
});

Router.get("/profile", function(req, res) {
    redirectWithQuery(req, res, '/app/profile');
});

Router.get("/landing", function(req, res) {
    if (isCallerMobile(req))
        redirectWithQuery(req, res, '/app/wizard/landing');
    else
        redirectWithQuery(req, res, '/');
});

Router.get("/landing1", pageCache(3600), function (req, res) {
    let geo = geoip.lookup(geoip.getIP(req));
    let price = geo.countryCode ? Prices.getPrice(geo.countryCode) : Prices.getDefaultPrice();
    let priceGift = Prices.format(price,'small');
    let priceLab = Prices.format(price,'small');
    res.render('landing1', { layout: false, priceGift: priceGift, priceLab : priceLab })
});

Router.get("/landing2", pageCache(3600), function (req, res) {
    res.render('landing2', { layout: false })
});

Router.get("/landing3", pageCache(3600), function (req, res) {
    let geo = geoip.lookup(geoip.getIP(req));
    let price = geo.countryCode ? Prices.getPrice(geo.countryCode) : Prices.getDefaultPrice();
    let priceGift = Prices.format(price,'small');
    let priceLab = Prices.format(price,'small');
    res.render('landing3', { layout: false, priceGift: priceGift, priceLab : priceLab })
});

Router.get("/landing4", pageCache(3600), function (req, res) {
    res.render('landing4', { layout: false })
});

Router.get(["/","/landing5", "/say-it-with-fragrance"], pageCache(3600), function (req, res) {
    if (isCallerMobile(req))
        redirectWithQuery(req, res, '/app/wizard/landing');
    else
        res.render('landing5', { layout: false });
});

Router.get(["/landing6"], pageCache(3600), function (req, res) {
    res.render('landing6', { layout: false });
});

Router.get(["/landing8"], pageCache(3600), function (req, res) {
    if (isCallerMobile(req))
        redirectWithQuery(req, res, '/app/wizard/landing');
    else
        res.render('landing8', { layout: false })
});

Router.get("/corporate", pageCache(3600), function(req, res) {
    res.render('corporateLanding', { layout: false })
});

Router.get("/biz", function (req, res) {
    redirectWithQuery(req, res, '/corporate');
});

Router.get("/health", function(req, res) {
    res.status(200).send('200 OK');
});

Router.get("/about", pageCache(3600), function (req, res) {
    res.render('about')
});

Router.get("/privacy", pageCache(3600), function (req, res) {
    res.render('privacy')
});

Router.get("/support", function (req, res) {
    redirectWithQuery(req, res, 'https://support.waft.com/');
});

Router.get("/shipping", function (req, res) {
    redirectWithQuery(req, res, 'https://support.waft.com/hc/en-us/articles/235465768');
});

Router.get("/faq", function (req, res) {
    redirectWithQuery(req, res, 'https://support.waft.com/hc/en-us/categories/204541687');
});

Router.get("/terms", pageCache(3600), function (req, res) {
    res.render('terms')
});

Router.get("/returns", function (req, res) {
    redirectWithQuery(req, res, 'https://support.waft.com/hc/en-us/articles/235465748');
});

Router.get(['/referral', '/referral-program'], pageCache(3600), function (req, res) {
    res.render('referral', { layout: false })
});

Router.get("/careers", pageCache(3600), function (req, res) {
    res.render('careers')
});

Router.get("/quality", pageCache(3600), function (req, res) {
    res.render('quality')
});

Router.get("/confirmwof", pageCache(3600), function (req, res) {
    res.render('confirmWof')
});

Router.get("/error/404", pageCache(3600), function (req, res) {
    res.status(404);
    res.render('error/404')
});

Router.get("/error/5xx", pageCache(3600), function (req, res) {
    res.status(500);
    res.render('error/5xx')
});

Router.get("/optout", pageCache(3600), function (req, res) {
    res.sendFile(path.join(__dirname, '../../public/optout.html'));
});

Router.get("/googleba993b694e4f9f5d.html", pageCache(3600), function (req, res) {
    res.send('google-site-verification: googleba993b694e4f9f5d.html')
});
