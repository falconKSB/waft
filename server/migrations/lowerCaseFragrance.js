var Fragrance = require('../models/fragrance');

function err(e) {
    console.log(e);
    System.exit(1);
}

var stream = Fragrance.find().stream();

stream.on('data', function (f) {
    f.name_lower = f.name.toLowerCase();
    f.save();
}).on('error', err)
    .on('close', function () {
        console.log('Done!')
});

// db.fragrances.getIndexes()
// db.fragrances.dropIndex('name_text');
// db.fragrances.createIndex( { name_lower: "text" })

