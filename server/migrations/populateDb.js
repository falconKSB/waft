var fs = require("fs");
var async = require('async');

var Fragrance = require('../models/fragrance');
var Ingredient = require('../models/ingredient');

async.waterfall([
    function (callback) {

        fs.readFile('server/migrations/perfumes.json', function read(err, data) {

            if (err) return callback(err);

            var content = JSON.parse(data);

            async.every(content, function (fragrance, callback) {

                if (fragrance.accords !== null && Object.keys(fragrance.accords).length == 6) {

                    var accords = [];
                    for (key in fragrance.accords) {
                        var newAccord = {
                            name: key,
                            weight: fragrance.accords[key]
                        }
                        accords.push(newAccord);
                    }
                    var newFragrance = new Fragrance({
                        name: fragrance.name || "",
                        description: fragrance.description || "",
                        family: fragrance.family || "",
                        female: fragrance.female || "",
                        main_accord: fragrance.main_accord || "",
                        male: fragrance.male || "",
                        accords: accords
                    });
                    newFragrance.save(function (err, fragrance) {
                        if (!err) return callback(null, true);
                    });
                }
                else {
                    return callback(null, true);
                }

            }, function (err, result) {
                return callback(null, true);
            });

        });
    },
    function (fragrancesResult, callback) {

        fs.readFile('server/migrations/ingredients.json', function read(err, data) {
            if (err) {
                throw err;
            }
            var content = JSON.parse(data);

            async.every(content.ingredients, function (ingredient, callback) {
                var file = ingredient.replace(" ","_");
                var newIngredient = new Ingredient({
                    name: ingredient,
                    imageUrl: "/assets/img/ingredients/"+file+".jpg",
                });

                newIngredient.save(function (err, ingredient) {
                    if (ingredient) return callback(null, true);
                });
            });

        });
    }
], function (err, results) {

    if (err) {
        console.log(err);
        process.exit();
    }
    process.exit();
    console.log('Success!');

});

