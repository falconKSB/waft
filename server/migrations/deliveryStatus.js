var Order = require('../models/order');
var async = require('async');

async.waterfall([
    function (result, callback) {

        Order.find(function (err, orders) {

            async.every(orders, function (order, callback) {  //set base status and currencySymbol to orders. 

                order.status = 'Received';

                order.createdAt = order.createAt;

                switch (order.currency) {
                    case 'aud':
                        order.currencySymbol = 'A$';
                        break;
                    case 'gbp':
                        order.currencySymbol = '£';
                        break;
                    case 'hkd':
                        order.currencySymbol = 'HK$';
                        break;
                    case 'nzd':
                        order.currencySymbol = 'NZ$';
                        break;
                    case 'sgd':
                        order.currencySymbol = 'S$';
                        break;
                    default:
                        order.currencySymbol = '$'; //for orders without currency.
                        break;
                }

                order.save(function (err, order) {
                    return callback(null, true);
                });

            }, function (err, result) {
                if (err) console.log(err);
                return callback(null, true);
            });
        }
        )
    }
], function (err, results) {

    if (err) {
        console.log(err);
        process.exit();
    }
    console.log('Success!');
    process.exit();

});



