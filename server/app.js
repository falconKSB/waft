"use strict";

if(process.env.NODE_ENV === "production") {
  require('newrelic');
}

const fs = require('fs');
const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const bodyparser = require('body-parser');
const cookieParser = require('cookie-parser');
const requestIp = require('request-ip');
const i18n = require('i18n');
const expressValidator = require('express-validator');
const log = require('./lib/logging');
const minifyHTML = require('express-minify-html');
const Geoip = require('./lib/geoip');
const AWSXRay = require('aws-xray-sdk');

AWSXRay.config([AWSXRay.plugins.ElasticBeanstalk]);
AWSXRay.setDefaultName('myDefaultSegment');

var app = express();
exports.app = app;

app.use(AWSXRay.express.openSegment());
app.enable('trust proxy');
app.disable("x-powered-by");
app.use(favicon(path.join(__dirname, "../public", "favicon.ico")));
app.use(require('less-middleware')(path.join(__dirname, '../public')));
app.use(express.static(path.join(__dirname, '../public')));
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));
app.use(expressValidator());
app.use(cookieParser());
app.use(minifyHTML({
    override:      true,
    exception_url: false,
    htmlMinifier: {
        removeComments:            true,
        collapseWhitespace:        true,
        collapseBooleanAttributes: true,
        removeAttributeQuotes:     true,
        removeEmptyAttributes:     true,
        minifyJS:                  true
    }
}));

app.use(requestIp.mw());

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

const hbs = require('hbs');
hbs.registerPartials(__dirname + '/views/partials', () => {
    fs.access(__dirname + '/../dist/partials', (err) => {
        if (!err) hbs.registerPartials(__dirname + '/../dist/partials');
    });
});
require('./lib/hbHelpers')(hbs);

i18n.configure({
  locales:['en', 'en_US'],
    directory: __dirname + '/i18n',
    defaultLocale: 'en',
    indent: "  ",
    objectNotation: true
});
app.use(i18n.init);
app.use(function(req, res, next) {
  const lang = Geoip.getLang(req);
  i18n.setLocale(req, lang);
  next();
});


app.use("/", require('./controllers/index'));

app.use(function (req, res) {
    res.status(404).render('error/404')
});

// production error handler
app.use(function(err, req, res, next) {
    const status = err.status || 500;
    const loglevel = err.loglevel || ((status == 500) ? 'error' : 'warn');
    log.logErrorReq(loglevel, err, req);
    res.status(status);
    if (req.url.startsWith('/api'))
        res.json({
            success: false,
            message: err.message
        });
    else {
        if ((status == 400) || (status == 402)) {
            res.render('error/400', {message: err.message})
        }
        else {
            res.render('error/5xx', {message: err.message})
        }
    }

});

app.use(AWSXRay.express.closeSegment());
