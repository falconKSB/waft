var mongoose = require('../config/mongoose');

// Schema defines how the user data will be stored in MongoDB
var subscriptionSchema = new mongoose.Schema({
  email: {
    type: String,
    lowercase: true
  },
  ip: {
    type: String,
    lowercase: true,
    required: true
  },
  city: {
    type: String,
    lowercase: true
  },
  country: {
    type: String,
    lowercase: true
  }
});

module.exports = mongoose.model('Subscribe', subscriptionSchema);