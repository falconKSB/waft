const mongoose = require('../config/mongoose');

const facebookStatSchema = new mongoose.Schema({
    account: { type: String },
    country: { type: String },
    sdate: { type: String },
    cost: {type: Number}
});

module.exports = mongoose.model('FacebookStat', facebookStatSchema);
