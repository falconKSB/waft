var mongoose = require('../config/mongoose');

// Schema defines how the user data will be stored in MongoDB
var deliverySchema = new mongoose.Schema({

    fullName: {type: String},

    addressLine1: {type: String},

    addressLine2: {type: String},

    city: {type: String},

    country: {type: String},

    county: {type: String},

    zipCode: {type: String},

    phoneNumber: {type: String}

}, {timestamps: true});

deliverySchema.methods.mapping = function (delivery) {

    if (delivery.fullName) this.fullName = delivery.fullName;
    if (delivery.addressLine1) this.addressLine1 = delivery.addressLine1;
    if (delivery.addressLine2) this.addressLine2 = delivery.addressLine2;
    if (delivery.city) this.city = delivery.city;
    if (delivery.country) this.country = delivery.country;
    if (delivery.county) this.county = delivery.county;
    if (delivery.zipCode) this.zipCode = delivery.zipCode;
    if (delivery.phoneNumber) this.phoneNumber = delivery.phoneNumber;
};

module.exports = mongoose.model('Delivery', deliverySchema);
