var crypto = require('crypto');
var mongoose = require('../config/mongoose');
var mongoosePaginate = require('mongoose-paginate');

// Schema defines how the user data will be stored in MongoDB
const journeyModel = new mongoose.Schema({

  step: {type: String},

  email: {type: String}, // in case when user is not signed in yet

  cookie: {type: String}, // in case when user is not signed in yet

  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },

  status: {
    type: String,
    enum: ['Active', 'Completed', 'Deleted'],
    default: 'Active'
  },
  gift: { // if journey made for a gift (rather than yourself)
    type: Boolean,
    default: false
  },
  order: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Order'
  },
  recipientGender: {
    type: String,
    enum: ['None', 'man', 'woman']
  },
  gender: {
    type: String,
    enum: ['None', 'man', 'woman', 'unisex']
  },
  timeOfDay: {
    type: String,
    enum: ['None', 'day', 'night']
  },
  activity: {
    type: String,
    enum: ['None', 'sport', 'social', 'work', 'dating']
  },
  mood: {
    type: String,
    enum: ['None', 'fresh', 'elegant', 'sexy', 'relaxed']
  },
  personalStyle: {
    type: String,
    enum: ['None', 'classic', 'trendy']
  },
  waft: {
    type: String,
    enum: ['None', 'subtle', 'balanced', 'daring']
  },

  ingredients: [String],

  customBottle: {
    initials: {
      type: String
    },
    name: {
      type: String
    },
    message: {
      type: String
    },
    style: {
      type: String
    }
  },

  primaryFragrance: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Fragrance'
  },

  baseFragrances: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Fragrance'
  }],

  scents: {
    main: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Scent'
    },
    add1: {
      addType: {
        type: String,
        enum: ['Intensify', 'Freshen', 'Dare']
      },
      scent: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Scent'
      }
    },
    add2: {
      addType: {
        type: String,
        enum: ['Intensify', 'Freshen', 'Dare']
      },
      scent: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Scent'
      }
    }
  }
}, {timestamps: true});

journeyModel.methods.mapping = function (journey) {

  this.step = journey.step || '';
  this.gender = journey.gender || 'None';
  this.timeOfDay = journey.timeOfDay || 'None';
  this.activity = journey.activity || 'None';
  this.mood = journey.mood || 'None';
  this.personalStyle = journey.personalStyle || 'None';
  this.waft = journey.waft || 'None';
  this.ingredients = journey.ingredients || [];
  this.baseFragrances = journey.baseFragrances || [];
  this.email = journey.email || '';
  this.recipientGender = journey.recipientGender || 'None';
  if (journey.gift) this.gift = journey.gift;
  if (journey.user) {
    if (journey.user.hasOwnProperty('_id'))
      this.user = journey.user._id;
    else
      this.user = journey.user;
  }
  if (journey.primaryFragrance) this.primaryFragrance = journey.primaryFragrance;
  this.customBottle = {
    initials: journey.customBottle.initials || '',
    name: journey.customBottle.name || '',
    message: journey.customBottle.message || '',
    style: journey.customBottle.style || ''
  };
};

journeyModel.methods.getParametersArray = function () {
  let params = [];
  params.push(this.timeOfDay);
  params.push(this.activity);
  params.push(this.mood);
  params.push(this.personalStyle);
  params.push(this.waft);
  return params;
};

const INGREDIENTS_MAPPING = {
  "black pepper": "spicy",
  "cedarwood": "woody",
  "chocolate": "gourmand",
  "encens": "spicy",
  "fig": "fruity",
  "frangipani": "white floral",
  "green tea": "green tea",
  "jasmin": "white floral",
  "lavender": "lavender",
  "lily": "white floral",
  "mint": "aromatic",
  "muguet": "white floral",
  "ocean": "aquatic",
  "orange blossom": "white floral",
  "orris": "powdery",
  "peach": "fruity",
  "pear": "fruity",
  "pink pepper": "spicy",
  "sandalwood": "woody",
  "sesame": "sesame",
  "strawberry": "fruity",
  "vetiver": "vetiver",
  "violet": "powdery",
  "white musk": "musk",
  "amber": "amber",
  "caramel": "caramel",
  "cinnamon": "warm spicy",
  "citrus": "citrus",
  "leather": "leather",
  "milk": "gourmand",
  "oud": "leather/woody",
  "patchouli": "patchouli",
  "powder": "powder",
  "rose": "rose",
};

journeyModel.methods.getInternalIngredients = function () {
  let ingredients = [];

  for (let i = 0; i < this.ingredients.length; i++) {
    ingredients.push(INGREDIENTS_MAPPING[this.ingredients[i]])
  }
  return ingredients;
};

journeyModel.methods.getJourneyString = function () {
  return [this.gift, this.recipientGender, this.gender, this.timeOfDay, this.activity, this.mood, this.personalStyle, this.waft,
    this.ingredients ? this.ingredients.join(', ') : ''].join(' / ')
};

function getGenderFromFragrance(fragrance) {
  if (!fragrance) return null;
  if (typeof fragrance.getGender !== 'function') return null;
  let g = fragrance.getGender();
  if (!g) return null;
  if (g.length != 1) return null;
  return g;
}

journeyModel.methods.isUnisex = function () {
  return this.gender == 'unisex';
};

journeyModel.methods.getGender = function () {
  if ((this.gender == 'man') || (this.gender == 'woman')) {
    return [this.gender];
  }
  let g = getGenderFromFragrance(this.primaryFragrance);
  if (g) return g;
  if (this.baseFragrances) {
    for (let i = 0; i < this.baseFragrances.length; i++) {
      g = getGenderFromFragrance(this.baseFragrances[i]);
      if (g) return g;
    }
  }
  return ['woman', 'man'];
};

journeyModel.methods.getHash = function () {
  return crypto.createHash("md5")
    .update(this.customBottle.style)
    .update(this.customBottle.name)
    .update(this.customBottle.message)
    .update(this.customBottle.initials)
    .digest("hex");
};

journeyModel.methods.processRecommendation = function () {
  if (this.timeOfDay == 'day') {
    this.scents.add1.addType = 'Intensify';
  } else {
    this.scents.add1.addType = 'Freshen';
  }
  this.scents.add2.addType = 'Dare';
};

journeyModel.methods.changePrimaryScent = function (newPrimary) {
  let mainScent = this.scents.main;
  let add1Scent = this.scents.add1.scent;
  let add2Scent = this.scents.add2.scent;
  if (add1Scent._id == newPrimary) {
    this.scents.main = add1Scent;
    this.scents.add1.scent = mainScent;
  } else if (add2Scent._id == newPrimary) {
    this.scents.main = add2Scent;
    this.scents.add2.scent = mainScent;
  } else if (mainScent._id == newPrimary) {
    return;
  } else
    throw new Error("Unknown scent");
};


journeyModel.methods.changeScents = function (mainScentCode, add1ScentCode, add2ScentCode) {
  if ((mainScentCode == add1ScentCode) || (mainScentCode == add2ScentCode) || (add1ScentCode == add2ScentCode))
    throw new Error("The order can't have repeating scents");
  let mainScent = this.scents.main;
  let add1Scent = this.scents.add1.scent;
  let add2Scent = this.scents.add2.scent;
  let allowedCodes = [mainScent.code, add1Scent.code, add2Scent.code];
  if ((mainScentCode in allowedCodes) && (add1ScentCode in allowedCodes) && (add2ScentCode in allowedCodes)) {
    if (mainScentCode == add1Scent.code)
      this.scents.main = add1Scent;
    if (mainScentCode == add2Scent.code)
      this.scents.main = add2Scent;
    if (add1ScentCode == mainScentCode.code)
      this.scents.add1.scent = mainScentCode;
    if (add1ScentCode == add2Scent.code)
      this.scents.add1.scent = add2Scent;
    if (add2ScentCode == mainScentCode.code)
      this.scents.add2.scent = mainScentCode;
    if (add2ScentCode == add1Scent.code)
      this.scents.add2.scent = add1Scent;
  }
  else throw new Error("Unknown scent");
};


mongoosePaginate.paginate.options = {
  lean: true,
  limit: 10,
  page: 1

};

journeyModel.plugin(mongoosePaginate);

module.exports = mongoose.model('Journey', journeyModel);
