var mongoose = require('../config/mongoose');

// Schema defines how the user data will be stored in MongoDB
var fontSchema = new mongoose.Schema({
    name: {
        type: String
    },
    imageUrl: {
        type: String
    },
    fontUrl:{
        type: String
    }
});

module.exports = mongoose.model('Font', fontSchema);