const mongoose = require('../config/mongoose');
const mongoosePaginate = require('mongoose-paginate');
const log = require("../lib/logging");
const Prices = require('./prices');

const orderSchema = new mongoose.Schema({

  key: {type: String},

  status: {
    type: String,
    enum: ['Processing Failed', 'Created', 'Payment Failed', 'Gift Invitation', 'Completed Trial', 'Received', 'On Hold', 'Processing', 'Processed', 'Shipped', 'Delivered', 'Returned', 'Refunded', 'None']
  },

  payment_status: {type: String},

  payment_id: {type: String},

  payment: {
    account: String,
    customerId: String,
    sourceId: String,
  },

  receipt_number: {type: String},

  type: {
    type: String,
    enum: ['WaftGift', 'WaftLab']
  },

  orderSize: {
    type: String,
    enum: ['big', 'medium', 'small', 'samples', '100ml', '50ml', '15ml', '5ml']
  },

  amount: {type: Number},

  amountMain: {type: Number},

  orderSizeMain: {
    type: String,
    enum: ['big', 'medium']
  },

  countryCode: {type: String},

  currency: {type: String},

  currencySymbol: {type: String},

  promoCode: {type: String},

  promoDiscount: {type: Number},

  promoMemberEmail: {type: String},

  salesRep: {type: String},

  preferredSample: {type: String}, // when in retail kiosk

  shipping: {type: String}, // normal or express, not in use

  optimizelyVariations: [String],

  changelog: {type: String},

  auditlog: [{date: Date, message: String}],

  cancellationReasons: [String],

  delivery: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Delivery'
  },

  journey: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Journey'
  },

  sender: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },

  userInfo: {
    ip: String,
    location: {
      countryCode: String,
      city: String
    },
    ua: String
  },

  gender: {
    type: String,
    enum: ['man', 'woman', 'unisex', 'man_unisex', 'woman_unisex']
  },

  gift: {
    status: {type: String}, // TODO enum?
    senderName: String,
    receiver: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    },
    inviteEmailDate: {
      type: Date
    },
    inviteEmailSentOn: {
      type: Date
    },
    emailFromUser: Boolean,
    note: {type: String}, // gift email text
    recipient: {
      firstName: {type: String},

      lastName: {type: String},

      email: {
        type: String,
        lowercase: true
      }
    }
  },

  customPackaging: {
    firstName: {type: String}
  },

  customBottle: {
    initials: {type: String},
    name: {type: String},
    message: {type: String},
    style: {type: String}
  },

  shippingInfo: {
    shippingCompany: {type: String},
    trackingNumber: {type: String}
  },

  placedOn: {
    type: Date,
    default: Date.now
  },

  sentForProcessingOn: {
    type: Date
  },

  batchNo: {
    type: String
  },

  transmissionNo: {
    type: String
  },

  shippedOn: {
    type: Date
  },

  deliveredOn: {
    type: Date
  },

  samplesStatus: {
    type: String,
    enum: ['Pending', 'Cancelled', 'Completed']
  },

  // Warehouse that fulfills this order
  warehouse: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Warehouse'
  },

  samplesOrderRef: { // used for samples order ref if this is big order with samples
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Order'
  },

  mainOrderRef: { // used for main order ref if this is samples
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Order'
  },

  repeatOrderRef: { // used for original order ref if this is re-order
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Order'
  },

  errorMessage: {type: String},

  onHoldReason: {type: String},

  refundReason: {type: String},

  isFirstOrder: {type: Boolean}

}, {timestamps: true});

orderSchema.methods.getOptimizelyVariationsString = function () {
  return this.optimizelyVariations ? this.optimizelyVariations.join(', ') : '';

};

orderSchema.methods.appendChangelog = function (message) {
  if (!this.auditlog)
    this.auditlog = [];
  this.auditlog.push({
    date: new Date(),
    message: message
  });
};

orderSchema.methods.appendChangelogChange = function (who, field, oldValue, newValue) {
  if (typeof oldValue == "object") {
    oldValue = JSON.stringify(oldValue);
  }
  if (typeof newValue == "object") {
    newValue = JSON.stringify(newValue);
  }
  this.appendChangelog(`Change to ${field} by ${who}: ${newValue} (was: ${oldValue})`);
};

orderSchema.methods.assertCanChangeSamplesOrder = function () {
  if (this.orderSize != 'samples')
    throw new Error("Not a samples order");
};

orderSchema.methods.setSamplesStatus = function (newStatus) {
  this.assertCanChangeSamplesOrder();
  this.samplesStatus = newStatus;
  this.appendChangelog(`Changed samples status to ${newStatus}`);
};

orderSchema.methods.generateMainOrderForSamples = function () {
  let mainOrder = new mongoose.models['Order'](
    {
      delivery: this.delivery,
      status: 'Created',
      promoCode: this.promoCode,
      promoDiscount: this.promoDiscount,
      journey: this.journey,
      type: this.type,
      gender: this.gender,
      orderSize: this.orderSizeMain ? this.orderSizeMain : 'big',
      currency: this.currency,
      currencySymbol: this.currencySymbol,
      countryCode: this.countryCode,
      sender: this.sender,
      customBottle: this.customBottle,
      amountMain: this.amountMain,
      samplesOrderRef: this,
      customPackaging: this.customPackaging,
      payment: this.payment,
      warehouse: this.warehouse
    }
  );

  if (this.amountMain) {
    mainOrder.amount = this.amountMain;
  } else {
    let price = Prices.getPrice(mainOrder.countryCode);
    mainOrder.amount = price[mainOrder.orderSize] + price.shipping[mainOrder.orderSize];
  }
  return mainOrder;
};

function toTitleCase(str) {
  return str.replace(/\w\S*/g, function (txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
}

orderSchema.methods.processInitials = function () {
  let initials = this.customBottle.initials;
  if (!initials) {
    initials = '';
    if (this.type == 'WaftGift') {
      if (this.gift) {
        let r = this.recipient;
        if (r && (r.firstName || r.lastName)) {
          initials = r.firstName ? r.firstName.charAt(0) : '';
          initials += r.lastName ? r.lastName.charAt(0) : '';
        }
      }
    }
    else if (this.sender) {
      initials = this.sender.firstName ? this.sender.firstName.charAt(0) : '';
      initials += this.sender.lastName ? this.sender.lastName.charAt(0) : '';
    }
  }
  this.customBottle.initials = initials.toUpperCase();
};

orderSchema.methods.processPackaging = function () {
  let packagingFirstName = this.customPackaging.firstName;
  if ((!packagingFirstName) || (packagingFirstName === '')) {
    if (this.sender) {
      packagingFirstName = this.sender.firstName || '';
    }
    else {
      packagingFirstName = '';
    }
  }
  this.customPackaging.firstName = toTitleCase(packagingFirstName);
};

orderSchema.methods.processRecommendation = function () {
  this.processInitials();
  this.processPackaging();
};

mongoosePaginate.paginate.options = {
  lean: true,
  limit: 3,
  page: 1
};

orderSchema.plugin(mongoosePaginate);

const orderModel = mongoose.model('Order', orderSchema);

module.exports = orderModel;
