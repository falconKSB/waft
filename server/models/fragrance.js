var mongoose = require('../config/mongoose');

// Schema defines how the user data will be stored in MongoDB
var fragranceSchema = new mongoose.Schema({
    brand: { type: String },

    short_name: { type: String },

    name: { type: String },

    name_lower: { type: String },

    description: { type: String },

    accords: [{
        name: { type: String },
        weight: { type: Number }
    }],

    family: { type: String },

    female: { type: Boolean },

    image: { type: String },

    main_accord: { type: String },

    male: { type: Boolean  },

    scents: {
        main: [ { type: String } ],
        unisexwomen: [ { type: String } ],
        unisexmen: [ { type: String } ]
    },
    
    // if the fragrance is main of the normalised set
    main: Boolean
});

fragranceSchema.methods.getIngredientsArray = function () {
    let result = [];
    for (let i = 0; i < this.accords.length; i++)
        result.push(this.accords[i].name.toLowerCase().trim())
    return result;
};

fragranceSchema.methods.isUnisex = function () {
    return this.male && this.female;
};

fragranceSchema.methods.getGenderString = function () {
    return this.male ? (this.female ? 'unisex' : 'man') : "woman";
};


fragranceSchema.methods.getGender = function () {
    let gender = [];
    if (this.male) gender.push('man');
    if (this.female) gender.push('woman');
    return gender;
};

fragranceSchema.methods.getDescriptionString = function () {
    return this.name + ' (' + this.getDetailsString() +')';
};

fragranceSchema.methods.getDetailsString = function () {
    let sex = (this.male && this.female) ? 'unisex' : (this.male? "men's":"women's");
    let accords_names = '';
    if (this.accords.length != 0) {
        this.accords.sort(function (a, b) {
            return b.weight - a.weight;
        });
        accords_names = this.accords[0].name;
        for (let i = 1; i < this.accords.length; i++) {
            accords_names += ', ' + this.accords[i].name
        }
    }
    return sex + ' / ' + (this.family || 'Unknown family ') + ' / ' + this.main_accord +
        ' / ' + accords_names;
};

module.exports = mongoose.model('Fragrance', fragranceSchema);