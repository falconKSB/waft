var mongoose = require('../config/mongoose');
var bcrypt = require('bcrypt-nodejs');

var config = require('../config/config');
var crypto = require("crypto");
var jwt = require('jsonwebtoken');

var TOKEN_AGE = '7d';

var userSchema = new mongoose.Schema({
    email: {
        type: String,
        lowercase: true,
        unique: true,
        required: true
    },
    firstName: {
        type: String
    },
    lastName: {
        type: String
    },
    password: {
        type: String
    },
    passwordResetToken: {
        token: {
            type: String
        },
        createdAt: {
            type: Date
        },
        expiredTime: {
            type: String //1h, 5h etc
        }
    },
    paymentMethods: {
        stripe_us: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'UserPaymentMethod'
        },
        stripe_sg: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'UserPaymentMethod'
        }
    },
    customerId: { // for payments
        type: String
    },
    paymentAccount: {
        type: String
    },
    facebookUserId: {
        type: String
    },
    gender: {
        type: String
    },
    journeys: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Journey'
    }],
    orders: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Order'
    }],
    auditlog: [{date: Date, message: String}]
}, { timestamps : true });

userSchema.pre('save', function (next) {

    var user = this;

    if (this.isModified('password') || this.isNew) {

        bcrypt.genSalt(10, function (err, salt) {
            if (err) {
                return next(err);
            }
            bcrypt.hash(user.password, salt, null, function (err, hash) {
                if (err) {
                    return next(err);
                }
                user.password = hash;
                next();
            });
        });
    } else {
        return next();
    }
});

userSchema.methods.comparePassword = function (pw) {
    return new Promise((resolve,reject) => {
        bcrypt.compare(pw, this.password, function (err, isMatch) {
            if (err) return reject(err);
            resolve(isMatch);
        });
    })
};

userSchema.methods.getCurrentUser = function () {
    var currentUser = {
        email: this.email,
        id: this._id,
        firstName: this.firstName,
        lastName: this.lastName
    };
    var accessToken = jwt.sign(currentUser, config.secret, { expiresIn: TOKEN_AGE });
    var refreshToken = this._id.toString() + '.' + crypto.randomBytes(40).toString('hex');
    return {
        currentUser: currentUser,
        accessToken: accessToken,
        refreshToken: refreshToken
    }
};

userSchema.methods.appendChangelog = function (message) {
    if (!this.auditlog)
        this.auditlog = [];
    this.auditlog.push({
        date: new Date(),
        message: message
    });
};

userSchema.methods.appendChangelogChange = function (who, oldValue, newValue) {
    if (typeof oldValue == "object") {
        oldValue = JSON.stringify(oldValue);
    }
    if (typeof newValue == "object") {
        newValue = JSON.stringify(newValue);
    }
    this.appendChangelog(who + " changed email address from " + oldValue + " to " + newValue);
};

module.exports = mongoose.model('User', userSchema);
