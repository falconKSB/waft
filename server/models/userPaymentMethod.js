var mongoose = require('../config/mongoose');

var paymentSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    // Stripe customer ID
    customerId: String,
    // our own Stripe account code, e.g. SG
    account: String,
    // ID of default payment source
    default_source: String,
    // payment sources available
    sources: [{
        id: String,
        card: {
            brand: String,
            country: String,
            exp_month: Number,
            exp_year: Number,
            last4: Number,
            funding: String,
            cvc_check: String,
            address_zip_check: String
        }
    }]
}, { timestamps : true });

module.exports = mongoose.model('UserPaymentMethod', paymentSchema);
