var mongoose = require('../config/mongoose');

// Schema defines how the user data will be stored in MongoDB
var ingredientSchema = new mongoose.Schema({

name: {
    type: String
},
imageUrl: {
    type: String
},
description:{
    type: String
}

});

module.exports = mongoose.model('Ingredient', ingredientSchema);