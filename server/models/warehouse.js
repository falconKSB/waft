const mongoose = require('../config/mongoose');

const warehouseSchema = new mongoose.Schema({
    // name of the warehouse, e.g. US
    name: String,

    stock: [{
        // SKU of the item
        sku : {
            type: String,
            maxlength: 20
        },
        // stock available at the time of last stock update
        stock : {
            type: Number,
            default: 0
        },
        // stock allocated from the time of last stock update
        allocated : {
            type: Number,
            default: 0
        }
    }]
}, {timestamps: true});

warehouseSchema.methods.getStock = function(sku) {
    let foundStock = this.stock.find(s => s.sku === sku);
    return (foundStock && foundStock.stock) || 0;
};

warehouseSchema.methods.isScentInStock = function(code) {
    return this.stock.find(s => s.sku.startsWith('A'+code) && (s.stock > s.allocated));
};

module.exports = mongoose.model('Warehouse', warehouseSchema);
