var mongoose = require('../config/mongoose');
var bcrypt = require('bcrypt-nodejs');

var config = require('../config/config');
var jwt = require('jsonwebtoken');
var crypto = require("crypto");

var TOKEN_AGE = '7d';

var adminSchema = new mongoose.Schema({
    email: {
        type: String,
        lowercase: true,
        unique: true,
        required: true
    },
    accessLevel: {
        type: String,
        enum: ['admin', 'operator', 'readonly']
    },
    status: {
        type: String,
        enum: ['Active', 'Inactive']
    },
    password: {
        type: String
    },
    refreshToken: {
        type: String
    }

}, {timestamps: true});

adminSchema.pre('save', function (next) {
    let user = this;

    if (this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, function (err, salt) {
            if (err) {
                return next(err);
            }
            bcrypt.hash(user.password, salt, null, function (err, hash) {
                if (err) {
                    return next(err);
                }
                user.password = hash;
                user.refreshToken = user._id.toString() + '.' + crypto.randomBytes(40).toString('hex');
                next();
            });
        });
    } else {
        return next();
    }
});

adminSchema.methods.comparePassword = function (pw, cb) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(pw, this.password, function (err, isMatch) {
            if (err) return reject(err);
            resolve(isMatch);
        });
    })
};

adminSchema.methods.getCurrentUser = function () {
    let currentUser = {
        id: this._id,
        email: this.email,
        accessLevel: this.accessLevel
    };
    return {
        currentUser: currentUser,
        access_token: jwt.sign(currentUser, config.secret, {expiresIn: TOKEN_AGE}),
        refresh_token: this.refreshToken,
        token_type: 'jwt'
    }
};


module.exports = mongoose.model('Admin', adminSchema);
