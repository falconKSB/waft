var prices = {
    at: {
        countryCode: 'at',
        name: 'Austria',
        warehouse: 'UK',
        paymentAccount: 'Stripe US',
        currency: 'eur',
        currencySymbol: '€',
        exchangeRate: 1.083,
        big: 99,
        medium: 79,
        small: 59,
        samples: 23,
        shipping: {
            big: 8.10,
            medium: 8.10,
            small: 8.10,
            samples: 8.10,
            min_days: 5,
            max_days: 10
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'medium',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'medium'
        }
    },
    au: {
        countryCode: 'au',
        name: 'Australia',
        warehouse: 'SG',
        paymentAccount: 'Stripe US',
        currency: 'aud',
        currencySymbol: 'A$',
        exchangeRate: 0.755,
        big: 199,
        medium: 149,
        small: 119,
        samples: 59,
        shipping: {
            big: 0,
            medium: 0,
            small: 0,
            samples: 0,
            min_days: 5,
            max_days: 10
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'big',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'big'
        }
    },
    be: {
        countryCode: 'be',
        name: 'Belgium',
        warehouse: 'UK',
        paymentAccount: 'Stripe US',
        currency: 'eur',
        currencySymbol: '€',
        exchangeRate: 1.083,
        big: 99,
        medium: 79,
        small: 59,
        samples: 23,
        shipping: {
            big: 8.10,
            medium: 8.10,
            small: 8.10,
            samples: 8.10,
            min_days: 5,
            max_days: 10
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'medium',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'medium'
        }
    },
    // ca: {
    //     name: 'Canada',
    //     countryCode: 'ca',
    //     warehouse: 'US',
    //     paymentAccount: 'Stripe US',
    //     currency: 'cad',
    //     currencySymbol: 'C$',
    //     exchangeRate: 1.331,
    //     big: 159,
    //     medium: 129,
    //     small: 89,
    //     samples: 49,
    //     shipping: {
    //         big: 0,
    //         medium: 0,
    //         small: 0,
    //         samples: 0,
    //         min_days: 7,
    //         max_days: 10
    //     }
    // },
    ch: {
        countryCode: 'ch',
        name: 'Switzerland',
        warehouse: 'UK',
        paymentAccount: 'Stripe US',
        currency: 'eur',
        currencySymbol: '€',
        exchangeRate: 1.083,
        big: 99,
        medium: 79,
        small: 59,
        samples: null,
        shipping: {
            big: 26.20,
            medium: 26.20,
            small: 26.20,
            samples: 26.20,
            min_days: 5,
            max_days: 10
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'medium',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'medium'
        }
    },
    cz: {
        countryCode: 'cz',
        name: 'Czech Republic',
        warehouse: 'UK',
        paymentAccount: 'Stripe US',
        currency: 'eur',
        currencySymbol: '€',
        exchangeRate: 1.083,
        big: 99,
        medium: 79,
        small: 59,
        samples: null,
        shipping: {
            big: 16.10,
            medium: 16.10,
            small: 16.10,
            samples: 16.10,
            min_days: 5,
            max_days: 10
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'medium',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'medium'
        }
    },
    de: {
        countryCode: 'de',
        name: 'Germany',
        warehouse: 'UK',
        paymentAccount: 'Stripe US',
        currency: 'eur',
        currencySymbol: '€',
        exchangeRate: 1.083,
        big: 99,
        medium: 79,
        small: 59,
        samples: 23,
        shipping: {
            big: 8.10,
            medium: 8.10,
            small: 8.10,
            samples: 8.10,
            min_days: 5,
            max_days: 10
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'medium',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'medium'
        }
    },
    dk: {
        countryCode: 'dk',
        name: 'Denmark',
        warehouse: 'UK',
        paymentAccount: 'Stripe US',
        currency: 'eur',
        currencySymbol: '€',
        exchangeRate: 1.083,
        big: 99,
        medium: 79,
        small: 59,
        samples: null,
        shipping: {
            big: 26.20,
            medium: 26.20,
            small: 26.20,
            samples: 26.20,
            min_days: 5,
            max_days: 10
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'medium',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'medium'
        }
    },
    es: {
        countryCode: 'es',
        name: 'Spain',
        paymentAccount: 'Stripe US',
        warehouse: 'UK',
        currency: 'eur',
        currencySymbol: '€',
        exchangeRate: 1.083,
        big: 99,
        medium: 79,
        small: 59,
        samples: null,
        shipping: {
            big: 16.10,
            medium: 16.10,
            small: 16.10,
            samples: 16.10,
            min_days: 5,
            max_days: 10
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'medium',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'medium'
        }
    },
    fi: {
        countryCode: 'fi',
        name: 'Finland',
        paymentAccount: 'Stripe US',
        warehouse: 'UK',
        currency: 'eur',
        currencySymbol: '€',
        exchangeRate: 1.083,
        big: 99,
        medium: 79,
        small: 59,
        samples: null,
        shipping: {
            big: 26.20,
            medium: 26.20,
            small: 26.20,
            samples: 26.20,
            min_days: 5,
            max_days: 10
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'medium',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'medium'
        }
    },
    fr: {
        countryCode: 'fr',
        name: 'France',
        warehouse: 'UK',
        paymentAccount: 'Stripe US',
        currency: 'eur',
        currencySymbol: '€',
        exchangeRate: 1.083,
        big: 99,
        medium: 79,
        small: 59,
        samples: null,
        shipping: {
            big: 16.10,
            medium: 16.10,
            small: 16.10,
            samples: 16.10,
            min_days: 5,
            max_days: 10
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'medium',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'medium'
        }
    },
    gb: {
        countryCode: 'gb',
        name: 'United Kingdom',
        warehouse: 'UK',
        paymentAccount: 'Stripe US',
        currency: 'gbp',
        currencySymbol: '£',
        exchangeRate: 1.234,
        big: 89,
        medium: 69,
        small: 49,
        samples: 19,
        shipping: {
            big: 4.91,
            medium: 4.91,
            small: 4.91,
            samples: 4.91,
            min_days: 5,
            max_days: 10
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'medium',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'medium'
        }
    },
    hk: {
        countryCode: 'hk',
        name: 'Hong Kong',
        warehouse: 'SG',
        paymentAccount: 'Stripe SG',
        currency: 'hkd',
        currencySymbol: 'HK$',
        exchangeRate: 0.129,
        big: 849,
        medium: 619,
        small: 379,
        samples: 149,
        shipping: {
            big: 0,
            medium: 0,
            small: 0,
            samples: 0,
            min_days: 5,
            max_days: 10
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'big',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'big'
        }
    },
    hr: {
        countryCode: 'hr',
        name: 'Croatia',
        warehouse: 'UK',
        paymentAccount: 'Stripe US',
        currency: 'eur',
        currencySymbol: '€',
        exchangeRate: 1.083,
        big: 99,
        medium: 79,
        small: 59,
        samples: null,
        shipping: {
            big: 26.20,
            medium: 26.20,
            small: 26.20,
            samples: 26.20,
            min_days: 5,
            max_days: 10
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'medium',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'medium'
        }
    },
    hu: {
        countryCode: 'hu',
        name: 'Hungary',
        warehouse: 'UK',
        paymentAccount: 'Stripe US',
        currency: 'eur',
        currencySymbol: '€',
        exchangeRate: 1.083,
        big: 99,
        medium: 79,
        small: 59,
        samples: null,
        shipping: {
            big: 16.10,
            medium: 16.10,
            small: 16.10,
            samples: 16.10,
            min_days: 5,
            max_days: 10
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'medium',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'medium'
        }
    },
    id: {
        countryCode: 'id',
        name: 'Indonesia',
        warehouse: 'SG',
        paymentAccount: 'Stripe SG',
        currency: 'usd',
        currencySymbol: '$',
        exchangeRate: 1,
        small: 49,
        big: 109,
        medium: 89,
        samples: 19,
        shipping: {
            big: 19,
            medium: 19,
            small: 19,
            samples: 19,
            min_days: 7,
            max_days: 15
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'big',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'big'
        }
    },
    in: {
        countryCode: 'in',
        name: 'India',
        warehouse: 'SG',
        paymentAccount: 'Stripe US',
        currency: 'usd',
        currencySymbol: '$',
        exchangeRate: 1,
        big: 109,
        medium: 89,
        small: 49,
        samples: 19,
        shipping: {
            big: 39,
            medium: 39,
            small: 39,
            samples: 39,
            min_days: 7,
            max_days: 15
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'big',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'big'
        }
    },
    it: {
        countryCode: 'it',
        name: 'Italy',
        warehouse: 'UK',
        paymentAccount: 'Stripe US',
        currency: 'eur',
        currencySymbol: '€',
        exchangeRate: 1.083,
        big: 99,
        medium: 79,
        small: 59,
        samples: null,
        shipping: {
            big: 16.10,
            medium: 16.10,
            small: 16.10,
            samples: 16.10,
            min_days: 5,
            max_days: 10
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'medium',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'medium'
        }
    },
    li: {
        countryCode: 'li',
        name: 'Liechtenstein',
        warehouse: 'UK',
        paymentAccount: 'Stripe US',
        currency: 'eur',
        currencySymbol: '€',
        exchangeRate: 1.083,
        big: 99,
        medium: 79,
        small: 59,
        samples: null,
        shipping: {
            big: 26.20,
            medium: 26.20,
            small: 26.20,
            samples: 26.20,
            min_days: 5,
            max_days: 10
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'medium',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'medium'
        }
    },
    lu: {
        countryCode: 'lu',
        name: 'Luxembourg',
        warehouse: 'UK',
        paymentAccount: 'Stripe US',
        currency: 'eur',
        currencySymbol: '€',
        exchangeRate: 1.083,
        big: 99,
        medium: 79,
        small: 59,
        samples: 23,
        shipping: {
            big: 8.10,
            medium: 8.10,
            small: 8.10,
            samples: 8.10,
            min_days: 5,
            max_days: 10
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'medium',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'medium'
        }
    },
    mc: {
        countryCode: 'mc',
        name: 'Monaco',
        warehouse: 'UK',
        paymentAccount: 'Stripe US',
        currency: 'eur',
        currencySymbol: '€',
        exchangeRate: 1.083,
        big: 99,
        medium: 79,
        small: 59,
        samples: null,
        shipping: {
            big: 16.10,
            medium: 16.10,
            small: 16.10,
            samples: 16.10,
            min_days: 5,
            max_days: 10
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'medium',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'medium'
        }
    },
    my: {
        countryCode: 'my',
        name: 'Malaysia',
        warehouse: 'SG',
        paymentAccount: 'Stripe SG',
        currency: 'usd',
        currencySymbol: '$',
        exchangeRate: 1,
        small: 49,
        big: 109,
        medium: 89,
        samples: 19,
        shipping: {
            big: 19,
            medium: 19,
            small: 19,
            samples: 19,
            min_days: 7,
            max_days: 15
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'big',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'big'
        }
    },
    nl: {
        countryCode: 'nl',
        name: 'Netherlands',
        warehouse: 'UK',
        paymentAccount: 'Stripe US',
        currency: 'eur',
        currencySymbol: '€',
        exchangeRate: 1.083,
        big: 99,
        medium: 79,
        small: 59,
        samples: 23,
        shipping: {
            big: 8.10,
            medium: 8.10,
            small: 8.10,
            samples: 8.10,
            min_days: 5,
            max_days: 10
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'medium',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'medium'
        }
    },
    nz: {
        countryCode: 'nz',
        name: 'New Zealand',
        warehouse: 'SG',
        paymentAccount: 'Stripe US',
        currency: 'nzd',
        currencySymbol: 'NZ$',
        exchangeRate: 0.72,
        big: 199,
        medium: 149,
        small: 119,
        samples: 59,
        shipping: {
            big: 0,
            medium: 0,
            small: 0,
            samples: 0,
            min_days: 5,
            max_days: 10
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'big',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'big'
        }
    },
    pl: {
        countryCode: 'pl',
        name: 'Poland',
        warehouse: 'UK',
        paymentAccount: 'Stripe US',
        currency: 'eur',
        currencySymbol: '€',
        exchangeRate: 1.083,
        big: 99,
        medium: 79,
        small: 59,
        samples: null,
        shipping: {
            big: 26.20,
            medium: 26.20,
            small: 26.20,
            samples: 26.20,
            min_days: 5,
            max_days: 10
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'medium',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'medium'
        }
    },
    pt: {
        countryCode: 'pt',
        name: 'Portugal',
        warehouse: 'UK',
        paymentAccount: 'Stripe US',
        currency: 'eur',
        currencySymbol: '€',
        exchangeRate: 1.083,
        big: 99,
        medium: 79,
        small: 59,
        samples: null,
        shipping: {
            big: 26.20,
            medium: 26.20,
            small: 26.20,
            samples: 26.20,
            min_days: 5,
            max_days: 10
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'medium',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'medium'
        }
    },
    se: {
        countryCode: 'se',
        name: 'Sweden',
        warehouse: 'UK',
        paymentAccount: 'Stripe US',
        currency: 'eur',
        currencySymbol: '€',
        exchangeRate: 1.083,
        big: 99,
        medium: 79,
        small: 59,
        samples: null,
        shipping: {
            big: 26.20,
            medium: 26.20,
            small: 26.20,
            samples: 26.20,
            min_days: 5,
            max_days: 10
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'medium',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'medium'
        }
    },
    si: {
        countryCode: 'si',
        name: 'Slovenia',
        warehouse: 'UK',
        paymentAccount: 'Stripe US',
        currency: 'eur',
        currencySymbol: '€',
        exchangeRate: 1.083,
        big: 99,
        medium: 79,
        small: 59,
        samples: null,
        shipping: {
            big: 16.10,
            medium: 16.10,
            small: 16.10,
            samples: 16.10,
            min_days: 5,
            max_days: 10
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'medium',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'medium'
        }
    },
    sk: {
        countryCode: 'sk',
        name: 'Slovakia',
        warehouse: 'UK',
        paymentAccount: 'Stripe US',
        currency: 'eur',
        currencySymbol: '€',
        exchangeRate: 1.083,
        big: 99,
        medium: 79,
        small: 59,
        samples: null,
        shipping: {
            big: 16.10,
            medium: 16.10,
            small: 16.10,
            samples: 16.10,
            min_days: 5,
            max_days: 10
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'medium',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'medium'
        }
    },
    sg: {
        countryCode: 'sg',
        name: 'Singapore',
        warehouse: 'SG',
        paymentAccount: 'Stripe SG',
        currency: 'sgd',
        currencySymbol: 'S$',
        exchangeRate: 0.705,
        big: 149,
        medium: 109,
        small: 69,
        samples: 19,
        shipping: {
            big: 0,
            medium: 0,
            small: 0,
            samples: 0,
            min_days: 5,
            max_days: 10
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'big',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'big'
        }
    },
    us: {
        countryCode: 'us',
        name: 'United States',
        warehouse: 'US',
        paymentAccount: 'Stripe US',
        currency: 'usd',
        currencySymbol: '$',
        exchangeRate: 1,
        big: 99,
        medium: 79,
        small: 49,
        samples: 19,
        shipping: {
            big: 0,
            medium: 0,
            small: 0,
            samples: 7.95,
            min_days: 5,
            max_days: 10
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'big',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'big'
        }
    },
    default: {
        name: 'Default',        
        warehouse: 'SG',
        currency: 'usd',
        paymentAccount: 'Stripe US',
        currencySymbol: '$',
        exchangeRate: 1,
        small: 49,
        big: 109,
        medium: 89,
        samples: 19,
        shipping: {
            big: 29,
            medium: 29,
            small: 29,
            samples: 29,
            min_days: 7,
            max_days: 15
        },
        orderSizesByGender: {
            man: 'medium',
            woman: 'big',
            unisex: 'medium',
            man_unisex: 'medium',
            woman_unisex: 'big'
        }
    },
    getPrice: function(countryCode) {
        return this[countryCode.toLowerCase()] || this.getDefaultPrice();
    },
    getDefaultPrice: function () {
        return this.default;
    },
    format : function(price, size) {
        return price.currencySymbol + price[size].toString()
    }
};

module.exports = prices;
