var mongoose = require('../config/mongoose');

var scentSchema = new mongoose.Schema({

    name: {type: String, maxlength: 50},

    code: {type: String, maxlength: 5},

    iffName: {type: String, maxlength: 50},

    iffCode: {type: String, maxlength: 50},

    family: {type: String, maxlength: 50},

    subFamily: {type: String, maxlength: 50},

    subGroup: {type: String, maxlength: 50},

    subGroup2: {type: String, maxlength: 50},

    subGroup3: {type: String, maxlength: 50},

    subGroup4: {type: String, maxlength: 50},

    marketReference: {type: String, maxlength: 500},

    keyIngredients: {type: String, maxlength: 500},

    otherIngredients: {type: String, maxlength: 500},

    description: {type: String, maxlength: 500},

    scentMood: {type: String, maxlength: 500},

    scent: {type: String, maxlength: 500},

    timeOfDay: [{
        type: String, maxlength: 50,
        enum: ['day', 'night']
    }],

    mood: [{
        type: String, maxlength: 50,
        enum: ['fresh', 'elegant', 'sexy', 'relaxed']
    }],

    activity: [{
        type: String,
        enum: ['sport', 'social', 'work', 'dating']
    }],

    scentGender: {
        type: String,
        enum: ['man', 'woman', 'unisex']
    },

    gender: [{
        type: String, maxlength: 50,
        enum: ['man', 'woman']
    }],

    personalStyle: [{
        type: String, maxlength: 50,
        enum: ['classic', 'trendy']
    }],

    waft: { // TODO: make an array
        type: String, maxlength: 50,
        enum: ['subtle', 'balanced', 'daring']
    },

    status: {
        type: String, maxlength: 50,
        enum: ['Active', 'Inactive']
    },

    inStock: {type: Boolean},

    isDeleted: {type: Boolean, default: false},

    SKU: {
        big: {type: String, maxlength: 20},
        medium: {type: String, maxlength: 20},
        small: {type: String, maxlength: 20},
        samples: {type: String, maxlength: 20}
    },

    availableQuantity: {
        big: {type: Number},
        medium: {type: Number},
        small: {type: Number},
        samples: {type: Number}
    }

});

scentSchema.methods.getFamiliesArray = function () {
    let families = [];
    if (this.family && (this.family.trim() != '')) {
        families.push(this.family.toLowerCase().trim())
    }
    if (this.subFamily && (this.subFamily.trim() != '')) {
        families.push(this.subFamily.toLowerCase().trim())
    }
    return families;
};

scentSchema.methods.getParametersArray = function () {
    let params = [];
    params = params.concat(this.timeOfDay);
    params = params.concat(this.activity);
    params = params.concat(this.mood);
    params = params.concat(this.personalStyle);
    params = params.concat(this.waft);
    return params;
};

scentSchema.methods.getIngredientsArray = function () {
    let ingredients = this.keyIngredients.split(/\s*,\s*/);
    for (let j = 0; j < ingredients.length; j++) {
        ingredients[j] = ingredients[j].toLowerCase();
    }
    return ingredients;
};

let toShortString = function () {
    return this.code + ' - ' + this.name;
};

let toShortObject = function () {
    return {
        code: this.code,
        name: this.name
    }
};

scentSchema.methods.toShortString = toShortString;
scentSchema.methods.toShortObject = toShortObject;

scentSchema.methods.toScentObject = function () {
    return {
        name: this.name,
        code: this.code,
        rank: 0,
        gender: this.scentGender,
        families: this.getFamiliesArray(),
        groups: this.getGroupsArray(),
        ingredients: this.getIngredientsArray(),
        params: this.getParametersArray(),
        scent: this,
        toShortString: toShortString,
        toShortObject: toShortObject
    }
};

scentSchema.methods.getAsAccords = function () {
    let result = [];
    if (this.family && (this.family.trim() != '')) {
        result.push({
            name: this.family.toLowerCase().trim(),
            weight: 1.0
        });
    }
    if (this.subFamily && (this.subFamily.trim() != '')) {
        result.push({
            name: this.subFamily.toLowerCase().trim(),
            weight: 0.9
        });
    }
    if (this.subGroup && (this.subGroup.trim() != '')) {
        result.push({
            name: this.subGroup.toLowerCase().trim(),
            weight: 0.8
        });
    }
    if (this.subGroup2 && (this.subGroup2.trim() != '')) {
        result.push({
            name: this.subGroup2.toLowerCase().trim(),
            weight: 0.6
        });
    }
    if (this.subGroup3 && (this.subGroup3.trim() != '')) {
        result.push({
            name: this.subGroup3.toLowerCase().trim(),
            weight: 0.4
        });
    }
    if (this.subGroup4 && (this.subGroup4.trim() != '')) {
        result.push({
            name: this.subGroup4.toLowerCase().trim(),
            weight: 0.2
        });
     }
    return result;
}

scentSchema.methods.getGroupsArray = function () {
    let groups = [];
    if (this.subGroup && (this.subGroup.trim() != '')) {
        groups.push(this.subGroup.toLowerCase().trim())
    }
    if (this.subGroup2 && (this.subGroup2.trim() != '')) {
        groups.push(this.subGroup2.toLowerCase().trim())
    }
    if (this.subGroup3 && (this.subGroup3.trim() != '')) {
        groups.push(this.subGroup3.toLowerCase().trim())
    }
    if (this.subGroup4 && (this.subGroup4.trim() != '')) {
        groups.push(this.subGroup4.toLowerCase().trim())
    }
    return groups;
};

scentSchema.methods.mapping = function (scent) {
    this.name = scent.name || '',
        this.code = scent.code || '',
        this.iffName = scent.iffName || '',
        this.iffCode = scent.iffCode || '',
        this.family = scent.family || '',
        this.subFamily = scent.subFamily || '',
        this.subGroup = scent.subGroup || '',
        this.subGroup2 = scent.subGroup2 || '',
        this.subGroup3 = scent.subGroup3 || '',
        this.subGroup4 = scent.subGroup4 || '',
        this.marketReference = scent.marketReference || '',
        this.keyIngredients = scent.keyIngredients || '',
        this.otherIngredients = scent.otherIngredients || '',
        this.description = scent.description || '',
        this.scentMood = scent.scentMood || '',
        this.scent = scent.scent || '',
        this.activity = scent.activity || '',
        this.timeOfDay = scent.timeOfDay || [],
        this.mood = scent.mood || [],
        this.personalStyle = scent.personalStyle || [],
        this.gender = scent.gender || [],
        this.inStock = scent.inStock || false,
        this.status = scent.status || 'Inactive';

    if (scent.scentGender) this.scentGender = scent.scentGender;
    if (scent.waft) this.waft = scent.waft;
};

module.exports = mongoose.model('Scent', scentSchema);


