'use strict';

var braintree = require('braintree');
var config = require("./config");

var environment = config.braintree.environment.charAt(0).toUpperCase() + config.braintree.environment.slice(1);

var gateway = braintree.connect({
    environment: braintree.Environment[environment],
    merchantId: config.braintree.merchantId,
    publicKey: config.braintree.publicKey,
    privateKey: config.braintree.privateKey
});

module.exports = gateway;