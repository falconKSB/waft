var mongoose = require("mongoose");
var config = require("./config");

mongoose.connect(config.database);
mongoose.Promise = global.Promise;

module.exports = mongoose;