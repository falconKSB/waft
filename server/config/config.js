"use strict";

exports.secret = "RbBQqA6uF#msRF8s7h*?@=95HUm&DgMDd6zLFn4XzWQ6dtwXSJwBX#?gL2JWf!";
exports.length = 128;
exports.domain = process.env.DOMAIN || 'localhost:3000';
exports.auth_cookie_age = 7*86400000; // auth expirty - 7 days
exports.homepath = process.env.DOMAIN ? 'https://'+process.env.DOMAIN : 'http://localhost:3000';
exports.database = process.env.DB || 'mongodb://localhost/waft';
exports.facebookAuth = {
    clientID: '275884922787904',
    clientSecret: 'dd4f2912de01c52ee2415249611d570b'
  };
exports.cdnbase = process.env.CDN_BASE_URL ? 'https:'+process.env.CDN_BASE_URL : exports.homepath;

const prod = (process.env.ENV === 'production') || (process.env.NODE_ENV === 'production');

exports.prod = prod;

let mailer_gmail = {
    transport: {
        transport: 'smtp',
        service: 'gmail',
        auth: {
            user: 'lopatkinalex92@gmail.com',
            pass: '9IHrj0RO'
        }
    },
    defaultFromAddress: 'Waft Lab <lopatkinalex92@gmail.com>'
};

let mailer_ses = {
    transport: {
        transport: 'ses',
        accessKeyId: process.env.SESKEYID,
        secretAccessKey: process.env.SESKEY,
        region: 'us-west-2',
        rateLimit: 5 // do not send more than 5 messages in a second
    },
    defaultFromAddress: 'Waft <info@waft.com>'
};

exports.mailer = process.env.SESKEYID ? mailer_ses : mailer_gmail;

exports.braintree = {
  environment: 'Sandbox',
  merchantId:'c3w33wtcb7mxv75z',
  publicKey:'pvbvwwsv2y42j9vy',
  privateKey:'0e4dace70f0c44cc7408aa481c7eb832'
};

exports.stripeSecretKeyUS = process.env.STRIPE_KEY_US || "sk_test_1t8rcoTWkTL6H3ZF4VzcwEEr";
exports.stripeSecretKeySG = process.env.STRIPE_KEY_SG || "sk_test_VadB6mmQmpSerzxReL7r1857";

exports.vaucharMerchantId = "e5fa4e43-3b54-453c-bb77-e24b1239a6f2";
exports.vaucharApiKey = "b4fcc8148ce1ac7ba8edc4c9490a9b18";

exports.referralRockUser = "blake@waft.com";
exports.referralRockPassword = "2Singapore!";

exports.aftershipToken = "FavXpNxXlaDvUkbclviCCvB4";

exports.ACTIVECAMPAIGN_KEY = "a76e38131e93c37e87e45ecabcca02b2557dcfbc14ec606096902d4dd5733e3a84e29cd0";

exports.LOGLEVEL = process.env.LOGLEVEL || (prod?'info':'debug');

exports.AWSKEY = process.env.AWSKEY;
exports.AWSSECRET = process.env.AWSSECRET;
